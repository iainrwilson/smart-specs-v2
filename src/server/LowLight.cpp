//
//  LowLight.cpp
//  image_processor_server
//
//  Created by Iain Wilson on 13/08/2018.
//

#include "LowLight.hpp"

namespace SmartSpecs{
    
    LowLight::LowLight(){
        
    }
    
    cv::Mat LowLight::run(std::vector<cv::Mat>& aligned){
        m_aligned = aligned;
        
        //merge all the images:
        
        
        
    }
    
    cv::Mat LowLight::merge(const std::string& mode){
        
        cv::Mat output = cv::Mat::zeros(m_aligned[0].size(),CV_16UC3);
        
        if(mode == "average"){
//            openmp?
            for(auto I:m_aligned){
                // accept only char type matrices
                CV_Assert(I.depth() != sizeof(uchar));
                
                int channels = I.channels();
                int nRows = I.rows;
                int nCols = I.cols * channels;
                
                if (I.isContinuous()){
                    nCols *= nRows;
                    nRows = 1;
                }
                
                int i,j;
                uchar* p;
                uchar* o;
                for( i = 0; i < nRows; ++i)
                {
                    p = I.ptr<uchar>(i);
                    o = output.ptr<uchar>(i);
                    for ( j = 0; j < nCols; ++j){
                        o[j] += p[j];
                    }
                }
            }
        }
        cv::Mat out;
        output.convertTo(out, CV_8UC3, 1./m_aligned.size());
        return out;
    }
    
    cv::Mat LowLight::localThreshold(const int size){
        
    }
    
}
