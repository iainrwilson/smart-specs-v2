//
//  LowLight.hpp
//  image_processor_server
//
//  Created by Iain Wilson on 09/08/2018.
//

#ifndef LowLight_hpp
#define LowLight_hpp

#include <stdio.h>
#include "opencv2/core.hpp"
#include "opencv2/reg/mapaffine.hpp"
#include "opencv2/reg/mapshift.hpp"
#include "opencv2/reg/mapprojec.hpp"
#include "opencv2/reg/mappergradshift.hpp"
#include "opencv2/reg/mappergradeuclid.hpp"
#include "opencv2/reg/mappergradsimilar.hpp"
#include "opencv2/reg/mappergradaffine.hpp"
#include "opencv2/reg/mappergradproj.hpp"
#include "opencv2/reg/mapperpyramid.hpp"
#include <memory>

namespace SmartSpecs{
    
    class FrameRegistration{
        
    public:
        typedef std::shared_ptr<FrameRegistration> Ptr;
        FrameRegistration(const std::string& mode="AFFINE");
        virtual ~FrameRegistration() = default;
        
        virtual std::vector<cv::Mat> run();
        void addImage(const cv::Mat &image);
        
        static FrameRegistration::Ptr create(){
            return std::make_shared<FrameRegistration>();
        }
        
    private:
        
        cv::Ptr<cv::reg::Map> reg(const cv::Mat& src, const cv::Mat& dst);
        
        std::string m_mode;
        std::vector<cv::Mat> m_images;
    };
    
}


#endif /* LowLight_hpp */
