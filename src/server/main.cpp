/*

ASIO test for sending data to python text detection server:

test read from camera and push to remote 

re-use smart specs code.

 */


#include <opencv2/opencv.hpp>
#include <sstream>

#include "networking/TCPClient.hpp"
#include "RingBuffer.hpp"
#include "networking/TCPServerAsync.hpp"
#include "Processor.hpp"

#include <functional>


using namespace SmartSpecs::Networking;

using namespace SmartSpecs;

std::atomic<bool> run(true);


void my_handler(int s){
    printf("Caught signal %d\n",s);
    run = false;
}


int main(int argc, char** argv){

    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
    
    
    RingBuffer<iImage::Ptr>::Ptr frame_buffer = RingBuffer<iImage::Ptr>::create(100);
    RingBuffer<Response::Ptr>::Ptr results_buffer = RingBuffer<Response::Ptr>::create();
    
    //init the processor;
    Processor::Ptr processor = Processor::create();
//    std::function<std::string (const std::string&)> queryP = [](const std::string& s){
//        LOGI("QUERYP :: %s",s.c_str());
//        if(s == "PING")
//            return "PING";
//        return "OKAY";
//    };
//
//    std::function<std::string (const Image::Ptr&)> frameP = [](const Image::Ptr& f){
//        //LOGI("QUERYP :: %s",s.c_str());
//
//        std::cout << f->size() << " T: "<<f->age() << std::endl;
//        std::stringstream ss;
//        ss << "image_" << f->age() <<".png";
//        cv::imwrite(ss.str(),f->get());
//        return "OKAY";
//    };
    
    
    //alternativly bind the imageProcessor;
    auto commandP = std::bind(&Processor::command,processor,std::placeholders::_1);
    auto frameP = std::bind(&Processor::frame,processor,std::placeholders::_1);
    
    auto qp = QueryProcessor::create(commandP,frameP);
    TCPServerAsync::Ptr query_server = TCPServerAsync::create("8081", qp);
    
    
    LOGD("Started servers");
    
    query_server->start();
    
    for(;;){
        
        if(!run)
            break;
        
        char temp;
        std::cin >> temp;
        if (temp == 'q'){
            std::cout << temp <<std::endl;
            break;
        }
            
    }
    
    LOGD("Stopping servers");
    
    if(query_server->isRunning()){
        query_server->stop();
    }
    
    LOGD("Finished");

    
}

