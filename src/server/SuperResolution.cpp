//
//  SuperResolution.cpp
//  cpp_server
//
//  Created by Iain Wilson on 26/07/2017.
//
//

#include "SuperResolution.hpp"
namespace SmartSpecs{
    
    SuperResolution::SuperResolution():m_scale(4),m_iterations(30),m_temporalAreaRadius(4),m_optFlow("farneback"),m_useGpu(true){
        
        if(m_useGpu)
            m_superRes = cv::superres::createSuperResolution_BTVL1_CUDA();
        else
            m_superRes = cv::superres::createSuperResolution_BTVL1();
        
        
        cv::Ptr<cv::superres::DenseOpticalFlowExt> of = createOptFlow(m_optFlow, m_useGpu);
        
        m_superRes->setOpticalFlow(of);
        
        m_superRes->setScale(m_scale);
        m_superRes->setIterations(m_iterations);
        m_superRes->setTemporalAreaRadius(m_temporalAreaRadius);
        
        m_counter=0;
	
        LOGD("[SuperResolution::SuperResolution] created");
    }
    
    
    void SuperResolution::load(cv::Ptr<std::vector<cv::Mat>> frames){
        
        // load video data,
        //TODO create CUDA compatible version
        
        m_framesource = IFrameSource::create(frames);
        m_superRes->setInput(m_framesource);
        LOGD("[SuperResolution::load] created");

    }
    
    cv::Mat SuperResolution::run(){
        LOGD("[SuperResolution::run] called");
        cv::Mat results;
        
        m_superRes->nextFrame(results);
        std::string out;
        std::stringstream ss;
        ss <<"superres.";
        ss << m_counter++;
        ss << ".png";
        cv::imwrite(ss.str(), results);
        
        return results;
    }
    
    cv::Ptr<cv::superres::DenseOpticalFlowExt> SuperResolution::createOptFlow(const std::string& name, bool useGpu)
    {
        if (name == "farneback")
        {
            if (useGpu)
                return cv::superres::createOptFlow_Farneback_CUDA();
            else
                return cv::superres::createOptFlow_Farneback();
        }
        /*else if (name == "simple")
         return createOptFlow_Simple();*/
        else if (name == "tvl1")
        {
            if (useGpu)
                return cv::superres::createOptFlow_DualTVL1_CUDA();
            else
                return cv::superres::createOptFlow_DualTVL1();
        }
        else if (name == "brox")
            return cv::superres::createOptFlow_Brox_CUDA();
        else if (name == "pyrlk")
            return cv::superres::createOptFlow_PyrLK_CUDA();
        else
            std::cerr << "Incorrect Optical Flow algorithm - " << name << std::endl;
        
        return cv::Ptr<cv::superres::DenseOpticalFlowExt>();
    }
    
    
    
    
    
}
