//
//  Processor.hpp
//  cpp_server
//
//  Created by Iain Wilson on 26/07/2017.
//
//

#ifndef Processor_hpp
#define Processor_hpp

#include <stdio.h>
#include "includes.hpp"
#include <atomic>
#include "SuperResolution.hpp"
#include "common/networking/Messages.hpp"
#include "networking/TCPClient.hpp"
#include "FrameRegistration.hpp"

namespace SmartSpecs {
    
    class Processor{
        
    public:
        typedef std::shared_ptr<Processor> Ptr;
        Processor();
        static Processor::Ptr create(){ return std::make_shared<Processor>();}
        
        std::string process(Message::Ptr &message);
        void setMode(const std::string& mode);
        
        std::string mode()const {return m_mode;}
        
        std::string command(const std::string& cmd);
        std::string frame(const Image::Ptr& frame);
        
        std::string processQuery(Message::Ptr &message);
     protected:
        std::string m_mode;
        cv::Ptr<std::vector<cv::Mat>> m_frames;
         
        int m_save_counter;
        
        FrameRegistration::Ptr m_frame_registration;
        Networking::TCPClient::Ptr m_tcp;
    };
}

#endif /* Processor_hpp */
