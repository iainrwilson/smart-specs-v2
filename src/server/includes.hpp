#pragma once

#include <stdio.h>
#include <stdarg.h>
#include <iostream>

#include "common/Thread.hpp"
#include "common/RingBuffer.hpp"
#include "common/networking/Response.hpp"
#include <termcolor/termcolor.hpp>

struct iImage {
    typedef std::shared_ptr<iImage> Ptr;
    iImage(cv::Mat img, const int fcount){
        image = img.clone();
        f_count = fcount;
    }
    cv::Mat image;
    unsigned int f_count;
    
    static iImage::Ptr create(cv::Mat& img, const int fcount){
        return std::make_shared<iImage>(img,fcount);
    }
};

/*#include <opencv2/tracking.hpp>

inline cv::Ptr<cv::Tracker> createTrackerByName(cv::String name)
{
    cv::Ptr<cv::Tracker> tracker;
    
    if (name == "KCF")
        tracker = cv::TrackerKCF::create();
    else if (name == "TLD")
        tracker = cv::TrackerTLD::create();
    else if (name == "BOOSTING")
        tracker = cv::TrackerBoosting::create();
    else if (name == "MEDIAN_FLOW")
        tracker = cv::TrackerMedianFlow::create();
    else if (name == "MIL")
        tracker = cv::TrackerMIL::create();
    else if (name == "GOTURN")
        tracker = cv::TrackerGOTURN::create();
    else
        CV_Error(cv::Error::StsBadArg, "Invalid tracking algorithm name\n");
    
    return tracker;
}
*/



