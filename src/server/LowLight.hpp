//
//  LowLight.hpp
//  image_processor_server
//
//  Created by Iain Wilson on 13/08/2018.
//

#ifndef LowLight_hpp
#define LowLight_hpp

#include <stdio.h>

#include <memory>
#include <opencv2/core.hpp>

namespace SmartSpecs {
    
    class LowLight{
        
    public:
        typedef std::shared_ptr<LowLight> Ptr;
        LowLight();
        cv::Mat run(std::vector<cv::Mat>& aligned_images);
    private:
        cv::Mat localThreshold(const int size=50);
        cv::Mat merge(const std::string& mode="average");
        std::vector<cv::Mat> m_aligned;
    };
}


#endif /* LowLight_hpp */
