//
//  Processor.cpp
//  cpp_server
//
//  Created by Iain Wilson on 26/07/2017.
//
//

#include "Processor.hpp"


namespace SmartSpecs{
    
    Processor::Processor(){
        m_frames.reset(new std::vector<cv::Mat>);
     
        m_save_counter=0;
        m_tcp = Networking::TCPClient::create();
        m_tcp->setRemote("127.0.0.1","8082");
        m_mode = Request::SAVE;
    }
    
    /*!
     \brief change the current mode.
     */
    void Processor::setMode(const std::string& mode){
        m_mode = mode;
    }
    
 
    /*!
     \brief Process the input image, according to the current state.
     */
    std::string Processor::frame(const Image::Ptr& frame){
        /* Manages what to do with the incoming image data */
        
        cv::Mat image = frame->get();
        
        if(m_mode == Request::SUPERRES_LOAD){
            //then we store all incomming images
            
            m_frames->push_back(image);
            LOGD("[Processor::imageProcessor] Saving images for superres");
        
        }else if(m_mode == Request::SAVE){
            // just save the image to file...used for testing commmunications
            
            LOGD("Saving Frame %d",m_save_counter);
            std::string out;
            std::stringstream ss;
            ss <<"image_";
            ss << m_save_counter;
            ss << ".png";
            cv::imwrite(ss.str(), image);
            m_save_counter++;
            
            
            m_tcp->connect();

            //testing the response system
            auto resp = Response::create(Request::TEXT);
            resp->add(cv::Rect2i(10,10,100,100));
            resp->setTimestamp(frame->timestamp());
            m_tcp->sendResponse(resp);
            
        }else if(m_mode == Request::SUPERRES_RUN){
            //actualy perform the superres;
            
            SuperResolution::Ptr sr = SuperResolution::create();
            
            sr->load(m_frames);
            cv::Mat retult;
            retult = sr->run();
            
        }else if(m_mode == Request::TEXT){
            //send to the text detector //which is written in python...grrr
            
        }else if(m_mode == Request::LOWLIGHT_LOAD){
            
            // start by sending data for registartion.
            if(m_frame_registration.get() == nullptr)
                m_frame_registration = FrameRegistration::create();
            
            m_frame_registration->addImage(image);
            
        }else if(m_mode == Request::FACE){
            //try the Face Recognition stuff;
            
            
            
            
        }
        
        return "OKAY";
    }
    
    std::string Processor::processQuery(Message::Ptr &message){
        
        std::string query = Messages::decode(message);
        return command(query);
    }
    
    
    /*!
        \brief process an incoming query, used to change the state
     */
    std::string Processor::command(const std::string &cmd){
        
//        std::string query(message.body(),message.bodyLength());
        
        LOGD("[QUERY] %s  : len %d",cmd.c_str(),cmd.size());
        
        if(std::string(cmd)==Request::PING){
            LOGD("[QueryProcessor::process] PING recieved");
            return "PING";
        }
        else if(m_mode == Request::SUPERRES_RUN){
            
            //execute Superres
            
            LOGD("[QueryProcessor::process] Processing images for superres");
            
            //actualy perform the superres;
            cv::imwrite("original.png", (*m_frames)[0]);
            
            SuperResolution::Ptr sr = SuperResolution::create();
            sr->load(m_frames);
            cv::Mat result;
            result = sr->run();
            cv::imwrite("result.png", result);
            
            return "SUPERRES_RUN";
            
        }else if(m_mode == Request::LOWLIGHT_RUN){
            
            LOGD("[QueryProcessor::process] Processing images for LOWLIGHGT");
            
            auto warped = m_frame_registration->run();
            int i=0;
            for(auto w : warped){
                std::string out;
                std::stringstream ss;
                ss <<"warped_";
                ss << i++;
                ss << ".png";
                cv::imwrite(ss.str(),w);
                
            }
            return "LOWLIGHT_RUN";
        }
        // used to switch processing modes:
        else if(m_mode != std::string(cmd)){
            m_mode = std::string(cmd);
            LOGD("[QueryProcessor::process] changing mode to: (%s)",m_mode.c_str());
            return "MODE: "+m_mode;
        }
        
        return "ERROR";
    }
    
}
