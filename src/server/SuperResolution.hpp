//
//  SuperResolution.hpp
//  cpp_server
//
//  Created by Iain Wilson on 26/07/2017.
//
//

#ifndef SuperResolution_hpp
#define SuperResolution_hpp

#include <stdio.h>
#include "includes.hpp"
#include "Logging.hpp"
#include "input_array_utility.hpp"

namespace SmartSpecs {
    class SuperResolution{
        
    public:
        typedef std::shared_ptr<SuperResolution> Ptr;
        
        SuperResolution();
        
        void load(cv::Ptr<std::vector<cv::Mat>> frames);
        
        static SuperResolution::Ptr create(){
            return std::make_shared<SuperResolution>();
        }
        
        cv::Mat run();
        
        
    private:

        static cv::Ptr<cv::superres::DenseOpticalFlowExt> createOptFlow(const std::string& name, bool useGpu);
        
        
        int m_scale;
        int m_iterations;
        int m_temporalAreaRadius;
        const std::string m_optFlow;
        bool m_useGpu;
        int m_counter;
    
        cv::Ptr<cv::superres::SuperResolution> m_superRes;
        cv::Ptr<cv::superres::FrameSource> m_framesource;
    };
    
    
    
    ///create a new FrameSoure thing
    
    class IFrameSource : public cv::superres::FrameSource{
        
    public:
        IFrameSource(cv::Ptr<std::vector<cv::Mat>> frames):m_frames(frames){
            m_frames_it = m_frames->begin();
        }
        
        void nextFrame(cv::OutputArray _frame){
            ++m_frames_it;
            bool res = (m_frames_it == m_frames->end());
            if (res){
                LOGD("[IFrameSource::nextFrame] end of frames");
            }
            
            //////
            
            if (_frame.kind() == cv::_InputArray::MAT)
                _frame.getMatRef() = *m_frames_it;
            else if(_frame.kind() == cv::_InputArray::CUDA_GPU_MAT)
            {
                //m_frames_it->copyTo(_frame);
                cv::superres::arrCopy(*m_frames_it, _frame);
            }
           // else if (_frame.isUMat())
           //     *(cv::UMat *)_frame.getObj() = *m_frames_it;
            else
            {
                // should never get here
                CV_Error(cv::Error::StsBadArg, "Failed to detect input frame kind" );
            }
            
        }
        void reset(){
            m_frames_it = m_frames->begin();
        }
        
        static cv::Ptr<IFrameSource> create(cv::Ptr<std::vector<cv::Mat>> frames){
            return cv::Ptr<IFrameSource>(new IFrameSource(frames));
        }
        
        
    private:
        cv::Ptr<std::vector<cv::Mat>> m_frames;
        std::vector<cv::Mat>::iterator m_frames_it;
        cv::Mat m_frame;
    };
    
    
}


#endif /* SuperResolution_hpp */
