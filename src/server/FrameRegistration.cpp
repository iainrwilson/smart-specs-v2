//
//  LowLight.cpp
//  image_processor_server
//
//  Created by Iain Wilson on 09/08/2018.
//
#include <stdio.h>

#include "FrameRegistration.hpp"
#include <iostream>
#include "Logging.hpp"

namespace SmartSpecs {
    
    FrameRegistration::FrameRegistration(const std::string& mode):m_mode(mode){
    }
    
    void FrameRegistration::addImage(const cv::Mat &image){
        LOGD("[FrameRegistration::addImage] adding image");
        m_images.push_back(image);
    }
    
    std::vector<cv::Mat> FrameRegistration::run(){
        
        std::vector<cv::Mat> warped_images;

        // pick an image to register other too.
        // go with the first to start with
        auto dst = std::begin(m_images);
        auto src = std::begin(m_images);
        
        warped_images.push_back(*dst);

        for(++src;src!=std::end(m_images);++src){
            auto map = reg(*src,*dst);
            cv::Mat res;
            map->inverseWarp(*src, res);
            warped_images.push_back(res);
        }
        
        return warped_images;
        
    }
    
    cv::Ptr<cv::reg::Map> FrameRegistration::reg(const cv::Mat& src, const cv::Mat& dst){
        // Register
        cv::Ptr<cv::reg::MapperGradShift> mapper = cv::makePtr<cv::reg::MapperGradShift>();
        cv::reg::MapperPyramid mappPyr(mapper);
        cv::Ptr<cv::reg::Map> mapPtr = mappPyr.calculate(src, dst);
        return mapPtr;
    }

    
}
