/*

ASIO test for sending data to python text detection server:

test read from camera and push to remote 

re-use smart specs code.

 */


#include <opencv2/opencv.hpp>
#include <sstream>
#include <stdio.h>
#include <signal.h>
#include "cameras/camera.hpp"

#include "face_detection/FaceDetector.hpp"
#include "face_detection/FaceLearner.hpp"
#include "skin_segmentation/SkinSegmenter.hpp"

#include <atomic>
#include <memory>
#include <chrono>

#include "common/timer.hpp"
#include "common/Image.hpp"

#include "variables.hpp"

std::atomic<bool> run(true);

void my_handler(int s){
    printf("Caught signal %d\n",s);
    run = false;
}

using namespace std;

int main(int argc, char** argv){
  
  
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    //connect to a camera
    auto cam = Camera::create(VIDEOFILE);
    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/skin_segmentation/lfw_funneled/";
//    cam->setFilename(dataDir);
//    cam->setExtension(".jpg");
    if(!cam->connect()){
        std::cout << " Cannot connect to camera " << std::endl;
        return -1;
    }
    
    auto fl = FaceLearner::create();
    auto fd = FaceDetector::create(FD_OPENCV_DNN);
    
    auto skin_seg = SkinSegmenter::create(SEG_DECISION);
    skin_seg->setColorConversion(CC_BGR2HSV);
    skin_seg->setDataset(DATA_PRA);
    if(!skin_seg->load())
        SkinSegmenter::train(skin_seg, DATA_PRA);
    
    
    for(;;){
        if(!run)
            break;
        SmartSpecs::Timer::begin("[ Main Loop ]");
       
        Image::Ptr image = Image::create(cam->grab());
        image->buildPyramid(4);
        
        auto rois = fd->detect(image->get());
        for(auto const &roi:rois){
            auto face = Face::create(roi,image);
            if(!face->isValid())
                continue;
            
            auto features = fd->detectLandmarks(image->get(),face);
            face->setFeatures(features);
            face->createFaceMask(image->get());
            face->create2DHistogram();
            
            //for each valid face...run the segmentation -
            bool okay = face->createTrainingData(skin_seg);
            if(okay){
                
                //use lfw filename.
//                auto path = boost::filesystem::path(cam->currentFilename());
                fl->save(face);
            }else{
                std::cout << " Bad face segmentation " <<std::endl;
            }
            
        }
        
        SmartSpecs::Timer::end("[ Main Loop ]");
        
       SmartSpecs::Timer::print();
        
    }
    
    std::cout << "Exiting." <<std::endl;
    
}

