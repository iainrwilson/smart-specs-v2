import pandas as pd
import os
import cv2
import numpy as np

ss_home = os.getenv("SMARTSPECS_HOME")
datadir = os.path.join(ss_home,"data/recorded/processing/lfw/")

df = pd.read_csv("rugby_2_pred.csv")


for i,row in df.iterrows():
    print("type: %s" % row['type'])
    if(row['type']=='bad'):
        continue
    fname = row['filename']
    i_name = fname[:-10] + "image.tiff"

    mask = cv2.imread(fname)
    img = cv2.imread(i_name)

    diff = np.bitwise_and(img, mask)

    out = np.hstack([img,diff,mask])
    cv2.imshow("comp", out)
    key = cv2.waitKey(100)