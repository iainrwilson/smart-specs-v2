"""
 Plot the Segmented face data

show a distriubution / average / heatmap thing of the segmented faces. Good vs Bad

Start with the LFW



masks will have to be reshaped to a standard size.


approximate aspect 0.7

(w) 210 x (h) 300


"""


import pandas as pd
import os
import cv2
import numpy as np

ss_home = os.getenv("SMARTSPECS_HOME")
datadir = os.path.join(ss_home,"data/recorded/processing/lfw/")


#start with the rugby dataset.
df = pd.read_csv("rugby_2_pred.csv")


size = [300,210]

sum = np.zeros(size,dtype=np.uint64)
bad_sum = np.zeros(size,dtype=np.uint64)
good_count = 0
bad_count=0

for i,row in df.iterrows():
    # print("type: %s" % row['type'])

    fname = row['filename']
    i_name = fname[:-10] + "image.tiff"

    _mask = cv2.imread(fname,cv2.IMREAD_GRAYSCALE)

    #resize mask
    mask = cv2.resize(_mask,(size[1],size[0]))

    if (row['type'] == 'bad'):
        bad_sum = np.add(mask, bad_sum)
        bad_count+=1
    else:
        sum = np.add(mask,sum)
        good_count+=1

    # print("Mask shape ",mask.shape, mask.shape[1] / mask.shape[0])
    # img = cv2.imread(i_name)
    #
    # diff = np.bitwise_and(img, mask)
    #
    # out = np.hstack([img,diff,mask])
    # cv2.imshow("comp", out)
    # key = cv2.waitKey(100)


g_avg = np.divide(sum,good_count)
b_avg = np.divide(bad_sum,bad_count)

g_avg = cv2.cvtColor(g_avg.astype(np.uint8),cv2.COLOR_GRAY2BGR)
b_avg = cv2.cvtColor(b_avg.astype(np.uint8),cv2.COLOR_GRAY2BGR)

cmap_g = cv2.applyColorMap(g_avg, cv2.COLORMAP_JET)
cmap_b = cv2.applyColorMap(b_avg, cv2.COLORMAP_JET)
cv2.imshow("maps",np.hstack([cmap_g,cmap_b]))
cv2.waitKey(1)

