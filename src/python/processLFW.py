import os, glob
import cv2
import numpy as np
import pandas as pd
import seaborn as sns
import HelperFunctions as hf
import yaml


ss_home = os.getenv("SMARTSPECS_HOME")
datadir = os.path.join(ss_home,"data/recorded/processing/lfw/")

lfw = os.path.join(datadir,"lfw_roi")

lfw_files = glob.glob(lfw+"/*_label.tiff",recursive=True)
# #
# df = hf.generateStats(lfw_files)
# df['area'] = df['area']*100

df = pd.read_csv("cached_df.csv")


#load ground truth
lfw_gt = os.path.join(ss_home, "data/skin_segmentation/parts_lfw_funneled_gt_images")

lfw_gt_files = glob.glob(lfw_gt+"/*.ppm",recursive=True)
gt_datas=[]


#load any previous data
tmp_df = None
if os.path.exists("temp_label.csv"):
    tmp_df = pd.read_csv("temp_label.csv")
    tmp_df = tmp_df[['area', 'aspect_ratio', 'bg_diff', 'centroid_x',
       'centroid_y', 'circularity', 'convexity', 'diff', 'face_diff',
       'filename', 'hair_diff', 'ir', 'type']]

for gt in lfw_gt_files:
    name = os.path.join(lfw,"face_"+os.path.basename(gt)[:-4]+"_label.tiff")
    i_name = os.path.join(lfw, "face_" + os.path.basename(gt)[:-4] + "_image.tiff")
    roi_name = os.path.join(lfw,"face_"+os.path.basename(gt)[:-4]+"_rect.yml")
    if not os.path.exists(name):
        # print("cannot find",name)
        continue

    if not os.path.exists(roi_name):
        # print("cannot find",roi_name)
        continue

    if len(df[df.filename == name]) ==0:
        continue

    #have we allready labled this?
    if(tmp_df is not None):
        if len(tmp_df[tmp_df.filename == name]) != 0:
            continue

    skip_lines = 2
    with open(roi_name) as infile:
        for i in range(skip_lines):
            _ = infile.readline()
        roi = yaml.load(infile)['roi']


    gt_img = cv2.imread(gt)[roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2],:]
    b  = gt_img[:,:,0] #backgroung
    f  = gt_img[:,:,1] #face
    h  = gt_img[:,:,2] # hair
    all = np.bitwise_or(f,h)


    img = cv2.imread(name,cv2.IMREAD_GRAYSCALE)
    # comapre the masks
    diff = np.bitwise_and(all,img)

    diff_b = np.bitwise_and(b, img)
    diff_f = np.bitwise_and(f, img)
    diff_h = np.bitwise_and(h, img)


    diff_b_per = float(cv2.countNonZero(diff_b)+1)/float(cv2.countNonZero(b)+1)
    diff_f_per = float(cv2.countNonZero(diff_f)+1)/float(cv2.countNonZero(f)+1)
    diff_h_per = float(cv2.countNonZero(diff_h)+1)/float(cv2.countNonZero(h)+1)

    diff_per = float(cv2.countNonZero(diff)+1) / float(cv2.countNonZero(all)+1)

    print("BG: %0.3f, Face: %0.3f, Hair: %0.3f, Head: %03f"%(diff_b_per,diff_f_per,diff_h_per,diff_per))

    _df = df[df.filename == name].to_dict(orient='records')[0]
    _df.pop('Unnamed: 0',None)
    _df['diff'] = diff_per
    _df['hair_diff'] = diff_h_per
    _df['face_diff'] = diff_f_per
    _df['bg_diff'] = diff_b_per

    #create some images
    org_img = cv2.imread(i_name)
    img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
    diff =cv2.cvtColor(diff,cv2.COLOR_GRAY2BGR)

    diff = np.bitwise_and(org_img,img)

    tile = []
    tile.append(np.hstack([img,org_img]))
    tile.append(np.hstack([gt_img,diff]))
    out = np.vstack(tile)
    # cv2.imshow("comp",out)
    # key = cv2.waitKey(0)
    # if(key ==103):
    #     _df['type']="good"
    # elif(key==98):
    #     _df['type'] = "bad"
    # elif (key == 44):
    #     _df['type'] = "maybe"
    # elif (key==115):
    #     break
    # print("Key ",key)
    if(_df['bg_diff'] > 0.03):
        _df['type'] = "bad"
    elif(_df['face_diff'] - (_df['bg_diff']*2)) > 0.80:
        _df['type'] = "good"
    else:
        _df['type'] = "bad"


    gt_datas.append(_df)

gt_df = pd.DataFrame.from_records(gt_datas)

if tmp_df is not None:
    gt_df = pd.concat([gt_df,tmp_df],sort=False)

if(len(gt_df) >0):
    gt_df.to_csv("temp_label.csv")

#sns.scatterplot(x=df["circularity"],y=df["convexity"],s=df["area"])
