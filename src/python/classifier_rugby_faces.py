"""

    Try some python classifers on the rugby faces data.


    test out some pycaret things....

"""


import pandas as pd

import pycaret.classification as pc
from pycaret.utils import check_metric


df = pd.read_csv('rugby_data.csv')

#more with difffernt pyramid levels.
df_0 = pd.read_csv('rugby_0.csv')
df_1 = pd.read_csv('rugby_1.csv')
df_2 = pd.read_csv('rugby_2.csv')
#
dataset = df.drop(columns=['filename','Unnamed: 0',])

# dataset = df[['convexity','circularity','type']]
#convert lable "type"
data = dataset.sample(frac=0.90, random_state=786)
data_unseen = dataset.drop(data.index)
data.reset_index(inplace=True, drop=True)
data_unseen.reset_index(inplace=True, drop=True)
print('Data for Modeling: ' + str(data.shape))
print('Unseen Data For Predictions ' + str(data_unseen.shape))

exp_clf = pc.setup(data=data, target='type', session_id=123,html = False,

                   fix_imbalance = True)

optimise_on = 'Precision'

#list of classifiers to try
cls = ['lr','nb','rf','dt','svm','knn']

results = []
for c in cls:
    _clf = pc.create_model(c)
    _tuned_clf = pc.tune_model(_clf,optimize=optimise_on)
    _final = pc.finalize_model(_tuned_clf)
    unseen_predictions = pc.predict_model(_final, data=data_unseen)
    unseen_predictions.head()
    # print(c, check_metric(unseen_predictions.type, unseen_predictions.Label, optimise_on), optimise_on)
    results.append({
        'Classifer':c,
        optimise_on:check_metric(unseen_predictions.type, unseen_predictions.Label, optimise_on)
    })

df_res = pd.DataFrame.from_records(results)

#try rf
# rf = pc.create_model('dt')
# print(rf)
# tuned_rf = pc.tune_model(rf, optimize=optimise_on)
#
#
# final_rf = pc.finalize_model(tuned_rf)
# unseen_predictions_rf = pc.predict_model(final_rf, data=data_unseen)
# unseen_predictions_rf.head()
#
# print("DT ",check_metric(unseen_predictions_rf.type, unseen_predictions_rf.Label, optimise_on),optimise_on)

#

best = pc.compare_models(include=cls,sort=optimise_on)
print(best)
tuned_best = pc.tune_model(best,optimize=optimise_on)


final_best = pc.finalize_model(tuned_best)
unseen_predictions = pc.predict_model(final_best, data=data_unseen)
unseen_predictions.head()
print("Best ",check_metric(unseen_predictions.type, unseen_predictions.Label, optimise_on),optimise_on)

pc.plot_model(tuned_best,'confusion_matrix')