"""

Process Faces -

go throuhg all of the saved training data and generate stats

1) Circularity
2) Convecity
3) inertia ratio


"""

import os, glob
import cv2
import numpy as np
import pandas as pd
import seaborn as sns

import HelperFunctions as hf

ss_home = os.getenv("SMARTSPECS_HOME")
datadir = os.path.join(ss_home,"data/recorded/")

datadir = os.path.join(datadir,"processing/v3/")


files_0 = glob.glob(datadir+"/pyr_0/*_label.tiff")
files_1 = glob.glob(datadir+"/pyr_1/*_label.tiff",recursive=True)
files_2 = glob.glob(datadir+"/pyr_2/*_label.tiff",recursive=True)

df_0 = hf.generateStats(files_0)
df_1 = hf.generateStats(files_1)
df_2 = hf.generateStats(files_2)


sns.scatterplot(x="circularity",y="convexity",data=df_0)

#save the data

df_0.to_csv("rugby_0.csv")
df_1.to_csv("rugby_1.csv")
df_2.to_csv("rugby_2.csv")

