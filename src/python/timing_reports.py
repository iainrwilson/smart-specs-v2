"""
    18/01/21
    Process and report timing information for the scamrt-specs app.

    load data from ~/experiments/smart-specs-v2/data/recorded


"""

import os,glob
import pandas as pd
import seaborn as sns

home = os.path.expanduser("~")
base_dir = os.path.join(home,'experiments/smart-specs-v2/data/recorded')
files = {"RF":"SEG_RANDOM_FOREST_1_0.75_1610972687338861_log.txt",
         "DT":"SEG_DECISION_1_0.75_1610973354035749_log.txt",
         "NB":"SEG_NAIVE_BAYES_1_0.75_1610980624550329_log.txt"}


modes = ["[ FaceLearner ] train()", '[ FaceLearner ] create lookup()']

for type,file in files.items():

    df = pd.read_csv(os.path.join(base_dir,file),error_bad_lines=False,encoding= 'unicode_escape')
    for mode in modes:
        _mean = df[df.Name == mode].mean()['Duration']
        _std = df[df.Name == mode].std()['Duration']
        _min = df[df.Name == mode].min()['Duration']
        _max = df[df.Name == mode].max()['Duration']

        print(f"{type}:: {mode} {_mean}±{_std} [{_min} {_max}]")

# files = glob.glob(os.path.join(base_dir,'SEG_*'))
