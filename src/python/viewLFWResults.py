"""

Validate the automated labeling and classification

"""

import pandas as pd
import os
import cv2
import numpy as np
import yaml

ss_home = os.getenv("SMARTSPECS_HOME")
datadir = os.path.join(ss_home,"data/recorded/processing/lfw/")
lfw = os.path.join(datadir,"lfw_roi")
lfw_gt = os.path.join(ss_home, "data/skin_segmentation/parts_lfw_funneled_gt_images")

#load data
df = pd.read_csv("temp_label.csv")


for i,row in df.iterrows():

    _d = row['face_diff'] - (row['bg_diff']*2)

    print("BG: %f FD: %f D: %f  type: %s" %(row['bg_diff'],row['face_diff'], _d,row['type']))

    fname = row['filename']
    gt_name = os.path.join(lfw_gt, os.path.basename(fname)[5:-11] + ".ppm")
    i_name = fname[:-10] + "image.tiff"
    roi_name = fname[:-10] +"rect.yml"

    skip_lines = 2
    with open(roi_name) as infile:
        for i in range(skip_lines):
            _ = infile.readline()
        roi = yaml.load(infile)['roi']

    # create some images
    img = cv2.imread(fname, cv2.IMREAD_GRAYSCALE)
    org_img = cv2.imread(i_name)
    img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    diff = np.bitwise_and(org_img, img)
    # diff = cv2.cvtColor(diff, cv2.COLOR_GRAY2BGR)
    gt_img = cv2.imread(gt_name)[roi[1]:roi[1]+roi[3],roi[0]:roi[0]+roi[2],:]






    tile = []
    tile.append(np.hstack([img, org_img]))
    tile.append(np.hstack([gt_img, diff]))
    out = np.vstack(tile)
    cv2.imshow("comp", out)
    key = cv2.waitKey(0)