import cv2,os
import pandas as pd
import numpy as np

def generateStats(files):

    detector = cv2.SimpleBlobDetector()
    datas = []
    for file in files:

        img = cv2.imread(file,cv2.IMREAD_GRAYSCALE)
        image = cv2.imread(file[:-10]+"image.tiff")

        im2,contours,hierachy = cv2.findContours(img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        for c in contours:
            _area = cv2.contourArea(c)
            if _area == 0:
                continue

            area = _area/img.size

            if area < 0.1:
                continue

            _arclength = cv2.arcLength(c, True)
            circularity = (4 * np.pi * _area) / (_arclength ** 2)

            _ch_area = cv2.contourArea(cv2.convexHull(c))
            convexity = _area / _ch_area

            M = cv2.moments(c)
            # calculate x,y coordinate of center
            cX = (M["m10"] / M["m00"])
            cY = (M["m01"] / M["m00"])

            _w = img.shape[0]
            _h = img.shape[1]

            centroid = (cX/_h, cY/_w)



            (x, y), (MA, ma), angle = cv2.fitEllipse(c)


            i_r = x/y #intertia ratio

            x, y, w, h = cv2.boundingRect(c)
            aspect_ratio = float(w) / h


            datas.append({
                "filename":file,
                "circularity":circularity,
                "convexity":convexity,
                "area":area*100,
                'ir':i_r,
                "centroid_x":centroid[0],
                "centroid_y":centroid[1],
                "aspect_ratio":aspect_ratio
            })

    return pd.DataFrame.from_records(datas)