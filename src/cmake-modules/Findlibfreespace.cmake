# library should have been installed into /usr/local/lib
find_path(LIBFREESPACE_INCLUDE_DIR
    NAMES freespace.h
    PATHS "$ENV{ProgramFiles}/freespace/" /usr/local/include/freespace
)

# find libfreespace.dylib  and store it in MLPACK_LIBRARY
find_library(LIBFREESPACE_LIBRARY
    NAMES freespace
    PATHS "$ENV{ProgramFiles}/freespace/" /usr/local/lib /usr/local/ /usr/lib64
)

