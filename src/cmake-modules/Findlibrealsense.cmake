##############################################################################
# Find librealsense
#
# This sets the following variables:
# LIBREALSENSE_FOUND - True if librealsense was found.
# LIBREALSENSE_INCLUDE_DIRS - Directories containing the librealsense include files.
# LIBREALSENSE_LIBRARIES - Libraries needed to use librealsense.


find_package(PkgConfig)

find_path(LIBREALSENSE_INCLUDE_DIR librealsense/rs.hpp
	         HINTS "${PROJECT_SOURCE_DIR}/libraries/librealsense/include/"   )


find_library(LIBREALSENSE_LIBRARY NAMES realsense
		 HINTS "${PROJECT_SOURCE_DIR}/libraries/librealsense/lib/"
			"${PROJECT_SOURCE_DIR}/libraries/librealsense/bin/Win32"  )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(librealsense DEFAULT_MSG LIBREALSENSE_LIBRARY LIBREALSENSE_INCLUDE_DIR)
mark_as_advanced(LIBREALSENSE_INCLUDE_DIR LIBREALSENSE_LIBRARY)

SET(LIBREALSENSE_LIBRARIES ${LIBREALSENSE_LIBRARY})
SET(LIBREALSENSE_INCLUDE_DIRS ${LIBREALSENSE_INCLUDE_DIR})

if(LIBREALSENSE_FOUND)
  message(STATUS "librealsense found (include: " ${LIBREALSENSE_INCLUDE_DIRS} ", lib: " ${LIBREALSENSE_LIBRARIES} )
endif(LIBREALSENSE_FOUND)