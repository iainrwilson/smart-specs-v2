#ifndef __SMARTSPECS__NETWORKING__REQUESTHANDLER_H
#define __SMARTSPECS__NETWORKING__REQUESTHANDLER_H

#include <memory>
#include <string>
#include <functional>
#include "Processor.hpp"
#include "Message.hpp"

namespace SmartSpecs{
  namespace Networking {
 
    // handles all incoming commands, processed and generates response.
    class RequestHandler{
      
    public:
        explicit RequestHandler(Processor::Ptr queryProcessor);

      RequestHandler(const RequestHandler&) = delete;
      RequestHandler& operator=(const RequestHandler&) = delete;
      
      bool check(const std::string& command);
      void process();
      bool process_header();
     
      std::string reply(); 
    
        char* readData(){return m_read_message.data();}
        char* readBody(){return m_read_message.body();}
        const int readBodySize() const {return m_read_message.body_length();}
    private:
        Message m_read_message;
        Message m_write_message;
      std::string m_reply;
      std::string m_command;
      Processor::Ptr m_queryProcessor;
    
        
        
        
    };

 }
}

#endif // __SMARTSPECS__NETWORKING__REQUESTHANDLER_H
