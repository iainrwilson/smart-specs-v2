//
//  LowLightMode.cpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#include "LowLightMode.hpp"


LowLightMode::LowLightMode(const Display::Ptr display, const ImageBuffer::Ptr &input,const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors):
ProcessingModule("PASS_MODE",display,input,display_buff,sensors){

    #ifdef USE_CUDA
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation_GPU::reset,m_stab));
    #else
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation::reset,m_stab));
    #endif
}

void LowLightMode::linkParameters(){

    m_display->link(m_parameters["enable-stable"],'6');

}

void LowLightMode::equalizeHist(const cv::Mat &src, cv::Mat &dst)
{

    //generate hist from downsampled image
    cv::Mat dwns;
    cv::resize(src,dwns,cv::Size(16,16));
   
    // Histogram
    std::vector<int> hist(256,0);
    for (int r = 0; r < dwns.rows; ++r) {
        for (int c = 0; c < dwns.cols; ++c) {
            hist[src.at<uint8_t>(r, c)]++;
        }
    }


    // Cumulative histogram
    float scale = 255.f / float(src.total());
    std::vector<uchar> lut(256);
    int sum = 0;
    for (int i = 0; i < hist.size(); ++i) {
        sum += hist[i];
        lut[i] = cv::saturate_cast<uchar>(sum * scale);
    }

    // Apply equalization
    for (int r = 0; r < src.rows; ++r) {
        for (int c = 0; c < src.cols; ++c) {
                dst.at<uint8_t>(r, c) = lut[src.at<uint8_t>(r,c)];
            }
        }
}

cv::Mat LowLightMode::process(const Image::Ptr &frame, cv::Rect &roi){


    cv::Mat image = frame->get();
    cv::Mat grey = frame->gray();

    cv::Mat equ = cv::Mat::zeros(image.size(),CV_8UC1);
    equalizeHist(grey,equ);


    cv::cvtColor(equ,equ,cv::COLOR_GRAY2BGR);
    return equ;
}
