//
//  DisplayOpenCV.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#ifndef DisplayOpenCV_hpp
#define DisplayOpenCV_hpp

#include <stdio.h>
#include "Display.hpp"

class DisplayOpenCV: public DisplayBase{
    
public:
    DisplayOpenCV(bool fullscreen);
    void init();
    void draw(cv::Mat &image,const cv::Rect &roi=cv::Rect());

    
private:
    std::string m_window_name;
    
};


#endif /* DisplayOpenCV_hpp */
