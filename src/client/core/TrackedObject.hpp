//
//  TrackedObject.hpp
//  smart_specs_client
//
//  Created by Iain Wilson on 01/04/2019.
//

#ifndef TrackedObject_hpp
#define TrackedObject_hpp

#include <stdio.h>

#include "tracker/Tracker.hpp"
//#include "face_detection/FaceDetector.hpp"
#include "Image.hpp"
#include <chrono>
#include <memory>
#include <mutex>
class TrackedObject{

public:
    typedef std::shared_ptr<TrackedObject> Ptr;
    TrackedObject(const cv::Rect &roi, const Image::Ptr &image):m_roi(roi),m_isTracked(false),m_is_valid(true){
        m_time = std::chrono::high_resolution_clock::now();
        m_image = image;
        m_id =  rand() % 10000;
        init(m_roi);
    }
    
    static Ptr create(const cv::Rect &roi, const Image::Ptr &image){
        return std::make_shared<TrackedObject>(roi,image);
    }
    /*!
     \brief init object and the kalman filter
     */
    void init(const cv::Rect &roi);
    
    /*!
     \brief update the kalman filter
     */
    void update(const cv::Rect &roi);
    
    /*!
     \brief, use the tracker to find the current object.
     */
    void track(const Image::Ptr &image);
    
    
    /*!
     \brief last update time (from detector, not tracker
     */
    int age() const{
        auto now = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - timestamp());
        return duration.count();
    }

    std::chrono::high_resolution_clock::time_point timestamp() const{
        std::lock_guard<std::mutex> lock(_mutex);
        return m_time;
    }
    void setTimestamp(const std::chrono::high_resolution_clock::time_point &time){
        std::lock_guard<std::mutex> lock(_mutex);
        m_time = time;
    }
    
    cv::Rect roi() const {return m_roi;}
    
    /*!
     \brief check for validity of this object. use age.
     */
    bool isValid() const {
        if(age()>1000)
            return false;
        return m_is_valid;
    }
    
    bool isTracked()const {return m_isTracked;}
    void setTracked(const bool tracked=true){
        if(m_isTracked && !tracked)
            m_tracker.reset();
        m_isTracked=tracked;
    }
    
    void setValid(const bool valid){m_is_valid=valid;}
    
    int id() const {return m_id;}
    
private:
    int m_id;
    
    cv::Rect m_roi;
    cv::KalmanFilter m_kf;
    cv::Mat_<float> m_kf_measurement;
    
    std::chrono::high_resolution_clock::time_point m_time;
    
    Tracker::Ptr m_tracker;
    Image::Ptr m_image;
    
    bool m_isTracked;
    bool m_is_valid;
    
    mutable std::mutex _mutex;
};

#endif /* TrackedObject_hpp */
