//
//  FaceMode.cpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#include "FaceMode.hpp"
const int pyr{2};


FaceMode::FaceMode(const Display::Ptr display, const ImageBuffer::Ptr &input, const ImageBuffer::Ptr &display_buff,const SensorBuffer::Ptr &sensors):ProcessingModule("FACE_MODE",display,input,display_buff,sensors),m_blend_count(0){
    
    //init all the things..
    m_detect_and_track = DetectAndTrack::create();
    m_face_learner = FaceLearner::create();
    
    
    //setup the parameters
    auto t1 = m_parameters.add<int>("cartoon-T1", 4);
    t1->setIncrement(1);
    t1->setMin(0);
    t1->setMax(255);
    auto t2 = m_parameters.add<int>("cartoon-T2", 25);
    t2->setIncrement(1);
    t2->setMin(0);
    t2->setMax(255);
    
    auto blur = m_parameters.add<double>("cartoon-blur", 1.0);
    blur->setIncrement(1.0);
    blur->setMin(1.0);
    blur->setMax(100.0);
    
    m_parameters.add<bool>("cartoon-dilate",true);
    auto iter = m_parameters.add<int>("cartoon-dilate-iter",1);
    iter->setIncrement(1);
    iter->setMin(1);
    iter->setMax(25);
    

    auto open = m_parameters.add<int>("cartoon-open",1);
    open->setIncrement(1);
    open->setMin(0);
    open->setMax(10);
    auto close = m_parameters.add<int>("cartoon-close",1);


    close->setIncrement(1);
    close->setMin(0);
    close->setMax(10);
    
    
    m_parameters.add<bool>("face-segment-mask",false);
    auto b = m_parameters.add<int>("face-segment-blend",50);
    b->setIncrement(5);
    b->setMax(100);
    b->setMin(0);
    
    m_parameters.add<bool>("face-track-crop",false);
    m_parameters.add<bool>("face-draw",false);
    m_parameters.add<bool>("show-face-ids",false);
    
    m_parameters.add<bool>("apply-cartoon",true);
    
    linkParameters();
    
    m_face_learner->start();
    
}

void FaceMode::linkParameters(){
    //link the parameters
    m_display->link(m_parameters["cartoon-T1"], 'q','a'); //+,-
    m_display->link(m_parameters["cartoon-T2"], 'w','s'); //+,-
    m_display->link(m_parameters["cartoon-blur"], 't','g'); //+,-
    m_display->link(m_parameters["cartoon-open"],'z','x');
    m_display->link(m_parameters["cartoon-close"],'v','b');
    m_display->link(m_parameters["cartoon-dilate-iter"],'y','h');
    m_display->link(m_parameters["face-segment-mask"], 'm');
    m_display->link(m_parameters["face-segment-blend"], 'e','d');
    m_display->link(m_parameters["face-track-crop"], 't');
    m_display->link(m_parameters["apply-cartoon"], 'c');
    m_display->link(m_parameters["face-draw"],'f');
    m_display->link(m_parameters["show-face-ids"],'i');
    
}

cv::Mat FaceMode::process(const Image::Ptr &frame, cv::Rect &roi){
    
    cv::Mat image = frame->get().clone();  //make sure this is cloned!
    cv::Mat pyr_image = frame->get(pyr).clone();
    
    //get all the valid tracked faces
    auto rois = m_detect_and_track->rois();
    
    Cartoon cartoon;
    cartoon.setT1(m_parameters.value<int>("cartoon-T1"));
    cartoon.setT2(m_parameters.value<int>("cartoon-T2"));
    cartoon.setBlurSigma(m_parameters.value<double>("cartoon-blur"));
    cartoon.enableDilate(m_parameters.value<bool>("cartoon-dilate"));
    cartoon.setDilateIter(m_parameters.value<int>("cartoon-dilate-iter"));
    
    cv::Mat out = image ;
    cv::Mat skin;
    cv::Mat skin_bgr;
    cv::Mat cartoon_image = cv::Mat::zeros(image.size(),CV_8UC3);
    
    bool has_faces=false;
    if(m_face_learner->isReady()){
        
        skin = m_face_learner->detectSkin(pyr_image);
        cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3));
        
        //remove noise
        int close = m_parameters.value<int>("cartoon-close");
        for(int i=0;i<close;++i)
            cv::erode(skin, skin, kernel);
        for(int i=0;i<close;++i)
            cv::dilate(skin, skin, kernel);
        
        //fill in the gaps
        int open = m_parameters.value<int>("cartoon-open");
        for(int i=0;i<open;++i)
            cv::dilate(skin,skin,kernel);
        for(int i=0;i<open;++i)
            cv::erode(skin, skin,kernel);
        
        
        //for(int i=0;i<pyr;++i){

        cv::pyrUp(skin,skin);
        cv::pyrUp(skin,skin,image.size());
        
        //}
        cv::threshold(skin,skin,1,255,cv::THRESH_BINARY);
        cv::cvtColor(skin, skin_bgr, cv::COLOR_GRAY2BGR);
        
        //        cv::Mat masked_skin;
        //        cv::bitwise_and(image, out, masked_skin);
        //        out = masked_skin;
        
        //if(draw_cartoon){
        if(m_parameters.value<bool>("apply-cartoon")){
            float padd = 0.4f;
            //apply cartoonisation
            for(auto const &roi:rois){
                float w = roi.width*(1.0f+padd);
                float h = roi.height*(1.0f+padd);
                int x = roi.x - (w - roi.width)/2;
                int y = roi.y - (h - roi.height)/2;
                if(x<0) x=0;
                if(y<0) y=0;
                
                if(x+w>=frame->width())
                    w=frame->width()-x-1;
                if(y+h>=frame->height())
                    h=frame->height()-y-1;
                
                cv::Rect _roi(x,y,w,h);
                //auto _roi = face->roi(pyr,0.4);
                //            auto roi = face->roi(0.4);
                
                cv::Mat face_image = image(_roi);
                cv::Mat face_mask = skin(_roi);
                
                cv::Mat face_cart = cartoon.run(face_image,false,face_mask,true);
                cv::cvtColor(face_cart,face_cart,cv::COLOR_GRAY2BGR);
                //cv::cvtColor(face_mask,face_mask,cv::COLOR_GRAY2BGR);=
                //            cv::bitwise_and(out,face_cart,out);
                
                face_cart.copyTo(cartoon_image(_roi));
                
                //cv::addWeighted(image, 0.3, cartoon_image, 0.7, 1.0, out);
                has_faces = true;
            }
        }
        
        
    }else{
        out = image;
    }
    
    if(m_parameters.value<bool>("show-face-ids")){
        cv::Mat ids = m_face_learner->showFaces();
        if(ids.rows >0)
            cv::imshow("IDS",ids);
        
    }
    
    int max_blend = m_parameters.value<int>("face-segment-blend");
    //overlay cartoon onto image, use a blend
    if(m_parameters.value<bool>("apply-cartoon")){
        if(!has_faces && (m_blend_count>0))
            m_blend_count--;
        else if(m_blend_count < max_blend)
            m_blend_count++;
        else if(m_blend_count > max_blend)
            m_blend_count--;
        
        double b = (double)m_blend_count * 0.01;
        double a = 1.0 - b;
        //        cv::cvtColor(cartoon_image, cartoon_image, cv::COLOR_GRAY2BGR);
        cv::addWeighted(image, a, cartoon_image, b, 1.0, out);
    }
    
    //use skin mask to segment background
    if(m_parameters.value<bool>("face-segment-mask")){
        if(m_face_learner->isReady()){

//            std::cout << " skin " << skin_bgr.size() << " out " << out.size() <<std::endl;

            cv::bitwise_and(out, skin_bgr, out);
        }
    }
    
    
    if(m_parameters.value<bool>("face-track-crop")){
        // set the roi to the first face found
        if(rois.size()>0)
            roi = cropRoi(rois[0], image.cols,image.rows);
    }
    
    
    /* Test switchin between faces */
    
    
    
    
    
    
    return out;
}

cv::Rect FaceMode::cropRoi(const cv::Rect &roi, const int w,const int h){
    
    //set the roi to be a fixed ration (to fit in the headset display; 16*9
    int height = roi.height;
    cv::Point centre (roi.x+roi.width/2, roi.y+roi.height /2);
    int width = (height/9)*16;
    
    cv::Point tl( centre.x-(width/2),centre.y-height/2);
    cv::Point br( centre.x+(width/2),centre.y+height/2);
    cv::Point _centre = centre;
    
    //keep the rect within the frame
    if(tl.x<0)
        _centre.x = centre.x+(abs(tl.x));
    if(tl.y<0)
        _centre.y = centre.y+(abs(tl.y));
    if(br.x>w)
        _centre.x = centre.x+(w - br.x);
    if(br.y>h)
        _centre.y = centre.y+(h - br.y);
    
    cv::Point _tl( _centre.x-(width/2),_centre.y-height/2);
    cv::Point _br( _centre.x+(width/2),_centre.y+height/2);
    return cv::Rect(_tl,_br);
}


void FaceMode::bg_process(){
    
    /*
     Just performs face detection and tracking.
     */
    
    Image::Ptr img = m_input_buffer->pop();
    if(img->isEmpty())
        return;
    
    //pre-processing - build a skin map (low pyramid level)
    cv::Mat image = img->get();
    cv::Mat pyr_image = img->get(pyr);
    cv::Mat skin;
    bool _has_skin = false;
    if(m_face_learner->isReady()){
        
        skin = m_face_learner->detectSkin(pyr_image);
        for(int i=0;i<pyr;++i){
            cv::pyrUp(skin,skin);
        }
        m_detect_and_track->addValidation(skin.clone());
        cv::cvtColor(skin, skin, cv::COLOR_GRAY2BGR);
        //add the skin to the detector - to aid with validataion
        _has_skin = true;
    }
    
    //find some faces
    m_detect_and_track->run(img);
    auto objects = m_detect_and_track->objects();
    
    for(auto &obj:objects){
        Face::Ptr face = Face::create(obj->roi(),img);
        
        if(!face->isValid()) //more work here...
            return;
        
        auto features = m_detect_and_track->detector()->detectLandmarks(img->get(),face);
        face->setFeatures(features);
        face->createFaceMask(img->get());
        face->create2DHistogram();
        //add only valid faces
        //            m_identity_tracker->add(face);
        
        //if the object was tracked (not detected) do not add to the learner.
        if(!obj->isTracked()){
            face->setTracked(false);
            m_face_learner->add(face);
        }
    }
    
}
