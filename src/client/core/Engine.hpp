//
//  Engine.hpp
//  cpp_server
//
//  Created by Iain Wilson on 08/01/2019.
//

// class that handle the main loop  and rendering.

#ifndef Engine_hpp
#define Engine_hpp


#include <stdio.h>
#include "common/Thread.hpp"
#include "common/RingBuffer.hpp"
#include "common/Image.hpp"
#include "core/Capture.hpp"
#include "core/Processor.hpp"
#include "core/Display.hpp"
#include "common/networking/Response.hpp"
#include "common/networking/Offloader.hpp"
#include "common/Parameters.hpp"
#include "ProcessingModule.hpp"
#include "core/Sensors.hpp"


typedef RingBuffer<Image::Ptr> ImageBuffer;



const std::string MODE_FACE{"MODE_FACE"};
const std::string MODE_TEXT{"MODE_TEXT"};
const std::string MODE_ZOOM{"MODE_ZOOM"};
const std::string MODE_PASS{"MODE_PASS"};

const std::string MODE_SUPERPIXELS{"MODE_SUPERPIXELS"};
const std::string MODE_FACEDETECT{"MODE_FACEDETECT"};

class Engine: public Thread{
    
public:
    Engine();
    void init();
    void thread_run();
    void start();
    void stop();
    
    void setMode(const std::string& mode);
    
    
    
    inline cv::Mat segmentFace(const Image::Ptr &frame, cv::Rect &roi);
    inline cv::Mat segmentFace(const Image::Ptr &frame,const Face::Ptr &face);

    inline cv::Mat faceTrack(const Image::Ptr &frame, cv::Rect &roi);
    inline cv::Mat textTrack(const Image::Ptr &frame, cv::Rect &roi);
    inline cv::Mat superpixels(const Image::Ptr &frame);
    
private:
    void setCameraMode(const int mode){
        m_capture->camera()->switchMode(mode);
    }
    
    void setCameraOffset(const int offset){
        m_capture->setOffset(offset);
    }
    
    
    const std::string m_display_type;
    Display::Ptr m_display;
    Capture::Ptr m_capture;
    Sensors::Ptr m_sensors;
    
    
    ImageBuffer::Ptr m_capture_buffer;
    ImageBuffer::Ptr m_processing_buffer;
    ImageBuffer::Ptr m_display_buffer;
    ImageBuffer::Ptr m_network_buffer;
    SensorBuffer::Ptr m_sensor_buffer;

    RingBuffer<SmartSpecs::Response::Ptr>::Ptr m_results_buffer;
    
    
    SmartSpecs::Offloader::Ptr m_offloader;
    SmartSpecs::Networking::TCPServerAsync::Ptr m_net_server;
    std::string m_mode;
    Parameters m_parameters;

    
    //TODO abstract this in a better way!
    ProcessingModule::Ptr m_current_mode;
    ProcessingModule::Ptr m_mode_face;
    ProcessingModule::Ptr m_mode_text;
    ProcessingModule::Ptr m_mode_zoom;
    ProcessingModule::Ptr m_mode_pass;

    
};


#endif /* Engine_hpp */
