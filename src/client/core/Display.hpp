//
//  Display.hpp
//  cpp_server
//
//  Created by Iain Wilson on 20/11/2018.
//

#ifndef Display_hpp
#define Display_hpp

#include "cmakedefines.h"



#include <stdio.h>
#include <memory>
#include <chrono>
#include <opencv2/opencv.hpp>
#include "core/variables.hpp"
#include "Parameter.hpp"
#define GL_SILENCE_DEPRECATION
#include <boost/filesystem.hpp>



class DisplayBase{
    
public:
    typedef std::shared_ptr<DisplayBase> Ptr;
    DisplayBase(bool fullscreen,const std::string &name):m_name(name),m_fullscreen(fullscreen),m_size(1280,720),m_zoom(1.0f),m_offset{0.f,0.f},m_overlay_count(0),m_overlay_duration(60),m_out_index(0){
        //create an output folder name
        std::stringstream ss;
        ss << getenv("HOME") << "/data/display_recording/";
        ss << std::chrono::system_clock::now().time_since_epoch().count();
        ss << "/";
        m_output_folder = ss.str();

            //make the actual output dir.
        boost::filesystem::path dir(m_output_folder);
        if(boost::filesystem::create_directory(dir))
            std::cout << "[ DisplayBase ] Created " << m_output_folder <<std::endl;
        else
            std::cout << "[ DisplayBase ] Error Creating " << m_output_folder <<std::endl;
        
    }
    virtual ~DisplayBase()=default;
    virtual void init()=0;
    virtual void draw(cv::Mat &image,const cv::Rect &roi=cv::Rect())=0;
    
    //use this class to register keypresses and link to the parameter system.
    // key is the ascii decimal code.
    void link(const BaseParameter::Ptr &param, const char &inc_key,const char &dec_key){
        m_inc_keys[inc_key] = param;
        m_dec_keys[dec_key] = param;
    }
    
    void link(const BaseParameter::Ptr &param, const char &inc_key){
        m_inc_keys[inc_key] = param;
    }
    
    void link_scroll(const BaseParameter::Ptr &param){
        m_scroll = param;
    }
    
    
    void keypress(const char &key){
        
        //std::cout << "Key Pressed::: "<< key <<std::endl;
        
        //hack for up/down arros as zoom.
/*        if(key == '+'){
            scroll(1.0);
            return;
        }
        if(key=='-'){
            scroll(-1.0);
            return;
        }
  */      
        
        if(m_inc_keys.find(key) != m_inc_keys.end()){
            m_inc_keys[key]->inc();
            m_last_param_change = m_inc_keys[key];
            m_overlay_count = 0;
            return;
        }
        
        if(m_dec_keys.find(key) != m_dec_keys.end()){
            m_dec_keys[key]->dec();
            m_last_param_change = m_dec_keys[key];
            m_overlay_count = 0;
            return;
        }
    }

    
    void scroll(const double offset){
        //use the parameter system for this?

        #ifdef __APPLE__

        float _zoom = m_zoom + offset;
        #else
        float _zoom = m_zoom + (offset*0.1f);
        #endif

        if(_zoom <= 1.0) //min zoom = 1:1
            m_zoom = 1.0;
        else if(_zoom > 10.0) //max zoom = 10:1
            m_zoom = 10.0;
        else
            m_zoom = _zoom;
        
//        std::cout << "Offset : " <<offset  << " zoom: "<<m_zoom <<std::endl;
        
    }
    
    
    
    void setZoom(const float zoom){m_zoom = zoom;}
    float zoom()const {return m_zoom;}
    
    /*!
     \brief If zoomed in, get the viewable roi.
     */
    cv::Rect getRoi(){
        if(m_zoom == 1.0)
            return cv::Rect(0,0,m_size.width,m_size.height);
        
        int _w = m_size.width / m_zoom;
        int _h = m_size.height / m_zoom;
        int _x = (m_size.width - _w) /2;
        int _y = (m_size.height - _h) /2;
        
        return cv::Rect(_x,_y,_w,_h);
    }
    
    /*!
     \brief Overlay (briefly) information about parameter changes
     */
    void overlay(cv::Mat& image);
    
    void drawFooter(cv::Mat& image);
    
    /*!
     \brief Save to file, each frame as an seperate file
     */
    void save(cv::Mat& image){
        //data dir
        
        std::string data_dir =  std::string(getenv("HOME"))+"/tmp/";
        std::stringstream ss;
        ss << m_output_folder << "output_" <<m_out_index++<<".bmp";
        cv::imwrite(ss.str(),image);
    }
    
protected:
    std::string m_name;
    bool m_fullscreen;
    cv::Size m_size;
    
    std::map<char,BaseParameter::Ptr> m_inc_keys;
    std::map<char,BaseParameter::Ptr> m_dec_keys;

    BaseParameter::Ptr m_scroll;
    
    float m_zoom;
    float m_offset[2]; //x,y offset (-1 -> +1)
    
    int m_overlay_count;
    int m_overlay_duration;
    BaseParameter::Ptr m_last_param_change;
    
    int m_out_index;
    
    std::chrono::high_resolution_clock::time_point m_time;

    std::string m_output_folder;
    
     
};



struct Display{
  /*
    A class that manages the rendering and display - using OpenGL.
    Must deal with multiple displays, find specificaly the Epson display.
    Also, handle option to render to second display for monitoring purposes.
   */
    typedef std::shared_ptr<DisplayBase> Ptr;
    static Ptr create(const std::string &type, const bool fullscreen=false);
    
    
};




#endif /* Display_hpp */
