//
//  DetectAndTrack.hpp
//  smart_specs_client
//
//  Created by Iain Wilson on 01/04/2019.
//

/*
 
 Class that wraps a detector and trackers.
 Manages the lufcycle of object tracking.
 
 Uses a detector, trakcer and Kalman Filter
 
 */
#ifndef DetectAndTrack_hpp
#define DetectAndTrack_hpp

#include <stdio.h>
#include <string>
#include "tracker/Tracker.hpp"
#include "face_detection/FaceDetector.hpp"
#include "Image.hpp"
#include <memory>

#include "TrackedObject.hpp"

class DetectAndTrack {
    
public:
    typedef std::shared_ptr<DetectAndTrack> Ptr;
    
    DetectAndTrack(const std::string &name):m_name(name),m_has_validation(false){
        //create the tackers and detectors .. hard code for the momoent
        m_detector = FaceDetector::create(FD_OPENCV_DNN);
    }
    
    static Ptr create(){
        return std::make_shared<DetectAndTrack>("TEST");
    }
    
   
    /*!
     \brief run the detector.
     adds new objects, tracks old.
      filter output, using rules degined in detector.
     */
    void run(const Image::Ptr &image);
    
   
    /*!
     \brief return a list of tracked rois.
     */
    std::vector<cv::Rect> rois();
    
    
    /*!
     \brief draw the rois -
     */
    void draw(cv::Mat &image);
    
    
    /*!
     \void clean up old tracked objects.
     */
    void clean();
    
    /*!
     \brief Objects accessor
     */
    std::vector<TrackedObject::Ptr> objects() const {return m_objects;}
    
    
    void addValidation(const cv::Mat &validation){
        m_validation = validation;
        m_has_validation = true;
    }
    
    FaceDetector::Ptr detector() const {return m_detector;}
    
private:
    
    /*!
     \brief used for creaing an estimate of the measurement noise for the kalman filter.
     store a load of object rois, and calculate the variance of each value.
     
     ..use a stationary object.
     
     */
    void _calcVariance(const cv::Rect &roi){
        
        const int _count = 600;
        
        if(m_variance.size() < _count){
            m_variance.push_back(roi);
            return;
        }
        
        //calculate the mean.
        std::vector<float> _mean={0,0,0,0};
        for(auto const &v:m_variance){
            _mean[0]+=v.x;
            _mean[1]+=v.y;
            _mean[2]+=v.width;
            _mean[3]+=v.height;
        }
        
        _mean[0]/=_count;
        _mean[1]/=_count;
        _mean[2]/=_count;
        _mean[3]/=_count;

        //calculate the variance.
        std::vector<float> _var={0,0,0,0};
        for(auto const &v:m_variance){
            _var[0]+= (v.x-_mean[0])*2;
            _var[1]+= (v.y-_mean[1])*2;
            _var[2]+= (v.width-_mean[2])*2;
            _var[3]+= (v.height-_mean[3])*2;
        }
        
        std::cout << " >>>>> Mean >>>>>>> " << std::endl;
        std::cout << "x: "<<_mean[0] << " y: " << _mean[1] << " w: " << _mean[2] <<" h: " << _mean[3]  <<std::endl;
        
        std::cout << " >>>>> Variance >>>>>>> " << std::endl;
        std::cout << "x: "<<_var[0] << " y: " << _var[1] << " w: " << _var[2] <<" h: " << _var[3]  <<std::endl;
        
        //reset;
        m_variance.clear();
    }
    
    std::vector<cv::Rect> m_variance;
    
    std::string m_name;
    Tracker::Ptr m_tracker;
    FaceDetector::Ptr m_detector;
    std::vector<TrackedObject::Ptr> m_objects;
    
    cv::Mat m_validation;
    bool m_has_validation;
    
    mutable std::mutex _mutex;
};


#endif /* DetectAndTrack_hpp */
