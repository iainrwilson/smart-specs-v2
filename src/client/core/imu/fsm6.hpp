#ifndef FSM6_H
#define FSM6_H

#include <freespace/freespace.h>
#include <freespace/freespace_util.h>
#include <math.h>
//#include "math/Quaternion.h"
#include "common/RingBuffer.hpp"
#include <string>
#include <Eigen/Eigen>

struct HeadPose{
    typedef std::shared_ptr<HeadPose> Ptr;
    
    HeadPose(){}
    HeadPose(const Eigen::Quaternionf &rot,
             const Eigen::Vector3f &accel,
             const Eigen::Vector3f &pos): rotation(rot),acceleration(accel),position(pos){}
    
    
    static Ptr create(){ return std::make_shared<HeadPose>();}
    static Ptr create(const Eigen::Quaternionf &rot,
                      const Eigen::Vector3f &accel,
                      const Eigen::Vector3f &pos){
        return std::make_shared<HeadPose>(rot,accel,pos);
    }

    Eigen::Quaternionf rotation;
    Eigen::Vector3f acceleration;
    Eigen::Vector3f position;
};

class Fsm6 {
public:
    typedef std::shared_ptr<Fsm6> Ptr;
    Fsm6();
    static Ptr create(){
        return std::make_shared<Fsm6>();
    }
    
    bool connect();
    void disconnect();
    HeadPose::Ptr read();

 private:
    bool m_connected;
    
  //freespace_UserFrame m_userFrame;
  FreespaceDeviceId m_device;

};
#endif
