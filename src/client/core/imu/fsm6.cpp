#include "core/imu/fsm6.hpp"

#define RADIANS_TO_DEGREES(rad) ((float) rad * (float) (180.0 / M_PI))
#define DEGREES_TO_RADIANS(deg) ((float) deg * (float) (M_PI / 180.0))



Fsm6::Fsm6():m_connected(false){
}

bool Fsm6::connect(){
    int rc, numIds;
    
    //init freespace Lib.
    rc = freespace_init();
    if (rc != FREESPACE_SUCCESS){
        printf("[Fsm6]Initialization error. rc=%d\n", rc);
        return false;
    }
    
    /** --- START EXAMPLE INITIALIZATION OF DEVICE -- **/
    rc = freespace_getDeviceList(&m_device, 1, &numIds);
    if (numIds == 0) {
        printf("\t[Fsm6] freespaceInputThread: Didn't find any devices.\n");
        return false;
    }
    
    rc = freespace_openDevice(m_device);
    if (rc != FREESPACE_SUCCESS) {
        printf("\t[Fsm6] freespaceInputThread: Error opening device: %d\n", rc);
        return false;
    }
    
    rc = freespace_flush(m_device);
    if (rc != FREESPACE_SUCCESS) {
        printf("\t[Fsm6] freespaceInputThread: Error flushing device: %d\n", rc);
        return false;
    }
    
    freespace_message message;
//    message.messageType = FREESPACE_MESSAGE_DATAMODECONTROLV2REQUEST;
//    message.dataModeControlV2Request.packetSelect = 4;
//    //message.dataModeControlV2Request.modeAndStatus = 8;//0x00;//|= 0 << 1;
//    message.dataModeControlV2Request.mode = 0;
//    message.dataModeControlV2Request.ff6 = 1;
//    //    message.dataModeControlV2Request.status =0;
    
    // Put the device in the right operating mode
    //memset(&message, 0, sizeof(message));
    message.messageType = FREESPACE_MESSAGE_DATAMODECONTROLV2REQUEST;
    message.dataModeControlV2Request.packetSelect = 8; // MEOut
    message.dataModeControlV2Request.mode = 0;         // Set full motion
    message.dataModeControlV2Request.formatSelect = 0; // MEOut format 0
    message.dataModeControlV2Request.ff1 = 1;          // Acceleration fields
    message.dataModeControlV2Request.ff3 = 1;          // Angular (velocity) fields
    message.dataModeControlV2Request.ff6 = 1;          // Angular (orientation) fields
    
    rc = freespace_sendMessage(m_device, &message);
    if (rc != FREESPACE_SUCCESS) {
        printf("\t[Fsm6] freespaceInputThread: Could not send message: %d.\n", rc);
        return false;
    }
    
    freespace_message resp;
    rc = freespace_readMessage(m_device, &resp, 1000 /* 1 second timeout */);
    if(resp.messageType == FREESPACE_MESSAGE_DATAMODECONTROLV2RESPONSE){
        printf("\t[Fsm6] Packet select Set to: %d\n",resp.dataModeControlV2Response.packetSelect );
    }else
        printf("\t[Fsm6] Did not receive DATAMODECONTROLV2RESPONSE. Intead: %d\n",resp.messageType);
    
    /** --- END EXAMPLE INITIALIZATION OF DEVICE -- **/
    m_connected=true;
    return true;
}

void Fsm6::disconnect(){
    if(!m_connected)
        return;
    
    int rc;
    
    freespace_message message;
    
    /** --- START EXAMPLE FINALIZATION OF DEVICE --- **/
    message.messageType = FREESPACE_MESSAGE_DATAMODECONTROLV2REQUEST;
    message.dataModeControlV2Request.packetSelect = 1;
    
    rc = freespace_sendMessage(m_device, &message);
    if (rc != FREESPACE_SUCCESS) {
        printf("\t[Fsm6] freespaceInputThread: Could not send message: %d.\n", rc);
    }
    
    freespace_closeDevice(m_device);
    /** --- END EXAMPLE FINALIZATION OF DEVICE --- **/
    
    m_connected=false;
}


HeadPose::Ptr Fsm6::read(){
    
    int rc;
    freespace_message message;
    
    rc = freespace_readMessage(m_device, &message, 1000 /* 1 second timeout */);
    if (rc == FREESPACE_ERROR_TIMEOUT ||
        rc == FREESPACE_ERROR_INTERRUPTED) {
        printf("\t[Fsm6] Freespace Timeout hit.. \n");
        return HeadPose::Ptr();
    }
    
    if(rc!=FREESPACE_SUCCESS){
        printf("\t[Fsm6] freespaceInputThread: Error reading: %d. Trying again after a second...\n", rc);
        sleep(1);
    }
    
    // Check if this is a user frame message.
    if (message.messageType == FREESPACE_MESSAGE_BODYUSERFRAME) {
        MultiAxisSensor sensor;
        freespace_util_getAngPos(&message.motionEngineOutput, &sensor);
        Eigen::Quaternionf rotation(
                                    sensor.w,
                                    sensor.x,
                                    sensor.y,
                                    sensor.z
                                    );
        rotation.normalize();
        
        freespace_util_getAcceleration(&message.motionEngineOutput,&sensor);
        Eigen::Vector3f linearAccel(
                                    sensor.x,
                                    sensor.y,
                                    sensor.z
                                    );
        linearAccel.normalize();
        
        
        freespace_util_getInclination(&message.motionEngineOutput, &sensor);
        Eigen::Vector3f linearPos(
                                  message.bodyUserFrame.linearPosX,
                                  message.bodyUserFrame.linearPosY,
                                  message.bodyUserFrame.linearPosZ
                                  );
        //are they the same..?
        //std::cout << linearPos.x() << " " <<  sensor.x << std::endl;
        
        linearPos.normalize();
        
        return HeadPose::create(rotation,linearAccel,linearPos);
        
    }else if(message.messageType == FREESPACE_MESSAGE_MOTIONENGINEOUTPUT){
        
        //rInfo("length: %d",message.motionEngineOutput.meData[1]);
        
        
//        int la_x = message.motionEngineOutput.meData[6];
//        la_x += message.motionEngineOutput.meData[7] <<8;
//        int la_y = message.motionEngineOutput.meData[8];
//        la_y += message.motionEngineOutput.meData[9] <<8;
//        int la_z = message.motionEngineOutput.meData[10];
//        la_z += message.motionEngineOutput.meData[11] <<8;
        

    
        MultiAxisSensor sensor_r;
        freespace_util_getAngPos(&message.motionEngineOutput, &sensor_r);
        Eigen::Quaternionf rotation(
                                    sensor_r.w,
                                    sensor_r.x,
                                    sensor_r.y,
                                    sensor_r.z
                                    );
        std::cout <<  sensor_r.w << " " << sensor_r.x << " " << sensor_r.y << " " <<  sensor_r.z <<std::endl;
        
        //rotation.normalize();
   
        MultiAxisSensor sensor_a;
        freespace_util_getAcceleration(&message.motionEngineOutput,&sensor_a);
        Eigen::Vector3f linearAccel(
                                    sensor_a.x,
                                    sensor_a.y,
                                    sensor_a.z
                                    );
        //linearAccel.normalize();
   
        MultiAxisSensor sensor_p;
        freespace_util_getInclination(&message.motionEngineOutput, &sensor_p);
        Eigen::Vector3f linearPos(
                                  sensor_p.x,
                                  sensor_p.y,
                                  sensor_p.z
                                  );
        //linearPos.normalize();
        
        
        
        return HeadPose::create(rotation,linearAccel,linearPos);
        
    }else
        printf("\t[Fsm6] got Message %d not %d",message.messageType,FREESPACE_MESSAGE_MOTIONENGINEOUTPUT);
    
    return HeadPose::Ptr();
    
}


