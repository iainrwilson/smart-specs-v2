//
//  Sensors.cpp
//  smart_specs_client
//
//  Created by Iain Wilson on 02/08/2019.
//

#include "Sensors.hpp"

Sensors::Sensors(SensorBuffer::Ptr &buffer):Thread("Sensors"),m_buffer(buffer){
    m_fsm6 = Fsm6::create();
}

bool Sensors::connect(){
    //connect to the fsm 6 thing
    return m_fsm6->connect();
}

void Sensors::disconnect(){
    m_fsm6->disconnect();
}

void Sensors::thread_run(){
    
    auto head_pose = m_fsm6->read();
    if(head_pose.get() != nullptr)
        m_buffer->push(head_pose);
}
   
