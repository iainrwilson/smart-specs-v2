//
//  DisplayNull.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#ifndef DisplayNull_hpp
#define DisplayNull_hpp

#include <stdio.h>

#include "Display.hpp"

class DisplayNull : public DisplayBase{
    
public:
    DisplayNull():DisplayBase(false,DISPLAY_NULL){}
    void init(){}
    void draw(cv::Mat &image,const cv::Rect &roi=cv::Rect()){}
};


#endif /* DisplayNull_hpp */
