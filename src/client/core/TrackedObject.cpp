//
//  TrackedObject.cpp
//  smart_specs_client
//
//  Created by Iain Wilson on 01/04/2019.
//

#include "TrackedObject.hpp"

void TrackedObject::init(const cv::Rect &roi){
    //setup kalman with 5 dynamic, 4 measurment, and no control.
    //measurement is 2D location (centre of face) and height,width of roi
    
    //dynamic is 2D location, 2D velocity
    int measSize = 4;
    int stateSize = 6;
    int contrSize = 0;
    cv::KalmanFilter KF(stateSize,measSize, contrSize);
    KF.transitionMatrix = (cv::Mat_<float>(stateSize, stateSize) << 1,0,1,0,0,0,   0,1,0,1,0,0,  0,0,1,0,0,0,  0,0,0,1,0,0,  0,0,0,0,1,0, 0,0,0,0,0,1);
    /*
     Transision State Matrix [6x6] aka Process Model
     dT is set to 1 (could be updated each step?)
     [ 1 0 dT 0  0  0 ]
     [ 0 1 0  dT 0  0 ]
     [ 0 0 1  0  0  0 ]
     [ 0 0 0  1  0  0 ]
     [ 0 0 0  0  1  0 ]
     [ 0 0 0  0  0  1 ]
     */
    
    KF.measurementMatrix = cv::Mat_<float>::zeros(measSize, stateSize);
    KF.measurementMatrix.at<float>(0) = 1.0f;
    KF.measurementMatrix.at<float>(7) = 1.0f;
    KF.measurementMatrix.at<float>(16) = 1.0f;
    KF.measurementMatrix.at<float>(23) = 1.0f;
    /*
     Measurment Matrix
     [ 1 0 0 0 0 0 ]
     [ 0 1 0 0 0 0 ]
     [ 0 0 0 0 1 0 ]
     [ 0 0 0 0 0 1 ]
     */
    
    cv::setIdentity(KF.processNoiseCov, cv::Scalar::all(1e-3)); //set all noise to the same value. 0.001
    //KF.processNoiseCov
    /*
     Process Noise Covariance Matrix
     [ Ex 0  0    0    0  0 ]
     [ 0  Ey 0    0    0  0 ]
     [ 0  0  Ev_x 0    0  0 ]
     [ 0  0  0    Ev_y 0  0 ]
     [ 0  0  0    0    Ew 0 ]
     [ 0  0  0    0    0  Eh ]
     
      Process Noise: Decides the accuracy and time lag in the estimated value. Higher Q, the higher gain, more weight to the noisy measurements and the estimation accuracy is compromised; In case of lower Q, the better estimation accuracy is achieved and time lag may be introduced in the estimated value.
     
     The lower the value, the more
     
     */
//    KF.processNoiseCov.at<float>(0) = 1e-6;
//    KF.processNoiseCov.at<float>(7) = 1e-6;
//    KF.processNoiseCov.at<float>(14) = 1e-6;
//    KF.processNoiseCov.at<float>(21) = 1e-6;
//    
//    KF.processNoiseCov.at<float>(28) = 1e-6; //1e-3
//    KF.processNoiseCov.at<float>(35) = 1e-6;
//    
    
    
    // initialise the state [x,y,x_v,y_v,w,h]
    KF.statePre.at<float>(0) = roi.x;
    KF.statePre.at<float>(1) = roi.y;
    KF.statePre.at<float>(2) = 0;
    KF.statePre.at<float>(3) = 0;
    KF.statePre.at<float>(4) = roi.width;
    KF.statePre.at<float>(5) = roi.height;
    
    cv::setIdentity(KF.measurementNoiseCov, cv::Scalar::all(1e-2)); //the smaller the value the less noisy the input is assumed to be
    /*
     Measurement Noise : calculated experimentaly
     
     [ Vx 0  0  0  ]
     [ 0  Vy 0  0  ]
     [ 0  0  Vw 0  ]
     [ 0  0  0  vh ]
     
     Measurement Noise(R): Represents (electronic, random) noise characteristics of the sensor. It i is calculated from the sensor accuracy which is represented using "standard deviation" of measured value from true values (sigma) during the calibration.
     sigma_sq = sigma^2; % MATLAB/Ocatve code
     R=sigma_sq*eye(3,3);  % example for 3 axis position measurements
     
     
     */
//    std::cout << KF.measurementNoiseCov.size() <<std::endl;
//
    
    KF.measurementNoiseCov.at<float>(0) = 0.035f;
    KF.measurementNoiseCov.at<float>(5) = 0.030f;
    KF.measurementNoiseCov.at<float>(10) = 0.011f;
    KF.measurementNoiseCov.at<float>(15) = 0.012f;
   
    cv::setIdentity(KF.errorCovPost, cv::Scalar::all(1));
    m_kf = KF;
    std::cout << " >>>>> KF: New INIT" << std::endl;

//    //hack to fix init drift
//    for(int i=0;i<1;++i)
//        this->update(roi);

    
}

void TrackedObject::update(const cv::Rect &roi){
    
    // First predict, to update the internal statePre variable
    cv::Mat prediction = m_kf.predict();
//    std::cout << " >>>>> KF: prediction" << prediction <<std::endl;

    //create our measuerment matrix
    cv::Mat_<float> measurement(4,1); // [z_x,z_y,z_h]
    measurement(0) = roi.x;
    measurement(1) = roi.y;
    measurement(2) = roi.width;
    measurement(3) = roi.height;
    
    cv::Mat estimated = m_kf.correct(measurement); //[p_x,p_y,p_xv,p_yv,p_w,p_h]
    
    cv::Point centre = cv::Point(estimated.at<float>(0),estimated.at<float>(1));
    int width = static_cast<int>(estimated.at<float>(4));
    int height = static_cast<int>(estimated.at<float>(5));
    
//    std::cout << " >>>>> KF: roi " << measurement <<std::endl;
//    std::cout << " >>>>> KF: Estimated" << estimated <<std::endl << std::endl;
    
    m_roi = cv::Rect(centre,cv::Size(width,height));
}

void TrackedObject::track(const Image::Ptr &image){
    
    //init tracker.
    if(m_tracker.get() == nullptr){
        m_tracker = Tracker::create(TRACKER_OPENCV);
        //init tracker on last know image / roi
        m_tracker->init(m_image, m_roi);
    }
    
    cv::Rect roi = m_tracker->update(image);
    
    //TODO perform checks on the returned roi.
    
    //update kalman.
    this->update(roi);
    //tell the object it is in a tracked state
    this->setTracked(true);
}
