//
//  variables.h
//  cpp_server
//
//  Created by Iain Wilson on 25/01/2019.
//

#ifndef variables_h
#define variables_h

const std::string REMOTE_SERVER_IP{"127.0.0.1"};


/*Camera types */
const std::string FLYCAP{"FLYCAP"};
const std::string OPENCV{"OPENCV"};
const std::string VIDEOFILE{"VIDEOFILE"};
const std::string DIRECTORY{"DIRECTORY"};


const std::string CAMERA_TYPE{OPENCV};


const std::string DISPLAY_OPENCV{"DISPLAY_OPENCV"};
const std::string DISPLAY_OPENGL{"DISPLAY_OPENGL"};
const std::string DISPLAY_NULL{"DISPLAY_NULL"};




/* TRACKER VARIABLES */

const int TRACKER_PYR_LEVEL{0};
const int TRACKER_MAX_AGE{300};

const std::string TRACKER_DLIB{"TRACKER_DLIB"};
const std::string TRACKER_OPENCV{"TRACKER_OPENCV"};

const float TRACKER_OVERLAP_TOLERANCE{0.1f};

const int TRACKER_HISTORY_LENGTH{10};

/* FACE TRACKER */
const std::string FACETRACKER_TRACKER_TYPE{TRACKER_OPENCV};
const std::string TRACKER_OPENCV_TYPE{"MOSSE"};

/*  FACEDETECTOR  */
const int FACEDETECT_PYR_LEVEL{0};
const float FACEDETECT_MIN_SIZE{0.05}; //reject faces smaller than this percentage of total image size

const std::string FD_OPENCV_HAAR{"FD_OPENCV_HAAR"};
const std::string FD_OPENCV_DNN{"FD_OPENCV_DNN"};
const std::string FD_DLIB_MMOD{"FD_DLIB_MMOD"};

const std::string FACEDETECT_METHOD{FD_OPENCV_DNN};

//histogram comparision threshold for rejecting non face faces.
const float FACEDETECT_REJECT_THRESHOLD{0.2};

/* SKINSEGMENT */
const int SKINSEGMENT_PYR_LEVEL{2};

/* HISTOGRAM BIN SIZES */
const int H_BINS{64};
const int S_BINS{64};

const std::string SEG_HISTOGRAM{"SEG_HISTOGRAM"};
const std::string SEG_DECISION{"SEG_DECISION"};
const std::string SEG_RANDOM_FOREST{"SEG_RANDOM_FOREST"};
const std::string SEG_NAIVE_BAYES{"SEG_NAIVE_BAYES"};

/* Skin classifier dataset */
const std::string DATA_NONE{""};
const std::string DATA_PRA{"DATA_PRA"};
const std::string DATA_LFW{"DATA_LFW"};
const std::string DATA_UCI_BGR{"DATA_UCI_BGR"};
const std::string DATA_UCI_HSV{"DATA_UCI_HSV"};

/* skin classifier colour space */
const std::string CC_NONE{"CC_NONE"};
const std::string CC_BGR2HSV{"CC_BGR2HSV"};
const std::string CC_BGR2HS{"CC_BGR2HS"};
const std::string CC_BGR2HV{"CC_BGR2HV"};
const std::string CC_BGR2H{"CC_BGR2H"};
const std::string CC_BGR2LAB{"CC_BGR2LAB"};
const std::string CC_BGR2AB{"CC_BGR2AB"};



/* FACE SKIN LEARNER */
const int FACELEARNER_FACE_BUFFER_SIZE{20};
const int FACELEARNER_INPUT_BUFFER_SIZE{2};
const int FACELEARNER_TRAIN_PYR_LEVEL{2};
const float FACELEARNER_MASK_PADD{0.75};


const std::string FACELEARNER_SEGMENTER_TYPE{SEG_DECISION};
const std::string FACELEARNER_COLOURSPACE{CC_BGR2LAB};

//age limit for storing identities (20 seconds)
const int IDENTITY_AGE_LIMIT{2000};
//minimum number of faces for an ID to be considered valid.
const int IDENTITY_MIN_NO_FACES{3};

//Face tracker age threhsold - how larger age difference invalidates a roi overlap.
const int FACETRACKER_AGE_THRESHOLD{100}; //100ms
const int FACETRACKER_INPUT_BUFFER_SIZE{2};

const int FACELEARNER_MIN_NO_FACES{2}; // the minumum number of faces required to start thse skin seg training.
const int FACELEARNER_MAX_NO_FACES{50}; // the minumum number of faces required to start thse skin seg training.
//identitu comparision threhsold
const float FACELEARNER_ID_HIST_THRESHOLD{0.95};








#endif /* variables_h */
