//
//  LowLightMode.hpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#ifndef LowLightMode_hpp
#define LowLightMode_hpp

#include <stdio.h>
#include "ProcessingModule.hpp"

#ifdef USE_CUDA
#include "VideoStablisation_GPU.hpp"
#else
#include "VideoStablisation.hpp"
#endif

class LowLightMode : public ProcessingModule {
    
public:
    LowLightMode(const Display::Ptr display, const ImageBuffer::Ptr &input,const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors);
    
    cv::Mat process(const Image::Ptr &frame, cv::Rect &roi);    



    void bg_process(){};
    
    void linkParameters();
private:


    void equalizeHist(const cv::Mat &src, cv::Mat &dst);    

#ifdef USE_CUDA
    VideoStablisation_GPU m_stab;
#else
    VideoStablisation m_stab;
#endif    

};

#endif /* LowLightMode_hpp */
