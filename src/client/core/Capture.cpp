//
//  Capture.cpp
//  cpp_server
//
//  Created by Iain Wilson on 21/11/2018.
//

#include "Capture.hpp"
#include "common/Image.hpp"
#include "variables.hpp"

Capture::Capture(ImageBuffer::Ptr &buffer, CameraBase::Ptr &camera):Thread("Capture"),m_camera(camera),m_buffer(buffer),m_v_offset(0){
}

Capture::Capture(ImageBuffer::Ptr &buffer, ImageBuffer::Ptr &buffer_2, CameraBase::Ptr &camera):Thread("Capture"),m_camera(camera),m_buffer(buffer),m_buffer_2(buffer_2),m_v_offset(0){
}

Capture::Capture(ImageBuffer::Ptr &buffer, const std::string &type): Thread("Capture"),m_buffer(buffer),m_v_offset(0){
    m_camera = Camera::create(type);
}
Capture::Capture(ImageBuffer::Ptr &buffer, ImageBuffer::Ptr &buffer_2, const std::string &type): Thread("Capture"),m_buffer(buffer),m_buffer_2(buffer_2),m_v_offset(0){
    m_camera = Camera::create(type);
}


bool Capture::connect(){
    m_start_time = std::chrono::high_resolution_clock::now();
    return m_camera->connect();
}

void Capture::disconnect(){
    m_camera->disconnect();
}

void Capture::thread_run(){
    
    //if headmounted and using seecam. rotate the image
    cv::Mat image = m_camera->grab();
    Image::Ptr img;
    
    if(CAMERA_TYPE==OPENCV){
    
        int v_offset = m_v_offset;
        
        cv::Rect roi;
        bool rotate_and_crop = false;
        int o_h = image.rows;; //origin image height
        int o_w = image.cols;; // original width
        int h,w=o_h; //new widht and height
        int x=0,y=0; //x,y positions; x is always 0.
        
    //    if( (o_w == 1280) & (o_h == 720)){
    //        rotate_and_crop = true;
    //        h = 405; // = h /(16:9)
    //        y = 158; // (o_h - h)/2
    //    }
    //    else
        if( (o_w == 1920) & (o_h == 1080)){
            rotate_and_crop = true;
            h = 607; // = h /(16:9)
            y = 236; // (o_h - h)/2
        }
        else if( (o_w == 3840) & (o_h == 2160) ){
            rotate_and_crop = true;
            h = 1215; // = h /(16:9)
            y = 1312; // (o_h - h)/2
        }
        else if( (o_w == 4096) & (o_h == 2160)){
            //ration of 1.896296
            rotate_and_crop = true;
            h = 1139; // = h /(16:9)
            y = 1478; // (o_h - h)/2
            
        }
        else if(o_w == 4224){
            rotate_and_crop = true;

            h = 1755; // = h /(16:9)
            y = 1226; // (o_h - h)/2
        }
        
        
        if(rotate_and_crop){
            if(v_offset+y+h>o_w)
                v_offset = o_w - h; //set to max
            else if(v_offset+y+h < 0 )
                v_offset = -y; //minimum
            
            roi = cv::Rect(x,y+v_offset,w,h); // mode 2
            
            cv::rotate(image, image, cv::ROTATE_90_CLOCKWISE);
            
            //TODO crop
            //1920 x 1080
            //3840 x 2160
            //4096 x 2160
            //4424 x 3156
            //W: 4208 H: 3120
            
            
            //x=0, y=(h-h_n)/2 , w_n = h, h_n = w/(16/9).

            cv::Mat crop = image(roi).clone();
            
            img = Image::create(crop);
        }else{
            img = Image::create(image);
        }
    }else{
        img = Image::create(image);
    }
    
    if(img->isEmpty())
        return;
    
    img->buildPyramid(4);
    
    m_buffer->push(img);
    m_buffer_2->push(img);
    
    
}
