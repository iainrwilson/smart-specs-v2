//
//  DisplayOpenGL.cpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#include "DisplayOpenGL.hpp"
#define GL_SILENCE_DEPRECATION

#ifdef USE_OPENGL

DisplayOpenGL::DisplayOpenGL(const bool fullscreen):DisplayBase(fullscreen,DISPLAY_OPENGL),m_screen_width(1280),m_screen_height(720),m_roi_changed(false){
}


void DisplayOpenGL::init(){
    
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        getchar();
        return;
    }
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    
    //Monitor [0] HDMI-0 6 w:708 h:399
    //Monitor [1] DP-2-1 6 w:890 h:500
    //Monitor [2] eDP-2-1 7 w:344 h:194

    
    //find monitors
    GLFWmonitor* primary = glfwGetPrimaryMonitor();
    m_primary = primary;
    
    int count;
    GLFWmonitor** monitors = glfwGetMonitors(&count);
    
    int hmd =-1;
    for(int i=0;i<count;++i){
        //print physical sizes
        int widthMM, heightMM;
        glfwGetMonitorPhysicalSize(monitors[i], &widthMM, &heightMM);
        const char* name = glfwGetMonitorName(monitors[i]);
	    std::cout << "Monitor ["<<i<<"] "<<name<< " " <<std::string(name).size()<< " w:"<< widthMM << " h:"<<heightMM<<std::endl;
        
       	if((std::strcmp("EPSON HMD",name) ==0) || (std::strcmp("DP-2-1",name) ==0) || (std::strcmp("DP-1",name) ==0)  || (std::strcmp("DP-1-1",name) ==0)){
            hmd = i;
            std::cout << "\tFound HMD ["<<i<<"] "<<name<< " w:"<< widthMM << " h:"<<heightMM<<std::endl;
        }
    }
    hmd = -1;
    if(hmd == -1){
        std::cout << "Cannot find HMD" << std::endl;
    }else{
        m_primary = monitors[hmd];
    }
    m_secondary = nullptr;
    m_secondary_window = nullptr;
    
    //create a window:
    const GLFWvidmode* mode = glfwGetVideoMode(m_primary);
    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
    glfwWindowHint(GLFW_AUTO_ICONIFY,GLFW_FALSE);
    
    std::cout<< "[Video Mode] " <<mode->width <<"x"<<mode->height<<std::endl;
    //hmd=(-1);
    if(hmd!= -1){
        m_window = glfwCreateWindow(m_screen_width,m_screen_height, "HMD", m_primary, NULL);
    }else{
        m_window = glfwCreateWindow(m_screen_width,m_screen_height, "HMD", NULL, NULL);
    }
    if( m_window == NULL ){
        fprintf( stderr, "Failed to open GLFW window.\n" );
        getchar();
        glfwTerminate();
        return;
    }
    
    //testing multi-displays
    if (m_primary != primary){
        
        std::cout << "\tMaking secondry screen" << std::endl;
        
        //then we have two displays.
        m_secondary = primary;
        m_secondary_window = glfwCreateWindow(m_screen_width, m_screen_height, "DEBUG", NULL, NULL);
        
        m_programID_2 = initDisplay(m_secondary_window, &m_textureID_2);
    }
    
    
    //set the size according to the window chosen.
    int _width, _height;
    glfwGetWindowSize(m_window, &_width, &_height);
    std::cout << "[ Window Created ] "<<_width << + "x" << _height <<std::endl;
    m_size = cv::Size(_width,_height);
    
    m_programID = initDisplay(m_window, &m_textureID);
    glfwFocusWindow(m_window);
    
    
    printf("OpenGL version supported by this platform (%s): \n", glGetString(GL_VERSION));
    
}


/*!
 //static callback that manages C glfw callback method.
 */
void DisplayOpenGL::key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    DisplayOpenGL *windowManager = static_cast<DisplayOpenGL*>(glfwGetWindowUserPointer(window));
   
    if(action == GLFW_PRESS){
        const char* code = glfwGetKeyName(key,scancode);
        if(code != nullptr)
            windowManager->keypress(*code);
    }
    
}

void DisplayOpenGL::scroll_callback(GLFWwindow *window, double xoffset, double yoffset){
    
//    std::cout << "Scroll y: " << yoffset << std::endl;
    DisplayOpenGL *windowManager = static_cast<DisplayOpenGL*>(glfwGetWindowUserPointer(window));
    windowManager->scroll(yoffset);
}



GLuint DisplayOpenGL::initDisplay(GLFWwindow* window, GLuint* textureID){
    
    glfwMakeContextCurrent(window);
    glewExperimental = GL_TRUE;
    
    // Initialize GLEW
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        // return -1;
    }
    
    
    //set the key press funtion callback
    glfwSetKeyCallback(window, key_callback);
    
    //set scroll callback
    glfwSetScrollCallback(window, scroll_callback);
    
    const char *v = (const char *) glGetString(GL_VERSION);
    std::cout << v << std::endl;
    
    // Ensure we can capture the escape key being pressed below
    glfwSetWindowUserPointer(window, this);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    
    // Dark blue background
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    
    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);


    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/glsl/";
    std::string vertex = dataDir+"TransformVertexShader.vertexshader";
    std::string fragment = dataDir+"TextureFragmentShader.fragmentshader";
    
    GLuint programID = loadShaders( vertex.c_str(), fragment.c_str() );
    //    GLuint programID = LoadShaders( "../../TransformVertexShader.vertexshader", "../../resample.frag" );
    
    
    initVertexBuffer();
    
    
    GLushort indices[] = { 0, 1, 2, 2, 3, 0 };
    
    GLuint ebo;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
 
    
    glGenTextures(1, textureID);
    glBindTexture(GL_TEXTURE_2D, *textureID);
    //glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, out.ptr());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    return programID;
}

GLuint DisplayOpenGL::loadShaders(const char * vertex_file_path,const char * fragment_file_path){
    
    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    
    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open()){
        std::stringstream sstr;
        sstr << VertexShaderStream.rdbuf();
        VertexShaderCode = sstr.str();
        VertexShaderStream.close();
    }else{
        printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
        getchar();
        return 0;
    }
    
    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::stringstream sstr;
        sstr << FragmentShaderStream.rdbuf();
        FragmentShaderCode = sstr.str();
        FragmentShaderStream.close();
    }
    
    GLint Result = GL_FALSE;
    int InfoLogLength;
    
    
    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);
    
    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }
    
    
    
    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);
    
    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }
    
    
    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glBindFragDataLocation(ProgramID, 0, "color");
    glLinkProgram(ProgramID);
    
    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }
    
    glDetachShader(ProgramID, VertexShaderID);
    glDetachShader(ProgramID, FragmentShaderID);
    
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);
    
    return ProgramID;
}


void DisplayOpenGL::render(GLFWwindow* window, GLuint programID, const cv::Mat& image, GLuint textureID){
    glfwMakeContextCurrent(window);
    
    m_image_size = image.size();
    
    // Clear the screen
    glClear( GL_COLOR_BUFFER_BIT );
    
    // Use our shader
    glUseProgram(programID);
    GLsizei stride = 5 * sizeof(GLfloat); // 3 for position, 2 for texture
    // 1rst attribute buffer : vertices

    //bind to the vertext buffer;
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glEnableVertexAttribArray(0);  // position as defined in the vertex shader
    glVertexAttribPointer(
                          0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                          3,                  // size (number of components per vertex attribute.
                          GL_FLOAT,           // type
                          GL_FALSE,           // normalized?
                          stride,                  // stride
                          (void*)0            // array buffer offset
                          );
    
    glEnableVertexAttribArray(1); // texture
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(GLfloat)));
    
    //set the zoom value
    GLint zoomLocation = glGetUniformLocation(programID, "zoom");
    glUniform1f(zoomLocation, m_zoom);
    
    GLint offsetLocation = glGetUniformLocation(programID, "offset");
    glUniform2f(offsetLocation, m_offset[0], m_offset[1]);
//    glUniform2f(offsetLocation, 0.7,0.4);
//    r
    //std::cout << m_zoom << " " << m_offset[0]  << " " << m_offset[1] << std::endl;
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, m_image_size.width, m_image_size.height, 0, GL_BGR, GL_UNSIGNED_BYTE, image.ptr());
    glUniform1i(glGetUniformLocation(programID, "viewTexture"), 0);
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
    
    // glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    
    // Swap buffers
    glfwSwapBuffers(window);
    glfwPollEvents();
}


void DisplayOpenGL::initVertexBuffer(){
    glGenBuffers(1, &m_vertexBuffer); //create buffer
    resetVertexBuffer();
}

void DisplayOpenGL::resetVertexBuffer(){
    glUseProgram(m_programID);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer); //bind to buffer
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertices), g_vertices, GL_STATIC_DRAW); // assign and copy data
    glBindBuffer(GL_ARRAY_BUFFER, 0); //unbind
    m_roi_changed = false;
}


/*!
 \brief update the vertex buffer to the specified roi - cropping the texture.
*/
void DisplayOpenGL::cropTexture(const cv::Rect &roi){
//    GLfloat vertices[] = {
//        //  Position        Texcoords
//        -1.0f,  1.0f, 0.0f, 0.0f, 0.0f, // Top-left
//        1.0f,  1.0f, 0.0f, 1.0f, 0.0f, // Top-right
//        1.0f, -1.0f,0.0f,  1.0f, 1.0f, // Bottom-right
//        -1.0f, -1.0f,0.0f,  0.0f, 1.0f  // Bottom-left
//    };
//
//    float scale_x = 1.0f/static_cast<float>(m_image_size.width);
//    float scale_y = 1.0f/static_cast<float>(m_image_size.height);
//
//    //tl
//    vertices[3] = (float)roi.tl().x * scale_x;
//    vertices[4] = (float)roi.tl().y * scale_y;
//    //tr
//    vertices[8] = (float)roi.br().x * scale_x;
//    vertices[9] = vertices[4];
//    //br
//    vertices[13] = vertices[8];
//    vertices[14] = (float)roi.br().y * scale_y;
//    //bl
//    vertices[18] = vertices[3];
//    vertices[19] = vertices[14];
//
//    glUseProgram(m_programID);
//    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
    m_roi_changed = true;

    
    m_zoom = (float)m_image_size.width / (float)roi.width;
    
    cv::Point2f centre = (roi.br() + roi.tl())*0.5f;

    m_offset[0] = -(( (centre.x / (float)m_image_size.width) * 2) - 1);
    m_offset[1] = (( (centre.y / (float)m_image_size.height) * 2) - 1);

    
    
//    std::cout << centre << " "  << m_offset[0]  << " " << m_offset[1] << std::endl;

}

void DisplayOpenGL::draw(cv::Mat &image, const cv::Rect &roi){
    
    if(roi.area()>0){
        cropTexture(roi);
//        cv::Mat _image = image(roi);
        //overlay(_image);
    }else{
        if(m_roi_changed){
            m_zoom = 1.f;
            m_offset[0]=0.f;
            m_offset[1]=0.f;
            m_roi_changed = false;
        }
        overlay(image);
    }
    

    render(m_window, m_programID, image, m_textureID);
    if(m_secondary_window != nullptr)
        render(m_secondary_window, m_programID_2, image, m_textureID_2);

}


#endif // USE_OPENGL
