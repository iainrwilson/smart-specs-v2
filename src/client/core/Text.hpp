//
//  Text.hpp
//  cpp_server
//
//  Created by Iain Wilson on 22/11/2018.
//

#ifndef Text_hpp
#define Text_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>

class Text{
  
public:
    Text(){};
    Text(const cv::Rect &roi);
    
    cv::Rect roi()const{return m_roi;}
    void setRoi(const cv::Rect roi){m_roi = roi;}
    
private:
    cv::Rect m_roi;
    
    
};


#endif /* Text_hpp */
