//
//  TextSegmentation.hpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#ifndef TextSegmentation_hpp
#define TextSegmentation_hpp

#include <stdio.h>
#include "ProcessingModule.hpp"


#ifdef USE_CUDA
#include "VideoStablisation_GPU.hpp"
#else
#include "VideoStablisation.hpp"
#endif



class TextMode : public ProcessingModule{
    
public:
    TextMode(const Display::Ptr display, const ImageBuffer::Ptr &input, const ImageBuffer::Ptr &display_buff,const SensorBuffer::Ptr &sensors);
    
    cv::Mat process(const Image::Ptr &frame, cv::Rect &roi);
    
    void bg_process();
    
    void linkParameters();
    
    
private:
    
    std::vector<cv::RotatedRect> detectText(cv::Mat &image);
    void decode(const cv::Mat& scores, const cv::Mat& geometry, float scoreThresh,
                std::vector<cv::RotatedRect>& detections, std::vector<float>& confidences);
   
    void selectTextBoxes();    
    
    //add text detection stuff // tracking etc.
    cv::dnn::Net m_net;
    
    std::vector<cv::RotatedRect> m_text_boxes;

#ifdef USE_CUDA
    VideoStablisation_GPU m_stab;
#else
    VideoStablisation m_stab;
#endif    

};


#endif /* TextSegmentation_hpp */
