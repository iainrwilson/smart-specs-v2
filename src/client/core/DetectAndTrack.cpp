//
//  DetectAndTrack.cpp
//  smart_specs_client
//
//  Created by Iain Wilson on 01/04/2019.
//
//
//
//
#include "DetectAndTrack.hpp"

const float FT_MIN_SKIN_RATIO{0.4};

void DetectAndTrack::run(const Image::Ptr &image){
        
    //run detector - on full size image
    auto rois = m_detector->detect(image->get());
    
    //clean up old trackers
    this->clean();
    std::vector<TrackedObject::Ptr> _objects;

    for(auto const &roi:rois){
        
        //std::cout << " >>>>> FD: roi " << roi <<std::endl;
        
        //filter the crap out.
        if(roi.x <= 0 || roi.y <= 0 || roi.width <=0 || roi.height <=0){
            std::cout << "[ DetectAndTrack ] Bad ROI" <<std::endl;
            continue;
        }
            
        if(roi.y + roi.height > image->height() || roi.x + roi.width > image->width() ){
            std::cout << "[ DetectAndTrack ] Oversized ROI" <<std::endl;
            continue;
        }
        //add validataion using an object specific thing (faces or text)
        // for faces use skin image.
        
//        if(m_has_validation){
//            cv::Mat _roi = m_validation(roi);
//            int _count = cv::countNonZero(_roi);
//            float _ratio = static_cast<float>(_count) / static_cast<float>(_roi.total());
//            //is there are less than 10% skin pixels ... ditch
////            std::cout << "Skin ratio: " << _ratio <<std::endl;
//            if(_ratio < FT_MIN_SKIN_RATIO ){
//                //ditch
//                std::cout << " >>>> ROI ditched " << _ratio <<std::endl;
//                continue;
//            }
//        }
        
        //compare detected to tracked objects.
        bool _matched = false;
        for(auto &obj:m_objects){
            
            //check for overlap.
            auto u = roi & obj->roi();
            auto i = roi | obj->roi();
            
            //  intersection / union.
            float r = static_cast<float>(u.area()) / static_cast<float>(i.area());
//            std::cout << "[ DetectAndTrack ] overlap " << r << std::endl;
            if(r >= TRACKER_OVERLAP_TOLERANCE){
                //then a probably match
                obj->update(roi);
                //set timestamp to the image used.
                obj->setTimestamp(image->timestamp());
                obj->setTracked(false);
                _matched = true;
                break;
            }
        }
        
        //if you get here, no matched object for the roi, so make a new one.
        if(!_matched){
            //protect with mutex.
            _objects.push_back(TrackedObject::create(roi,image));
        }
    }

    //now perform tracking - loop through all older objects (ones that have not been recentlu updated. Start trackers.
    
    for(auto &obj:m_objects){
        
        if(obj->timestamp() == image->timestamp())
            continue;
        
        //use validation here as well.
//        if(m_has_validation){
//            cv::Mat _roi = m_validation(obj->roi());
//            int _count = cv::countNonZero(_roi);
//            float _ratio = static_cast<float>(_count) / static_cast<float>(_roi.total());
//            //is there are less than 10% skin pixels ... ditch
////            std::cout << "Skin ratio: " << _ratio <<std::endl;
//            if(_ratio < FT_MIN_SKIN_RATIO ){
//                //ditch
//                std::cout << " >>>> ROI ditched " << _ratio <<std::endl;
//                obj->setValid(false);
//                continue;
//            }
//        }
        
        //object has not been detected, run it's tracker.
//        obj->track(image);
    }
    
    //add the new objects into the buffer.
    {
        std::lock_guard<std::mutex> lock(_mutex);
        std::copy(_objects.begin(),_objects.end(),std::back_inserter(m_objects));
    }
    //remember to clear the validatiaon, as it only works for this frame.
    m_has_validation = false;
}


std::vector<cv::Rect> DetectAndTrack::rois(){
    std::vector<cv::Rect> rois;
    {
        std::lock_guard<std::mutex> lock(_mutex);
        for(auto &obj:m_objects){
            if(obj.get() != nullptr){
                if(obj->isValid())
                    rois.push_back(obj->roi());
            }
        }
    }
    return rois;
}


void DetectAndTrack::clean(){
    
    auto it = std::begin(m_objects);
    for(;it!=std::end(m_objects);){
        if(!(*it)->isValid()){
            std::cout << "[ DetectAndTrack ] Erase " << std::endl;
            it = m_objects.erase(it);
        }else{
            ++it;
        }
    }
}

void DetectAndTrack::draw(cv::Mat &image){
//    std::cout << " [DetectAndTrack ] tracking: " << m_objects.size() <<std::endl;

    for(auto &obj:m_objects){
        if(!obj->isValid())
            continue;
        if(obj->isTracked())
           cv::rectangle(image, obj->roi(), cv::Scalar(0,0,255),2);
        else
           cv::rectangle(image, obj->roi(), cv::Scalar(0,255,0),2);
    }
    
    
}
