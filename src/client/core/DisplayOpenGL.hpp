
//
//  DisplayOpenGL.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#ifndef DisplayOpenGL_hpp
#define DisplayOpenGL_hpp

#include <stdio.h>
#include "Display.hpp"
#ifdef USE_OPENGL
#include <GL/glew.h>
#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>

const GLfloat g_vertices[] = {
    //  Position        Texcoords
    -1.0f,  1.0f, 0.0f, 0.0f, 0.0f, // Top-left
    1.0f,  1.0f, 0.0f, 1.0f, 0.0f, // Top-right
    1.0f, -1.0f,0.0f,  1.0f, 1.0f, // Bottom-right
    -1.0f, -1.0f,0.0f,  0.0f, 1.0f  // Bottom-left
};

class DisplayOpenGL: public DisplayBase {
    
public:
    DisplayOpenGL(const bool fullscreen);
    void init();
    void draw(cv::Mat& image, const cv::Rect &roi = cv::Rect());
    
    
private:
    void render(GLFWwindow* window, GLuint programID, const cv::Mat& image, GLuint textureID);

    GLuint initDisplay(GLFWwindow* window, GLuint* textureID);
    GLuint loadShaders(const char * vertex_file_path,const char * fragment_file_path);
    
    static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
    static void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);
    
    void initVertexBuffer();
    void resetVertexBuffer();

    void cropTexture(const cv::Rect &roi);
    
    cv::Size m_size;
    GLuint m_textureID;
    GLuint m_textureID_2;
    GLFWmonitor* m_primary;
    GLFWmonitor* m_secondary;
    GLFWwindow* m_window;
    GLFWwindow* m_secondary_window;

    GLuint m_programID;
    GLuint m_programID_2;
    
    GLuint m_vertexBuffer;
    
    
    int m_screen_width;
    int m_screen_height;

    bool m_roi_changed;
    cv::Size m_image_size;

    
};

#endif //USE_OPENGL
#endif /* DisplayOpenGL_hpp */
