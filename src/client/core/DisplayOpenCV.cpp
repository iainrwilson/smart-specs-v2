//
//  DisplayOpenCV.cpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#include "DisplayOpenCV.hpp"

DisplayOpenCV::DisplayOpenCV(bool fullscreen):DisplayBase(fullscreen,DISPLAY_OPENCV),m_window_name("DISPLAY"){
    
}

void DisplayOpenCV::init(){
    // Create opencv window
    cv::namedWindow(m_window_name,cv::WINDOW_NORMAL);
    cv::resizeWindow(m_window_name,m_size.height,m_size.width);

    if(m_fullscreen)
        cv::setWindowProperty(m_window_name, cv::WND_PROP_FULLSCREEN, 1);
}

void DisplayOpenCV::draw(cv::Mat &image, const cv::Rect &roi){
    if(image.empty())
        return;
    
    if(roi.area()>0){
        cv::Mat _image = image(roi);
        overlay(_image);
        cv::imshow(m_window_name,image(roi));
    }else{
        overlay(image);
        cv::imshow(m_window_name,image);
    }
    
    int key = cv::waitKey(1);
    if(key > 0)
        keypress(static_cast<char>(key));
}
