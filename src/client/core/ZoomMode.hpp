//
//  ZoomMode.hpp
//  smart_specs_client
//
//  Created by Iain Wilson on 03/08/2019.
//

#ifndef ZoomMode_hpp
#define ZoomMode_hpp

#include <stdio.h>
#include "ProcessingModule.hpp"
#ifdef USE_CUDA
#include "VideoStablisation_GPU.hpp"
#else
#include "VideoStablisation.hpp"
#endif
#include "TrackedObject.hpp"


class ZoomMode : public ProcessingModule {
    
public:
    ZoomMode(const Display::Ptr display, const ImageBuffer::Ptr &input,const ImageBuffer::Ptr &display_buff,const SensorBuffer::Ptr &sensors);
    
    cv::Mat process(const Image::Ptr &frame, cv::Rect &roi);
    void bg_process(){};
    
    void linkParameters();
private:
    void printSensorData();
    
    
    TrackedObject::Ptr m_to;
    
#ifdef USE_CUDA
    VideoStablisation_GPU m_stab;
#else
    VideoStablisation m_stab;
#endif
    
};
#endif /* ZoomMode_hpp */
