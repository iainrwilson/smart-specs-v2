//
//  Processor.hpp
//  cpp_server
//
//  Created by Iain Wilson on 21/11/2018.
//

#ifndef Processor_hpp
#define Processor_hpp

#include <stdio.h>

#include "common/Thread.hpp"
#include "common/RingBuffer.hpp"
#include "face_detection/FaceDetector.hpp"
#include "common/Image.hpp"
#include <atomic>
#include "skin_segmentation/HistogramSkinSegmenter.hpp"
#include "skin_segmentation/SkinSegmenter.hpp"
#include "face_detection/FaceLearner.hpp"
#include "face_detection/IdentityTracker.hpp"
#include "DetectAndTrack.hpp"

//#include "face_detection/FaceRecognition.hpp"

#include <ThreadPool.h>

const std::string PASS{"PASS"};
const std::string FACEDETECT{"FACEDETECT"};
const std::string FACEDETECT_TEST{"FACEDETECT_TEST"};
const std::string FACETRACKER{"FACETRACKER"};
const std::string FACESEGMENT{"FACESEGMENT"};
const std::string TEXTDETECT{"TEXTDETECT"};


typedef RingBuffer<Image::Ptr> ImageBuffer;


class Processor : public Thread {

public:
    typedef std::shared_ptr<Processor> Ptr;
    
    Processor(ImageBuffer::Ptr &in_buffer,ImageBuffer::Ptr &out_buffer);
    static Ptr create(ImageBuffer::Ptr &in_buffer,ImageBuffer::Ptr &out_buffer){
        return std::make_shared<Processor>(in_buffer,out_buffer);
    }
    
    void thread_run();
    void setMode(const std::string &mode){m_mode = mode;}
    
    /*!
     \brief Initialise all the relevant processors, some take a little setup
     */
    void init();
     
    IdentityTracker::Ptr faceTracker() const {return m_identity_tracker;}
    FaceLearner::Ptr faceLearner() const {return m_face_learner;}
    
    DetectAndTrack::Ptr tracker() const {return m_detect_and_track;}
    
private:
    
    
    RingBuffer<Image::Ptr>::Ptr m_input_buffer;
    RingBuffer<Image::Ptr>::Ptr m_output_buffer;
    
    std::string m_mode;
    
    FaceDetector::Ptr m_face_detector;
    FaceLearner::Ptr m_face_learner;
    IdentityTracker::Ptr m_identity_tracker;
    DetectAndTrack::Ptr m_detect_and_track;
    
};

#endif /* Processor_hpp */
