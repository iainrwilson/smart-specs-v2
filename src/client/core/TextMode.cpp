//
//  TextSegmentation.cpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#include "TextMode.hpp"

TextMode::TextMode(const Display::Ptr display, const ImageBuffer::Ptr &input,const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors):ProcessingModule("TEXT_MODE",display,input,display_buff,sensors){
    
    
    auto den = m_parameters.add<int>("diameter",6);
    den->setMin(1);
    den->setMax(100);
    den->setIncrement(1);
    
    auto sigma = m_parameters.add<int>("sigma",150);
    sigma->setMin(0);
    sigma->setMax(1000);
    sigma->setIncrement(10);
    
    m_parameters.add<bool>("yellow",false);
    m_parameters.add<bool>("invert",false);

    
    auto conf = m_parameters.add<float>("conf-threshold",0.99f);
    conf->setMin(0.0f);
    conf->setMax(1.0f);
    conf->setIncrement(0.1f);
    
    auto nms = m_parameters.add<float>("nms-threshold",0.4f);
    nms->setMin(0.0f);
    nms->setMax(1.0f);
    nms->setIncrement(0.1f);
    
    
    auto size = m_parameters.add<int>("east-size",256); //320
    size->setMin(32);
    size->setMax(1024);
    size->setIncrement(32);
    
    auto bsize = m_parameters.add<int>("block-size",49);
    bsize->setMin(3);
    bsize->setMax(111);
    bsize->setIncrement(2);
    
    
    //load the EAST text deteion model.
    
    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/models/";
    
    m_net.setPreferableTarget(cv::dnn::DNN_TARGET_OPENCL);
    m_net = cv::dnn::readNet(dataDir+"frozen_east_text_detection.pb");
    
    #ifdef USE_CUDA
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation_GPU::reset,m_stab));
    #else
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation::reset,m_stab));
    #endif
    
    
}

void TextMode::linkParameters(){
    
    m_display->link(m_parameters["diameter"],'[',']');
    m_display->link(m_parameters["sigma"],'9','0');
    m_display->link(m_parameters["yellow"],'y');
    m_display->link(m_parameters["invert"],'i');
    
    m_display->link(m_parameters["conf-threshold"], 'q', 'a');
    m_display->link(m_parameters["nms-threshold"], 'w', 's');
    m_display->link(m_parameters["block-size"],'e','d');
    m_display->link(m_parameters["east-size"],'t','g');

    m_display->link(m_parameters["enable-stable"],'6');
}

cv::Mat TextMode::process(const Image::Ptr &frame, cv::Rect &roi){
    //cv::Mat gray = frame->gray();
    cv::Mat image = frame->get();
    cv::Mat out;
    
    if(m_parameters.value<bool>("enable-stable"))
        image = m_stab.run(image).clone();
   
    cv::Mat gray;
    cv::cvtColor(image,gray, cv::COLOR_BGR2GRAY);

    /*
     cv::cuda::GpuMat gpu_gray(gray);
     cv::cuda::GpuMat gpu_out;
     //very slow --- there is a cuda version.
     cv::cuda::fastNlMeansDenoising(gpu_gray, gpu_out,m_parameters.value<int>("denoise-h"));
     gpu_out.download(out);
     return out;
     */
    //cv::Rect _roi = m_display->getRoi();
//    cv::Size size = image.size();
//
//    float zoom = m_display->zoom();
//    int _w = size.width / zoom;
//    int _h = size.height / zoom;
//    int _x = (size.width - _w) /2;
//    int _y = (size.height - _h) /2;
////
//     cv::Rect _roi(_x,_y,_w,_h);
////   // std::cout << "ROI:: "<< _roi << "Image ::: " << image.size() << std::endl;
//    cv::Mat crop = image(_roi);
//    detectText(crop);
//
//    //draw roi for debug
//   // cv::rectangle(image, _roi.tl(), _roi.br(), cv::Scalar(0,0,255));
//
//    return image;

   
    cv::bilateralFilter(gray, out, m_parameters.value<int>("diameter"), m_parameters.value<int>("sigma"), m_parameters.value<int>("sigma"));
    
    //    cv::GaussianBlur(out, out, cv::Size(5,5), m_parameters.value<double>("cartoon-blur"));
    
    //cv::equalizeHist(out,out);
    cv::adaptiveThreshold(out, out, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY, m_parameters.value<int>("block-size"), 5.0);
    
    if(m_parameters.value<bool>("invert"))
        cv::bitwise_not(out, out);
    
    cv::Mat colour_out;
    cv::cvtColor(out, colour_out, cv::COLOR_GRAY2BGR);
    
    //apply yellow
    if(m_parameters.value<bool>("yellow")){
        colour_out.setTo(cv::Scalar(0,255,255),out);
    }

    
    return colour_out;
}







std::vector<cv::RotatedRect> TextMode::detectText(cv::Mat &image){

   SmartSpecs::Timer::begin("[ TextMode::detectText ]" );
  
    //testing out some detection...
    std::vector<cv::Mat> outs;
    std::vector<cv::String> outNames(2);
    outNames[0] = "feature_fusion/Conv_7/Sigmoid";
    outNames[1] = "feature_fusion/concat_3";
    
    int inpWidth = m_parameters.value<int>("east-size");
    int inpHeight = inpWidth;
    float confThreshold = m_parameters.value<float>("conf-threshold");
    float nmsThreshold = m_parameters.value<float>("nms-threshold");
    
    cv::Mat blob;
    cv::dnn::blobFromImage(image, blob, 1.0, cv::Size(inpWidth, inpHeight), cv::Scalar(123.68, 116.78, 103.94), true, false);
    m_net.setInput(blob);
    m_net.forward(outs, outNames);
    
    cv::Mat scores = outs[0];
    cv::Mat geometry = outs[1];
    
    // Decode predicted bounding boxes.
    std::vector<cv::RotatedRect> boxes;
    std::vector<float> confidences;
    decode(scores, geometry, confThreshold, boxes, confidences);
    
    // Apply non-maximum suppression procedure.
    std::vector<int> indices;
    cv::dnn::NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
    
    
    std::vector<cv::RotatedRect> text_boxes;
    std::vector<float> distances; //distance from the center of the image
    
   
    // Render detections.
    cv::Point2f ratio((float)image.cols / inpWidth, (float)image.rows / inpHeight);
    for (size_t i = 0; i < indices.size(); ++i){
        cv::RotatedRect& box = boxes[indices[i]];
        
        text_boxes.push_back(boxes[indices[i]]);
        cv::Point2f vertices[4];
        box.points(vertices);
        for (int j = 0; j < 4; ++j)
        {
            vertices[j].x *= ratio.x;
            vertices[j].y *= ratio.y;
        }
        for (int j = 0; j < 4; ++j)
            cv::line(image, vertices[j], vertices[(j + 1) % 4], cv::Scalar(0, 255, 0), 1);
        
        //draw centre point
        cv::Point2f c = box.center;
        c.x *= ratio.x;
        c.y *= ratio.y;
        cv::circle(image,c,5,cv::Scalar(0,255,0),-1);
        //std::cout << box.center  <<std::endl;
        
    }
    
    cv::Point2f c(image.cols/2,image.rows/2);
    
    double _last_dist = -1.;
    int _last_dist_id = -1;
    for(int i=0;i<text_boxes.size();++i){
        cv::Point2f _c = text_boxes[i].center;
        _c.x *= ratio.x;
        _c.y *= ratio.y;
        double _dist = cv::norm(c-_c);
        if(_last_dist<0){
            _last_dist=_dist;
            _last_dist_id = i;
        }else{
            if (_dist < _last_dist) {
                _last_dist = _dist;
                _last_dist_id = i;
            }
        }
    }
    
//    std::cout << "Found " <<text_boxes.size() << " boxes " <<std::endl;
    
    //draw around the most central box.
    if(_last_dist_id >-1){
        cv::RotatedRect& box = boxes[indices[_last_dist_id]];
        cv::Point2f vertices[4];
        box.points(vertices);
        for (int j = 0; j < 4; ++j)
        {
            vertices[j].x *= ratio.x;
            vertices[j].y *= ratio.y;
        }
        for (int j = 0; j < 4; ++j)
            cv::line(image, vertices[j], vertices[(j + 1) % 4], cv::Scalar(0, 0, 255), 2);
        
    }
    
    
    
    // Put efficiency information.
//    std::vector<double> layersTimes;
//    double freq = cv::getTickFrequency() / 1000;
//    double t = m_net.getPerfProfile(layersTimes) / freq;
//    std::string label = cv::format("Inference time: %.2f ms", t);
//    putText(image, label, cv::Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(0, 255, 0),2);


  
    
    //get center of all the boxes, and their disatacen to the cetntre of thre image.
    
    
    SmartSpecs::Timer::end("[ TextMode::detectText ]" );

    
    return text_boxes;
    
}



void TextMode::bg_process(){
    
//    auto frame = m_input_buffer->pop();
//    cv::Mat image = frame->get();
//
//    //if we are zoomed, only process the cropped image;
//
//    //cv::Rect roi = m_display->getRoi();
//
//    detectText(image);
//
//
    
}

void TextMode::decode(const cv::Mat& scores, const cv::Mat& geometry, float scoreThresh, std::vector<cv::RotatedRect>& detections, std::vector<float>& confidences)
{
    detections.clear();
    CV_Assert(scores.dims == 4); CV_Assert(geometry.dims == 4); CV_Assert(scores.size[0] == 1);
    CV_Assert(geometry.size[0] == 1); CV_Assert(scores.size[1] == 1); CV_Assert(geometry.size[1] == 5);
    CV_Assert(scores.size[2] == geometry.size[2]); CV_Assert(scores.size[3] == geometry.size[3]);
    
    const int height = scores.size[2];
    const int width = scores.size[3];
    for (int y = 0; y < height; ++y)
    {
        const float* scoresData = scores.ptr<float>(0, 0, y);
        const float* x0_data = geometry.ptr<float>(0, 0, y);
        const float* x1_data = geometry.ptr<float>(0, 1, y);
        const float* x2_data = geometry.ptr<float>(0, 2, y);
        const float* x3_data = geometry.ptr<float>(0, 3, y);
        const float* anglesData = geometry.ptr<float>(0, 4, y);
        for (int x = 0; x < width; ++x)
        {
            float score = scoresData[x];
            if (score < scoreThresh)
                continue;
            
            // Decode a prediction.
            // Multiple by 4 because feature maps are 4 time less than input image.
            float offsetX = x * 4.0f, offsetY = y * 4.0f;
            float angle = anglesData[x];
            float cosA = std::cos(angle);
            float sinA = std::sin(angle);
            float h = x0_data[x] + x2_data[x];
            float w = x1_data[x] + x3_data[x];
            
            cv::Point2f offset(offsetX + cosA * x1_data[x] + sinA * x2_data[x],
                           offsetY - sinA * x1_data[x] + cosA * x2_data[x]);
            cv::Point2f p1 = cv::Point2f(-sinA * h, -cosA * h) + offset;
            cv::Point2f p3 = cv::Point2f(-cosA * w, sinA * w) + offset;
            cv::RotatedRect r(0.5f * (p1 + p3), cv::Size2f(w, h), -angle * 180.0f / (float)CV_PI);
            detections.push_back(r);
            confidences.push_back(score);
        }
    }
}
