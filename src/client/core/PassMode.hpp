//
//  PassMode.hpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#ifndef PassMode_hpp
#define PassMode_hpp

#include <stdio.h>
#include "ProcessingModule.hpp"

#ifdef USE_CUDA
#include "VideoStablisation_GPU.hpp"
#else
#include "VideoStablisation.hpp"
#endif

class PassMode : public ProcessingModule {
    
public:
    PassMode(const Display::Ptr display, const ImageBuffer::Ptr &input,const ImageBuffer::Ptr &display_buff,const SensorBuffer::Ptr &sensors);
    
    cv::Mat process(const Image::Ptr &frame, cv::Rect &roi);    
    void bg_process(){};
    
    void linkParameters();
private:
#ifdef USE_CUDA
    VideoStablisation_GPU m_stab;
#else
    VideoStablisation m_stab;
#endif    

};

#endif /* PassMode_hpp */
