//
//  OpenCVCamera.cpp
//  cpp_server
//
//  Created by Iain Wilson on 21/03/2018.
//

#include "OpenCVCamera.hpp"

OpenCVCamera::OpenCVCamera(){
    m_name = "opencv";
    
}

bool OpenCVCamera::connect(){
    
    
#ifdef __APPLE__
    m_cam.open(0);
#else
    m_cam.open("/dev/video6");
#endif
    
    //test setting res
    
    // see3cam_139
    //supported
    /*
     640x480   @120
     1280x720  @60
     1920x1080 @60
     3840x2160 @30
     4096x2160 @30
     4224x3156 @20
  

     BT-35E cam
     640x480 @60
     128x720 @60
     1920x720 @30
     2592x1944 @15
     

    */



  
  if(!m_cam.set(cv::CAP_PROP_FRAME_WIDTH,1920))
       std::cout << "Cannot set WIDTH " << std::endl;
    if(!m_cam.set(cv::CAP_PROP_FRAME_HEIGHT,720))
        std::cout << "Cannot set HEIGHT " << std::endl;
    if(!m_cam.set(cv::CAP_PROP_FPS,30))
        std::cout << "Cannot set FPS " << std::endl;
      
    
    
    getInfo();
    
    if(m_cam.isOpened())
        return true;
    
    return false;
}

void OpenCVCamera::getInfo(){
    
    double fps = m_cam.get(cv::CAP_PROP_FPS);
    double width = m_cam.get(cv::CAP_PROP_FRAME_WIDTH );
    double height = m_cam.get(cv::CAP_PROP_FRAME_HEIGHT);

    std::cout << "FPS: " <<fps << " W: " << width << " H: " << height << std::endl;
    
}


void OpenCVCamera::disconnect(){
    m_cam.release();
}

cv::Mat OpenCVCamera::grab(){
    cv::Mat frame;
    m_cam >> frame;
    return frame;
}
