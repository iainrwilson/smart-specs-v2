//
//  ProcessingModule.cpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//
//   Base implementaion of a processing module.
//   This class encapsulates all elements of a mode (face segment etc)
//
//  contains both backgournd and foreground threads.
//
//
//


#include "FaceMode.hpp"
#include "TextMode.hpp"
#include "PassMode.hpp"
#include "ZoomMode.hpp"

Processors::Ptr Processors::create(const std::string &type, const Display::Ptr &display, const ImageBuffer::Ptr &input, const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors){
    
    if(type == PROCESSOR_FACE)
        return ProcessingModule::Ptr(new FaceMode(display,input,display_buff,sensors));
    
    if(type == PROCESSOR_TEXT)
        return ProcessingModule::Ptr(new TextMode(display,input,display_buff,sensors));
    
    if(type == PROCESSOR_ZOOM)
        return ProcessingModule::Ptr(new ZoomMode(display,input,display_buff,sensors));
    
    return ProcessingModule::Ptr(new PassMode(display,input,display_buff,sensors));
}
