//
//  Capture.hpp
//  cpp_server
//
//  Created by Iain Wilson on 21/11/2018.
//

#ifndef Capture_hpp
#define Capture_hpp

#include <stdio.h>
#include "common/Thread.hpp"
#include "cameras/camera.hpp"
#include "common/RingBuffer.hpp"
#include "opencv2/opencv.hpp"
#include <chrono>
#include "common/Image.hpp"
#include <memory>

typedef RingBuffer<Image::Ptr> ImageBuffer;

class Capture : public Thread {
    
public:
    typedef std::shared_ptr<Capture> Ptr;
    
    Capture(ImageBuffer::Ptr &buffer, CameraBase::Ptr &camera);
    Capture(ImageBuffer::Ptr &buffer, ImageBuffer::Ptr &buffer_2, CameraBase::Ptr &camera);
    Capture(ImageBuffer::Ptr &buffer, const std::string &type=CAMERA_TYPE);
    Capture(ImageBuffer::Ptr &buffer, ImageBuffer::Ptr &buffer_2, const std::string &type=CAMERA_TYPE);
    
    static Ptr create(ImageBuffer::Ptr &buffer, const std::string &type=CAMERA_TYPE){
        return std::make_shared<Capture>(buffer,type);
    }
    static Ptr create(ImageBuffer::Ptr &buffer, ImageBuffer::Ptr &buffer_2, const std::string &type=CAMERA_TYPE){
        return std::make_shared<Capture>(buffer,buffer_2,type);
    }
    
    /*!
     \brief run the capture thread ; pre-processing steps, build image pyramid.
     */
    void thread_run();
    bool connect();
    void disconnect();
    void setOffset(const int offset){m_v_offset = offset;}
    
    Camera::Ptr camera(){return m_camera;}
private:
    std::chrono::high_resolution_clock::time_point m_start_time;

    CameraBase::Ptr m_camera;
    RingBuffer<Image::Ptr>::Ptr m_buffer;
    RingBuffer<Image::Ptr>::Ptr m_buffer_2;
    std::atomic<int> m_v_offset;
    
};


#endif /* Capture_hpp */
