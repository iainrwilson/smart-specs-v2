//
//  Processor.cpp
//  cpp_server
//
//  Created by Iain Wilson on 21/11/2018.
//

#include "Processor.hpp"
#include "image_processing/cartoon.hpp"
#include "core/variables.hpp"


Processor::Processor(RingBuffer<Image::Ptr>::Ptr &in_buffer,RingBuffer<Image::Ptr>::Ptr &out_buffer):Thread("Processor"),m_input_buffer(in_buffer),m_output_buffer(out_buffer),m_mode(FACEDETECT){
    init();
}

void Processor::init(){
    
    m_face_detector = FaceDetector::create(FD_OPENCV_DNN);
    m_face_learner = FaceLearner::create();
    m_face_learner->start();
    m_identity_tracker = IdentityTracker::create();
    m_detect_and_track = DetectAndTrack::create();
    
}
void Processor::thread_run(){
    
    
    Image::Ptr img = m_input_buffer->pop();
    if(img->isEmpty())
        return;
    
    
    
    
    if(m_mode == PASS){
        //do nothing.
    }else if (m_mode == FACEDETECT){
        
        int s = 1 << FACEDETECT_PYR_LEVEL; // chose the pyramid level
        
        m_detect_and_track->run(img);
        auto rois = m_detect_and_track->rois();
        for(auto r : rois){
            cv::Rect _f(r.x*s,r.y*s,r.width*s,r.height*s);
            Face::Ptr face = Face::create(_f,img);
            auto features = m_face_detector->detectLandmarks(img->get(),face);
            face->setFeatures(features);
            face->createFaceMask(img->get());
            face->create2DHistogram();
            m_identity_tracker->add(face);
        }
        
    }else if (m_mode == FACETRACKER){
        
        /*
          Just performs face detection and tracking.
         */

        //pre-processing - build a skin map (low pyramid level)
        cv::Mat image = img->get();
        cv::Mat skin;
        bool _has_skin = false;
        if(faceLearner()->isReady()){
            skin = faceLearner()->detectSkin(image);
             m_detect_and_track->addValidation(skin.clone());
            cv::cvtColor(skin, skin, cv::COLOR_GRAY2BGR);
            //add the skin to the detector - to aid with validataion
            _has_skin = true;
        }

        
        //find some faces
        m_detect_and_track->run(img);
        
        auto objects = m_detect_and_track->objects();

        for(auto &obj:objects){
            Face::Ptr face = Face::create(obj->roi(),img);
        
            if(!face->isValid()) //more work here...
                return;
            
            auto features = m_face_detector->detectLandmarks(img->get(),face);
            face->setFeatures(features);
            face->createFaceMask(img->get());
            face->create2DHistogram();
            //add only valid faces
//            m_identity_tracker->add(face);
            
            //if the object was tracked (not detected) do not add to the learner.
            if(!obj->isTracked()){
                face->setTracked(false);
                m_face_learner->add(face);
            }
        }
    }
    m_output_buffer->push(img);
 
}


