//
//  PassMode.cpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#include "PassMode.hpp"


PassMode::PassMode(const Display::Ptr display, const ImageBuffer::Ptr &input,const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors):
ProcessingModule("PASS_MODE",display,input,display_buff,sensors){

    #ifdef USE_CUDA
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation_GPU::reset,m_stab));
    #else
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation::reset,m_stab));
    #endif
}

void PassMode::linkParameters(){

    m_display->link(m_parameters["enable-stable"],'6');

}


cv::Mat PassMode::process(const Image::Ptr &frame, cv::Rect &roi){


        cv::Mat image = frame->get();
       
        if(m_parameters.value<bool>("enable-stable"))
           return m_stab.run(image);
        
        return image;
}
