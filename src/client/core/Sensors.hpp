//
//  Sensors.hpp
//  smart_specs_client
//
//  Created by Iain Wilson on 02/08/2019.
//

#ifndef Sensors_hpp
#define Sensors_hpp

#include <stdio.h>
#include "Thread.hpp"
#include "RingBuffer.hpp"
#include "imu/fsm6.hpp"
#include <Eigen/Eigen>
#include <memory>

typedef RingBuffer<HeadPose::Ptr> SensorBuffer;

class Sensors : public Thread {
    
public:
    typedef std::shared_ptr<Sensors> Ptr;
    
    Sensors(SensorBuffer::Ptr &buffer);
    static Ptr create(SensorBuffer::Ptr &buffer){
        return std::make_shared<Sensors>(buffer);
    }
    
    void thread_run();
    bool connect();
    void disconnect();
    
private:
    Fsm6::Ptr m_fsm6;
    SensorBuffer::Ptr m_buffer;
};

#endif /* Sensors_hpp */
