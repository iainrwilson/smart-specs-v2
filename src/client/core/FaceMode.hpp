//
//  FaceMode.hpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#ifndef FaceMode_hpp
#define FaceMode_hpp

#include <stdio.h>


#include "ProcessingModule.hpp"
#include "DetectAndTrack.hpp"
#include "face_detection/FaceLearner.hpp"
#include "image_processing/cartoon.hpp"


class FaceMode : public ProcessingModule {
    
public:
    FaceMode(const Display::Ptr display, const ImageBuffer::Ptr &input, const ImageBuffer::Ptr &display_buff,const SensorBuffer::Ptr &sensors);
        
    cv::Mat process(const Image::Ptr &frame, cv::Rect &roi);
    
    void bg_process();
    
    void linkParameters();
    
private:
    cv::Rect cropRoi(const cv::Rect &roi,const int w,const int h);
   
    
    FaceLearner::Ptr m_face_learner;
    DetectAndTrack::Ptr m_detect_and_track;
    
    int m_blend_count;
    
};


#endif /* FaceMode_hpp */
