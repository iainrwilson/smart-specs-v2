//
//  VideoStablisation_GPU.hpp
//  dlib
//
//  Created by Iain Wilson on 13/06/2019.
//
#include "cmakedefines.h"
#ifdef USE_CUDA

#ifndef VideoStablisation_GPU_hpp
#define VideoStablisation_GPU_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "timer.hpp"
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/highgui.hpp"
#include "opencv2/cudaoptflow.hpp"
#include "opencv2/cudaarithm.hpp"


// This video stablisation smooths the global trajectory using a sliding average window

//const int SMOOTHING_RADIUS = 15; // In frames. The larger the more stable the video, but less reactive to sudden panning
const int HORIZONTAL_BORDER_CROP = 20; // In pixels. Crops the border to reduce the black borders from stabilisation being too noticeable.

struct TransformParam
{
    TransformParam() {}
    TransformParam(double _dx, double _dy, double _da) {
        dx = _dx;
        dy = _dy;
        da = _da;
    }
    
    double dx;
    double dy;
    double da; // angle
};

struct Trajectory
{
    Trajectory() {}
    Trajectory(double _x, double _y, double _a) {
        x = _x;
        y = _y;
        a = _a;
    }
    // "+"
    friend Trajectory operator+(const Trajectory &c1,const Trajectory  &c2){
        return Trajectory(c1.x+c2.x,c1.y+c2.y,c1.a+c2.a);
    }
    //"-"
    friend Trajectory operator-(const Trajectory &c1,const Trajectory  &c2){
        return Trajectory(c1.x-c2.x,c1.y-c2.y,c1.a-c2.a);
    }
    //"*"
    friend Trajectory operator*(const Trajectory &c1,const Trajectory  &c2){
        return Trajectory(c1.x*c2.x,c1.y*c2.y,c1.a*c2.a);
    }
    //"/"
    friend Trajectory operator/(const Trajectory &c1,const Trajectory  &c2){
        return Trajectory(c1.x/c2.x,c1.y/c2.y,c1.a/c2.a);
    }
    //"="
    Trajectory operator =(const Trajectory &rx){
        x = rx.x;
        y = rx.y;
        a = rx.a;
        return Trajectory(x,y,a);
    }
    
    double x;
    double y;
    double a; // angle
};


class VideoStablisation_GPU {
    
public:
    VideoStablisation_GPU();
    
    cv::Mat run(const cv::Mat &image,const cv::Mat &alt=cv::Mat());
    void reset(){m_first_run=true;}


private:
    
    static void download(const cv::cuda::GpuMat& d_mat, std::vector<cv::Point2f>& vec)
    {
        vec.resize(d_mat.cols);
        cv::Mat mat(1, d_mat.cols, CV_32FC2, (void*)&vec[0]);
        d_mat.download(mat);
    }

    static void download(const cv::cuda::GpuMat& d_mat, std::vector<uchar>& vec)
    {
        vec.resize(d_mat.cols);
        cv::Mat mat(1, d_mat.cols, CV_8UC1, (void*)&vec[0]);
        d_mat.download(mat);
    }

    bool m_first_run;
    
    std::vector<TransformParam> prev_to_cur_transform; // previous to current
    // Accumulated frame to frame transform
    double a = 0;
    double x = 0;
    double y = 0;
    
    std::vector<Trajectory> trajectory; // trajectory at all frames
    std::vector<Trajectory> smoothed_trajectory; // trajectory at all frames
    Trajectory X;//posteriori state estimate
    Trajectory X_;//priori estimate
    Trajectory P;// posteriori estimate error covariance
    Trajectory P_;// priori estimate error covariance
    Trajectory K;//gain
    Trajectory z;//actual measurement
    
    double pstd = 4e-3;//can be changed
    double cstd = 0.25;//can be changed
    Trajectory Q;// process noise covariance
    Trajectory R;// measurement noise covariance
    
    std::vector<TransformParam> new_prev_to_cur_transform;
    cv::Mat T;
    int vert_border; // get the aspect ratio correct
    
    int k;
    cv::Mat prev;
    cv::Mat last_T;
    cv::cuda::GpuMat cur_grey;
    cv::cuda::GpuMat prev_grey;
    cv::Mat prev_grey_;
    cv::Mat cur_grey_;
    
    
};
#endif /* VideoStablisation_GPU_hpp */
#endif /* USE_CUDA */ 
