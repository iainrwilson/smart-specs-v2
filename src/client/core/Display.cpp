//
//  Display.cpp
//  cpp_server
//
//  Created by Iain Wilson on 20/11/2018.
//

#include "Display.hpp"
#include <vector>
#include <iostream>
#define GL_SILENCE_DEPRECATION

#include "DisplayOpenCV.hpp"
#include "DisplayOpenGL.hpp"
#include "DisplayNull.hpp"




Display::Ptr Display::create(const std::string &type, const bool fullscreen){
    
    if(type == DISPLAY_OPENCV)
        return Display::Ptr(new DisplayOpenCV(fullscreen));
    
#ifdef USE_OPENGL
    if(type == DISPLAY_OPENGL)
        return Display::Ptr(new DisplayOpenGL(fullscreen));
#endif// USE_OPENGL
    
    if(type == DISPLAY_NULL)
        return Display::Ptr(new DisplayNull);
    
    return Display::Ptr(new DisplayNull);
}

void DisplayBase::overlay(cv::Mat& image){
    
//    drawFooter(image);
    
    if(m_last_param_change.get() == nullptr)
        return;
    
    if(m_overlay_count>= (m_overlay_duration))
        return;
    
    
    std::stringstream ss;
    ss << "[" <<m_last_param_change->name() << "] " << m_last_param_change->toString();
    
    //print to terminal
//    std::cout << ss.str() << std::endl;
    
    //max overlay time ~ 100
    float alpha = 1.0;
    alpha = 1.0 - (static_cast<float>(m_overlay_count)/static_cast<float>(m_overlay_duration));
    
    cv::Scalar colour(0,255,0);
    int thickness = 2;
    double fontScale = 2.0;
    int fontFace =cv::FONT_HERSHEY_PLAIN;
    int baseline;
    cv::Size textSize = cv::getTextSize(ss.str(),fontFace,fontScale,thickness, &baseline);
    
    
    cv::Point textOrg(0,0);
    int padd = 12;
    
    
    int _w = textSize.width+padd;
    int _h = textSize.height+padd;
    //text can be too big for the image! Especialy when in zoomed in mode...just skip if that is the case.
    if(_w > image.cols || _h > image.rows)
        return;
    
    cv::Rect roi_rect(0,0,_w,_h);
    cv::Mat roi_image(image(roi_rect));
    cv::Mat overlay(roi_rect.size(),CV_8UC3);
    cv::rectangle(overlay, textOrg,cv::Point(textSize.width+padd,(textSize.height+padd)), colour,-1);
    cv::putText(overlay, ss.str(), textOrg+cv::Point(0,textSize.height+padd/2), fontFace, fontScale, cv::Scalar(255,255,255),thickness,cv::LINE_AA);
    
    cv::addWeighted(overlay, alpha, roi_image, 1.0, 0, roi_image);
    
    m_overlay_count++;
}

void DisplayBase::drawFooter(cv::Mat& image){
    
    auto now = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_time);
    m_time=now;
    
    
    //set height to be ~100pxs
    const int _h = 50;
    cv::Rect roi_rect(0,image.rows-_h,image.cols,_h);
    
    cv::Mat roi_image(image(roi_rect));
    cv::Mat footer(roi_rect.size(),CV_8UC3,cv::Scalar(0,255,0));
    
    //draw some text:
    std::stringstream ss;
    ss << "FPS: " << duration.count();
    int thickness = 2;
    double fontScale = 2.0;
    int fontFace =cv::FONT_HERSHEY_PLAIN;
    cv::Point textOrg(10,30);
    
    std::cout << ss.str() << std::endl;
    
    cv::putText(footer, ss.str(), textOrg, fontFace, fontScale, cv::Scalar(0,0,0),thickness,cv::LINE_AA);
    cv::addWeighted(footer, 0.75, roi_image, 1.0, 0, roi_image);
    
}
