//
//  Engine.cpp
//  cpp_server
//
//  Created by Iain Wilson on 08/01/2019.
//

#include "Engine.hpp"
#include "image_processing/cartoon.hpp"
#include <opencv2/core.hpp>
#include <opencv2/ximgproc.hpp>
#include "image_processing/grid_seams.hpp"
#include "Face.hpp"

#include "FaceMode.hpp"
#include "TextMode.hpp"
#include "ZoomMode.hpp"

const std::string DISPLAY_TYPE{DISPLAY_OPENGL};

Engine::Engine() : Thread("Engine"),m_display_type(DISPLAY_TYPE){
   
    auto zoom = m_parameters.add<float>("zoom",1.0);
    zoom->setMin(0.5);
    zoom->setMax(10.0);
    zoom->setIncrement(0.1);
    
    //setup parameters:
    auto mode = m_parameters.add<int>("camera-mode",1,std::bind(&Engine::setCameraMode,this,std::placeholders::_1));
    mode->setIncrement(1);
    mode->setMin(0);
    mode->setMax(5);//6 modes in total.
    
    
    //set this as an enumerator, set a callback function.
    auto p = m_parameters.add<std::string>("mode",MODE_PASS,std::bind(&Engine::setMode,this,std::placeholders::_1));
    *(p) << MODE_FACE;
    *(p) << MODE_TEXT;
    //*(p) << MODE_ZOOM;

    
    //add parameter call back to the capture thread - for changing zoom offset (when using HMD)
    auto c = m_parameters.add<int>("camera-vertical-offset",400,std::bind(&Engine::setCameraOffset,this,std::placeholders::_1));
    c->setIncrement(100);
    c->setMin(-1000);
    c->setMax(1000);
    
    init();
}

void Engine::init(){
    
    // Create all the required buffers
    m_capture_buffer = ImageBuffer::create();
    m_processing_buffer = ImageBuffer::create();
    m_display_buffer = ImageBuffer::create();
    m_sensor_buffer = SensorBuffer::create();
    
    
    
    
    /**************** DISPLAY OUTPUT ******************/
    m_display = Display::create(m_display_type,false);
    m_display->init();
    
    //register parameters with the display
    m_display->link(m_parameters["mode"], '1','2'); //+,-
    m_display->link(m_parameters["camera-mode"],',','.');
    //m_display->link(m_parameters["zoom"],'=','-');
    m_display->link(m_parameters["camera-vertical-offset"],'-','=');
    
    
    
    
    /*********** CAMERA CONNECT ****************/
    //setup capture - grabs an image and pushes it onto two output buffers.
    m_capture = Capture::create(m_capture_buffer,m_display_buffer);
    
    
    
    //connect to camera and start threads
    if(!m_capture->connect())
        LOGE("[ Engine::init ] cannot connect to camera" );
    
    m_capture->start();

    
    
    /*********** SENSORS CONNECT ****************/
    m_sensors = Sensors::create(m_sensor_buffer);
    if(m_sensors->connect()){
        m_sensors->start();
        LOGI("[ Engine::init ] connected so Sensors");
    }else{
        LOGE("[ Engine::init ] cannot connect to Sensors" );
    }
        
    
    
    
    /*********** PROCESSING MODULES  ****************/

    //initialise processing modules
    
    m_mode_face =  Processors::create(PROCESSOR_FACE,m_display,m_capture_buffer,m_display_buffer, m_sensor_buffer);
    m_mode_text =  Processors::create(PROCESSOR_TEXT,m_display,m_capture_buffer,m_display_buffer, m_sensor_buffer);
    m_mode_zoom =  Processors::create(PROCESSOR_ZOOM,m_display,m_capture_buffer,m_display_buffer, m_sensor_buffer);
    m_mode_pass =  Processors::create(PROCESSOR_PASS,m_display,m_capture_buffer,m_display_buffer, m_sensor_buffer);

    m_current_mode = m_mode_pass;
    
    //set the mode.
    setMode(MODE_PASS);
    
    //start all processing modules
    m_mode_face->start();
    m_mode_face->suspend();
    
    m_mode_text->start();
    m_mode_text->suspend();
    
    
    
    /***********8 NETWORKING ****************/
    //setup the networking
    std::string ip_address = REMOTE_SERVER_IP;
    //output buffer for the networking
    m_network_buffer = ImageBuffer::create();
    m_results_buffer = RingBuffer<SmartSpecs::Response::Ptr>::create();
    //defaults to port 8081
    m_offloader = SmartSpecs::Offloader::create(m_network_buffer,m_results_buffer,ip_address);
    
    //do test ping.
    if(m_offloader->ping())
        std::cout << " Ping Success " << std::endl;
    else{
        std::cout << "no response from server: " << ip_address << std::endl;
        return;
    }
    //set the mode to just save
    m_offloader->send_request(SmartSpecs::Request::SAVE);
    
    
    /* Lambda to temporarily handle commands */
    std::function<std::string (const std::string&)> commandP = [](const std::string& s){
        LOGI("QUERYP :: %s",s.c_str());
        if(s == "PING")
            return "PING";
        return "OKAY";
    };
    
    /* Lambda to temporarily handle frames */
    std::function<std::string (const Image::Ptr&)> frameP = [](const Image::Ptr& f){
        //LOGI("QUERYP :: %s",s.c_str());

        std::cout << f->size() << " T: "<<f->age() << std::endl;
        std::stringstream ss;
        ss << "image_" << f->age() <<".png";
        cv::imwrite(ss.str(),f->get());
        return "OKAY";
    };
    
    /* Lambda to temporarily handle responses */
    std::function<std::string (const SmartSpecs::Response::Ptr&)> respP = [](const SmartSpecs::Response::Ptr& r){
        
        LOGI("Response :: type %s, len: %d Age: %dms",r->type().c_str(),r->rois().size(),r->age());
        
        for(auto const &roi:r->rois()){
            std::cout << "ROI: " << roi << std::endl;
        }
        
        return "OKAY";
    };
    
    
    //start the recieving server 8082
    auto qp = SmartSpecs::QueryProcessor::create(commandP,frameP,respP);
    m_net_server = SmartSpecs::Networking::TCPServerAsync::create("8082", qp);
    m_net_server->start();
    
}

void Engine::start(){
    m_capture->start();
    
    //start all processing modules
    m_mode_face->start();
    m_mode_face->suspend();
    
    m_mode_text->start();
    m_mode_text->suspend();
    
    //dont start the passMode, as the bg thread does nothing.
    
    Thread::start();
}

void Engine::stop(){
    Thread::stop();
    
    m_mode_face->stop();
    m_mode_text->stop();
    
    m_capture->stop();
}


void Engine::setMode(const std::string &mode){
    //set the engine mode, also changes the processor accordingly
    
    m_mode = mode;
    
    //suspend the current mode;
    m_current_mode->suspend();
    
    if(m_mode == MODE_FACE){
        m_current_mode = m_mode_face;
    }else if(m_mode == MODE_TEXT){
        m_current_mode = m_mode_text;
    }else if(m_mode == MODE_ZOOM){
        m_current_mode = m_mode_zoom;
    }
    else{
        m_current_mode = m_mode_pass;
    }
    
    //relink the parameters as we sometimes have an overalp
    m_current_mode->linkParameters();
    
    m_current_mode->resume();
    
    
}


void Engine::thread_run(){
    
    /// the main loop    
    m_current_mode->ui_thread();
    
    
}

