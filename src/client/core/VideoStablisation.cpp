//
//  VideoStablisation.cpp
//
//
//  Created by Iain Wilson on 13/06/2019.
//

#include "VideoStablisation.hpp"

VideoStablisation::VideoStablisation():m_first_run(true){
    
    //    // Step 1 - Get previous to current frame transformation (dx, dy, da) for all frames
    //    std::vector<TransformParam> prev_to_cur_transform; // previous to current
    //    // Accumulated frame to frame transform
    //    double a = 0;
    //    double x = 0;
    //    double y = 0;
    //
    //    // Step 2 - Accumulate the transformations to get the image trajectory
    //    std::vector<Trajectory> trajectory; // trajectory at all frames
    //
    //
    //    // Step 3 - Smooth out the trajectory using an averaging window
    //    std::vector<Trajectory> smoothed_trajectory; // trajectory at all frames
    //    Trajectory X;//posteriori state estimate
    //    Trajectory    X_;//priori estimate
    //    Trajectory P;// posteriori estimate error covariance
    //    Trajectory P_;// priori estimate error covariance
    //    Trajectory K;//gain
    //    Trajectory    z;//actual measurement
    
    
    pstd = 4e-3;//can be changed
    cstd = 0.25;//can be changed
    Q = Trajectory(pstd,pstd,pstd);// process noise covariance
    R = Trajectory(cstd,cstd,cstd);// measurement noise covariance
    
    // Step 4 - Generate new set of previous to current transform, such that the trajectory ends up being the same as the smoothed trajectory
    //std::vector<TransformParam> new_prev_to_cur_transform;
    
    // Step 5 - Apply the new transformation to the video
    //cap.set(CV_CAP_PROP_POS_FRAMES, 0);
    T = cv::Mat(2,3,CV_64F);
}


cv::Mat VideoStablisation::run(const cv::Mat &image, const cv::Mat &alt){
    
    if(m_first_run){
        prev = image.clone();
        cv::cvtColor(prev, prev_grey, cv::COLOR_BGR2GRAY);
        vert_border = HORIZONTAL_BORDER_CROP * image.rows / image.cols; // get the aspect ratio correct
        m_first_run = false;
        return image;
    }
    
    //convert input to greyscale
    cv::cvtColor(image, cur_grey, cv::COLOR_BGR2GRAY);

    // vector from prev to cur
    std::vector <cv::Point2f> prev_corner, cur_corner;
    std::vector <cv::Point2f> prev_corner2, cur_corner2;
    std::vector <uchar> status;
    std::vector <float> err;
    
    SmartSpecs::Timer::begin("Features");
    //perform feature detection and optic flow.
    cv::goodFeaturesToTrack(prev_grey, prev_corner, 200, 0.01, 30);
    SmartSpecs::Timer::end("Features");

    SmartSpecs::Timer::begin("OpticFlow");
    cv::calcOpticalFlowPyrLK(prev_grey, cur_grey, prev_corner, cur_corner, status, err);
    SmartSpecs::Timer::end("OpticFlow");

    // weed out bad matches
    for(size_t i=0; i < status.size(); i++) {
        if(status[i]) {
            prev_corner2.push_back(prev_corner[i]);
            cur_corner2.push_back(cur_corner[i]);
        }
    }
    
    // translation + rotation only
    cv::Mat T = cv::estimateRigidTransform(prev_corner2, cur_corner2, false); // false = rigid transform, no scaling/shearing
    
    // in rare cases no transform is found. We'll just use the last known good transform.
    if(T.data == NULL) {
        last_T.copyTo(T);
    }
    
    T.copyTo(last_T);
    
    // decompose T
    double dx = T.at<double>(0,2);
    double dy = T.at<double>(1,2);
    double da = atan2(T.at<double>(1,0), T.at<double>(0,0));
    //
    //prev_to_cur_transform.push_back(TransformParam(dx, dy, da));
    
    //std::cout << k << " " << dx << " " << dy << " " << da << std::endl;
    //
    // Accumulated frame to frame transform
    x += dx;
    y += dy;
    a += da;
    
    //trajectory.push_back(Trajectory(x,y,a));
    //
    //std::cout << k << " " << x << " " << y << " " << a << std::endl;
    //
    z = Trajectory(x,y,a);
    //
    if(k==1){
        // intial guesses
        X = Trajectory(0,0,0); //Initial estimate,  set 0
        P = Trajectory(1,1,1); //set error variance,set 1
    }
    else
    {
        //time update（prediction）
        X_ = X; //X_(k) = X(k-1);
        P_ = P+Q; //P_(k) = P(k-1)+Q;
        // measurement update（correction）
        K = P_/( P_+R ); //gain;K(k) = P_(k)/( P_(k)+R );
        X = X_+K*(z-X_); //z-X_ is residual,X(k) = X_(k)+K(k)*(z(k)-X_(k));
        P = (Trajectory(1,1,1)-K)*P_; //P(k) = (1-K(k))*P_(k);
    }
    //smoothed_trajectory.push_back(X);
    //std::cout << k << " " << X.x << " " << X.y << " " << X.a << std::endl;
    //-
    // target - current
    double diff_x = X.x - x;//
    double diff_y = X.y - y;
    double diff_a = X.a - a;
    
    dx = dx + diff_x;
    dy = dy + diff_y;
    da = da + diff_a;
    
    //new_prev_to_cur_transform.push_back(TransformParam(dx, dy, da));
    //
    //std::cout << k << " " << dx << " " << dy << " " << da << std::endl;
    //
    T.at<double>(0,0) = cos(da);
    T.at<double>(0,1) = -sin(da);
    T.at<double>(1,0) = sin(da);
    T.at<double>(1,1) = cos(da);
    
    T.at<double>(0,2) = dx;
    T.at<double>(1,2) = dy;
    
    cv::Mat cur2;
    
    if(!alt.empty())
        cv::warpAffine(alt,cur2,T,image.size());
    else
        cv::warpAffine(prev, cur2, T, image.size());
    
    cur2 = cur2(cv::Range(vert_border, cur2.rows-vert_border), cv::Range(HORIZONTAL_BORDER_CROP, cur2.cols-HORIZONTAL_BORDER_CROP));
    
    // Resize cur2 back to cur size, for better side by side comparison
    cv::resize(cur2, cur2, image.size());

    prev = image.clone();//cur.copyTo(prev);
    cur_grey.copyTo(prev_grey);
    return cur2;
}
