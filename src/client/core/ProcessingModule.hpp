//
//  ProcessingModule.hpp
//  dlib
//
//  Created by Iain Wilson on 12/06/2019.
//

#ifndef ProcessingModule_hpp
#define ProcessingModule_hpp

#include <stdio.h>
#include "common/RingBuffer.hpp"
#include "common/Image.hpp"
#include "Display.hpp"
#include "common/Parameters.hpp"
#include "timer.hpp"
#include "Thread.hpp"
#include "Sensors.hpp"
typedef RingBuffer<Image::Ptr> ImageBuffer;

class ProcessingModule : public Thread {

public:
    typedef std::shared_ptr<ProcessingModule> Ptr;
   
    ProcessingModule(const std::string &name,
                     const Display::Ptr display,
                     const ImageBuffer::Ptr &input,
                     const SensorBuffer::Ptr &sensors):
    Thread(name),
    m_display(display),
    m_input_buffer(input),
    m_sensor_buffer(sensors){}
    
    
    ProcessingModule(const std::string &name,
                     const Display::Ptr display,
                     const ImageBuffer::Ptr &input,
                     const ImageBuffer::Ptr &display_buff,
                     const SensorBuffer::Ptr &sensors):
    Thread(name),
    m_display(display),
    m_input_buffer(input),
    m_display_buffer(display_buff),
    m_sensor_buffer(sensors){}
    
    virtual ~ProcessingModule(){}
    
   
    void ui_thread(){
        auto frame = m_display_buffer->pop();
        if(frame->isEmpty()){
            std::cout <<"No Image Data" << std::endl;
            return;
        }
        
        cv::Rect roi;
        std::string title="[ "+name()+" ] ui_process()";
//        SmartSpecs::Timer::begin(title.c_str());
        cv::Mat img = process(frame,roi);
//        SmartSpecs::Timer::end(title.c_str());
        
        m_display->draw(img,roi);
    }
    
    virtual cv::Mat process(const Image::Ptr &frame, cv::Rect &roi)=0;
    
    void thread_run(){
        bg_process();
    }
    
    virtual void bg_process()=0;
    
    virtual void linkParameters(){};
    
protected:
   
    Display::Ptr m_display;
    
    ImageBuffer::Ptr m_input_buffer;
    ImageBuffer::Ptr m_display_buffer;
    ImageBuffer::Ptr m_output_buffer;
    SensorBuffer::Ptr m_sensor_buffer;
    Parameters m_parameters;
    
};

const std::string PROCESSOR_TEXT{"PROCESSOR_TEXT"};
const std::string PROCESSOR_FACE{"PROCESSOR_FACE"};
const std::string PROCESSOR_ZOOM{"PROCESSOR_ZOOM"};
const std::string PROCESSOR_PASS{"PROCESSOR_PASS"};

struct Processors{
    
    typedef std::shared_ptr<ProcessingModule> Ptr;
    
    static Ptr create(const std::string &type, const Display::Ptr &display, const ImageBuffer::Ptr &input, const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors);
};


#endif /* ProcessingModule_hpp */
