//
//  ZoomMode.cpp
//  smart_specs_client
//
//  Created by Iain Wilson on 03/08/2019.
//

#include "ZoomMode.hpp"

ZoomMode::ZoomMode(const Display::Ptr display, const ImageBuffer::Ptr &input, const ImageBuffer::Ptr &display_buff, const SensorBuffer::Ptr &sensors):
ProcessingModule("PASS_MODE",display,input,display_buff,sensors){
    
    m_parameters.add<bool>("track",false);
    
#ifdef USE_CUDA
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation_GPU::reset,m_stab));
#else
    m_parameters.add<bool>("enable-stable",false,std::bind(&VideoStablisation::reset,m_stab));
#endif
}

void ZoomMode::linkParameters(){
    
    m_display->link(m_parameters["enable-stable"],'6');
    m_display->link(m_parameters["track"],'t');
    
}


cv::Mat ZoomMode::process(const Image::Ptr &frame, cv::Rect &roi){

    cv::Mat image = frame->get();
    
    //set an roi to the center of the image.
    //int rect_size=50;
    cv::Point centre(image.cols/2,image.rows/2);
    cv::Point tl(centre.x-50,centre.y-50);
    cv::Point br(centre.x+50,centre.y+50);
    
 
    if(m_parameters.value<bool>("track")){
        cv::Rect roi(tl,br);
        m_to = TrackedObject::create(roi,frame);
        //m_to->update();
        cv::rectangle(image, roi, cv::Scalar(0,0,255));
        return image;
    }
    
    //if we are tracking.draw
    if(m_to.get() != nullptr){
        m_to->track(frame);
        auto roi = m_to->roi();
        cv::rectangle(image, roi, cv::Scalar(255,0,0));
        return image;
    }
    
    //m_parameters.set<bool>("track",false);
    cv::rectangle(image, tl, br, cv::Scalar(0,255,0));
    return image;
}





void ZoomMode::printSensorData(){
    std::vector<HeadPose::Ptr> poses;
    while(!m_sensor_buffer->empty()){
        poses.push_back(m_sensor_buffer->pop());
    }
    
    if(poses.size()>0){
        //just print the sensor data to start with.
        {
            std::stringstream ss;
            ss.precision(3);
            ss << "position:";
            ss << " x["<< poses[0]->position.x() << "]";
            ss << " y["<< poses[0]->position.y() << "]";
            ss << " z["<< poses[0]->position.z() << "]";
//            cv::putText(image, ss.str(), cv::Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 255, 0),2);
        }
        {
            std::stringstream ss;
            ss.precision(3);
            ss << "acceleration:";
            ss << " x["<< poses[0]->acceleration.x() << "]";
            ss << " y["<< poses[0]->acceleration.y() << "]";
            ss << " z["<< poses[0]->acceleration.z() << "]";
//            cv::putText(image, ss.str(), cv::Point(0, 100), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 255, 0),2);
        }
        {
            std::stringstream ss;
            ss.precision(3);
            ss << "rotation:";
            ss << " w["<< poses[0]->rotation.w() << "]";
            ss << " x["<< poses[0]->rotation.x() << "]";
            ss << " y["<< poses[0]->rotation.y() << "]";
            ss << " z["<< poses[0]->rotation.z() << "]";
//            cv::putText(image, ss.str(), cv::Point(0, 150), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 255, 0),2);
        }
    }
}
