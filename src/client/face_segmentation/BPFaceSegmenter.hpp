#pragma once

/*
 
 Back Projection segmentation
 
 
 */

#include "FaceSegementerBase.hpp"

class BPFaceSegmenter : public FaceSegmenterBase{
    
public:
    BPFaceSegmenter();
    cv::Mat run(const cv::Mat& face);

    
    
};
