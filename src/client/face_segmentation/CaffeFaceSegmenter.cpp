#include "FaceSegementer.hpp"
#ifdef USE_CAFFE

CaffeFaceSegmenter::CaffeFaceSegmenter(){
    m_name = CAFFE_SEGMENTER;
    
    //load models
    std::string dataPath = "/home/iain/experiments/face_detection/libs/face_segmentation/data/";
    std::string deploy  = dataPath+"face_seg_fcn8s_deploy.prototxt";
    std::string model = dataPath + "face_seg_fcn8s.caffemodel";
    
    //initialise face face_segmenter
    m_fs.reset(new face_seg::FaceSeg(deploy,model,true));
    std::cout << "FaceSegementer::INIT" <<std::endl;
}

cv::Mat CaffeFaceSegmenter::run(const cv::Mat &face){
  SmartSpecs::Timer::begin("Caffe Segment");
  cv::Mat mask =  m_fs->process(face);
  SmartSpecs::Timer::end("Caffe Segment");
  return mask;
}

#endif // USE_CAFFE