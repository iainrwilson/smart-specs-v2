#pragma once


/*
 * 
 *Face Segementation Factory 
 * 
 * 
 * 
 */
#include "cmakedefines.h"

#include "FaceSegementerBase.hpp"

#ifdef USE_CAFFE
#include "CaffeFaceSegmenter.hpp"
#endif //USE_CAFFE

#include "HistogramSegmenter.hpp"

#include <string>

const std::string CAFFE_SEGMENTER{"CAFFE_SEGMENTER"};
const std::string CAMSHIFT_SEGMENTER{"CAMSHIFT_SEGMENTER"};
const std::string HISTOGRAM_SEGMENTER{"HISTOGRAM_SEGMENTER"};

class FaceSegmenter{
  
  public:
    static FaceSegmenterBase::Ptr create(const std::string &type);
    
  private:
    FaceSegmenter() = delete;
 
};
