#pragma once


/*
 * 
 * Based on the code by
 * 
 * 
 * https://github.com/YuvalNirkin/face_segmentation
 * 
 * 
 * 
 */
#include "cmakedefines.h"

#ifdef USE_CAFFE

#include "FaceSegementerBase.hpp"

// face_seg
#include <face_seg/face_seg.h>
#include <face_seg/utilities.h>



class CaffeFaceSegmenter : public FaceSegmenterBase{
  
  public:
    CaffeFaceSegmenter();
    
    cv::Mat run(const cv::Mat& face);
    
  private:
    std::unique_ptr<face_seg::FaceSeg> m_fs;
  
};

#endif // USE_CAFFE
