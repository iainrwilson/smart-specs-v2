#pragma once


/*
 * 
 *Face Segementation Base class
 * 
 *  
 */


#include <opencv2/opencv.hpp>
#include <memory>

#include "common/timer.hpp"
#include "Face.hpp"
class FaceSegmenterBase{
  
public:
    typedef std::shared_ptr<FaceSegmenterBase> Ptr;
    
    FaceSegmenterBase(){};
    virtual ~FaceSegmenterBase() = default;
    virtual cv::Mat run(const cv::Mat& face)=0;
    virtual cv::Mat run(const Face::Ptr &face){return cv::Mat();}
    std::string name() const {return m_name;}
    
protected:
  std::string m_name;
  
};
