#include "FaceSegementer.hpp"


FaceSegmenterBase::Ptr FaceSegmenter::create(const std::string& type){

#ifdef USE_CAFFE
   if(type == CAFFE_SEGMENTER)
     return FaceSegmenterBase::Ptr(new CaffeFaceSegmenter);
#endif
   
//   if(type == HISTOGRAM_SEGMENTER)
//       return FaceSegmenterBase::Ptr(new HistogramSegmenter);
//   
//if(type == CAMSHIFT_SEGMENTER)
   //  return FaceSegmenterBase::Ptr(new CamShiftFaceSegmenter);
   
   std::cout << "FaceSegementer::create Segmenter [" << type << "] not found" <<std::endl;
   
   return FaceSegmenterBase::Ptr();
}
