#include "grid_seams.hpp"

#include <cmath>
#include <iostream>
#include <limits>
#include <iterator>
#include "timer.hpp"

using namespace std;

//double GridSeams::f(int x, int y) {
//    return gradientMap.at<double>(y,x);
//}

double GridSeams::g(int x, int S) {
	int d = (x+1) % S;
	d = abs(d - S/2);
	return (double)d / (S/2);
}

void GridSeams::calculateEnergyMap(int S, int numSeams) {
	energyMap = Mat::zeros(m_size, CV_64F);	// initializing energy map

	/////////////////////////
	// Calculating energies
	/////////////////////////

    int s_ns = S*numSeams;
//    int nRows =1;
//    int nCols = size.width * size.height;
//
	// first row
    double *p = energyMap.ptr<double>(0);
    double *gm = gradientMap.ptr<double>(0);

	for(int x = 0; x < m_size.width; x++) {
		if((x+1)%S == 0 && x+1 < s_ns) {	// on the border
			p[x] = numeric_limits<double>::max();
		} else {
			p[x] = gm[x] + w*g(x,S);
		}
	}

    
//    uchar* p = I.data;
//
//    for( unsigned int i =0; i < ncol*nrows; ++i)
//        *p++ = table[*p];
    
	// Calculating energies of the rest using dynamic programming
    double *p_1 = energyMap.ptr<double>(0);
    p = energyMap.ptr<double>(1);
    gm = gradientMap.ptr<double>(1);
    
	for(int y = 1; y < m_size.height; ++y) {
		for(int x = 0; x < m_size.width; ++x,++p,++p_1,++gm) {
			if((x+1)%S == 0 && x+1 < s_ns) {	// on the border
				*p = numeric_limits<double>::max();
			} else {
				double energy = *gm + w*g(x,S);
				// Get the path with smallest energy
				double energyMin = numeric_limits<double>::max();
                //for(int offset = -1; offset <= 1; offset++) {
                    if(x-1 >= 0 && x-1 < m_size.width) {
                        double energy = *(p_1-1);
                        if(energy < energyMin)
                            energyMin = energy;
                    }
                    if(x >= 0 && x < m_size.width) {
                        double energy = *(p_1);
                        if(energy < energyMin)
                            energyMin = energy;
                    }
                    if(x+1 >= 0 && x+1 < m_size.width) {
                        double energy = *(p_1+1);
                        if(energy < energyMin)
                            energyMin = energy;
                    }
                //}
				*p = energy + energyMin;
			}
		}
	}
}

void GridSeams::getSeamMap(int S, int numSeams, int direction) {
	for(int i = 0; i < numSeams; i++) {	// find seams one by one
		int seamX; // seam x coordinate
		// find the end-point with minimum energy cost
		double xMin;
		double energyMin = numeric_limits<double>::max();
		if(i < numSeams - 1) {
			for(int x = i*S; x < (i+1)*S - 1; x++) {
				double energy = energyMap.at<double>(m_size.height-1, x);
				if(energy < energyMin) {
					energyMin = energy;
					xMin = x;
				}
			}
		} else {
			for(int x = i*S; x < m_size.width; x++) {
				double energy = energyMap.at<double>(m_size.height-1, x);
				if(energy < energyMin) {
					energyMin = energy;
					xMin = x;
				}
			}
		}
		seamX = xMin;
		if(direction == 0)
			seamMapV.at<uchar>(m_size.height-1, seamX) = 255;
		else
			seamMapH.at<uchar>(m_size.height-1, seamX) = 255;

		// find all points on a seam
		for(int y = m_size.height-2; y >= 0; y--) {
			double xMin;
			double energyMin = numeric_limits<double>::max();
			for(int offset = -1; offset <= 1; offset++) {
				if(i < numSeams - 1) {
					if(seamX+offset >= i*S && seamX+offset < (i+1)*S - 1) {
						double energy = energyMap.at<double>(y, seamX+offset);
						if(energy < energyMin) {
							energyMin = energy;
							xMin = seamX + offset;
						}
					}
				} else {
					if(seamX+offset >= i*S && seamX+offset < m_size.width) {
						double energy = energyMap.at<double>(y, seamX+offset);
						if(energy < energyMin) {
							energyMin = energy;
							xMin = seamX + offset;
						}
					}
				}
			}
			seamX = xMin;
			if(direction == 0)
				seamMapV.at<uchar>(y, seamX) = 255;
			else
				seamMapH.at<uchar>(y, seamX) = 255;
		}
	}
}

void GridSeams::processOne(int S, int numSeams, int direction) {
//    SmartSpecs::Timer::begin("[ GridSeams ] calculateEnergyMap" );
	calculateEnergyMap(S, numSeams);
//    SmartSpecs::Timer::end("[ GridSeams ] calculateEnergyMap" );
    
//    SmartSpecs::Timer::begin("[ GridSeams ] getSeamMap" );
	getSeamMap(S, numSeams, direction);
//    SmartSpecs::Timer::end("[ GridSeams ] getSeamMap" );

}

/*!
 \brief generate labels.
 
 First walkk horizontaly, incrementing when seam is hit.
 Then verticaly
 
 Addition:
    sum the rgb values on the original image, for later calculating the mean colour for given superpixel.
*/
void GridSeams::getLabelMap() {
    
    Mat labelMapV(m_size, CV_32S);
    int *lm_v;
    uchar *sm_v;
    for(int y = 0; y < m_size.height; ++y) {
        lm_v = labelMapV.ptr<int>(y);
        sm_v = seamMapV.ptr<uchar>(y);
        int currLabelV = 0;
        for(int x = 0; x < m_size.width; ++x) {
            lm_v[x] = currLabelV;
            if(sm_v[x] != 0)
                ++currLabelV;
        }
    }
    
    for(int x = 0; x < m_size.width; ++x) {
        int currLabelH = 0;
        for(int y = 0; y < m_size.height; ++y) {
            labelMap.at<int>(y,x) = currLabelH * (M+1) + labelMapV.at<int>(y,x);
            if(seamMapH.at<uchar>(y,x) != 0)
                ++currLabelH;
        }
    }
}

void GridSeams::process(const Mat& img, int SX, int SY, double w) {
    m_size = img.size();	// get image size
    
    this->w = w;	// weight
    
    // grid size
    // the last grid might be big
    this->SX = SX;
    this->SY = SY;
    
    // number of seams
    M = m_size.width / this->SX;
    N = m_size.height / this->SY;
    
    // the seams are marked as non-zero values
    seamMapV = Mat::zeros(img.size(), CV_8U);
    seamMapH = Mat::zeros(img.size(), CV_8U);
    seamMap = Mat::zeros(img.size(), CV_8U);
    
    if(img.channels()==3){
        // Calculate gray image
        cvtColor(img, imgGray, cv::COLOR_BGR2GRAY);
    }else{
        imgGray = img;
    }
    // Calculate gradient map
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;
    Sobel(imgGray, grad_x, CV_16S, 1, 0, 3, 1, 0);
    convertScaleAbs(grad_x, abs_grad_x);
    Sobel(imgGray, grad_y, CV_16S, 1, 0, 3, 1, 0);
    convertScaleAbs(grad_y, abs_grad_y);
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, gradientMap);
    normalize(gradientMap, gradientMap, 0, 1, NORM_MINMAX, CV_64F);
    gradientMap = Mat::ones(m_size, CV_64F) - gradientMap;
    
    // Generate vertical seams
    processOne(this->SX, M, 0);
    
    // Generate horizontal seams
    transpose(gradientMap, gradientMap);
    transpose(energyMap, energyMap);
    transpose(seamMapH, seamMapH);
    m_size = Size(m_size.height, m_size.width);
    processOne(this->SY, N, 1);
    transpose(seamMapH, seamMapH);
    // transpose(energyMap, energyMap);	// efficiency consideration
    // transpose(seamMapH, seamMapH);	// efficiency consideration
    m_size = Size(m_size.height, m_size.width);
    
    
    // combine vertical and horizontal seam maps
    
    uchar* smV;
    uchar* smH;
    uchar* sm;
    int nRows =1;
    int nCols = m_size.width * m_size.height;
    
    for(int y = 0; y < nRows; ++y) {
        smV = seamMapV.ptr<uchar>(y);
        smH = seamMapH.ptr<uchar>(y);
        sm = seamMap.ptr<uchar>(y);
        for(int x = 0; x < nCols; ++x) {
            if(smV[x] != 0 || smH[x] != 0)
                sm[x] = 255;
        }
    }
    
    
    // labeling
    labelMap = Mat::zeros(m_size, CV_32S);
    getLabelMap();
}


/*!
 \brief calculate the median values for each superpixel.
 
 Use a histogram to select peak value.
 */
//cv::Mat GridSeams::median(const cv::Mat &image){
//    
//    
//    
//    //create a vector for each supperpixel
//    const int sp_count = (M+1)*(N+1);
//    //initialise a vector for all the superpixels
//    std::vector<std::vector<cv::Vec3b>> superpixels(sp_count);
//    
//    int n_rows = 1;
//    int n_cols = m_size.width * m_size.height;
//    
//    //accumilate pass
//    for(int row=0;row<n_rows;++row){
//        const cv::Vec3b* img_ptr = image.ptr<cv::Vec3b>(row);
//        const int* label_ptr = labelMap.ptr<int>(row);
//        for(int col=0;col<n_cols;++col){
//            const int label = label_ptr[col];
//            superpixels[label].push_back(img_ptr[col]);
//        }
//    }
//    
//    
//    //calculate median;
//    std::vector<cv::Vec3b> medians(sp_count);
//    for(int i=0;i<sp_count;++i){
//        medians[i] = calcMedian<cv::Vec3b>(superpixels[i]);
//    }
//    
//    cv::Mat median(image.size(),CV_8UC3);
//    
//    //    //calculate and assign
//    for(int row=0;row<n_rows;++row){
//        cv::Vec3b* median_ptr = median.ptr<cv::Vec3b>(row);
//        const int* label_ptr = labelMap.ptr<int>(row);
//        for(int col=0;col<n_cols;++col){
//            median_ptr[col] = medians[label_ptr[col]];
//        }
//        
//    }
//    return median;
//}

/*!
 \brief average the pixel values in each superpixel.
 
 loop over the label map, and accumilate.
 
*/
cv::Mat GridSeams::average(const cv::Mat &image){
    
    cv::Mat avg = image.clone();
    
    int n_rows = 1;
    int n_cols = m_size.width * m_size.height;
    const int sp_count = (M+1)*(N+1);
    std::vector<cv::Vec3i> labels(sp_count,cv::Vec3i(0,0,0));
    std::vector<uint32_t> label_count(sp_count,0);
    
    //average pass
    for(int row=0;row<n_rows;++row){
        const cv::Vec3b* img_ptr = image.ptr<cv::Vec3b>(row);
        const int* label_ptr = labelMap.ptr<int>(row);
        for(int col=0;col<n_cols;++col){
            const int label = label_ptr[col];
            labels[label] += img_ptr[col];
            label_count[label]++;
        }
    }
    
    
    //assignment pass
    for(int row=0;row<n_rows;++row){
        cv::Vec3b* avg_ptr = avg.ptr<cv::Vec3b>(row);
        const int* label_ptr = labelMap.ptr<int>(row);
        
        for(int col=0;col<n_cols;++col){
            const int _label = label_ptr[col];
            const int _count = label_count[_label];
            avg_ptr[col][0] = labels[_label][0] / _count;
            avg_ptr[col][1] = labels[_label][1] / _count;
            avg_ptr[col][2] = labels[_label][2] / _count;
        }
    }
    return avg;
}

void GridSeams::display(const Mat& img) {
//    SmartSpecs::Timer::begin("[ GridSeams ] display" );

	Mat imgSeams = img.clone();
	MatIterator_<Vec3b> it1, end1;
	MatIterator_<uchar> it2, end2;
	for(it1 = imgSeams.begin<Vec3b>(), end1 = imgSeams.end<Vec3b>(), it2 = seamMap.begin<uchar>(), end2 = seamMap.end<uchar>(); it1 != end1 && it2 != end2; it1++, it2++) {
		if(*it2 != 0) {
			(*it1)[0] = 255;
			(*it1)[1] = 255;
			(*it1)[2] = 255;
		}
	}
	imshow("seams", seamMap);
	imshow("img seams", imgSeams);

	RNG rng(123457);
	vector<Vec3b> colors((M+1)*(N+1));
	for(int i = 0; i < (M+1)*(N+1); i++) {
		colors[i][0] = rng.uniform(0, 256);
		colors[i][1] = rng.uniform(0, 256);
		colors[i][2] = rng.uniform(0, 256);
	}
	
    Mat colorMap(m_size, CV_8UC3);
	for(int y = 0; y < m_size.height; y++) {
		for(int x = 0; x < m_size.width; x++) {
			int label = labelMap.at<int>(y,x);
			colorMap.at<Vec3b>(y,x) = colors[label];
            std::cout << "Label: " << label <<std::endl;
		}
	}
	imshow("color map", colorMap);
//    SmartSpecs::Timer::end("[ GridSeams ] display" );
}
