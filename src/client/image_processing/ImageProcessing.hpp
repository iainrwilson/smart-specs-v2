//
//  ImageProcessing.hpp
//  cpp_server
//
//  Created by Iain Wilson on 11/12/2018.
//

#ifndef ImageProcessing_hpp
#define ImageProcessing_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "Face.hpp"

struct ImageProcessing{

    /* bunch of helper functions */
    
    static cv::Mat kmeans(const cv::Mat &in, const int clusters);
    static cv::Mat canny(const cv::Mat &image, const int low,const int ratio,const int blur_ammount=3);
    
    static cv::Mat diffuse(const cv::Mat &in, const cv::Mat &canny, const cv::Mat &skin,const cv::Mat &labels,const Face::Ptr &face);
    
    
};
#endif /* ImageProcessing_hpp */
