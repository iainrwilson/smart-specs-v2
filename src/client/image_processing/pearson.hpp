#pragma once
#include <opencv2/core.hpp>

struct Pearson {

  static void pearson (const cv::Mat &input, cv::Mat &output, int min_valley_depth, int min_ridge_depth);

 
};
