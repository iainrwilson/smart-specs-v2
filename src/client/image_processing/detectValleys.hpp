#pragma once
//#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>

using namespace cv;

struct DetectValleys {

  static void detectValleys(const cv::Mat &image,cv::Mat &out,const int T1, const int T2);
  static void detectValleys(const cv::Mat &image, cv::Mat &out, int windowsize, int T1, int T2);

    
    static void detectValley(const cv::Mat &input,cv::Mat &output,const int direction=0,const int ksize=3);

};

struct Canny2{
  static void canny2( cv::InputArray _src, cv::OutputArray _dst,
		      double low_thresh, double high_thresh,
		      int aperture_size, bool L2gradient, cv::Mat &dx, cv::Mat &dy );
};
