//
//  ImageProcessing.cpp
//  cpp_server
//
//  Created by Iain Wilson on 11/12/2018.
//

#include "ImageProcessing.hpp"
#include "common/timer.hpp"

cv::Mat ImageProcessing::kmeans(const cv::Mat &in, const int clusters){
    SmartSpecs::Timer::begin("[ImageProcessing::kmeans] kmeans");

    //perform kmeans clustering on the colour channels of the input image.
    cv::Mat data;
    in.convertTo(data,CV_32F);
    cv::cvtColor(data,data,cv::COLOR_BGR2HSV);
    data = data.reshape(1,data.total());
    
    // do kmeans
    cv::Mat labels, centers;
    cv::kmeans(data, clusters, labels, cv::TermCriteria(cv::TermCriteria::EPS+cv::TermCriteria::COUNT, 10, 1.0), 3,
               cv::KMEANS_PP_CENTERS, centers);
    
    // reshape both to a single row of Vec3f pixels:
    centers = centers.reshape(3,centers.rows);
    data = data.reshape(3,data.rows);
    
    // replace pixel values with their center value:
    cv::Vec3f *p = data.ptr<cv::Vec3f>();
    for (size_t i=0; i<data.rows; i++) {
        int center_id = labels.at<int>(i);
        p[i] = centers.at<cv::Vec3f>(center_id);
    }
    
    // back to 2d, and uchar:
    data = data.reshape(3, in.rows);
    cv::Mat cluster;
    data.convertTo(cluster, CV_8U);
//    cv::cvtColor(cluster,cluster,cv::COLOR_HSV2BGR);
    SmartSpecs::Timer::end("[ImageProcessing::kmeans] kmeans");

    return cluster;
}


cv::Mat ImageProcessing::canny(const cv::Mat &image, const int low,const int ratio,const int blur_ammount){
    cv::Mat gray,hsv;
    cv::cvtColor(image,gray,cv::COLOR_BGR2GRAY);
    //    cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
//    std::vector<cv::Mat> channels;
//    cv::split(hsv, channels);
//    gray = channels[0];
    cv::Mat dst, detected_edges;
    const int kernel_size = 7;
    //testing edge detect.
    for(int i=0;i<blur_ammount;++i){
        cv::blur( gray, gray, cv::Size(11,11) );
    }

    cv::Canny( gray, detected_edges, low, low*ratio, kernel_size );
    dst = cv::Scalar::all(0);
    image.copyTo( dst, detected_edges);
    return detected_edges;
}

cv::Mat ImageProcessing::diffuse(const cv::Mat &in, const cv::Mat &canny, const cv::Mat &skin,const cv::Mat &labels,const Face::Ptr &face){
    /* Attempt at diffusion skin segmnetoation
     
     Uses the canny, histogram skin segmentaion, and cluster labels.
    
     all images should be the same shape.
     
     
     //use the landmarks to start diffusion.
     
     */
    
    
    //left ebrow, start going up.
    cv::Mat image = in(face->paddRoi());
    auto point = face->features()[17];
    
    //start walking up...
    for(int y=point.y;y<0;--y){
        
    }
    
    return cv::Mat();
}
