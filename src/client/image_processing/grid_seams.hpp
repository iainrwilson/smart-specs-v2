#ifndef __GRID_SEAMS_H__
#define __GRID_SEAMS_H__

#include <opencv2/opencv.hpp>

using namespace cv;

class GridSeams {

private:

	Size m_size;

	Mat imgGray;

	Mat gradientMap;

	Mat energyMap;

	int SX, SY; //supperpixel size (30x30)

	int M, N;	// number of vertical and horizontal seams

	double w;

	Mat seamMapV;

	Mat seamMapH;

//     double f(int x, int y);
	 inline double g(int x, int S);

	void calculateEnergyMap(int S, int numSeams);

	void getSeamMap(int S, int numSeams, int direction);

	// direction - 0 for vertical, 1 for horizontal
	void processOne(int S, int numSeams, int direction);

	void getLabelMap();
    
//    template<typename T>
//    T calcMedian(std::vector<T> &vec){
//        if (vec.size() % 2 == 0) {
//            auto median_it1 = vec.begin() + vec.size() / 2 - 1;
//            auto median_it2 = vec.begin() + vec.size() / 2;
//            
//            std::nth_element(vec.begin(), median_it1 , vec.end());
//            auto e1 = *median_it1;
//            
//            std::nth_element(vec.begin(), median_it2 , vec.end());
//            auto e2 = *median_it2;
//            
//            return (e1 + e2) / 2;
//            
//        } else {
//            const auto median_it = vec.begin() + vec.size() / 2;
//            std::nth_element(std::begin(vec), median_it , std::end(vec));
//            return *median_it;
//        }
//    }

    
public:

	Mat seamMap;
	Mat labelMap;

	// SX - number of pixels per virtical grid
	// SY - number of pixels per horizontal grid
	// w - weight
	void process(const Mat& img, int SX, int SY, double w);

	void display(const Mat& img);

    
    cv::Mat average(const cv::Mat& image);
    cv::Mat median(const cv::Mat& image);
};




#endif
