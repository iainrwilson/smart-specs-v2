#include "pearson.hpp" 
#include <stdio.h>
#include <string.h>
#include <iostream>



#define a row0[0]
#define b row0[1]
#define c row0[2]
#define d row0[3]
#define e row0[4]
#define f row1[0]
#define g row1[1]
#define h row1[2]
#define i row1[3]
#define j row1[4]
#define k row2[0]
#define l row2[1]
#define m row2[2]
#define n row2[3]
#define o row2[4]
#define p row3[0]
#define q row3[1]
#define r row3[2]
#define s row3[3]
#define t row3[4]
#define u row4[0]
#define v row4[1]
#define w row4[2]
#define x row4[3]
#define y row4[4]

#define val 0xff
#define no_val 0

void Pearson::pearson(const cv::Mat &input, cv::Mat &output, int min_valley_depth, int min_ridge_depth)
{
	uint8_t const *row0;
	uint8_t const *row1;
	uint8_t const *row2;
	uint8_t const *row3;
	uint8_t const *row4;

	auto base = input.data;
	int fkp, jot, hmr, glq, ins, hmr2;

	for (int row = 2; row < input.rows - 2; ++row, base += input.step)
	{
		row0 = base;
		row1 = row0 + input.step;
		row2 = row1 + input.step;
		row3 = row2 + input.step;
		row4 = row3 + input.step;

		for (int col = 2; col < input.cols - 2; ++col, ++row0, ++row1, ++row2, ++row3, ++row4)
		{
			if ((l - m) > min_valley_depth || (n - m) > min_valley_depth)
			{
				fkp = f + k + p;
				jot = j + o + t;
				hmr = h + m + r;
				glq = g + l + q;
				ins = i + n + s;

				hmr2 = hmr * 2;
				if (((fkp + jot - hmr2) > min_ridge_depth) &&
					(glq + ins - hmr2) > (fkp + hmr - 2 * glq) &&
					(glq + ins - hmr2) > (hmr + jot - 2 * ins))
				{
					output.at<uint8_t>(row, col) = val;
					continue;
				}
			}

			if ((h - m) > min_valley_depth || (r - m) > min_valley_depth)
			{
				fkp = b + c + d;
				jot = v + w + x;
				hmr = l + m + n;
				glq = g + h + i;
				ins = q + r + s;

				hmr2 = hmr * 2;
				if (((fkp + jot - hmr2) > min_ridge_depth) &&
					(glq + ins - hmr2) > (fkp + hmr - 2 * glq) &&
					(glq + ins - hmr2) > (hmr + jot - 2 * ins))
				{
					output.at<uint8_t>(row, col) = val;
					continue;
				}
			}

			if ((g - m) > min_valley_depth || (s - m) > min_valley_depth)
			{
				fkp = c + g + k;
				jot = o + s + w;
				hmr = i + m + q;
				glq = d + h + l;
				ins = n + r + v;

				hmr2 = hmr * 2;
				if (((fkp + jot - hmr2) > min_ridge_depth) &&
					(glq + ins - hmr2) > (fkp + hmr - 2 * glq) &&
					(glq + ins - hmr2) > (hmr + jot - 2 * ins))
				{
					output.at<uint8_t>(row, col) = val;
					continue;
				}
			}

			if ((i - m) > min_valley_depth || (q - m) > min_valley_depth)
			{
				fkp = k + q + w;
				jot = c + i + o;
				hmr = g + m + s;
				glq = l + r + x;
				ins = b + h + n;

				hmr2 = hmr * 2;
				if (((fkp + jot - hmr2) > min_ridge_depth) &&
					(glq + ins - hmr2) > (fkp + hmr - 2 * glq) &&
					(glq + ins - hmr2) > (hmr + jot - 2 * ins))
				{
					output.at<uint8_t>(row, col) = val;
					continue;
				}
			}

			output.at<uint8_t>(row, col) = no_val;
		}
	}
}
