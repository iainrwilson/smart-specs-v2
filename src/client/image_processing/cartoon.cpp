#include "cartoon.hpp"


Cartoon::Cartoon():m_T1(10),m_T2(25),m_threshold(-1),m_blur_sigma(1.0),m_dilate(true),m_dilate_iter(1){
}

cv::Mat Cartoon::run(const cv::Mat& img, bool invert,const cv::Mat &mask, bool use_hsv){
    
    /* add HSV option */
    
    cv::Mat gray,mass,cartoon,valleys;
    
    if(img.channels() == 3){
        if(use_hsv){
            cv::Mat hsv;
            cv::Mat channels[3];
            cv::cvtColor(img, hsv, cv::COLOR_BGR2HSV);
            cv::split(hsv,channels);
            gray = channels[2];
        }else{
            cv::cvtColor(img,gray,cv::COLOR_BGR2GRAY);
        }
    }else{
        gray = img;
    }
    
//    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
//    clahe->setClipLimit(4);
//    cv::Mat dst;
//    clahe->apply(gray, gray);
//    cv::equalizeHist(gray,gray);

    //mask input
    if(!mask.empty()){
        cv::bitwise_and(gray, mask, gray);
    }
    
    cv::GaussianBlur(gray, gray, cv::Size(5,5), m_blur_sigma);
    cartoon = cv::Mat::zeros(gray.size(),gray.type());
    valleys = cv::Mat::zeros(gray.size(),gray.type());
    Pearson::pearson(gray,valleys,m_T1,m_T2);
    
    if(m_dilate)
        cv::dilate(valleys, valleys, cv::Mat(),cv::Point(-1,-1),m_dilate_iter);
    
    int flags=0;
    if (invert)
        flags |= cv::THRESH_BINARY;
    else
        flags |= cv::THRESH_BINARY_INV;
    
    if(m_threshold<0){
        if(mask.empty()){
            double thresh = cv::threshold(gray,mass,m_threshold,255,flags | cv::THRESH_OTSU);
            cv::threshold(gray,mass,thresh,255,flags);
        }else{
            flags  |= cv::THRESH_OTSU;
            cv::threshold(gray,mass,m_threshold,255,flags);
        }
    }else{
        cv::threshold(gray,mass,m_threshold,255,flags);
    }
    
    cv::addWeighted(valleys,1.0,mass,1.0,1.0,cartoon);
    cv::bitwise_not(cartoon,cartoon);//
    return cartoon;
}
