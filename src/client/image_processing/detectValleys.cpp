#include "detectValleys.hpp"
using namespace cv;

// copied from VP. 
void DetectValleys::detectValleys(const cv::Mat &image,cv::Mat &out,const int T1, const int T2){

  //initialise output image... 
  if(out.empty())
    out = cv::Mat::zeros(image.size(),image.type());
  
  const int windowsize = 5;

  //loop through the image.
#pragma omp parallel for collapse(2)
  for(int _x=2;_x<image.cols-2;_x++){
    for(int _y=2;_y<image.rows-2;_y++){
      
      cv::Mat roi = image(cv::Rect(_x-2,_y-2,windowsize,windowsize));

      uint8_t a = roi.at<uint8_t>(0,0);
      uint8_t b = roi.at<uint8_t>(0,1);
      uint8_t c = roi.at<uint8_t>(0,2);
      uint8_t d = roi.at<uint8_t>(0,3); 
      uint8_t e = roi.at<uint8_t>(0,4); 
      uint8_t f = roi.at<uint8_t>(1,0);
      uint8_t g = roi.at<uint8_t>(1,1);
      uint8_t h = roi.at<uint8_t>(1,2);      
      uint8_t i = roi.at<uint8_t>(1,3);
      uint8_t j = roi.at<uint8_t>(1,4);
      uint8_t k = roi.at<uint8_t>(2,0);
      uint8_t l = roi.at<uint8_t>(2,1);
      uint8_t m = roi.at<uint8_t>(2,2);
      uint8_t n = roi.at<uint8_t>(2,3);
      uint8_t o = roi.at<uint8_t>(2,4);
      uint8_t p = roi.at<uint8_t>(3,0);
      uint8_t q = roi.at<uint8_t>(3,1);
      uint8_t r = roi.at<uint8_t>(3,2);
      uint8_t s = roi.at<uint8_t>(3,3);
      uint8_t t = roi.at<uint8_t>(3,4);
      uint8_t u = roi.at<uint8_t>(4,0);
      uint8_t v = roi.at<uint8_t>(4,1);
      uint8_t w = roi.at<uint8_t>(4,2);
      uint8_t x = roi.at<uint8_t>(4,3); 
      uint8_t y = roi.at<uint8_t>(4,4); 

      //vertical
           
       if( ((l - m) > T1) || ((n - m) > T1)) {
	
	if( 
	   ( ((f+k+p+j+o+t) - 2*(h+m+r)) > T2 ) 
	   &&
	   ((g+l+q+i+n+s) - 2*(h+m+r)) > ((f+k+p+h+m+r) - 2*(g+l+q))
	   &&
	   ((g+l+q+i+n+s) - 2*(h+m+r)) > ((h+m+r+j+o+t) - 2*(i+n+s))
	    )
	  {
	    out.at<uint8_t>(_y,_x)=255;
	    continue;
	  }
       
	  }      
      //horizontal
      if( ((h - m) > T1) || ((r - m) > T1)) {
	if(
	   ( ((b+c+d+v+w+x) - 2*(l+m+n)) > T2  )
	   &&
	   ((g+h+i+q+r+s) - 2*(l+m+n)) > ((b+c+d+l+m+n) - 2*(g+h+i))
	   &&
	   ((g+h+i+q+r+s) - 2*(l+m+n)) > ((l+m+n+v+w+x) - 2*(q+r+s))
	   )
	  {
	    out.at<uint8_t>(_y,_x)=255;
	    continue;
	  }
      }
      
      
      //diagonal "\"					
      if( ((g - m) > T1) || ((s - m) > T1)) {
	if(
	   ( ((a+f+b+t+x+y) - 2*(i+m+q)) > T2)
	   &&
	   ((g+l+h + r+s+n) - 2*(i+m+q)) > ((a+f+b+q+m+i)-2*(g+h+l))
	   &&
	   ((g+l+h + r+s+n) - 2*(i+m+q)) > ((y+x+t+q+m+i)-2*(r+s+n))

	   )
	  {
	    out.at<uint8_t>(_y,_x)=255;
	    continue;
	  }
      }
      //diagonal "/"					
      if( ((i - m) > T1) || ((q - m) > T1)) {
	if(
	   ( ((p+v+u+d+j+e) - 2*(g+m+s)) > T2)
	   &&
	   ((q+l+r + i+h+n) - 2*(g+m+s)) > ((p+u+v+g+m+s) -2*(q+l+r))
	   &&
	   ((q+l+r + i+h+n) - 2*(g+m+s)) > ((d+j+e+g+m+s) - 2*(i+h+n))
	 
	   )
	  {
	    out.at<uint8_t>(_y,_x)=255;
	    continue;
	  }
      }
    }
  }
}


/// implement a version similar to sobel
void DetectValleys::detectValley(const cv::Mat &input,cv::Mat &output,const int direction,const int ksize){
    //use opencv filter2d
    
    /* Pearsonkernelt
     
     0 + - + 0
     0 + - + 0
     0 + - + 0
     0 + - + 0
     0 + - + 0
     
     0 0  0 0 0
     1 0 -2 0 1
     1 0 -2 0 1
     1 0 -2 0 1
     0 0  0 0 0
     
     try this 5x5 and 3x3 kernels
     
     1 -2 1
     2 -4 2
     1 -2 1
     
     direction 0 = h, 1=v
     
     */
    
    cv::Mat_<float> v_kernel(3,3);
    v_kernel << 1, -2, 1,
                2, -4, 2,
                1, -2, 1;
    
    cv::Mat_<float> h_kernel(3,3);
    h_kernel << 1,  2,  1,
               -2, -4, -2,
                1,  2,  1;
    
    cv::Mat_<float> v_5x5_kernel(5,5);
    v_5x5_kernel << -1, 2,-2, 2, -1,
                    -1, 2,-2, 2, -1,
                    -1, 2,-2, 2, -1,
                    -1, 2,-2, 2, -1,
                    -1, 2,-2, 2, -1;
    
    cv::Mat_<float> h_5x5_kernel(5,5);
    h_5x5_kernel <<-1,-1,-1,-1,-1,
                   2, 2, 2, 2, 2,
                    -2,-2,-2,-2,-2,
                    2, 2, 2, 2, 2,
                    -1,-1,-1,-1,-1;
   
    cv::Mat_<float> kernel;
    if(ksize==3){
        if(direction==0){
            kernel = h_kernel;
        }else
            kernel = v_kernel;
    }else{
        if(direction==0){
            kernel = h_5x5_kernel;
        }else
            kernel = v_5x5_kernel;
    }
    
    cv::filter2D(input, output, CV_16S, kernel);
    
}


void DetectValleys::detectValleys(const cv::Mat &image, cv::Mat &out, int windowsize, int T1, int T2){

 //initialise output image... 
  if(out.empty())
    out = cv::Mat::zeros(image.size(),image.type());

  int offset  = std::floor(windowsize/2);
#pragma omp parallel for collapse(2)
  for(int x=offset;x<image.cols-offset;x++){
    for(int y=offset;y<image.rows-offset;y++){

      cv::Mat roi = image(cv::Rect(x-offset,y-offset,windowsize,windowsize));
     

      uint8_t cc = roi.at<uint8_t>(offset,offset);
      uint8_t cl = roi.at<uint8_t>(offset,offset-1);
      uint8_t cr = roi.at<uint8_t>(offset,offset+1);
      uint8_t ct = roi.at<uint8_t>(offset-1,offset);
      uint8_t cb = roi.at<uint8_t>(offset+1,offset);

      uint8_t ctl = roi.at<uint8_t>(offset-1,offset-1);
      uint8_t cbl = roi.at<uint8_t>(offset+1,offset-1);
      uint8_t ctr = roi.at<uint8_t>(offset-1,offset+1);
      uint8_t cbr = roi.at<uint8_t>(offset+1,offset+1);
      
      
      //vertical.
      if( cl - cc > T1 || cr - cc > T1){ 
	cv::Mat roi_sum;
          cv::reduce(roi,roi_sum,0,cv::REDUCE_SUM,CV_32S);
	int32_t el = roi_sum.at<int32_t>(0,0);
	int32_t il = roi_sum.at<int32_t>(0,(offset/2));
	int32_t c = roi_sum.at<int32_t>(0,offset);
	int32_t ir = roi_sum.at<int32_t>(0,offset+(offset/2));
	int32_t er = roi_sum.at<int32_t>(0,windowsize-1);
	
	if( 
	   ((el + er - 2*c) > T2) 
	      &&
	      ((il+ir - 2*c) > (el+c - 2*il))
	      &&
	     ((il+ir - 2*c) > (c+er - 2*ir))
	    )
	  {
	    out.at<uint8_t>(y,x)=255;
	    continue;	  
	  } 
      }

      //horizontal
      if( ct - cc > T1 || cb - cc > T1){ 
	cv::Mat roi_sum;
          cv::reduce(roi,roi_sum,1,cv::REDUCE_SUM,CV_32S);
	int32_t el = roi_sum.at<int32_t>(0,0);
	int32_t il = roi_sum.at<int32_t>((offset/2),0);
	int32_t c = roi_sum.at<int32_t>(offset,0);
	int32_t ir = roi_sum.at<int32_t>(offset+(offset/2),0);
	int32_t er = roi_sum.at<int32_t>(windowsize-1,0);
	if( 
	   ((el + er - 2*c) > T2) 
	   	   &&
	      ((il+ir - 2*c) > (el+c - 2*il))
	      &&
	     ((il+ir - 2*c) > (c+er - 2*ir))
	    )
	  {
	    out.at<uint8_t>(y,x)=255;
	    continue;
	  } 
      }
      
      // Diagonal "/"
      if(ctl - cc > T1 || cbr - cc > T2){
	//top left corner
	int val = std::floor((windowsize-2)/2);
	int tlc =0;
	//go along two diagonals
	for(int i=0,j=val;i<=val;i++,j--){
	  tlc+= roi.at<uint8_t>(j,i);
	}
	for(int i=0,j=val-1;i<=val-1;i++,j--){
	  tlc+= roi.at<uint8_t>(j,i);
	}
	
	//bottom right corner
	int brc=0;
	for(int i=(windowsize-1)-val,j=windowsize-1;i<windowsize;i++,j--){
	  brc+= roi.at<uint8_t>(j,i);
	}
	for(int i=(windowsize-1)-(val-1),j=windowsize-1;i<windowsize;i++,j--){
	  brc+= roi.at<uint8_t>(j,i);
	}

	//get center 
	int cc=0;
	for(int i=1,j=windowsize-1;i<windowsize-1;i++,j--){
	  cc+= roi.at<uint8_t>(j,i);
	}
	
	if( (tlc + brc - 2*cc) > T2 ){
	  out.at<uint8_t>(y,x) = 255;
	  continue;	  
	}
	
	}

      //Diagonal "\"
          if(cbl - cc > T1 || ctr - cc > T2){
	
	//top right corner
	int val = std::floor((windowsize-2)/2);
	int trc =0;
	//go along two diagonals
	for(int i=(windowsize-1)-val,j=0;i<windowsize;i++,j++){
	  trc+= roi.at<uint8_t>(j,i);
	}
	for(int i=(windowsize-1)-(val-1),j=0;i<windowsize;i++,j++){
	  trc+= roi.at<uint8_t>(j,i);
	}
		
	//top right inner.
	//int tri=0;
	
	//	val = offset/2;
	//	for(int i=(windowsize-1)-val,j=0;i<windowsize;i++,j++){
	//	  trc+= roi.at<uint8_t>(j,i);
	//	}
	//	for(int i=(windowsize-1)-(val-1),j=0;i<windowsize;i++,j++){
	  //	  trc+= roi.at<uint8_t>(j,i);
	//	}
	
	

	//bottom left corner
	int blc=0;
	for(int i=0,j=(windowsize-1)-val;i<val;i++,j++){
	  blc+= roi.at<uint8_t>(j,i);
	}
	for(int i=0,j=(windowsize-1)-(val-1);i<val-1;i++,j++){
	  blc+= roi.at<uint8_t>(j,i);
	}

	//get center 
	int cc=0;
	for(int i=1,j=1;i<windowsize-2;i++,j++){
	  cc+= roi.at<uint8_t>(j,i);
	}
	
	if( (trc + blc - 2*cc) > T2 ){
	  out.at<uint8_t>(y,x) = 255;
	  continue;	  
	}
	
	  }

    }
  }





}

