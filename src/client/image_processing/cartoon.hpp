#pragma once

#include <opencv2/opencv.hpp>
#include "pearson.hpp"

class Cartoon{
    
public:
    Cartoon();
    
    cv::Mat run(const cv::Mat &img,bool invert=false,const cv::Mat& mask=cv::Mat(),bool use_hsv=false);
    
    int update(const int step, const std::string &type){
        
        if(type =="T1"){
            m_T1+=step;
            return m_T1;
        }
        else if(type == "T2"){
            m_T2+=step;
            return m_T2;
        }
        else if(type=="threshold"){
            m_threshold+=step;
            return m_threshold;
        }
        return -1;
    }
    
    void setT1(const int &T1){m_T1=T1;}
    void setT2(const int &T2){m_T2=T2;}
    void setBlurSigma(const double &sigma){m_blur_sigma=sigma;}
    void enableDilate(const bool dilate=true){m_dilate=dilate;}
    
    void setDilateIter(const int iter){m_dilate_iter=iter;}

    int T1()const {return m_T1;}
    int T2()const {return m_T2;}
    
private:
    int m_T1;
    int m_T2;
    int m_threshold;
    double m_blur_sigma;
    bool m_dilate;
    int m_dilate_iter;
    
};
