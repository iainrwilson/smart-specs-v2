//
//  HistogramSegmenter.hpp
//  cpp_server
//
//  Created by Iain Wilson on 09/04/2018.
//

#ifndef HistogramSegmenter_hpp
#define HistogramSegmenter_hpp

#include <stdio.h>
#include "face_segmentation/FaceSegementerBase.hpp"


class HistogramSegmenter: public FaceSegmenterBase {
  
public:
    HistogramSegmenter();

    cv::Mat run(const cv::Mat &image);
    cv::Mat run(const Face::Ptr &image);
    
};


#endif /* HistogramSegmenter_hpp */
