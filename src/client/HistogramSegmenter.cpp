//
//  HistogramSegmenter.cpp
//  cpp_server
//
//  Created by Iain Wilson on 09/04/2018.
//

#include "HistogramSegmenter.hpp"
#include "FaceEnhance.hpp"

HistogramSegmenter::HistogramSegmenter(){
    m_name = "Histogram";
}

cv::Mat HistogramSegmenter::run(const cv::Mat &image){
    
    // use HSV to segment
        

    
}

cv::Mat HistogramSegmenter::run(const Face::Ptr &face){
  SmartSpecs::Timer::begin("HistogramSegmenter::run");
  auto t = face->current();
  
    int pad = 20;
    cv::Rect _roi = t->roi + cv::Size(pad,pad);
    _roi -= cv::Point(pad/2,pad/2);

  
    cv::Mat image = t->image;

    //generate histogram from current face 
    FaceEnhance fe(face);
    cv::Mat mask = fe.ellipseMask();
  
  
    //forehead 
    cv::Point fhead(t->points[27].x,  t->roi.tl().y + ((t->points[27].y-t->roi.tl().y )/2) );
    cv::Point lcheek( ((t->points[31].x - t->points[2].x)/2) + t->points[2].x,t->points[31].y);
    cv::Point rcheek( ((t->points[14].x - t->points[35].x)/2) + t->points[35].x,t->points[35].y);
    cv::Point chin( t->points[8].x, t->points[57].y + ((t->points[8].y - t->points[57].y)/2));


    cv::Mat yuv;
    cv::cvtColor(image,yuv,cv::COLOR_BGR2YCrCb);
    const int lower_y = 60;
    const int upper_y = 80;
    const int lower_cr= 15;
    const int upper_cr= 25;
    const int lower_cb= 10;
    const int upper_cb= 25;
    cv::Scalar lowerDiff(lower_y,lower_cr,lower_cb);
    cv::Scalar upperDiff(upper_y,upper_cr,upper_cb);

    //create mask
    cv::Mat maskBorder = cv::Mat::zeros(image.size().height+2,image.size().width+2,CV_8UC1);
    
    cv::rectangle(maskBorder,_roi,cv::Scalar(1),1);
   //v::bitwise_not(maskBorder,maskBorder);
    

    // try some flood filling
    cv::floodFill(yuv,maskBorder,fhead,cv::Scalar(),nullptr,lowerDiff,upperDiff,cv::FLOODFILL_FIXED_RANGE | cv::FLOODFILL_MASK_ONLY|4 | ( 255 << 8 ) );
    cv::floodFill(yuv,maskBorder,lcheek,cv::Scalar(),nullptr,lowerDiff,upperDiff,cv::FLOODFILL_FIXED_RANGE | cv::FLOODFILL_MASK_ONLY|4 | ( 255 << 8 ) );
    cv::floodFill(yuv,maskBorder,rcheek,cv::Scalar(),nullptr,lowerDiff,upperDiff,cv::FLOODFILL_FIXED_RANGE | cv::FLOODFILL_MASK_ONLY|4 | ( 255 << 8 ) );

    //coompbine the two
    cv::Rect mask_wb(cv::Point(1,1),mask.size());
    cv::bitwise_or(mask,maskBorder(mask_wb),mask);

    //cv::morphologyEx(mask,mask,cv::MORPH_CLOSE,cv::Mat(),cv::Point(),5);
    cv::medianBlur(mask,mask,9);


  SmartSpecs::Timer::end("HistogramSegmenter::run");

  return mask;
}
