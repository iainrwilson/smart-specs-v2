/*

ASIO test for sending data to python text detection server:

test read from camera and push to remote 

re-use smart specs code.

 */


#include <opencv2/opencv.hpp>
#include <sstream>
#include <stdio.h>
#include <signal.h>
#include "cameras/camera.hpp"
#include "face_detection/FaceDetector.hpp"

#include <atomic>
#include <memory>
#include <chrono>
#include "common/RingBuffer.hpp"

#include "common/timer.hpp"
#include "core/Display.hpp"
#include "core/Capture.hpp"
#include "core/Processor.hpp"
#include "common/Image.hpp"

#include "cmakedefines.h"
#include "core/Engine.hpp"

std::atomic<bool> run(true);

void my_handler(int s){
    printf("Caught signal %d\n",s);
    run = false;
}

using namespace std;

int main(int argc, char** argv){
  
  
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    Engine engine;
//    engine.start();
    
    int count=0;
    for(;;){
        if(!run)
            break;
//        SmartSpecs::Timer::begin("[ Main Loop ]");
        
        engine.thread_run();
        
//        SmartSpecs::Timer::end("[ Main Loop ]");
        count++;

       SmartSpecs::Timer::print();

        if(count == 100){
            SmartSpecs::Timer::reset();
        }
    }
    
    std::cout << "Exiting." <<std::endl;
    
}

