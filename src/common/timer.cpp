#include "timer.hpp"
#include "variables.hpp"
//hack and add a file output

namespace SmartSpecs {

Timer* Timer::m_instance = NULL;
	Timer* Timer::Inst(){
			if(m_instance==NULL){
				{
					std::lock_guard<std::mutex> lock(getMutex());
					if(m_instance == NULL){
						m_instance = new Timer();
					}
				}
			}
			return m_instance;
	}

Timer::Timer()
    {
        std::stringstream ss;
        ss << getenv("SMARTSPECS_HOME") << "/data/recorded/" << FACELEARNER_SEGMENTER_TYPE <<"_"<<FACELEARNER_TRAIN_PYR_LEVEL<< "_"<<FACELEARNER_MASK_PADD<<"_";
        ss << std::chrono::system_clock::now().time_since_epoch().count();
        ss << "_log.txt";
        
        m_outfile.open(ss.str(), std::ofstream::out | std::ofstream::app);
        m_outfile << "Name" << "," << "Duration" <<", TimeStamp" << std::endl;
    }


}
