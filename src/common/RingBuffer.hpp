#ifndef __SMARTSPECS__RINGBUFFER_H
#define __SMARTSPECS__RINGBUFFER_H

#include <stdio.h>
#include <iostream>
#include <mutex>
#include <vector>
#include <atomic>
#include <condition_variable>
#include <memory>

#include <boost/circular_buffer.hpp>

//namespace SmartSpecs{

    /*!
     \brief Ring Buffer class used for image Input and Output.
     
     Utilises boost::circular_buffer
     */
 template<typename Data>
 class RingBuffer{
  
  public:
     
     typedef std::shared_ptr<RingBuffer<Data>> Ptr;
     typedef std::unique_ptr<RingBuffer<Data>> Ptr_unique;
     
     /*!
      \brief constructor buffer is initialised to a default size 2
      \param size buffer size
      */
     RingBuffer(const int& size=2):m_size(size),m_buffer(size){
     }
    
     /*!
      \brief copy constructor
      */
     RingBuffer(const RingBuffer &obj){
         m_size = obj.size;
         m_buffer = obj.buffer;
     }
     
     
     /*!
      \brief push data onto the back of the buffer, if full it overwrites.
      \param data
      \return void
      */
    void push(Data const& data){
      {
          std::unique_lock<std::mutex> lock(m_mutex);
          m_buffer.push_back(data);
      }    
       m_empty.notify_one();
    }
    
    /*!
     \brief pop data from the front of the buffer, if empty thei call blocks untill data is avaliable.
     \param data output from operation
     \return void
     */
    void pop(Data& data){
      std::unique_lock<std::mutex> lock(m_mutex);
      while(m_buffer.empty()){
          m_empty.wait(lock);
      }
      data = m_buffer.front();
      m_buffer.pop_front();
    }
    
     /*!
      \brief pop data from the front of the buffer, wrapper for pop(Data& data)
      \return Data
      */
    Data pop(){
      Data data;
      this->pop(data);
      return data;
    }
     
     /*!
      \brief pop data from the back of the buffer, if empty thei call blocks untill data is avaliable.
      \param data output from operation
      \return void
      */
     void pop_back(Data& data){
         std::unique_lock<std::mutex> lock(m_mutex);
         while(m_buffer.empty()){
             m_empty.wait(lock);
         }
         data = m_buffer.back();
         m_buffer.pop_back();
     }
     
     
     /*!
      \brief pop data from the back of the buffer, wrapper for pop_back(Data& data)
      \return Data
      */
     Data pop_back(){
         Data data;
         this->pop_back(data);
         return data;
     }
     
     /*!
      \brief copy the most recent (head) without popping.  like a peek
      \return Data
      */
     
     Data head(){
         std::unique_lock<std::mutex> lock(m_mutex);
         Data data;
         if(!m_buffer.empty())
             data = m_buffer.back();
         return data;
     }
     
     /*!
      \brief copy the oldest (tail) without popping.  like a peek
      \return Data
      */
     
     Data tail(){
         std::unique_lock<std::mutex> lock(m_mutex);
         while(m_buffer.empty()){
             m_empty.wait(lock);
         }
         return m_buffer.front();
     }
     
     
     /*!
      \brief create an instance of a buffer
      \return Ptr
      */
     static RingBuffer<Data>::Ptr create(const int& size=2){
         return std::make_shared<RingBuffer<Data>>(size);
     }
     
     /*!
      \brief create a unique_ptr instance of a buffer
      \return Ptr
      */
     static RingBuffer<Data>::Ptr_unique create_unique(const int& size=2){
         return RingBuffer::Ptr_unique(new RingBuffer<Data>(size));
     }
     
     bool empty(){
         std::unique_lock<std::mutex> lock(m_mutex);
         return m_buffer.empty();
     }
     
     bool full(){
         std::unique_lock<std::mutex> lock(m_mutex);
         return m_buffer.full();
     }
     
     size_t size(){
         std::unique_lock<std::mutex> lock(m_mutex);
         return m_buffer.size();
     }
     
     typename boost::circular_buffer<Data>::const_iterator begin(){
         std::unique_lock<std::mutex> lock(m_mutex);
         return m_buffer.begin();
     }
     
    typename boost::circular_buffer<Data>::const_reverse_iterator rbegin(){
         std::unique_lock<std::mutex> lock(m_mutex);
         return m_buffer.rbegin();
     }
     
     typename boost::circular_buffer<Data>::const_iterator end(){
         std::unique_lock<std::mutex> lock(m_mutex);
         return m_buffer.end();
     }
     
     std::vector<Data> getSnapshot(){
         std::unique_lock<std::mutex> lock(m_mutex);
         std::vector<Data> data;
         typename boost::circular_buffer<Data>::iterator it = m_buffer.begin();
         for(;it!=m_buffer.end();++it)
             data.push_back(*it);
         return data;
         
     }
     
     
  private:
   int m_size;
    boost::circular_buffer_space_optimized<Data> m_buffer;
    std::mutex m_mutex;
    std::condition_variable m_empty;
  };
//}
#endif //__SMARTSPECS__RINGBUFFER_H
