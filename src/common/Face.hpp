//
//  Face.hpp
//  cpp_server
//
//  Created by Iain Wilson on 22/11/2018.
//

#ifndef Face_hpp
#define Face_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <memory>
#include "skin_segmentation/SkinSegmenter.hpp"
#include "common/Image.hpp"
#include <dlib/image_processing.h>
//#include "TrackedObject.hpp"

class Face{
  /*
   Contains all the details about a face, roi etc.
   */
public:
    typedef std::vector<cv::Point> Features;
    typedef std::shared_ptr<Face> Ptr;
    
    static Face::Ptr create(){
        return std::make_shared<Face>();
    }

    static Face::Ptr create(const cv::Rect &roi, const Image::Ptr &image){
        return std::make_shared<Face>(roi,image);
    }
    
//    static Face::Ptr create(const TrackedObject::Ptr obj, const Image::Ptr &image){
//        auto face = std::make_shared<Face>(obj->roi(),image);
//        face->setId(obj->id());
//        return face;
//    }
    
    Face(){}
    Face(const cv::Rect &roi);
    Face(const cv::Rect &roi, const Image::Ptr &image);

    std::chrono::high_resolution_clock::time_point timestamp() const{ return m_image->timestamp(); }
    
    void setBad(const bool bad=true){m_is_bad = bad;}
    bool isBad()const {return m_is_bad;}
    
    cv::Mat getPyr(const int level){
        if(level>=m_pyr_levels.size())
            return m_pyr_levels[m_pyr_levels.size()-1];
        return m_pyr_levels[level];
    }
    
    cv::Rect roi(const int level){
        // return roi for a specific pyramid level.
        if(level == 0)
            return m_roi;
        int scale = 1<<level;
    
        int x = m_roi.x / scale;
        int y = m_roi.y / scale;
        int w = m_roi.width / scale;
        int h = m_roi.height / scale;
        
        return cv::Rect(x,y,w,h);
    }
    
    /*!
     \brief return the cenre point of the face rect, realtive to the image at specific pyramid level.
    */
    cv::Point centre(const int level=0,const bool use_features=false){
        
        if(m_has_features && use_features){
            if(level==0)
                return m_feature_points[30];
            return m_feature_points[30] / (level*2);
        
        }
        return (roi().br() + roi().tl())*0.5;
    }
    
    cv::Rect roi()const{return m_roi;}
    void setRoi(const cv::Rect roi){m_roi = roi;}

    /*!
     \brief return padded and scaled to pyrleve roi.
     Padding is pergormed first, then scaled.
     */
    cv::Rect roi(const int &pyr,const float& padd){
        float w = m_roi.width*(1.0f+padd);
        float h = m_roi.height*(1.0f+padd);
        int x = m_roi.x - (w - m_roi.width)/2;
        int y = m_roi.y - (h - m_roi.height)/2;
        
        if(x<0) x=0;
        if(y<0) y=0;
        
        if(x+w>=m_image->width())
            w=m_image->width()-x-1;
        if(y+h>=m_image->height())
            h=m_image->height()-y-1;
        
        // return roi for a specific pyramid level.
        if(pyr == 0)
            return cv::Rect(x,y,w,h);
    
        int scale = 1<<pyr;
        x /= scale;
        y /= scale;
        w /= scale;
        h /= scale;
        return cv::Rect(x,y,w,h);
    }
    
    bool isValid();
    
    cv::Rect paddRoi(const float padd=0.2f){
        float w = m_roi.width*(1.0f+padd);
        float h = m_roi.height*(1.0f+padd);
        int x = m_roi.x - (w - m_roi.width)/2;
        int y = m_roi.y - (h - m_roi.height)/2;
        
        if(x<0) x=0;
        if(y<0) y=0;
        
        if(x+w>=m_image->width())
            w=m_image->width()-x-1;
        if(y+h>=m_image->height())
            h=m_image->height()-y-1;
        
        return cv::Rect(x,y,w,h);
    }

    void setFeatures(const Features &features){
        m_feature_points = features;
        m_has_features=true;
    }
    Features features()const {return m_feature_points;}
    
    void draw(cv::Mat &image, const cv::Scalar &colour=cv::Scalar(255,0,0));
    void drawHistogram(cv::Mat &image);
    
    cv::Mat createHistogram(const cv::Mat& image);
    void create2DHistogram();
    void create2DHistogram(const cv::Mat& image);
    
    void create2DHistogram(const cv::Mat& image, cv::Mat &histogram);

    void createFaceMask(const cv::Mat &image);
    bool hasFaceMask()const{return m_has_mask;}
    
    /*!
     \brief return a histogram of the whole face area, cropped but not masked.
     */
    cv::Mat imageHistogram();
    
    
    cv::Mat faceMask(const int pyrLevel=0){
        if(!m_has_mask)
            createFaceMask(m_image->get());
        
        if(pyrLevel == 0)
            return m_face_mask;
        
        cv::Mat mask;
        cv::pyrDown(m_face_mask,mask);
        for(int i=1;i<pyrLevel;++i)
            cv::pyrDown(mask,mask);
        return mask;
    }
    void setFaceMask(const cv::Mat mask){
        m_face_mask=mask;
        m_has_mask=true;
    }
    
    
    
    bool hasHistogram()const{return m_has_histogram;}
    cv::Mat histogram()const{return m_histogram;}
    void setHistogram(const cv::Mat &hist){m_histogram=hist;m_has_histogram=true;}

    double compareHistogram(const cv::Mat &hist);
    double compareHistogram();
    
    
    void segmentSkin(const cv::Mat &image,SkinSegmenterBase::Ptr seg);
    
    void setSkinSegmenter(const SkinSegmenter::Ptr seg){m_skin_seg = seg;}
    
    bool hasSkin()const {return m_has_skin;}
    cv::Mat skin()const {return m_skin;}
    
    cv::Mat forehead(const cv::Mat &image){
        //using features and boundin box.
    //    int points[]={18,28,27}; (-1 on all)
        Face::Features points;
        points.push_back(m_feature_points[17]);
        points.push_back(cv::Point(m_feature_points[27].x,m_roi.y));
        points.push_back(m_feature_points[26]);

        const cv::Point* p[1] = {&points[0]};
        int size = points.size();
        cv::Mat mask= cv::Mat::zeros(image.size(),CV_8UC1);
        cv::fillPoly(mask,p,&size,1,cv::Scalar(255),8);
        return mask;
    }
    
    cv::Mat faceImage(){return m_image->get()(m_roi);}
    
    
    Image::Ptr image() {return m_image;}
    
    /*!
     \brief Create training data from the face mask. Using grabCut to segment.
     */
    bool createTrainingData(const SkinSegmenter::Ptr &skin_seg);
    
    /*!
     \brief Check the quality of the training data
     use ratio of skin to non_skin. if not between 20%-80% - reject.
     */
    bool checkTrainingData();
    arma::mat data(){return m_data;}
    arma::mat labels(){return m_labels;}
    
    int age(){return m_image->age();}
    

    dlib::rectangle dlib_roi(){
        return dlib::rectangle((long)roi().tl().x, (long)roi().tl().y, (long)roi().br().x - 1, (long)roi().br().y - 1);
    }
    
    void setId(const int &id){m_id=id;}
    int id()const{return m_id;}
    
    void setFgdMask(const cv::Mat &fgd){m_fgd_mask=fgd;m_has_fgd_mask=true;}
    cv::Mat fgdMask(const int pyr=0) {return _scaled(m_fgd_mask,pyr);}
    bool hasFgdMask()const {return m_has_fgd_mask;}
    void setBgdMask(const cv::Mat &bgd){m_bgd_mask=bgd;m_has_bgd_mask=true;}
    cv::Mat bgdMask(const int pyr=0) {return _scaled(m_bgd_mask,pyr);}
    bool hasBgdMask()const {return m_has_bgd_mask;}

    
    void setGcMask(const cv::Mat &mask){ m_gc_mask=mask;}
    cv::Mat gcMask() const {return m_gc_mask;}
    
    /*!
     \brief use the background mnask
     */
    cv::Mat maskedFace(){
        cv::Mat img = m_image->get(FACELEARNER_TRAIN_PYR_LEVEL);
        cv::Mat out = cv::Mat::zeros(img.size(),CV_8UC3);
        img.copyTo(out,m_fgd_mask);
        auto _roi = roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
        return out(_roi);
    }
    /*!
     \brief return a cropped, scaled image of the face
     */
    cv::Mat trainigImage(){
        auto _roi = roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
        cv::Mat img = m_image->get(FACELEARNER_TRAIN_PYR_LEVEL);
        return img(_roi);
    }
//    void setIdentity(const std::shared_ptr<Identity> &id){m_identity = id;}
//    std::shared_ptr<Identity> identity()const {return m_identity;}
//
    
    void setType(const std::string& type){m_type=type;}
    std::string type()const{return m_type;}
    
    void save(const std::string &filename);
    
    void setTracked(const bool tracked=true){m_is_tracked=tracked;}
    bool isTracked()const {return m_is_tracked;}
    
    /*!
     \brief calculate the blur of the image - using laplacian
     */
    double calcBlur(){
        
        if(m_has_blur)
            return m_blur;
        
        //calc blur
        cv::Mat lap;
        cv::Laplacian(faceImage(), lap, CV_64F);
        cv::Scalar mean,std;
        cv::meanStdDev(lap,mean,std);
        m_blur = std.val[0]*std.val[0];
        m_has_blur = true;
        return m_blur;
    }
    
    
    
private:
   
    
    inline int getRand(const uint8_t* fm, const size_t &size){
        int index = rand() % size;
        if(fm[index]!=0)
            return getRand(fm,size);
        return index;
    }
    
    /*!
     \brief return a pyramnid level of this image
     */
    cv::Mat _scaled(const cv::Mat &img, const int pyr=0){
        if(pyr==0)
            return img;
        
        cv::Mat out = img;
        for(int i=0;i<pyr;++i){
            cv::pyrDown(out,out);
        }
        return out;
    }
    
    cv::Rect m_roi; // bounding box of face

    Image::Ptr m_image;

    bool m_has_features;
    Features m_feature_points;
    
    bool m_has_mask;
    cv::Mat m_face_mask;
    
    bool m_has_histogram;
    cv::Mat m_histogram;
    
    double m_hist_diff;
    
    bool m_has_skin;
    cv::Mat m_skin;
    
    std::vector<cv::Mat> m_pyr_levels;
    SkinSegmenter::Ptr m_skin_seg;
    
    bool m_has_training_data;
    arma::mat m_data;
    arma::mat m_labels;
    
    int m_id;
    
    bool m_is_bad;
    
    bool m_is_tracked;
    
    cv::Mat m_fgd_mask;
    bool m_has_fgd_mask;
    cv::Mat m_bgd_mask;
    bool m_has_bgd_mask;
    
    cv::Mat m_gc_mask;
    
    std::string m_type;
    
    //temp
    int m_counter;
    
    bool m_has_blur;
    double m_blur;
    
    bool m_has_image_histogram;
    cv::Mat m_image_histogram;
};

//typedef std::shared_ptr<_Face> Face;
//typedef _Face::Features Features;


#endif /* Face_hpp */
