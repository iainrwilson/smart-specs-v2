#include "Thread.hpp"
#include <iostream>

//namespace SmartSpecs {

Thread::Thread(const std::string name):m_run(false),m_suspended(false),m_name(name){
}

Thread::~Thread(){
    this->stop();
}
std::string Thread::name() const {return m_name;}

void Thread::start(){
    std::cout << "[ Thread ] Starting : " <<m_name <<std::endl;
    if(!m_run)
        m_run=true;
    m_thread=std::thread(&Thread::run,this);
}

void Thread::_stop(){
    if(!m_run)
        return;
    m_run.store(false);
    resume();
}

void Thread::suspend(){
    {
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        m_suspended = true;
    }
}

void Thread::resume(){
    {
        std::unique_lock<decltype(m_mutex)> lock(m_mutex);
        m_suspended = false;
    }
    m_cv.notify_one();
}

void Thread::stop(){
    
    if(!m_run)
        return;
    
    m_run.store(false);
    resume();
    join();
}

void Thread::join(){
    m_thread.join();
    // m_thread.timed_join(boost::posix_time::milliseconds(1000));
}

bool Thread::isRunning() const {
    return m_run;
}

void Thread::run(){
    while(1){
        
        {
            //check if suspended
            std::unique_lock<decltype(m_mutex)> lock(m_mutex);
            m_cv.wait(lock,[this]{return !m_suspended;});
        }
        
        // check the exit clause
        if(!m_run.load(std::memory_order_relaxed)){
            break;
        }
        
        std::string title="[ "+name()+" Thread ] run()";
	// SmartSpecs::Timer::begin(title.c_str());
        thread_run();
        //SmartSpecs::Timer::end(title.c_str());
    }
}


//}
