#ifndef BASEPARAMETER_H
#define BASEPARAMETER_H
#include <typeinfo>
#include <list>
#include <string>


class BaseParameter{
    
public:
    typedef std::shared_ptr<BaseParameter> Ptr;
    
    BaseParameter(const std::string &name,  bool enumerate=false){
        m_is_enumerate=enumerate;
        m_name=name;
        m_type="unknown";
        m_hasDescription=false;
    }
    
    BaseParameter(const std::string &name, const std::string &type, bool enumerate=false){
        m_is_enumerate=enumerate;
        m_name=name;
        m_type=type;
        m_hasDescription=false;
    }
    virtual ~BaseParameter() = default;
    
    bool hasDescription()const {return m_hasDescription;}
    std::string description() const {return m_description;}
    void setDescription(const std::string &description){
        m_description=description;
        m_hasDescription=true;
    }
    std::string type()const {return m_type;}
    std::string name()const {return m_name;}
    
    virtual void inc(){};
    virtual void dec(){};

    virtual std::string toString() const {return m_string_value;}
    
    bool isEnumerator()const {return m_is_enumerate;}
    
    virtual void init()=0;
    
protected:
    void setType(const std::string &type){m_type=type;}
    bool m_is_enumerate; //is the parameter just an option list...?
    bool m_hasDescription;
    
    std::string m_name;
    std::string m_type;
    std::string m_description;
    
    std::string m_string_value;
    
    
};
#endif 

