#ifndef PARAMETER_H
#define PARAMETER_H

#include <typeinfo>
#include <list>
#include <string>
#include <functional>
#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>
#include "BaseParameter.hpp"


template <class T>
class Parameter : public virtual BaseParameter{
    
public:
    
    typedef  std::shared_ptr<Parameter<T>> Ptr;
    
    Parameter(const std::string &name,const T &value):
    BaseParameter(name),
    m_has_max(false),
    m_has_min(false),
    m_is_enumerate(false),
    m_has_increment(false),
    m_value(value),
    m_has_callback(false){
    }
    
    Parameter(const std::string &name, const T &value, std::function<void (const T&)> callback):
    BaseParameter(name),
    m_has_max(false),
    m_has_min(false),
    m_is_enumerate(false),
    m_has_increment(false),
    m_value(value),
    m_has_callback(true),
    m_callback(std::move(callback)){
    }
   
    static Ptr create(const std::string &name,const T &value){
        return std::make_shared<Parameter<T>>(name,value);
    }
    
    static Ptr create(const std::string &name, const T &value, std::function<void (const T&)> callback){
        return std::make_shared<Parameter<T>>(name,value,callback);
    }

    
    /////// accessors /////
    
    T value() const {
        boost::mutex::scoped_lock lock(_access);
        return m_value;
    }
    
    std::string toString() const {
        boost::mutex::scoped_lock lock(_access);
        return  boost::lexical_cast<std::string>(m_value);
    }
    
    bool setValue(const T &value){
        boost::mutex::scoped_lock lock(_access);
        
        if(!m_is_enumerate){
            if(value>m_max && m_has_max){
                //	rWarning("Out of range");
                return false;
            }else if(value<m_min && m_has_min){
                //maybe assert?
                //	rWarning("Out of range");
                return false;
            }else{
                _setValue(value);
                return true;
            }
        }else{
            for(auto it=m_list.begin();it!=m_list.end();it++){
                if((*it)==value){
                    _setValue(value);
                    return true;
                }
            }
            //   rWarning("not in enum list: %s", boost::lexical_cast<string>(value).c_str());
            return false;
        }
    }
    
    void setCallback(std::function<void (std::string)> callback){ m_callback = callback;}
    
    bool setEnumeratorValue(const int &index){
        m_value=enumerator(index);
        return true;
    }
    
    bool hasIncrement()const {return m_has_increment;}
    void setIncrement(const T &value) {
        if(!m_has_increment)
            m_has_increment=true;
        m_incValue=value;
    }
    T incrementValue()const {return m_incValue;}
    bool hasMax() const { return m_has_max;}
    T max() const {return m_max;}
    void setMax(const T &max){
        if(!m_has_max)
            m_has_max=true;
        m_max=max;
    }
    bool hasMin() const {return m_has_min;}
    T min() const {return m_min;}
    void setMin(const T &min){
        if(!m_has_min)
            m_has_min=true;
        m_min=min;
    }
    
    std::list<T> getEnumerates() const {return m_list;}
    T enumerator(const int &index){
        typename std::list<T>::iterator it=m_list.begin();
        for(int i=0;i<index;i++)
            it++;
        return (*it);
    }
    
    friend Parameter& operator<<(Parameter& _lhs, const T _rhs){
        _lhs.addEnumerator(_rhs);
        return _lhs;
    }
    
    
    void inc(){
        if(m_is_enumerate){
            _incEnumerator();
            return;
        }
        if(!m_has_increment)
            return;
        if((m_value+m_incValue) > m_max)
            _setValue(m_max);
        else
            _setValue(m_value + m_incValue);
    }
    
    //post incrementor
    T operator++(int){
        inc();
        return m_value;
    }
    
    void dec(){
        if(m_is_enumerate){
            _decEnumerator();
            return;
        }
        if(!m_has_increment)
            return;
        if((m_value-m_incValue) < m_min)
            _setValue(m_min);
        else
            _setValue(m_value - m_incValue);
    }
    
    //post decrementor
    T operator--(int){
        dec();
        return m_value;
    }
    
    
    void addEnumerator(const T &enumerator){
        if(!m_is_enumerate){
            m_list.push_back(m_value);
            m_is_enumerate=true;
            m_list_it = m_list.begin();
        }
        m_list.push_back(enumerator);
    }
    
    void delEnumerator(const T &enumerator){
        for(m_list_it=m_list.begin();m_list_it!=m_list.end();++m_list){
            if((*m_list_it)==enumerator){
                m_list.erase(m_list_it);
                break;
            }
        }
        if(m_list.size==0)
            m_is_enumerate=false;
    }
    
    
    
    int enumerateCount()const {return m_list.size();}
    void init(){}
    
private:
    
    void _incEnumerator(){
        ++m_list_it;
        if(m_list_it==m_list.end())
            m_list_it=m_list.begin();
        _setValue(*m_list_it);
        
    }
    
    void _decEnumerator(){
        if(m_list_it==m_list.begin())
            m_list_it = m_list.end();
        --m_list_it;
        _setValue(*m_list_it);
    }
    
    /*!
     \brief private use for trigerring callback if valid.
     */
    void _setValue(const T& value){
        m_value = value;
        if(m_has_callback)
            m_callback(m_value);
    }
    
    
    bool m_has_max;
    bool m_has_min;
    bool m_is_enumerate;
    bool m_has_increment;
    
    T m_value;
    T m_max;
    T m_min;
    T m_incValue;
    
    std::list<T> m_list;
    typename std::list<T>::iterator m_list_it;
    
    mutable boost::mutex _access;
    
    bool m_has_callback;
    //call back function
    std::function<void (const T&)> m_callback;
    
};

/* spciealisation for strings */
template<>
inline void Parameter<std::string>::dec(){
    if(!m_is_enumerate)
        return;
    _decEnumerator();
}

/* spciealisation for strings */
template<>
inline void Parameter<std::string>::inc(){
    if(!m_is_enumerate)
        return;
    _incEnumerator();
}


/* spciealisation for bool */
template<>
inline void Parameter<bool>::dec(){
        _setValue(!m_value);
}

/* spciealisation for bool */
template<>
inline void Parameter<bool>::inc(){
    _setValue(!m_value);
}


#endif //PARAMETER_H
