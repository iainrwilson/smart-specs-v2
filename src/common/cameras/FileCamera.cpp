#include  "FileCamera.hpp"
#include <memory>
#include <thread>

FileCamera::FileCamera(){
    //  m_filename= "/Users/iain/Downloads/The.Big.Bang.Theory.S12E01.720p.HDTV.x264-KILLERS.mkv";
    std::string home = getenv("HOME");
    
//    m_filename = home+"/Downloads/henry_echo.mp4";
   // m_filename = home+"/Downloads/rugby_faces.mp4";
    m_filename = home+"/Movies/one_plus/VID_20201006_193144.mp4";
    m_loop = true;
    m_grab_loop = std::chrono::high_resolution_clock::now();

    m_epoch_start = 0;
    m_epoch_end = 100070;
    
}

bool FileCamera::connect(){
    m_cam.open(m_filename);
    if(!m_cam.isOpened()){
     std::cout << "[FileCamera::connect] cannot open [" << m_filename <<"]" << std::endl;
     return false;
    }
    
    //report details about the file:
    m_frameRate = m_cam.get(cv::CAP_PROP_FPS);
    m_length = m_cam.get(cv::CAP_PROP_FRAME_COUNT);
    
    if(m_epoch_end > m_length)
        m_epoch_end = m_length-1;
    
    std::cout << "[FileCamera::properties] FPS: "<<m_cam.get(cv::CAP_PROP_FPS) <<std::endl;
    std::cout << "[FileCamera::properties] Frame Count: "<<m_cam.get(cv::CAP_PROP_FRAME_COUNT) <<std::endl;
    std::cout << "[FileCamera::properties] Frame Width: "<<m_cam.get(cv::CAP_PROP_FRAME_WIDTH) <<std::endl;
    std::cout << "[FileCamera::properties] Frame Height: "<<m_cam.get(cv::CAP_PROP_FRAME_HEIGHT) <<std::endl;
    
    
    //hack for epoch
    setFrame(m_epoch_start);
    
    return true;

}

cv::Mat FileCamera::grab(){
  
  /*static bool wasRun = false;
  if(wasRun)
    m_grab_loop = std::chrono::high_resolution_clock::now();
  wasRun = true;
  */
  
  if(m_loop){
    if(m_cam.get(cv::CAP_PROP_POS_FRAMES) >= m_epoch_end)
      m_cam.set(cv::CAP_PROP_POS_FRAMES,m_epoch_start);
  }
  
  cv::Mat frame;
  m_cam >> frame;  
  
 //rotate
//    cv::flip(frame, frame, 1);
    cv::rotate(frame, frame, cv::ROTATE_90_CLOCKWISE);
  auto delta_time = std::chrono::high_resolution_clock::now() - m_grab_loop;
  
  auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(delta_time).count();
  int wait = (int)(1000.0/m_frameRate);
  if(delta<=wait)
    std::this_thread::sleep_for (std::chrono::milliseconds( wait-delta ));
  
  m_grab_loop = std::chrono::high_resolution_clock::now();
  return frame;
}

void FileCamera::setFrame(const int fnumber){
    if(m_cam.get(cv::CAP_PROP_FRAME_COUNT) <= fnumber)
        return;
    m_cam.set(cv::CAP_PROP_POS_FRAMES,fnumber);
}
