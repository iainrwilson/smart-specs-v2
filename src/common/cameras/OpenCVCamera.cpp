//
//  OpenCVCamera.cpp
//  cpp_server
//
//  Created by Iain Wilson on 21/03/2018.
//

#include "OpenCVCamera.hpp"

OpenCVCamera::OpenCVCamera(): m_cam(){
    m_name = "opencv";
 
    m_capture_modes = {
        {640,480,120},  // 0 , width,height,fps
        {1280,720,60},  // 1
        {1920,1080,60}, // 2
        {3840,2160,30}, // 3
        {4096,2160,30}, // 4
        {4224,3156,20}  // 5
    };
    
}


int OpenCVCamera::findCamera(){
    
    //if linux run command = v4l2-ctl --list-devices
    std::string res = exec_cmd("v4l2-ctl --list-devices");
    int cam_no = 0;
    
    //secam string to find.
    const std::string seecam = "See3CAM_130";
    bool has_seecam = false;
    auto lines = split(res,'\n');
    for( auto &line : lines){
        if(line[0] != '\t')
            std::cout << line << std::endl;

        if(line.find(seecam) != std::string::npos){
            has_seecam = true;
            continue;
        }
        
        if(has_seecam){
            //the next line with a \t will hold the camera number
            if(line[0]=='\t'){
                int _len = line.size();
                std::stringstream ss;
                ss << line[_len-1];
                ss >> cam_no;
                std::cout << "Found SeeCam : " << cam_no << std::endl;
                return cam_no;

            }
        }
    }
    //std::cout << res << std::endl;
    return 0;
}

std::vector<std::string> OpenCVCamera::split(std::string strToSplit, char delimeter){
    std::stringstream ss(strToSplit);
    std::string item;
    std::vector<std::string> splittedStrings;
    while (std::getline(ss, item, delimeter))
    {
        splittedStrings.push_back(item);
    }
    return splittedStrings;
}


std::string OpenCVCamera::exec_cmd(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

bool OpenCVCamera::connect(){
    
#ifdef __APPLE__
    m_cam.open(0);
#else
    int cam = findCamera();
    m_cam.open(cam);
    //m_cam.open("/dev/video6");
#endif

 
    //test setting res
    
    // see3cam_139
    //supported
    /*
     640x480   @120
     1280x720  @60
     1920x1080 @60
     3840x2160 @30
     4096x2160 @30
     4224x3156 @20  
  

     BT-35E cam
     640x480 @60
     128x720 @60
     1920x720 @30
     2592x1944 @15
     

    */




    switchMode(2);

  
//  if(!m_cam.set(cv::CAP_PROP_FRAME_WIDTH,1920))
//       std::cout << "Cannot set WIDTH " << std::endl;
//    if(!m_cam.set(cv::CAP_PROP_FRAME_HEIGHT,720))
//        std::cout << "Cannot set HEIGHT " << std::endl;
//    if(!m_cam.set(cv::CAP_PROP_FPS,30))
//        std::cout << "Cannot set FPS " << std::endl;
//

    
    
    getInfo();
    
    if(m_cam.isOpened())
        return true;
    
    return false;
}

void OpenCVCamera::getInfo(){
    
    double fps = m_cam.get(cv::CAP_PROP_FPS);
    double width = m_cam.get(cv::CAP_PROP_FRAME_WIDTH );
    double height = m_cam.get(cv::CAP_PROP_FRAME_HEIGHT);

    std::cout << "FPS: " <<fps << " W: " << width << " H: " << height << std::endl;
    
}

void OpenCVCamera::switchMode(const int mode){
    
    if(!m_cam.set(cv::CAP_PROP_FRAME_WIDTH,m_capture_modes[mode][0]))
        std::cout << "[ OpenCVCamera ] Cannot set WIDTH to " << m_capture_modes[mode][0]<<std::endl;
        if(!m_cam.set(cv::CAP_PROP_FRAME_HEIGHT,m_capture_modes[mode][1]))
        std::cout << "[ OpenCVCamera ] Cannot set HEIGHT to" << m_capture_modes[mode][1]<<std::endl;
    if(!m_cam.set(cv::CAP_PROP_FPS,m_capture_modes[mode][2]))
        std::cout << "[ OpenCVCamera ] Cannot set FPS tp " << m_capture_modes[mode][2]<< std::endl;
    
    getInfo();
    
}

void OpenCVCamera::disconnect(){
    m_cam.release();
}

cv::Mat OpenCVCamera::grab(){
    cv::Mat frame;
    m_cam >> frame;
    return frame;
}
