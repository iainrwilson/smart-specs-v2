#pragma once

/*
 
 Camera factory class
 
 */


#include <opencv2/opencv.hpp>
#include "CameraBase.hpp"
#include "FlyCapCamera.hpp"
#include "FileCamera.hpp"
#include "OpenCVCamera.hpp"
#include "DirCamera.hpp"
#include <memory>
#include "variables.hpp"

struct Camera{
    typedef std::shared_ptr<CameraBase> Ptr;
    static CameraBase::Ptr create(const std::string &type);
};
