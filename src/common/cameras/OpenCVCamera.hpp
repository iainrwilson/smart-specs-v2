//
//  OpenCVCamera.hpp
//  cpp_server
//
//  Created by Iain Wilson on 21/03/2018.
//

#ifndef OpenCVCamera_hpp
#define OpenCVCamera_hpp
#include "CameraBase.hpp"

#include <stdio.h>
#include <opencv2/opencv.hpp>


class OpenCVCamera : public CameraBase{
    
public:
    OpenCVCamera();
    bool connect();
    void disconnect();
    cv::Mat grab();
 
    void switchMode(const int mode);
private:
    
    int findCamera();
    void getInfo();
    std::vector<std::string> split(std::string strToSplit, char delimeter);
    std::string exec_cmd(const char* cmd);  
    
    cv::VideoCapture m_cam;
    
    std::vector<std::vector<int>> m_capture_modes;
};

#endif /* OpenCVCamera_hpp */
