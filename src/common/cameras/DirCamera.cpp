//
//  DirCamera.cpp
//  cpp_server
//
//  Created by Iain Wilson on 06/04/2018.
//
// loop throuhg the LFW dataset


#include "DirCamera.hpp"
#include <chrono>
#include <thread>

DirCamera::DirCamera(): m_index(0){
}

bool DirCamera::connect(){
    //needs filename set
    if(!boost::filesystem::exists(m_filename) || !boost::filesystem::is_directory(m_filename)){
        std::cout << "[DirCamera::connect] " << m_filename <<" doesnot exist" <<std::endl;
        return false;
    }
    if(findFiles(m_extension)==0)
        return false;
    
    return true;
}

cv::Mat DirCamera::grab(){
    if(m_index>=m_files.size())
        return cv::Mat();
    
    m_current_file =m_files[m_index].string();
    //std::cout << "[DirCamera::grab] opening "<< m_current_file<<std::endl;
    cv::Mat out =  cv::imread(m_current_file,-1);
    ++m_index;
    
   // std::this_thread::sleep_for (std::chrono::milliseconds( 1000 ));
    return out;
}

cv::Mat DirCamera::getImage(const boost::filesystem::path &name){
    // Get a file with a specific name //ignore extentions
    
    for(auto &f:m_files){
        if(f.stem() == name.stem())
            return cv::imread(f.string(),-1);
    }
    return cv::Mat();
}

cv::Mat DirCamera::getImage(const std::string &name){
    // Get a file with a specific name //ignore extentions
    return getImage(boost::filesystem::path(name));
}


int DirCamera::findFiles(const std::string &ext){
    
    boost::filesystem::recursive_directory_iterator it(m_filename);
    boost::filesystem::recursive_directory_iterator endit;
    
    while(it != endit){
        if(boost::filesystem::is_regular_file(*it) && it->path().extension() == ext)
            m_files.push_back(it->path());
        ++it;
    }
    
    std::cout << "[DirCamera::findFiles] Found " << m_files.size() << " Files" <<std::endl;
    
    return m_files.size();
}
