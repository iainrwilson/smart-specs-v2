#include "cmakedefines.h"

#ifdef USE_FLYCAP
#include "CameraBase.hpp"
#include "FlyCapture2.h"

#include <memory>

class FlyCapCamera : public CameraBase {
  
public:
   FlyCapCamera();
   ~FlyCapCamera();
   bool connect();
   void disconnect();
   void start(){
    m_cam.StartCapture();
  }
   void stop(){
   m_cam.StopCapture();
  }
   
   cv::Mat grab();
   void capture();
   cv::Mat retrieve();
  
   void cameraStats();
   
   FlyCapture2::Image retrieveRaw();
   static cv::Mat FlyCap2Mat(const FlyCapture2::Image &img);
   
private:
  FlyCapture2::Image m_rawImage;
  FlyCapture2::PGRGuid m_guid;
  FlyCapture2::Camera m_cam;
  int m_serialNumber;
  
  cv::Mat m_image;
  
  static bool errorCheck(const FlyCapture2::Error error);
  static bool errorCheck(const std::string &message,FlyCapture2::Error error);

  std::vector<int> findDevices();
  bool setDevice(const std::string &device);
  
   struct FlyCapDevice{   
    std::string model;
    std::string serial;
    std::string resolution;
    FlyCapture2::PGRGuid guid;
    FlyCapture2::CameraInfo cam;
    FlyCapDevice(){}
    FlyCapDevice(FlyCapture2::CameraInfo camInfo){
     cam = camInfo;
     model = camInfo.modelName;
     serial = camInfo.serialNumber;
     resolution = camInfo.sensorResolution;
    }
  };

  
  std::vector<FlyCapDevice> m_devices;
  FlyCapDevice m_device;
  
};

#endif //USE_FLYCAP
