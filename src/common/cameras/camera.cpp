#include "camera.hpp"

CameraBase::Ptr Camera::create(const std::string &type){
#ifdef USE_FLYCAP
    if(type == FLYCAP) return CameraBase::Ptr(new FlyCapCamera);
#endif //USE_FLYCAP
    if(type == OPENCV) return CameraBase::Ptr(new OpenCVCamera);
    
    if(type == VIDEOFILE) return CameraBase::Ptr(new FileCamera);
    
    if(type == DIRECTORY) return CameraBase::Ptr(new DirCamera);
    
    
    std::cout << "Camera::create camera [" << type <<"] not found" <<std::endl;
    return CameraBase::Ptr(new OpenCVCamera);
    
}

