#pragma once

#include "CameraBase.hpp"
#include <chrono>

class FileCamera : public CameraBase{
  
  public:
    FileCamera();
    bool connect();
    cv::Mat grab();
    
    void setFrame(const int fnumber);
private:
  cv::VideoCapture m_cam;
  int m_length;
  bool m_loop;
  
  std::chrono::high_resolution_clock::time_point m_grab_loop;
};
