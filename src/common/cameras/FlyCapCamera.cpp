
#include "FlyCapCamera.hpp"
#ifdef USE_FLYCAP


FlyCapCamera::FlyCapCamera(){

  m_name = "flycap";
  
  std::vector<int> devices = findDevices();
  if(devices.size()==0)
    std::cout<<"No Cameras found" << std::endl;
  else{
    m_device = m_devices[0];
  }
}

FlyCapCamera::~FlyCapCamera(){
  disconnect();  
}

//bool FlyCapCamera::connect()
//{
//  if(errorCheck(m_cam.Connect(&m_device.guid))){
//    m_connected=false;
//    return false;
//  }
//
// /* if(errorCheck(m_cam.SetVideoModeAndFrameRate(FlyCapture2::VIDEOMODE_640x480Y8,FlyCapture2::FRAMERATE_30)))
//    return false;
//*/
//
//  FlyCapture2::FC2Config BufferFrame;
//  m_cam.GetConfiguration(&BufferFrame);
//  BufferFrame.numBuffers = 10;
//  BufferFrame.grabMode = FlyCapture2::BUFFER_FRAMES;
//  BufferFrame.highPerformanceRetrieveBuffer = true;
//  m_cam.SetConfiguration(&BufferFrame);
//  m_connected=true;
//
//
//  FlyCapture2::CameraInfo camInfo;
//  FlyCapCamera::errorCheck(m_cam.GetCameraInfo(&camInfo));
//  std::cout << "Connected to: " << camInfo.modelName << camInfo.serialNumber << std::endl;
//  m_serialNumber=camInfo.serialNumber;
//
//
//  //test getting protperties
//  FlyCapture2::Property prop;
//  prop.type = FlyCapture2::FRAME_RATE;
//  FlyCapCamera::errorCheck("Checking Frame rate propery",m_cam.GetProperty(&prop));
//
//  float val = prop.absValue;
//  std::cout << "FlyCapCamera::Requested Frame Rate: "<<val<<std::endl;
//
//
//  m_frameRate = val;
//
//  //errorCheck(m_cam.StartCapture());
//  return true;
//}

bool FlyCapCamera::connect(){
    if(m_connected)
        return true;
    
    BusManager busMgr;
    FlyCapture2::Error error;
    errorCheck(busMgr.GetCameraFromIndex(0, &m_guid));
    
    
    if(errorCheck(m_cam.Connect(&m_guid))){
        m_connected=false;
        return false;
    }
    
    CameraInfo camInfo;
    errorCheck(m_cam.GetCameraInfo(&camInfo));
    
    
    if(!errorCheck(m_cam.StartCapture())){
        std::cout << "[FlyCapGrabber::connect()] Connected to: "camInfo.modelName << "::" <<camInfo.serialNumber <<std::endl;
        m_connected=true;
        return true;
    }
    return false;
}

void FlyCapCamera::disconnect()
{
  if(!m_connected)
    return;

  errorCheck(m_cam.Disconnect());
}

void FlyCapCamera::capture()
{
  m_hasData = false;
  errorCheck(m_cam.RetrieveBuffer( &m_rawImage ));
  m_hasData = true;
}

FlyCapture2::Image FlyCapCamera::retrieveRaw()
{
  FlyCapture2::Image rawImage;
  m_hasData = false;
  errorCheck(m_cam.RetrieveBuffer( &rawImage ));
  m_hasData = true;
 
  return rawImage;
}

cv::Mat FlyCapCamera::FlyCap2Mat(const FlyCapture2::Image &rawImage)
{
  FlyCapture2::Image convertedImage;
  FlyCapture2::PixelFormat pixFormat;
  FlyCapture2::BayerTileFormat bayerFormat;
  
  unsigned int rows, cols, stride;
  rawImage.GetDimensions( &rows, &cols, &stride, &pixFormat, &bayerFormat);
  
  //FlyCapture2::PIXEL_FORMAT_BGRU
  if(errorCheck(rawImage.Convert(FlyCapture2::PIXEL_FORMAT_RGB8, &convertedImage ))){
    return cv::Mat();
  }
  
  cv::Mat image(rows,cols,CV_8UC3);
  memcpy(image.data,convertedImage.GetData(),convertedImage.GetDataSize());
  cv::cvtColor(image,image,cv::COLOR_RGB2BGR);

  return image;
}



cv::Mat FlyCapCamera::retrieve()
{
  if(!m_hasData)
    return cv::Mat();
  
  FlyCapture2::Image convertedImage;
  FlyCapture2::PixelFormat pixFormat;
  FlyCapture2::BayerTileFormat bayerFormat;
  
  unsigned int rows, cols, stride;
  m_rawImage.GetDimensions( &rows, &cols, &stride, &pixFormat, &bayerFormat);
  
  //FlyCapture2::PIXEL_FORMAT_BGRU
  if(errorCheck(m_rawImage.Convert(FlyCapture2::PIXEL_FORMAT_RGB8, &convertedImage ))){
    return cv::Mat();
  }
  
  cv::Mat image(rows,cols,CV_8UC3);
  memcpy(image.data,convertedImage.GetData(),convertedImage.GetDataSize());
  cv::cvtColor(image,image,cv::COLOR_RGB2BGR);

  return image;
}


cv::Mat FlyCapCamera::grab()
{
  capture();
  return retrieve();
  
}


void FlyCapCamera::cameraStats()
{
  /* Display camera stats */
  FlyCapture2::CameraStats stats;
  m_cam.GetStats(&stats);
  
  //print stats;
  std::cout << "Images Dropped " << stats.imageDropped << std::endl;
  
  m_cam.ResetStats();
}




/**************** PRIVATE ************************/

std::vector< int > FlyCapCamera::findDevices(){
  std::vector<int> devices;
  FlyCapture2::Error error;
  FlyCapture2::BusManager busMgr;
  unsigned int numCameras;
  error = busMgr.GetNumOfCameras(&numCameras);
  if(errorCheck(error))
    return devices;
  
  if(numCameras<1)
    return devices;
    
  for(int i=0;i<numCameras;i++){
    FlyCapture2::PGRGuid guid;
    FlyCapture2::Camera cam;
    FlyCapture2::CameraInfo camInfo;
    error=busMgr.GetCameraFromIndex(i, &guid);
    if(errorCheck(error))
      return devices;
    
    error=cam.Connect(&guid);
    if(errorCheck(error))
      return devices;
    
    error=cam.GetCameraInfo(&camInfo);
    if(errorCheck(error))
      return devices;
    
    std::cout<< "FlyCap: Found " << camInfo.modelName <<" "<< camInfo.serialNumber<<std::endl;
    
    devices.push_back(camInfo.serialNumber);
    FlyCapDevice dev(camInfo);
    dev.guid = guid;
    m_devices.push_back(dev);
    error = cam.Disconnect();
    if(errorCheck(error))
      return devices;
  }
  return devices;
}






bool FlyCapCamera::errorCheck(const std::string &message, const FlyCapture2::Error error){
   if(error != FlyCapture2::PGRERROR_OK){ 
      std::cout<< message << "FlyCap: " << error.GetType() << "::"<< error.GetDescription() << std::endl;
      //error.PrintErrorTrace();
      return true;
    }
   return false;
}

bool FlyCapCamera::errorCheck(const FlyCapture2::Error error){
   return errorCheck("",error);
}





#endif //USE_FLYCAP
