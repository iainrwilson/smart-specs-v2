//
//  DirCamera.hpp
//  cpp_server
//
//  Created by Iain Wilson on 06/04/2018.
//

#ifndef DirCamera_hpp
#define DirCamera_hpp

#include <stdio.h>
#include "CameraBase.hpp"
#include <boost/filesystem.hpp>

class DirCamera : public CameraBase {
  
public:
    typedef std::shared_ptr<DirCamera> Ptr;
    
    DirCamera();
    bool connect();
    cv::Mat grab();
    std::string currentFilename() const {return m_current_file;}
    cv::Mat getImage(const boost::filesystem::path &name);
    cv::Mat getImage(const std::string &name);
private:
    int findFiles(const std::string &ext);
    
    std::vector<boost::filesystem::path> m_files;
    int m_index;
    std::string m_current_file;
};

#endif /* DirCamera_hpp */
