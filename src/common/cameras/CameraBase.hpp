#pragma once

#include <string>
#include <memory>
#include <opencv2/opencv.hpp>

class CameraBase{
    
public:
    typedef std::shared_ptr<CameraBase> Ptr;
    
    CameraBase(){
        m_hasData=false;
        m_connected = false;
        m_frameRate = 30.0f;
        m_extension = ".jpg";
    };
    virtual ~CameraBase(){disconnect();}
    virtual bool connect()=0;
    virtual void disconnect(){};
    virtual cv::Mat grab()=0;
    float frameRate() const {return m_frameRate;}
    
    void setFilename(const std::string &filename){m_filename = filename;}
    virtual void setFrame(const int fnumber){};
    void setExtension(const std::string &ext){m_extension = ext;}
    const std::string filename() const {return m_filename;}
    
    virtual void switchMode(const int mode){}
    
protected:
    bool m_connected;
    std::string m_name;
    std::string m_deviceId;
    bool m_hasData;
    std::string m_filename;
    float m_frameRate;
    std::string m_extension;
    
    //epoch looping for filecam
    int m_epoch_start;
    int m_epoch_end;

};
