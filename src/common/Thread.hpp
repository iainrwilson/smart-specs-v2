#ifndef THREAD_H
#define THREAD_H

#include <thread>
#include <mutex>
#include <string>
#include <atomic>
#include "timer.hpp"
#include <condition_variable>
//#include <opencv2/opencv.hpp>
//#include "SmartSpecsIncludes.h"

//namespace SmartSpecs {
    /*!
     \brief Thread base class
     
     Inheriting from this will enable threading, must implement thread_run(), which is called continously upon starting the thread.
     */
    class Thread{

	 public:
        Thread() = delete;
        
        /*!
         \brief constructor, initialised the thread members
         \param name a name for the thread
         */
        Thread(const std::string name);
        
        /*!
         \brief destructor, stops the thread on destruction (if running)
         */
        virtual ~Thread();
        
        /*!
         \brief get the name assigned to this thread
         \return string
         */
        std::string name() const;
        
        /*!
         \brief start the thread
         \return void
         */
        virtual void start();
        
        /*!
         \brief stop the thread
         \return void
         */
        virtual void stop();
        
        
        /*!
         \brief suspend / pause the thread.
         \return void
         */
        virtual void suspend();
        
        /*!
         \brief resume from suspension
         \return void
         */
        virtual void resume();
        
        /*!
         \brief stop the thread
         \return void
         */
        virtual void _stop();
        
        /*!
         \brief join the thread
         \return void
         */
        void join();
        
        /*!
         \brief is this thread running?
         \return True if running
         */
        bool isRunning() const;
        
      
        virtual void thread_run()=0;

    protected:
        std::atomic<bool> m_run;
        bool m_suspended; 
     
    private:
        /*!
         \brief main thread loop, called by the std::Thread, this then calls thread_run()
         \return void
         */
        void run();
        
        std::thread m_thread;
        std::string m_name;
        
        std::condition_variable m_cv;
        std::mutex m_mutex;
        
	};
//}
#endif
