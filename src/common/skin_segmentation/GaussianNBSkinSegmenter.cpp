//  GuassianNBSkinSegmenter.cpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#include "common/timer.hpp"
#include "GaussianNBSkinSegmenter.hpp"
#include "mlpack/core/cv/k_fold_cv.hpp"
#include "mlpack/core/cv/metrics/accuracy.hpp"
#include "mlpack/core/cv/metrics/precision.hpp"
#include "mlpack/core/cv/metrics/recall.hpp"
#include "mlpack/core/cv/metrics/f1.hpp"


GaussianNBSkinSegmenter::GaussianNBSkinSegmenter():SkinSegmenterBase("NaiveBayes"){
    
}

cv::Mat GaussianNBSkinSegmenter::run(const cv::Mat &in){
    SmartSpecs::Timer::begin("GuassianNBSkinSegmenter::run");

    if(in.empty())
        return cv::Mat();
    
    //convert image to armadillo format
    cv::Mat image(in.clone());
    image = image.t();
    
    convertColor(image,image,FACELEARNER_COLOURSPACE);
    auto arma_mat = opencv2arma(image);
    
    arma::Row<size_t> predictions;
    arma::mat probabilities;
    m_clf.Classify(arma_mat, predictions, probabilities);
    
    arma::mat _mat = arma::conv_to<arma::mat>::from(predictions);
    cv::Mat out(image.size(),CV_64F,_mat.memptr());
    out.convertTo(out, CV_8U,255);
    
    SmartSpecs::Timer::end("GuassianNBSkinSegmenter::run");

     return out.t();
    
}


void GaussianNBSkinSegmenter::train(const arma::mat &images, const arma::Row<size_t> &labels){
    
    
    SmartSpecs::Timer::begin("GuassianNBSkinSegmenter::train_images");
    std::cout <<"[GuassianNBSkinSegmenter] ----- Training ------" <<std::endl;

    const size_t numClasses = 2;
    m_clf = mlpack::naive_bayes::NaiveBayesClassifier<> (images, labels, numClasses);

    
    arma::Row<size_t> predictions;
    m_clf.Classify(images, predictions);
    const size_t correct = arma::accu(predictions == labels);
    std::cout << "[GuassianNBSkinSegmenter] Training Accuracy: " << (double(correct) / double(labels.n_elem)) << std::endl;

    SmartSpecs::Timer::end("GuassianNBSkinSegmenter::train_images");
    
    
    
//    SmartSpecs::Timer::begin("[ GuassianNBSkinSegmenter ] train_images()");
//
//    const size_t numClasses = arma::max(arma::max(labels))+1;
//    arma::Row<size_t> _labels = arma::conv_to<arma::Row<size_t>>::from(labels.row(0));
//
//    m_clf = mlpack::naive_bayes::NaiveBayesClassifier<> (images, labels, numClasses);
//
//    arma::Row<size_t> predictions;
//    arma::mat probabilities;
//    m_clf.Classify(images, predictions, probabilities);
//
//    size_t correct = 0;
//    for (size_t i = 0; i < images.n_cols; ++i)
//        if (predictions[i] == labels[i])
//            ++correct;
//
//    // Print number of correct points.
//    std::cout << double(correct) / double(images.n_cols) * 100 << "%% "
//    << "correct on training set (" << correct << " / "
//    << images.n_cols << ")." << std::endl;
//
//    SmartSpecs::Timer::end("[ GuassianNBSkinSegmenter ] train_images()");

}

bool GaussianNBSkinSegmenter::save(const std::string &file){
    if(file == std::string()){
        //use the data dir;
        std::string home = getenv("SMARTSPECS_HOME");
        std::string dataDir = home+"/data/models/";
        return mlpack::data::Save(dataDir+filename(),"model",m_clf);
    }
   return mlpack::data::Save(file+filename(),"model",m_clf);
}

bool GaussianNBSkinSegmenter::load(const std::string &file){
    if(file == std::string()){
        //use the data dir;
        std::string home = getenv("SMARTSPECS_HOME");
        std::string dataDir = home+"/data/models/";
        return mlpack::data::Load(dataDir+filename(),"model",m_clf);
    }
    return mlpack::data::Load(file+filename(),"model",m_clf);

}
