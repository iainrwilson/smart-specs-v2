
//
//  GuassianNBSkinSegmenter.hpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#ifndef GuassianNBSkinSegmenter_hpp
#define GuassianNBSkinSegmenter_hpp

#include <stdio.h>
#include "SkinSegmenter.hpp"
#include <mlpack/core.hpp>
#include <mlpack/prereqs.hpp>
#include <mlpack/methods/naive_bayes/naive_bayes_classifier.hpp>
class GaussianNBSkinSegmenter : public SkinSegmenterBase{
  
public:
    GaussianNBSkinSegmenter();
    
    void train(const arma::mat &images, const arma::Row<size_t> &labels);
    cv::Mat run(const cv::Mat &image);
   
    bool save(const std::string &file = std::string());
    bool load(const std::string &file = std::string());
private:
    
    mlpack::naive_bayes::NaiveBayesClassifier<> m_clf;
    arma::mat m_data;
};


#endif /* GuassianNBSkinSegmenter_hpp */
