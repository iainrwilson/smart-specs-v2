//
//  DecisionTreeSkinSegmenter.hpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#ifndef DecisionTreeSkinSegmenter_hpp
#define DecisionTreeSkinSegmenter_hpp

#include <stdio.h>
#include "SkinSegmenter.hpp"
#include <mlpack/core.hpp>
#include <mlpack/prereqs.hpp>
#include <mlpack/methods/decision_tree/decision_tree.hpp>
class DecisionTreeSkinSegmenter : public SkinSegmenterBase{
  
public:
    DecisionTreeSkinSegmenter();
    
    void train();
    void train(const arma::mat &images, const arma::Row<size_t> &labels);
    cv::Mat run(const cv::Mat &image);
   
    bool save(const std::string &filename = std::string());
    bool load(const std::string &filename = std::string());
private:
    
    mlpack::tree::DecisionTree<> m_tree;
    arma::mat m_data;
};


#endif /* DecisionTreeSkinSegmenter_hpp */
