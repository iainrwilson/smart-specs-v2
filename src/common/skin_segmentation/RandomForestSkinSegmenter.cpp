//
//  RandomForestSkinSegmenter.cpp
//  cpp_server
//
//  Created by Iain Wilson on 30/11/2018.
//
#include "common/timer.hpp"

#include "RandomForestSkinSegmenter.hpp"

#include "mlpack/core.hpp"
#include "mlpack/methods/random_forest/random_forest.hpp"
#include "mlpack/methods/decision_tree/random_dimension_select.hpp"
#include "mlpack/core/cv/k_fold_cv.hpp"
#include "mlpack/core/cv/metrics/accuracy.hpp"
#include "mlpack/core/cv/metrics/precision.hpp"
#include "mlpack/core/cv/metrics/recall.hpp"
#include "mlpack/core/cv/metrics/f1.hpp"

RandomForestSkinSegmenter::RandomForestSkinSegmenter():SkinSegmenterBase("RandomForest"){}


void RandomForestSkinSegmenter::train(const arma::mat &images, const arma::Row<size_t> &labels){
    SmartSpecs::Timer::begin("RandomForestSkinSegmenter::train_images");
    std::cout <<"[RandomForestSkinSegmenter] ----- Training ------" <<std::endl;

    const size_t numClasses = 2;
    const size_t minimumLeafSize = 20;
    const size_t numTrees = 10;
    m_rf = mlpack::tree::RandomForest<mlpack::tree::GiniGain, mlpack::tree::RandomDimensionSelect>(images, labels,
                                                       numClasses, numTrees, minimumLeafSize);
    
    arma::Row<size_t> predictions;
    m_rf.Classify(images, predictions);
    const size_t correct = arma::accu(predictions == labels);
    std::cout << "[RandomForestSkinSegmenter] Training Accuracy: " << (double(correct) / double(labels.n_elem)) << std::endl;

    SmartSpecs::Timer::end("RandomForestSkinSegmenter::train_images");
}

cv::Mat RandomForestSkinSegmenter::run(const cv::Mat &in){
    SmartSpecs::Timer::begin("RandomForestSkinSegmenter::run");
    if(in.empty())
        return cv::Mat();
    
    //convert image to armadillo format
    cv::Mat image(in.clone());
    image = image.t();
    
    convertColor(image, image,FACELEARNER_COLOURSPACE);
    
    auto arma_mat = opencv2arma(image);
    arma::Row<size_t> predictions;
    arma::mat probabilities;
    m_rf.Classify(arma_mat, predictions, probabilities);
// /    arma::mat  _mat = probabilities.row(1);

    arma::mat _mat = arma::conv_to<arma::mat>::from(predictions);
    cv::Mat out(image.size(),CV_64F,_mat.memptr());
    out.convertTo(out, CV_8U,255);
//    out.setTo(255,out == 1);
    
    
    
    SmartSpecs::Timer::end("RandomForestSkinSegmenter::run");
    return out.t();
}

bool RandomForestSkinSegmenter::save(const std::string &file){
    if(file == std::string()){
        //use the data dir;
        std::string home = getenv("SMARTSPECS_HOME");
        std::string dataDir = home+"/data/models/";
        return mlpack::data::Save(dataDir+filename(),"model",m_rf);
    }
   return mlpack::data::Save(file+filename(),"model",m_rf);
}

bool RandomForestSkinSegmenter::load(const std::string &file){
    if(file == std::string()){
        //use the data dir;
        std::string home = getenv("SMARTSPECS_HOME");
        std::string dataDir = home+"/data/models/";
        return mlpack::data::Load(dataDir+filename(),"model",m_rf);
    }
    return mlpack::data::Load(file+filename(),"model",m_rf);

}
