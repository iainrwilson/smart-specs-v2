//
//  HistogramSkinSegmenter.cpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#include "HistogramSkinSegmenter.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include "common/timer.hpp"

HistogramSkinSegmenter::HistogramSkinSegmenter():SkinSegmenterBase("Histogram"){
    
    m_train_image_dir = m_data_dir+"/skin_segmentation/lfw_funneled";
    m_train_gt_image_dir = m_data_dir+"/skin_segmentation/parts_lfw_funneled_gt_images";

}

cv::Mat HistogramSkinSegmenter::run(const cv::Mat &image){
    return run(image,m_histogram);
}

cv::Mat HistogramSkinSegmenter::run(const cv::Mat &image,const cv::Mat &hist){
    SmartSpecs::Timer::begin("HistogramSkinSegmenter::run");
    
    //perfo/Users/iain/Dropbox/docs/ORG/Msc/project/experiments/face_detection/src/core/DisplayOpenCV.cpprm simple back projection
    if(image.empty())
        return cv::Mat();
    //TODO run on dataset and compare to the ground truth data (implement this in the base class?)
    cv::Mat hsv,backproject;
    cv::cvtColor(image, hsv,cv::COLOR_BGR2HSV);
    float hranges[] = { 0, 180 };
    float sranges[] = { 0, 256 };
    float vranges[] = { 0, 256 };
    const float* ranges[] = { hranges, sranges, vranges };
    int channels[] = {0, 1,2};
    
    cv::calcBackProject(&hsv,1,channels,hist,backproject,ranges);
    
    //std::cout <<backproject << std::endl;
    cv::equalizeHist(backproject,backproject);
    SmartSpecs::Timer::end("HistogramSkinSegmenter::run");
    return backproject;
}

void HistogramSkinSegmenter::train(const arma::mat &images, const arma::Row<size_t> &labels){
    SmartSpecs::Timer::begin("HistogramSkinSegmenter::train");

    // just pull out the skin (label 1=skin)
//    cv::Mat::zeros(1,labels.size(),CV_8UC3);
    std::vector<cv::Vec3b> data;
    for(int i=0;i<labels.size();++i){
        if(labels.at(i)==0)// not skin
            continue;
        uint8_t b = static_cast<uint8_t>(images.at(0,i));//B
        uint8_t g = static_cast<uint8_t>(images.at(1,i));//G
        uint8_t r = static_cast<uint8_t>(images.at(2,i));//R
     //   std::cout << "B:"<< (int)b <<" G:"<< (int)g <<" R:" << (int)r<<std::endl;
        data.push_back(cv::Vec3b(b,g,r));
    }
    
    cv::Mat image(1,data.size(),CV_8UC3);
    std::copy(data.begin(),data.end(),image.ptr<cv::Vec3b>());
    
    m_histogram = create2DHistogram(image,cv::Mat());
    SmartSpecs::Timer::end("HistogramSkinSegmenter::train");

}







void HistogramSkinSegmenter::train(){
    
    /* if traingin already run .. load */
    
//    std::string trained = m_data_dir+"/avg_histogram.bmp";
//    cv::Mat hist = cv::imread(trained);
//    if(!hist.empty()){
//        m_histogram = hist;
//        return;
//    }
    
    /*Use the lfw data set*/
    const std::string train_file = m_data_dir+"/skin_segmentation/parts_train.txt";
    
    std::cout << "[HistogramSkinSegmenter::train()] Loading " << train_file <<std::endl;
    std::ifstream in(train_file);
    std::string line;
    std::vector<std::string> images;
    while(std::getline(in,line)){
        //split on space:
        std::string name = line.substr(0,line.find_first_of(" "));
        std::string dir = name;
        std::string fname = name+"_"+line.substr(line.find_first_of(" ")+1);
        images.push_back(fname);
    }
    
    
    std::cout << "[HistogramSkinSegmenter::train()] generating Histograms" <<std::endl;
    
    // make some histograms
    std::vector<cv::Mat> histograms;

    int hbins = 180;
    int sbins = 256;
    cv::Mat histogram = cv::Mat::zeros(hbins, sbins, CV_32SC1);
    for(auto &i:images){
        
        std::cout << "[HistogramSkinSegmenter::train()] processing: "<<i<<std::endl;
        
        std::string mask_file = m_train_gt_image_dir+"/"+i+".ppm";
        std::string image_file = m_train_image_dir+"/"+i+".jpg";
        
//        std::cout << "[HistogramSkinSegmenter::train()] processing: "<<mask_file<<std::endl;
//        std::cout << "[HistogramSkinSegmenter::train()] processing: "<<image_file<<std::endl;

        // green channel is the face, blue=background and red=hair
        cv::Mat mask = cv::imread(mask_file);
        cv::Mat image = cv::imread(image_file);
        
        if(image.empty() || mask.empty())
            continue;
        
        cv::Mat channel[3];
        cv::split(mask,channel);

        //cv::Mat histogram = create2DHistogram(image,channel[1]);
        create2DHistogram(image,channel[1],histogram,hbins,sbins);
//        histograms.push_back(histogram);
    }
    
    //normalise the accualiated histogram: // convert to 32F
    cv::normalize(histogram,histogram,0,255,cv::NORM_MINMAX,CV_32F,cv::Mat());
    
    m_histogram = histogram;
    
}


cv::Mat HistogramSkinSegmenter::createHistogram(const cv::Mat &image,const cv::Mat &mask){
    
    // create a hisrogram from the ROI.
    cv::Mat hsv,hue;
    cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
    //threshold
//    cv::inRange(hsv,cv::Scalar(0.0,60.,32.),cv::Scalar(180.,255.,255.),mask);
    int ch[] = {0, 0};
    hue.create(hsv.size(), hsv.depth());
    cv::mixChannels(&hsv, 1, &hue, 1, ch, 1);
    
    int hsize=16; //increase this... ! ? 
    float hranges[] = {0,180};
    const float* phranges = hranges;
    cv::Mat histogram;
    cv::calcHist(&hue, 1, 0, mask, histogram, 1, &hsize, &phranges);
    cv::normalize(histogram, histogram, 0, 255, cv::NORM_MINMAX);
    return histogram;
}
cv::Mat HistogramSkinSegmenter::create2DHistogram(const cv::Mat &image,const cv::Mat &mask,const int hbins,const int sbins){
    cv::Mat hist;
    create2DHistogram(image,mask,hist,hbins,sbins);
    return hist;
}

void HistogramSkinSegmenter::create2DHistogram(const cv::Mat &image,const cv::Mat &mask,cv::Mat &histogram,const int hbins,const int sbins){
    
    //accumalative approach.
    // create a hisrogram from the ROI.
    cv::Mat hsv,hue;
    cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
    
    
    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    //    int hbins = 30, sbins = 32;
    int histSize[] = {hbins, sbins,sbins};
    // hue varies from 0 to 179, see cvtColor
    float hranges[] = { 0, 180 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float sranges[] = { 0, 256 };
    float vranges[] = { 0, 256 };
    const float* ranges[] = { hranges, sranges,vranges};
    cv::Mat hist;
    // we compute the histogram from the 0-th and 1-st channels
    int channels[] = {0, 1, 2};
    calcHist( &hsv, 1, channels, mask, // do not use mask
             histogram, 2, histSize, ranges,
             true, // the histogram is uniform
             true );
    
//    std::cout << histogram.size() <<std::endl;
    
    cv::normalize(histogram, histogram, 0, 1, cv::NORM_MINMAX,CV_32F,cv::Mat());
//    return hist;

}

bool HistogramSkinSegmenter::load(const std::string &file){
    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/models/";
    cv::Mat hist = cv::imread(dataDir+filename()+".exr",-1);
    if(hist.empty())
        return false;
    m_histogram = hist;
    return true;
}


bool HistogramSkinSegmenter::save(const std::string &file){
    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/models/";
    cv::imwrite(dataDir+filename()+".exr", m_histogram);
    return true;
}
