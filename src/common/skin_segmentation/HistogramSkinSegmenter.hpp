//
//  HistogramSkinSegmenter.hpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#ifndef HistogramSkinSegmenter_hpp
#define HistogramSkinSegmenter_hpp

#include <stdio.h>
#include "SkinSegmenter.hpp"
#include <opencv2/opencv.hpp>
#include "variables.hpp"



class HistogramSkinSegmenter : public SkinSegmenterBase{
    
public:
    HistogramSkinSegmenter();
    
    cv::Mat run(const cv::Mat &image);
    cv::Mat run(const cv::Mat &image,const cv::Mat &hist);
    void train();
    
    void train(const arma::mat &images, const arma::Row<size_t> &labels);
    
    static cv::Mat create2DHistogram(const cv::Mat &image,const cv::Mat &mask,const int hbins=H_BINS,const int sbins=S_BINS);
    static void create2DHistogram(const cv::Mat &image,const cv::Mat &mask,cv::Mat &histogram,const int hbins=H_BINS,const int sbins=S_BINS);
    
    cv::Mat histogram() const {return m_histogram;}
    void setHistogram(const cv::Mat hist){m_histogram=hist;}
    
    bool save(const std::string &file = std::string());
    bool load(const std::string &file = std::string());
    
private:
    inline cv::Mat createHistogram(const cv::Mat &image,const cv::Mat &mask);
    cv::Mat m_histogram;
};

#endif /* HistogramSkinSegmenter_hpp */
