//
//  RandomForestSkinSegmenter.hpp
//  cpp_server
//
//  Created by Iain Wilson on 30/11/2018.
//

#ifndef RandomForestSkinSegmenter_hpp
#define RandomForestSkinSegmenter_hpp

#include <stdio.h>
#include "SkinSegmenter.hpp"
#include "mlpack/methods/random_forest/random_forest.hpp"
#include "mlpack/methods/decision_tree/random_dimension_select.hpp"


class RandomForestSkinSegmenter : public SkinSegmenterBase {

public:
    RandomForestSkinSegmenter();
    
    cv::Mat run(const cv::Mat &image);
    void train(const arma::mat &images, const arma::Row<size_t> &labels);
  
    bool save(const std::string &file = std::string());
    bool load(const std::string &file = std::string());
    
private:
    mlpack::tree::RandomForest<mlpack::tree::GiniGain, mlpack::tree::RandomDimensionSelect> m_rf;

    
};
#endif /* RandomForestSkinSegmenter_hpp */
