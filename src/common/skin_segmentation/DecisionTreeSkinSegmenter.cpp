//
//  DecisionTreeSkinSegmenter.cpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//
#include "common/timer.hpp"

#include "DecisionTreeSkinSegmenter.hpp"
#include "cameras/DirCamera.hpp"

DecisionTreeSkinSegmenter::DecisionTreeSkinSegmenter():SkinSegmenterBase("DecisionTree"){
    
}


cv::Mat DecisionTreeSkinSegmenter::run(const cv::Mat &in){
    

    
    if(in.empty())
        return cv::Mat();
    
    SmartSpecs::Timer::begin("[ DecisionTreeSkinSegmenter ] run");
    
    //convert image to armadillo format
    cv::Mat image(in.clone());    //opencv's mat, already transposed.
    //segments on HSV
    convertColor(image,image,FACELEARNER_COLOURSPACE);
    
    
    //label image, split into channels
//
//    image= _image.t();
    auto arma_mat = opencv2arma(image);
    
    arma::Row<size_t> predictions;
    arma::mat probabilities;
    m_tree.Classify(arma_mat, predictions, probabilities);
    //std::cout << predictions <<std::endl;
    
    //return predictions...
//    arma::mat  _mat = probabilities.row(1);
    
    arma::mat _mat = arma::conv_to<arma::mat>::from(predictions);
    cv::Mat out(image.size(),CV_64F,_mat.memptr());
    out.convertTo(out, CV_8U,255);
    //out.setTo(255,out == 1);
//    out.setTo(255,out == 0);
    
    
    SmartSpecs::Timer::end("[ DecisionTreeSkinSegmenter ] run");
    return out;
}


void DecisionTreeSkinSegmenter::train(const arma::mat &images, const arma::Row<size_t> &labels){
    
    SmartSpecs::Timer::begin("[ DecisionTreeSkinSegmenter ] train_images()");
    
   // std::cout <<" ----- Training ------" <<std::endl;
    const size_t numClasses = arma::max(arma::max(labels)) +1;
    float minGainSplit = 0.001;
    int minLeafSize = 20;
    
    arma::Row<size_t> _labels = arma::conv_to<arma::Row<size_t>>::from(labels.row(0));
    
    
    m_tree = mlpack::tree::DecisionTree<>(images, _labels, numClasses, minLeafSize, minGainSplit);
    arma::Row<size_t> predictions;
    arma::mat probabilities;
    m_tree.Classify(images, predictions, probabilities);
    const size_t correct = arma::accu(predictions == labels);
    std::cout << "[ DecisionTreeSkinSegmenter ] Training Accuracy: " << (double(correct) / double(labels.n_elem)) << std::endl;

//    size_t correct = 0;
//    for (size_t i = 0; i < images.n_cols; ++i)
//        if (predictions[i] == _labels[i])
//            ++correct;
//    // Print number of correct points.
//    std::cout << double(correct) / double(images.n_cols) * 100 << "%% "
//    << "correct on training set (" << correct << " / "
//    << images.n_cols << ")." << std::endl;
//
    SmartSpecs::Timer::end("[ DecisionTreeSkinSegmenter ] train_images()");
}


void DecisionTreeSkinSegmenter::train(){
    SmartSpecs::Timer::begin("[ DecisionTreeSkinSegmenter ] train()");

    // all the data needs formatting for the mlpack
    arma::mat mat;
    arma::mat trainingSet;
    arma::Row<size_t> labels;
    mlpack::data::DatasetInfo info;

    bool loaded = mlpack::data::Load(m_data_dir+"/skin_segmentation/HSV_Skin_NonSkin.txt",trainingSet);
    //bool loaded = mlpack::data::Load(m_data_dir+"/skin_segmentation/Skin_NonSkin.txt",trainingSet);
    if(!loaded)
        std::cout << "[DecisionTreeSkinSegmenter::train] error loading data"<<std::endl;
    
    labels = arma::conv_to<arma::Row<size_t>>::from(trainingSet.row(trainingSet.n_rows - 1));
    trainingSet.shed_row(trainingSet.n_rows - 1);
    labels-=1;
    std::cout << labels.at(100) <<std::endl;
    const size_t numClasses = arma::max(arma::max(labels)) +1;
    float minGainSplit = 0.001;
    int minLeafSize = 20;
    

    
    m_tree = mlpack::tree::DecisionTree<>(trainingSet,  labels, numClasses, minLeafSize, minGainSplit);
    arma::Row<size_t> predictions;
    arma::mat probabilities;
    m_tree.Classify(trainingSet, predictions, probabilities);
    
    size_t correct = 0;
    for (size_t i = 0; i < trainingSet.n_cols; ++i)
        if (predictions[i] == labels[i])
            ++correct;
    
    // Print number of correct points.
    std::cout << double(correct) / double(trainingSet.n_cols) * 100 << "%% "
    << "correct on training set (" << correct << " / "
    << trainingSet.n_cols << ")." << std::endl;
    
    SmartSpecs::Timer::end("[ DecisionTreeSkinSegmenter ] train()");

}

bool DecisionTreeSkinSegmenter::save(const std::string &file){
    std::string dir = file;
    if(file == std::string()){
        //use the data dir;
        std::string home = getenv("SMARTSPECS_HOME");
        std::string dataDir = home+"/data/models/";
        dir = dataDir;
    }
    
    boost::filesystem::path _ddir(dir);
    boost::filesystem::path _fname(filename());
    auto path = _ddir / _fname;
    return mlpack::data::Save(path.string(),"model",m_tree);
}

bool DecisionTreeSkinSegmenter::load(const std::string &file){
    std::string dir = file;
    if(file == std::string()){
        //use the data dir;
        std::string home = getenv("SMARTSPECS_HOME");
        std::string dataDir = home+"/data/models/";
        dir = dataDir;
    }
    
    boost::filesystem::path _ddir(dir);
    boost::filesystem::path _fname(filename());
    auto path = _ddir / _fname;
    return mlpack::data::Load(path.string(),"model",m_tree);
}
