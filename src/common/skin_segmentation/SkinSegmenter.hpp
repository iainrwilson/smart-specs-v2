//
//  SkinSegmenter.hpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#ifndef SkinSegmenter_hpp
#define SkinSegmenter_hpp

#include <stdio.h>
#include <memory>
#include <string>
#include <opencv2/opencv.hpp>
#include <mlpack/core.hpp>
#include "variables.hpp"




/*
 Base class for all the Segmenters
 */
class SkinSegmenterBase{
public:
    typedef std::shared_ptr<SkinSegmenterBase> Ptr;
    SkinSegmenterBase(const std::string &name):m_name(name){
        std::string home = getenv("SMARTSPECS_HOME");
        m_data_dir = home+"/data";
        m_color_conversion = CC_NONE;
        m_dataset = "";
    }
    virtual ~SkinSegmenterBase() = default;
    
    virtual void train(){}
    virtual void train(const arma::mat &images, const arma::Row<size_t> &labels)=0;
    arma::mat opencv2arma(const cv::Mat &data);
    cv::Mat arma2opencv(const arma::mat &data);

    bool loadData(const std::string &image_dir, const std::string &image_extension,
                  const std::string &label_dir, const std::string &label_extension,
                  arma::mat &images, arma::Row<size_t> &labels);
    
   
    void setColorConversion(const std::string &cvtColor){m_color_conversion = cvtColor;}
    std::string colorConversion() const {return m_color_conversion;}

    void trainFromImage(const cv::Mat &image, const cv::Mat &lables);
    
    virtual cv::Mat run(const cv::Mat& image) = 0;
    virtual bool save(const std::string &filename = std::string())=0;
    virtual bool load(const std::string &filename = std::string())=0;
    
    void setDataset(const std::string &name){m_dataset=name;}
    std::string dataset()const {return m_dataset;}

    
    static void convertColor(const cv::Mat &in, cv::Mat &out,const std::string color_conversion);
protected:
    
    std::string filename(){
        std::stringstream ss;
        ss << m_name <<"_"<<m_dataset<< "_" << m_color_conversion << ".bin";
        return ss.str();
    }
    
    std::string m_name;
    std::string m_train_image_dir;
    std::string m_train_gt_image_dir;
    std::string m_data_dir;
    std::string m_dataset;
    std::string m_color_conversion;
    
};



/*
 
 Factory class/struct for creating all the different segmenters.
 */



struct SkinSegmenter{
    typedef std::shared_ptr<SkinSegmenterBase> Ptr;
    static Ptr create(const std::string &type);
    static void train(Ptr seg,const std::string &dataset);
};

#endif /* SkinSegmenter_hpp */
