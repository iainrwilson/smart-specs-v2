//
//  SkinSegmenter.cpp
//  cpp_server
//
//  Created by Iain Wilson on 23/11/2018.
//

#include "SkinSegmenter.hpp"
#include "HistogramSkinSegmenter.hpp"
#include "DecisionTreeSkinSegmenter.hpp"
#include "RandomForestSkinSegmenter.hpp"
#include "GaussianNBSkinSegmenter.hpp"

#include "cameras/DirCamera.hpp"
#include <mlpack/core.hpp>
#include "common/timer.hpp"




SkinSegmenterBase::Ptr SkinSegmenter::create(const std::string &type){
    if(type==SEG_HISTOGRAM)
        return SkinSegmenterBase::Ptr(new HistogramSkinSegmenter);
    if(type==SEG_DECISION)
        return SkinSegmenterBase::Ptr(new DecisionTreeSkinSegmenter);
    if(type==SEG_RANDOM_FOREST)
        return SkinSegmenterBase::Ptr(new RandomForestSkinSegmenter);
    if(type==SEG_NAIVE_BAYES)
        return SkinSegmenterBase::Ptr(new GaussianNBSkinSegmenter);
    return SkinSegmenterBase::Ptr();
}

void SkinSegmenter::train(SkinSegmenterBase::Ptr seg,const std::string &dataset){
    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/";
    
    seg->setDataset(dataset);
    
    if(dataset==DATA_PRA){
        arma::mat images;
        arma::Row<size_t> labels;
        std::string image_dir = dataDir+"skin_segmentation/Face_Dataset/Pratheepan_Dataset";
        std::string label_dir = dataDir+"skin_segmentation/Face_Dataset/Ground_Truth";
        seg->loadData(image_dir,".jpg",label_dir,".png",images,labels);
        seg->train(images,labels);
        
    }else if(dataset == DATA_LFW){
        arma::mat images;
        arma::Row<size_t> labels;
        std::string image_dir = dataDir+"skin_segmentation/lfw_funneled";
        std::string label_dir = dataDir+"skin_segmentation/parts_lfw_funneled_gt_images";
        seg->loadData(image_dir,".jpg",label_dir,".ppm",images,labels);
        seg->train(images,labels);
        
    }else if(dataset == DATA_UCI_BGR){
        // just a text file
        arma::mat trainingSet;
        arma::Row<size_t> labels;
        
        
        bool loaded = mlpack::data::Load(dataDir+"/skin_segmentation/Skin_NonSkin.txt",trainingSet);
        if(!loaded)
            std::cout << "[DecisionTreeSkinSegmenter::train] error loading data"<<std::endl;
        
        labels = arma::conv_to<arma::Row<size_t>>::from(trainingSet.row(trainingSet.n_rows - 1));
        for(int i=0;i<labels.size();++i){
            if(labels[i]==2) // skin
                labels[i]=0;
        }
        std::cout << labels[10] << std::endl;
        std::cout << labels[244841] << std::endl;
        std::cout << arma::max(arma::max(labels)) +1 << std::endl;
        trainingSet.shed_row(trainingSet.n_rows - 1);
        
        seg->train(trainingSet,labels);
    }else if(dataset == DATA_UCI_HSV){
        // just a text file
        arma::mat trainingSet;
        arma::Row<size_t> labels;
        
        
        bool loaded = mlpack::data::Load(dataDir+"/skin_segmentation/HSV_Skin_NonSkin.txt",trainingSet);
        if(!loaded)
            std::cout << "[DecisionTreeSkinSegmenter::train] error loading data"<<std::endl;
        
        labels = arma::conv_to<arma::Row<size_t>>::from(trainingSet.row(trainingSet.n_rows - 1));
        for(int i=0;i<labels.size();++i){
            if(labels[i]==2) // skin
                labels[i]=0;
        }
        std::cout << labels[10] << std::endl;
        std::cout << labels[244841] << std::endl;
        std::cout << arma::max(arma::max(labels)) +1 << std::endl;
        trainingSet.shed_row(trainingSet.n_rows - 1);
        
        seg->train(trainingSet,labels);
    }else{
        std::cout << "[DecisionTreeSkinSegmenter::train] Unkown DataSet "<<std::endl;
    }
    
    //save the model - TODO give dataset name.
    seg->save();
    
}

/* ********* Skin Segmenter Base Implementations ****************/

arma::mat SkinSegmenterBase::opencv2arma(const cv::Mat &data){
    /* convert mutlichannel opencv to armadillo matrix */
    cv::Mat opencv_Mat;
    data.convertTo(opencv_Mat, CV_64FC3);
    size_t n_rows = data.channels();
    size_t n_cols = data.cols * data.rows;
    arma::mat arma_mat(reinterpret_cast<double*>(opencv_Mat.data),n_rows,n_cols);
    return arma_mat;
}

cv::Mat SkinSegmenterBase::arma2opencv(const arma::mat &data){
    /* convert mutlichannel opencv to armadillo matrix */
//    return cv::Mat(data.n_cols, data.n_rows, CV_64FC1, data.memptr());
    return cv::Mat();
}




void SkinSegmenterBase::convertColor(const cv::Mat &in, cv::Mat &out, const std::string color_conversion){
    // convert colour, sometimes returning only two channels.
    
    if(color_conversion == ""){
        out = in;
        return;
    }
    
    if(color_conversion == CC_BGR2LAB){
        cv::cvtColor(in, out, cv::COLOR_BGR2Lab);
    }else if(color_conversion == CC_BGR2AB){
        cv::Mat lab;
        cv::cvtColor(in, lab, cv::COLOR_BGR2Lab);
        std::vector<cv::Mat> channels;
        cv::split(lab, channels);
        channels.erase(channels.begin());
        cv::merge(channels,out);
    }
    else if(color_conversion == CC_BGR2HSV){
        cv::cvtColor(in, out, cv::COLOR_BGR2HSV);
    }else if(color_conversion == CC_BGR2HS){
        cv::Mat hsv;
        cv::cvtColor(in, hsv, cv::COLOR_BGR2HSV);
        std::vector<cv::Mat> channels;
        cv::split(hsv, channels);
        channels.erase(channels.begin() + 2);
        cv::merge(channels,out);
    }else if(color_conversion == CC_BGR2HV){
        cv::Mat hsv;
        cv::cvtColor(in, hsv, cv::COLOR_BGR2HSV);
        std::vector<cv::Mat> channels;
        cv::split(hsv, channels);
        channels.erase(channels.begin() + 1);
        cv::merge(channels,out);
    }else if(color_conversion == CC_BGR2H){
        cv::Mat hsv;
        cv::cvtColor(in, hsv, cv::COLOR_BGR2HSV);
        std::vector<cv::Mat> channels;
        cv::split(hsv, channels);
        out = channels[0];
    }
    
}


void SkinSegmenterBase::trainFromImage(const cv::Mat &image, const cv::Mat &labels){
    
    cv::Mat _image = image.clone();
    convertColor(_image,_image,FACELEARNER_COLOURSPACE);
    arma::mat image_mat = opencv2arma(_image);
    arma::mat label_mat = opencv2arma(labels);
    arma::Row<size_t> label_row = arma::conv_to<arma::Row<size_t>>::from(label_mat.row(0));
    
    train(image_mat,label_row);
}


bool SkinSegmenterBase::loadData(const std::string &image_dir, const std::string &image_extension,
              const std::string &label_dir, const std::string &label_extension,
              arma::mat &images, arma::Row<size_t> &labels){
    
    //using the Pratheepan_Dataset as an example
    
    // load all images and labels into aram mats.
    // if set convert to HSV
    
    
    DirCamera i_cam;
    i_cam.setFilename(image_dir);
    i_cam.setExtension(image_extension);
    if(!i_cam.connect()){
        std::cout <<"No Camera connection." << std::endl;
        return false;
    }
    
    DirCamera l_cam;
    l_cam.setFilename(label_dir);
    l_cam.setExtension(label_extension);
    if(!l_cam.connect()){
        std::cout <<"No Label Camera connection." << std::endl;
        return false;
    }
    
    arma::mat all_labels;
    arma::mat all_images;
    int count=0;
    int limit=10;
    while(count<limit){
        
        cv::Mat _label = l_cam.grab();
        cv::Mat _image = i_cam.getImage(l_cam.currentFilename());
        
        
        if(_label.empty() || _image.empty())
            break;

        convertColor(_image,_image,FACELEARNER_COLOURSPACE);
        
        // labels are loaded a 3 channel image, convert to single.
        cv::Mat _labels;
        cv::cvtColor(_label, _labels, cv::COLOR_BGR2GRAY);

        //change value of label. //data set specific.
        _labels.setTo(1,_labels == 255);

        arma::mat arma_label = opencv2arma(_labels);
        arma::mat arma_image = opencv2arma(_image);
        all_labels = arma::join_rows(all_labels,arma_label);
        all_images = arma::join_rows(all_images,arma_image);
    
        count++;
    }
    
    labels = arma::conv_to<arma::Row<size_t>>::from(all_labels.row(0));
    images = all_images;
    
    std::cout << " [SkinSegmenter::Load] found " << labels.size() << " : " << images.size() << std::endl;
    
    return true;
    
    }
