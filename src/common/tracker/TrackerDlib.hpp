//
//  TrackerDlib.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/01/2019.
//

#ifndef TrackerDlib_hpp
#define TrackerDlib_hpp

#include <stdio.h>
#include "Tracker.hpp"
#include <dlib/image_processing.h>
#include <dlib/opencv.h>

class TrackerDLib : public TrackerBase{
    
public:
    TrackerDLib();
    void init(const Image::Ptr &image, const cv::Rect &roi);
    cv::Rect update(const cv::Mat &image);
    cv::Rect getPosition();
private:
    dlib::correlation_tracker m_tracker;
};

#endif /* TrackerDlib_hpp */
