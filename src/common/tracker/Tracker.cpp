#include "Tracker.hpp"
#include "TrackerDlib.hpp"
#include "TrackerOpenCV.hpp"

Tracker::Ptr Tracker::create(const std::string &type){
    if(type == TRACKER_DLIB)
        return Tracker::Ptr(new TrackerDLib());
    if(type == TRACKER_OPENCV)
        return Tracker::Ptr(new TrackerOpenCV());
    return Tracker::Ptr();
}


cv::Rect TrackerBase::update(const Image::Ptr &image){
    m_image = image;
    m_time = std::chrono::high_resolution_clock::now();

    return update(image->gray(m_pyr_level));
}


/*
 Is this tracker valid...use age check
 */
bool TrackerBase::valid(){
    //set the deftault time out to 10 seconds = 10,000 milliseconds
    return (age() < m_max_age);
}

