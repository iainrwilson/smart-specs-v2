//
//  TrackerOpenCV.cpp
//  cpp_server
//
//  Created by Iain Wilson on 07/01/2019.
//

#include "TrackerOpenCV.hpp"

TrackerOpenCV::TrackerOpenCV():TrackerBase("OpenCV"){
    
    //Opencv has many trackers;
    const std::string tracker_type = "MOSSE";
    
//    if (tracker_type == "BOOSTING")
//        m_tracker = cv::TrackerBoosting::create();
//    if (tracker_type == "MIL")
//        m_tracker = cv::TrackerMIL::create();
//    if (tracker_type == "KCF")
//        m_tracker = cv::TrackerKCF::create();
//    if (tracker_type == "TLD")
//        m_tracker = cv::TrackerTLD::create();
//    if (tracker_type == "MEDIANFLOW")
//        m_tracker = cv::TrackerMedianFlow::create();
//    if (tracker_type == "GOTURN")
//        m_tracker = cv::TrackerGOTURN::create();
    if (tracker_type == "MOSSE")
        m_tracker = cv::TrackerMOSSE::create();
//    if (tracker_type == "CSRT")
//        m_tracker = cv::TrackerCSRT::create();
    
}


void TrackerOpenCV::init(const Image::Ptr &image, const cv::Rect &roi){
    //store image
    m_image = image;
    float s = m_pyr_level*2;
    if(m_pyr_level==0)
        s=1.0f;
    cv::Rect _roi(roi.tl().x/s,roi.tl().y/s,roi.width/s,roi.height/s);
    m_tracker->init(m_image->gray(m_pyr_level),_roi);
    m_bbox = _roi;
}


cv::Rect TrackerOpenCV::update(const cv::Mat &image){
    m_tracker->update(image,m_bbox);
    return getPosition();
}

cv::Rect TrackerOpenCV::getPosition(){
    float s = m_pyr_level*2;
    if(m_pyr_level==0)
        s=1.0f;
    return cv::Rect(m_bbox.tl().x*s,m_bbox.tl().y*s,m_bbox.width*s,m_bbox.height*s);
}
