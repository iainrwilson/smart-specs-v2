#pragma once

/*
 Class to handle different trackers, notably for faces and text.
 
 */
#include <stdio.h>
#include <memory>
#include <string>
#include <opencv2/opencv.hpp>
#include "common/Image.hpp"
#include "variables.hpp"


class TrackerBase {
public:
    typedef std::shared_ptr<TrackerBase> Ptr;
    TrackerBase(const std::string &name):m_name(name),m_pyr_level(TRACKER_PYR_LEVEL),m_max_age(TRACKER_MAX_AGE){
        m_time = std::chrono::high_resolution_clock::now();
    };
    
    virtual ~TrackerBase() = default;
    virtual void init(const Image::Ptr &image,const cv::Rect &roi)=0;
    virtual cv::Rect update(const cv::Mat &image)=0;
    virtual cv::Rect getPosition()=0;
    virtual cv::Rect update(const Image::Ptr &image);
    
    //return the age of the tracker (when was it last updated)
    int age(){
        auto now = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_time);
        return duration.count();
    }
    void setMaxAge(const int age){m_max_age = age;}
    bool valid();
    
protected:
    std::string m_name;
    Image::Ptr m_image;
    cv::Rect m_roi;
    int m_pyr_level;

private:
    int m_max_age;
    std::chrono::high_resolution_clock::time_point m_time;

};


struct Tracker{
    typedef std::shared_ptr<TrackerBase> Ptr;
    static Ptr create(const std::string &type);
    
};
