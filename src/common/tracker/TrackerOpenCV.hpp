//
//  TrackerOpenCV.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/01/2019.
//

#ifndef TrackerOpenCV_hpp
#define TrackerOpenCV_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>

#include "Tracker.hpp"

class TrackerOpenCV : public TrackerBase {
    
public:
    TrackerOpenCV();
    void init(const Image::Ptr &image, const cv::Rect &roi);
    cv::Rect update(const cv::Mat &image);
    cv::Rect getPosition();
private:
    cv::Ptr<cv::Tracker> m_tracker;
    cv::Rect_<double> m_bbox;
};

#endif /* TrackerOpenCV_hpp */
