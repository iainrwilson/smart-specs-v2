//
//  TrackerDlib.cpp
//  cpp_server
//
//  Created by Iain Wilson on 07/01/2019.
//

#include "TrackerDlib.hpp"
TrackerDLib::TrackerDLib():TrackerBase("Dlib"){}

void TrackerDLib::init(const Image::Ptr &image, const cv::Rect &roi){
    
    //store image
    m_image = image;
    dlib::cv_image<uchar> cimg(image->gray(m_pyr_level));
    float s = m_pyr_level*2;
    if(m_pyr_level==0)
        s=1.0f;
    dlib::rectangle rect((long)roi.tl().x/s, (long)roi.tl().y/s, (long)roi.br().x/s - 1, (long)roi.br().y/s - 1);
    m_tracker.start_track(cimg, rect);
    
}


cv::Rect TrackerDLib::update(const cv::Mat &image){
    dlib::cv_image<uchar> cimg(image);
    m_tracker.update(cimg);
    auto rect = m_tracker.get_position();
    return cv::Rect(rect.left(),rect.top(),rect.width(),rect.height());
}

cv::Rect TrackerDLib::getPosition(){
    auto rect = m_tracker.get_position();
    float s = m_pyr_level*2;
    if(m_pyr_level==0)
        s=1.0f;
    return cv::Rect(rect.left()*s,rect.top()*s,rect.width()*s,rect.height()*s);
}
