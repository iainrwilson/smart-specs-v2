//
//  Identities.cpp
//  cpp_server
//
//  Created by Iain Wilson on 04/02/2019.
//

#include "Identities.hpp"

Identities::Identities(){
  
}

int Identities::checkIdentity(const Face::Ptr &face){
   
    //std::unique_lock<std::mutex> lock(m_faces_mutex);
    
    /*
     Use a voting strategy - avoid having duplicate identities.
     */
    
    int chosen_id=-1;
    
    for( auto const &id:m_ids){
        
//        std::cout << " Checking Match for " <<id <<std::endl;
        
        /* Fisrt check if the roi overlapps with the most recent face for this id.*/
        auto last_face = id.second->latest();
        auto roi = last_face->roi();
        auto u = roi & face->roi();
        auto i = roi | face->roi();
        
        //  intersection / union.
        float r = static_cast<float>(u.area()) / static_cast<float>(i.area());
        auto age_diff = (last_face->age()-face->age());
        auto h_diff = last_face->compareHistogram(face->histogram());
        
        float probability = (r+h_diff) * (1.0f - (age_diff/10000) );
        
        if(r >= TRACKER_OVERLAP_TOLERANCE){
            //then this face is quite likely part of this identity.
            
            probability+=0.5;
            
            //check how old, if we havent seen it foe a while, may not be the same ID.
            if( (face->age()-last_face->age()) < FACETRACKER_AGE_THRESHOLD){
                //below threshold, so add to identity
                
                if(chosen_id == -1)
                    chosen_id = id.second->id();
                continue;
            }
            
            
            //could still be the same id of the face, use a High Histogram comparision threshold
            if(h_diff > FACELEARNER_ID_HIST_THRESHOLD){
                if(chosen_id == -1)
                    chosen_id = id.second->id();
                continue;
            }
            
            
            {
                // face is proabebly not part of this ID (although still could be)
                // this can be cleaned up in a backgroud thread, using dlib or facenet embeddings.
                //TODO
                
            }
        }
        //then no overlap - if very recent, face is unlikley to be appart of this ID.
        else{
            if( (face->age()-last_face->age()) < FACETRACKER_AGE_THRESHOLD){
                //below threshold, so reject ID
                continue;
            }
            
            //try Hist comparision, if very close assume ID matches
            if(h_diff > FACELEARNER_ID_HIST_THRESHOLD){
                if(chosen_id == -1)
                    chosen_id = id.second->id();
                continue;
            }
            
        }
        
        
        {
            //only thing left to do is compare with the embeddings
            //TODO
        }
        
    }
    return chosen_id;
}


/*!
 \brief get a list of the current faces in the frame.
 used for drawing latest detected face roi
 */
std::vector<Face::Ptr> Identities::currentFaces(){
    std::vector<Face::Ptr> faces;
    std::unique_lock<std::mutex> lock(m_identity_mutex);
    for(auto const &id:m_ids){
        //check age of face
        if(id.second->age() < 500)
            faces.push_back(id.second->latest());
    }
    return faces;
}
    
/*!
 \brief Clean up the identities, remove old / redundant / incomplete.
 */
void Identities::clean(){
   

    //second pass deletes the ID.
    {
        std::lock_guard<std::mutex> lock(m_identity_mutex);
        for(auto id=m_ids.begin();id!=m_ids.end();){
            if(!id->second->isRunning()){
                //killing identites
                id->second->join();
                id = m_ids.erase(id);
            }else{
                ++id;
            }
        }
    }

}

/*!
 \brief Create an image containing all the valid / active identities
 */
cv::Mat Identities::showIds(){
    
    //output image max dimensions:
    std::vector<cv::Mat> _ids;
    int max_w=0,max_h=0;
    int sum_h=0;
    {
        std::lock_guard<std::mutex> lck(m_identity_mutex);
        if(m_ids.size()==0)
            return cv::Mat();
        
        for(auto const &id:m_ids){
            if(id.second.get() == nullptr)
                continue;
            cv::Mat img = id.second->drawIdentity();
            if(img.empty())
                continue;
            _ids.push_back(img);
            if(img.rows>max_h)
                max_h=img.rows;
            if(img.cols>max_w)
                max_w=img.cols;
            sum_h+=img.rows;
        }
    }
    
    if(_ids.size()==0)
        return cv::Mat();
    
    //stick them all into an image
    cv::Mat out = cv::Mat::zeros(sum_h,max_w,CV_8UC3);
    int y=0;
    for(auto &id:_ids){
        id.copyTo(out(cv::Rect(0,y,id.cols,id.rows)));
        y+=id.rows;
    }
    
    return out;
}


