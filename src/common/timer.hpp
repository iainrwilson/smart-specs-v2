#ifndef __SMARTSPECS__TIMER_H
#define __SMARTSPECS__TIMER_H

/*
 * Timer class, implemented as a Singleton.
 *
 *
 */

#include <chrono>
#include <ctime>
#include <memory>
#include <mutex>
#include <unordered_map>
#include <sstream>
#include <stack>
#include <vector>
#include <opencv2/core.hpp>
#include <cstdarg>
#include <fstream>


//include "SmartSpecsIncludes.h"

namespace SmartSpecs{

	class Timer {

	public:
		static Timer* Inst();

		static void begin(const char * name){
			Timer* inst = Timer::Inst();
			//inst->m_data[name].end();
            inst->logTimeIn(name);
		}

		static void end(const char * name){
			Timer* inst = Timer::Inst();
            //inst->m_data[name].begin();
            inst->logTimeOut(name);
		}

		static void print(){
            Timer* inst = Timer::Inst();
            inst->dumpAllStats();
		}

		static std::string print(const std::string& name){
			//Timer* inst = Timer::Inst();
            
			return name;// +" "+inst->m_data[name].print();
		}
        
        static void reset(){
            Timer* inst = Timer::Inst();
            inst->clear();
            
        }

		 std::string getStatsAsText(const size_t column_width=80)
		        {
		            std::string s;

		            s+="-------------------------------- Logger report -------------------------------\n";
		            s+="           FUNCTION                                #CALLS  MIN.T  MEAN.T MAX.T\n";
		            s+="------------------------------------------------------------------------------\n";
		            for (std::unordered_map<std::string,TCallData>::const_iterator i=m_data.begin();i!=m_data.end();++i)
		            {
		                const std::string sMinT = unitsFormat(i->second.min_t,1,false);
		                const std::string sMaxT = unitsFormat(i->second.max_t,1,false);
		                const std::string sMeanT = unitsFormat(i->second.n_calls ? i->second.mean_t/i->second.n_calls : 0,1,false);
		                s+=format("%s %7u %6ss %6ss %6ss\n",
		                          aux_format_string_multilines(i->first,47).c_str(),
		                          static_cast<unsigned int>(i->second.n_calls),
		                          sMinT.c_str(),
		                          sMeanT.c_str(),
		                          sMaxT.c_str() );
		            }

		            s+="----------------------------- End Logger report ------------------------------\n";

		            return s;
		        }

	private:


        
        //! Data of all the calls: //robbed from MRPT
        struct TCallData
        {
            TCallData():n_calls(0),min_t(0),max_t(0),mean_t(0){};
            
            size_t n_calls;
            double min_t,max_t,mean_t;
            
            std::stack<double,std::vector<double> >  open_calls;
        };
        
        
        void logTimeIn(const char *func_name){
            const std::string s = func_name;
            TCallData &d = m_data[s];
            
            d.n_calls++;
            d.open_calls.push(0); // Dummy value, it'll be written below
            d.open_calls.top() = cv::getTickCount()/cv::getTickFrequency();
        }
        
        double logTimeOut(const char *func_name){
            const double tim = cv::getTickCount()/cv::getTickFrequency();
            const std::string s = func_name;
            TCallData &d = m_data[s];
            
            
            if (!d.open_calls.empty())
            {
                const double At = tim - d.open_calls.top();
                m_outfile << s << "," << At << ","<< tim<<std::endl;
                
                d.open_calls.pop();
                
                d.mean_t+=At;
                if (d.n_calls==1)
                {
                    d.min_t= At;
                    d.max_t= At;
                }
                else
                {
                    if(At < d.min_t) 
                        d.min_t=At;
                    if(At > d.max_t)
                        d.max_t=At;
                }
                return At;
            }
            else return 0; // This shouldn't happen!
            
        }
        
        


        void dumpAllStats(const size_t  column_width=80)
        {
            std::string s = getStatsAsText(column_width);
	    // LOGI("\n%s\n", s.c_str());
            printf("\n%s\n", s.c_str() );
        }
        
        
        void clear(){
            m_data.clear();
        }
        
        /* formatting helper things..... */
        
        std::string unitsFormat(const double val,int nDecimalDigits, bool middle_space)
        {
            char	prefix;
            double	mult;
            
            if (val>=1e12)
            {mult=1e-12; prefix='T';}
            else if (val>=1e9)
            {mult=1e-9; prefix='G';}
            else if (val>=1e6)
            {mult=1e-6; prefix='M';}
            else if (val>=1e3)
            {mult=1e-3; prefix='K';}
            else if (val>=1)
            {mult=1; prefix=' ';}
            else if (val>=1e-3)
            {mult=1e+3; prefix='m';}
            else if (val>=1e-6)
            {mult=1e+6; prefix='u';}
            else if (val>=1e-9)
            {mult=1e+9; prefix='n';}
            else
            {mult=1e+12; prefix='p';}
            
            return format(
                          middle_space ? "%.*f %c" : "%.*f%c",
                          nDecimalDigits,
                          val*mult,
                          prefix );
        }
        
        std::string format_arg_list(const char *fmt, va_list args)
        {
            if (!fmt) return "";
            int   result = -1, length = 256;
            char *buffer = 0;
            while (result == -1)
            {
                if (buffer) delete [] buffer;
                buffer = new char [length + 1];
                memset(buffer, 0, length + 1);
                result = vsnprintf(buffer, length, fmt, args);
                length *= 2;
            }
            std::string s(buffer);
            delete [] buffer;
            return s;
        }
        
        std::string format(const char *fmt, ...)
        {
            va_list args;
            va_start(args, fmt);
            std::string s = format_arg_list(fmt, args);
            va_end(args);
            return s;
        }
        
        std::string aux_format_string_multilines(const std::string &s, const size_t len)
        {
            std::string ret;
            
            for (size_t p=0;p<s.size();p+=len)
            {
                ret+=rightPad(s.c_str()+p,len,true);
                if (p+len<s.size())
                    ret+="\n";
            }
            return ret;
        }
        
        std::string rightPad(const std::string &str, const size_t total_len, bool truncate_if_larger)
        {
            std::string r = str;
            if (r.size()<total_len || truncate_if_larger)
                r.resize(total_len,' ');
            return r;
        }

        
        
        
        
		static std::mutex& getMutex(){
			static std::mutex mutex;
			return mutex;
		}

		static Timer* m_instance;
//		Timer(){}
        
        Timer();
		~Timer(){
            m_outfile.close();
        }
        
        std::ofstream m_outfile;
        
		//std::unordered_map<std::string,Elapsed> m_data;

        std::unordered_map<std::string,TCallData> m_data;


	};

}
#endif
