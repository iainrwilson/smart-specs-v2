//
//  Image.h
//  cpp_server
//
//  Created by Iain Wilson on 21/11/2018.
//

#ifndef Image_h
#define Image_h

#include <opencv2/opencv.hpp>
#include <chrono>
#include <memory>

class Image{
        /*
    
         Simple wrapper for a cv Mat,
         
         has timestamps and things
         
     */
public:
    typedef std::shared_ptr<Image> Ptr;
    typedef std::unique_ptr<Image> Ptr_unique;
    
    
    static Image::Ptr create(){
        return std::make_shared<Image>();
    }
    static Image::Ptr create(const cv::Mat &image){
        return std::make_shared<Image>(image);
    }
    
    
    
    Image();
    Image(const cv::Mat &image);
    
    bool isEmpty()const{
        if(!m_has_image)
            return true;
        return m_image.empty();
    }
    
    /*!
     \brief comparision operator overlaod, uses the creation time to compare.
     */
    bool operator==(const Image &rhs) const{
        return this->m_time == rhs.m_time;
    }
    
    
    cv::Mat get() const {return m_image;}
    cv::Mat get(const int pyr){return getPyr(pyr);}
    cv::Mat gray(const int pyr=0) {
        if(!m_has_gray){
            cv::cvtColor(getPyr(pyr),m_gray,cv::COLOR_BGR2GRAY);
            m_has_gray = true;
        }
        return m_gray;
    }
    void set(const cv::Mat &image){
        m_image = image;
        m_has_image = true;
    }
    
    int width() const{return m_image.cols;}
    int height() const{return m_image.rows;}
    cv::Size size() const{return m_image.size();}
    cv::Size size(const int pyr)const{return m_pyr_levels[pyr].size();}
    
    
    void buildPyramid(const int max=4){
        //how quick?
        cv::buildPyramid(m_image,m_pyr_levels,max);
    }
    
    cv::Mat getPyr(const int level){
        if(level>=m_pyr_levels.size())
            return m_pyr_levels[m_pyr_levels.size()-1];
        return m_pyr_levels[level];
    }
    
    void pyrDown(){
        if(m_level == 0)
            cv::pyrDown(m_image,m_pyr_image);
        else
            cv::pyrDown(m_pyr_image,m_pyr_image);
        m_level++;
    }
    
    cv::Mat getPyr(){
        if(m_level==0)
            return m_image;
        return m_pyr_image;
    }
    
    void pyrUp(){/* TODO */}
    int getPyrLevel()const{return m_level;}
    
    void setSkin(const cv::Mat &skin){
        m_skin = skin;
        m_has_skin=true;
    }
    cv::Mat skin()const {return m_skin;}
    bool hasSkin()const {return m_has_skin;}
    
    std::chrono::high_resolution_clock::time_point timestamp() const{ return m_time; }
    void setTimestamp(const std::chrono::high_resolution_clock::time_point &time){
        m_time = time;
    }
    int age(){
        auto now = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - timestamp());
        return duration.count();
    }
    /*!
     \brief return the cenre point of the image at specific pyramid level.
     */
    cv::Point centre(const int level=0){
        cv::Size s = size(level);
        return cv::Point(s.width,s.height) * 0.5;
    }
private:
    cv::Mat m_image;
    std::chrono::high_resolution_clock::time_point m_time;
    
//    std::vector<Face::Ptr> m_faces;
//    std::vector<Text> m_text;
    bool m_has_image;
    float m_level;
    std::vector<cv::Mat> m_pyr_levels;
    cv::Mat m_pyr_image;
    
    bool m_has_skin;
    cv::Mat m_skin;
    
    bool m_has_gray;
    cv::Mat m_gray;
    
};


#endif /* Image_h */
