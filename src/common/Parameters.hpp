#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <map>
#include <string>
#include <boost/lexical_cast.hpp>
#include <memory>
#include "common/Parameter.hpp"

class Parameters {
    
public:
    Parameters(){}
    
    /*!
     \brief Create a new paremeter of type T.
     */
    template <class T>
    typename Parameter<T>::Ptr add(const std::string &name,const T &value){
        assert(!hasParameter(name));
        auto p = Parameter<T>::create(name,value);
        std::lock_guard<std::mutex> lock(m_access);
        m_parameters.push_back(std::dynamic_pointer_cast<BaseParameter>(p));
        return p;
    }
    
    /*!
     \brief Create a new paremeter of type T.
     */
    template <class T>
    typename Parameter<T>::Ptr add(const std::string &name,const T &value,std::function<void (T)> callback){
        assert(!hasParameter(name));
        auto p = Parameter<T>::create(name,value,callback);
        std::lock_guard<std::mutex> lock(m_access);
        m_parameters.push_back(std::dynamic_pointer_cast<BaseParameter>(p));
        return p;
    }
    
    /*!
     \brief Create a new paremeter of type T. specigying type
     */
    template<class T>
    typename Parameter<T>::Ptr add(const std::string &name, const std::string &type, const T &value, const bool enumerate=false){
        assert(!hasParameter(name));
        std::shared_ptr<Parameter<T> > p(new Parameter<T>(name,type,value,enumerate));
        std::lock_guard<std::mutex> lock(m_access);
        m_parameters.push_back(std::dynamic_pointer_cast<BaseParameter>(p));
        return p;
    }
    
    /*!
     \brief Create a new paremeter of type T. specigying type, and min/max
     */
    template<class T>
    void add(const std::string &name, const std::string &type,const T &max, const T &min, const T &value, bool enumerate=false){
        assert(!hasParameter(name));
        std::shared_ptr<Parameter<T> > p(new Parameter<T>(name,type,max,min,value,enumerate));
        
        std::lock_guard<std::mutex> lock(m_access);
        m_parameters.push_back(std::dynamic_pointer_cast<BaseParameter>(p));
    }
    
    
    /*!
     \brief return the specified paramter, by name
     */
    template<class T>
    std::shared_ptr<Parameter<T> > get(const std::string &name){
        std::shared_ptr<BaseParameter> b = findParameter(name);
        assert(b.get()!=NULL);
        return std::dynamic_pointer_cast<Parameter<T> >(b);
    }
    
    /*!
     \brief return the value of the specified paramter, by name
     */
    template<class T>
    T value(const std::string &name){
        return get<T>(name)->value();
    }
    
    
    /*!
     \brief return the specified paramter, by index
     */
    template<class T>
    std::shared_ptr<Parameter<T> > get(int index){
        std::lock_guard<std::mutex> lock(m_access);
        std::list<std::shared_ptr<BaseParameter> >::iterator it=m_parameters.begin();
        for(int i=0;i<index;i++)
            it++;
        return std::dynamic_pointer_cast<Parameter<T> >(*it);
    }
    
    
   /*!
    \brief update a parameter by name
    */
    template<class T>
    bool set(const std::string &name, const T &value){
        std::lock_guard<std::mutex> lock(m_access);
        return get<T>(name)->setValue(value);
    }
    
    BaseParameter::Ptr operator[] (const std::string& name) const {
        return findParameter(name);
    }
    
    
//    // update a parameter from a string, used when loading parameters from file
//    bool updateParameter(string name, string value){
//        if(!hasParameter(name))
//            return false;
//        std::shared_ptr<BaseParameter> b = findParameter(name);
//        string type= b->type();
//
//        if(type=="bool")
//            return dynamic_pointer_cast<Parameter<bool> >(b)->setValue(boost::lexical_cast<bool>(value));
//        if(type=="int")
//            return dynamic_pointer_cast<Parameter<int> >(b)->setValue(boost::lexical_cast<int>(value));
//        if(type=="float")
//            return dynamic_pointer_cast<Parameter<float> >(b)->setValue(boost::lexical_cast<float>(value));
//        if(type=="double")
//            return dynamic_pointer_cast<Parameter<double> >(b)->setValue(boost::lexical_cast<double>(value));
//        if(type=="string")
//            return dynamic_pointer_cast<Parameter<string> >(b)->setValue(boost::lexical_cast<string>(value));
//    }
    
    // don't like this style, but helps as a shortcut.
   
    std::string getParameterType(const std::string &name){
        return findParameter(name)->type();
    }
    
    int parameterCount() const{
        std::lock_guard<std::mutex> lock(m_access);
        return m_parameters.size();
    }
    int getParameterCount()const {
        return parameterCount();
    }
    void start(){
        std::lock_guard<std::mutex> lock(m_parameterMutex);
        m_pIt=m_parameters.begin();
    }
    bool next(){
        //return a name and move allong
        std::lock_guard<std::mutex> lock(m_parameterMutex);
        if (m_pIt==m_parameters.end())
            return false;
        m_pIt++;
        return true;
    }
    
    std::string pName() const {
        return (*m_pIt)->name();
    }
    
    std::string print(){return "Need to implement!!\n";};
    
    bool hasParameter(const std::string &name){
        std::lock_guard<std::mutex> lock(m_access);
        
        //mutex::scoped_lock lock(m_parameterMutex);
        std::list<std::shared_ptr<BaseParameter> >::iterator it;
        for(it=m_parameters.begin();it!=m_parameters.end();++it){
            if((*it)->name()==name)
                return true;
        }
        return false;
    }
    
    
    
private:
    std::shared_ptr<BaseParameter> findParameter(const std::string &name) const {
        std::lock_guard<std::mutex> lock(m_access);
        

        for(auto it=m_parameters.begin();it!=m_parameters.end();++it){
            if((*it)->name()==name)
                return (*it);
        }
        std::cout <<"ERROR can't find parameter:: "<< name <<std::endl;
        return std::shared_ptr<BaseParameter>();
    }
    
    
    mutable std::mutex m_parameterMutex;
    mutable boost::shared_mutex _access;
    mutable std::mutex m_access;
    
    std::list<std::shared_ptr<BaseParameter> > m_parameters;
    std::list<std::shared_ptr<BaseParameter> >::iterator m_pIt;
    
};
#endif //PARAMETERS_H
