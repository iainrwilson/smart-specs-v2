//
//  Identity.cpp
//  cpp_server
//
//  Created by Iain Wilson on 19/12/2018.
//

#include "Identity.hpp"

Identity::Identity(const Face::Ptr &face):Thread("Identity"),m_has_new_data(false){
    
    
    //m_id = rand() % 10000;
    m_faces = RingBuffer<Face::Ptr>::create(FACELEARNER_FACE_BUFFER_SIZE);
    
    //create an input buffer...small.
    m_input_buffer = RingBuffer<Face::Ptr>::create();
    
    
    //set this Identity ID to that of the face that initialised this.
    m_id = face->id();
    
    add(face);
    
    //start the thread.
    this->start();
    
    std::cout << "[ Identity ] created a new ID: " << m_id << std::endl;
    
}


/*!
 \brief add a new face (that we think is appart of this  identity
 push onto the input buffer. and update the most recent.
*/
void Identity::add(const Face::Ptr &face){
    
    bool _tracked = (face->type() == "TRACKED");
    
    {
        std::lock_guard<std::mutex> lock(m_latest_face_mutex);
        m_latest_face = face;
        if(!_tracked)
            m_time=std::chrono::high_resolution_clock::now();
    }
    
    //don't add tracked faces to the segmenter.
    if(!_tracked)
        m_input_buffer->push(face);
}


/*!
 \brief do all the work.
 */
void Identity::thread_run(){
    
    if(m_input_buffer->empty()){
        //sleep for 10 ms ( can't remember why?? )
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        
        //check age
        if(age() > IDENTITY_AGE_LIMIT){
            _stop();
            std::cout << "[ Identity ] Stopping ID: " << m_id << std::endl;
        }
        return;
    }
    
    auto face = m_input_buffer->pop();
    {
        m_faces->push(face);
        //log time last face was added.
//        m_time=std::chrono::high_resolution_clock::now();
        //set flag so we know to genereate training data.
        m_has_new_data=true;
    }

}


double Identity::compareHistogram(const cv::Mat &hist){
    //TODO: loop through face list
    // for the mo, just get the most recent
    return m_faces->head()->compareHistogram(hist);
}



cv::Mat Identity::drawIdentity(){
    //loop and draw the faces stored for this id.
    
    if(m_faces->empty())
        return cv::Mat();
    
    //find the largest face
    
    auto faces = m_faces->getSnapshot();
    int s = faces.size();
    int max_w=0,max_h=0;
    for(auto &face:faces){
        if(max_w<face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).width)
            max_w=face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).width;
        if(max_h<face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).height)
            max_h=face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).height;
    }
    
    //create an output image - containing the cropped image, and the masks used.
    cv::Mat out = cv::Mat::zeros(max_h,max_w*s,CV_8UC3);
    
    //copy the faces to the output image:
    int i=0;
    for(auto &face:faces){
        auto _roi = face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
        
        cv::Rect roi(i*max_w,
                     0,
                     _roi.width,
                     _roi.height);
        face->maskedFace().copyTo(out(roi));
        
//        if(face->hasBgdMask()){
//            cv::Rect roi_m(i*max_w,
//                         max_h,
//                         _roi.width,
//                         _roi.height);
//
//            cv::Mat bgMask = face->bgdMask()(_roi);
//            cv::cvtColor(bgMask, bgMask, cv::COLOR_GRAY2BGR);
//            bgMask.copyTo(out(roi_m));
//        }
//        if(face->hasFgdMask()){
//            cv::Rect roi_b(i*max_w,
//                         max_h*2,
//                         _roi.width,
//                         _roi.height);
//            cv::Mat fgMask = face->fgdMask()(_roi);
//            cv::cvtColor(fgMask, fgMask, cv::COLOR_GRAY2BGR);
//            fgMask.copyTo(out(roi_b));
//        }
        i++;
    }
    
    
//    std::stringstream ss;
//    ss << "ID:: " << id();
//    cv::putText(out, ss.str(), cv::Point(0,20), cv::FONT_HERSHEY_PLAIN, 1., cv::Scalar(0,255,0));
    
    return out;
}
