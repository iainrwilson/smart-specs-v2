//
//  Identity.hpp
//  cpp_server
//
//  Created by Iain Wilson on 19/12/2018.
//

#ifndef Identity_hpp
#define Identity_hpp

#include <stdio.h>
#include "Face.hpp"
#include "common/RingBuffer.hpp"
#include <memory>
#include "common/timer.hpp"

//age limit for storing identities (20 seconds)
const int IDENTITY_AGE_LIMIT{2000};
//minimum number of faces for an ID to be considered valid.
const int IDENTITY_MIN_NO_FACES{3};

const int IDENTITY_FACE_BUFFER_SIZE{10};


typedef RingBuffer<Face::Ptr>::Ptr FaceBuffer;

class Identity{
    
public:
    typedef std::shared_ptr<Identity> Ptr;
    
    static Ptr create(const Face::Ptr &face){
        return std::make_shared<Identity>(face);
    }
    
    Identity(const Face::Ptr &face);
    ~Identity() = default;
    
    void add(const Face::Ptr &face);
    
    double compareHistogram(const cv::Mat &hist);
    
    int id() const {return m_id;}
    FaceBuffer buff(){return m_faces;}
    bool isFull(){ return m_faces->full();}
    
    
    bool valid(){
        // a valid ID must meet certain critera
        
        //must have more than the minimum number of faces
        if(m_faces->size() < IDENTITY_MIN_NO_FACES)
            return false;
        
        //most recent face must be within limits.
        return age() < IDENTITY_AGE_LIMIT;
        
    }
    
    int age() {
        auto now = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_time);
        return duration.count();
    }
    
    Face::Ptr head(){return m_faces->head();}
    Face::Ptr latest() {
        std::lock_guard<std::mutex> lock(m_latest_face_mutex);
        return m_latest_face;
    }
    
private:
    int m_id;
    std::chrono::high_resolution_clock::time_point m_time;
    bool m_has_new_data;
    FaceBuffer m_faces;
    FaceBuffer m_input_buffer;
    
    Face::Ptr m_latest_face;
    std::mutex m_latest_face_mutex;
    
    

};

#endif /* Identity_hpp */
