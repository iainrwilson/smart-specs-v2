//
//  FaceRecognition.cpp
//  cpp_server
//
//  Created by Iain Wilson on 01/02/2019.
//

#include "FaceRecognition.hpp"
#include "FaceRecognitionDlib.hpp"

FaceRecognition::Ptr FaceRecognition::create(const std::string &type){
    return FaceRecognition::Ptr(new FaceRecognitionDlib);
}
