//
//  FaceRecognitionDlib.cpp
//  cpp_server
//
//  Created by Iain Wilson on 01/02/2019.
//

#include "FaceRecognitionDlib.hpp"
#include "common/timer.hpp"
FaceRecognitionDlib::FaceRecognitionDlib() : FaceRecognitionBase("DLIB"){
    
    //load the models
    std::string home = getenv("HOME");
    std::string dataDir = home+"/experiments/face_detection/data/models/";
    
    dlib::deserialize(dataDir+"shape_predictor_5_face_landmarks.dat") >> m_shape_predictor;
    dlib::deserialize(dataDir+"dlib_face_recognition_resnet_model_v1.dat") >> m_net;
    
}


/*!
 \brief Create a 128D feature descriptors for a given face.

 Distance between the descriptors is an idicator of similarity. Similar identities shoudl be closer.
 
 */
std::vector<FaceDescriptor> FaceRecognitionDlib::createDescriptor(Face::Ptr &face){
    
    SmartSpecs::Timer::begin("[ FaceRecognitionDlib ] createDescriptor ");
    dlib::cv_image<dlib::bgr_pixel> cv_image(face->faceImage());
    dlib::matrix<dlib::rgb_pixel> dlib_image;
    dlib::assign_image(dlib_image, cv_image);
    
    std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
    
    
    auto shape = m_shape_predictor(dlib_image, face->dlib_roi());
    dlib::matrix<dlib::rgb_pixel> face_chip;
    dlib::extract_image_chip(dlib_image, dlib::get_face_chip_details(shape,150,0.25), face_chip);
    faces.push_back(std::move(face_chip));
    
    
    std::vector<FaceDescriptor> face_descriptors = m_net(faces);
    
    SmartSpecs::Timer::end("[ FaceRecognitionDlib ] createDescriptor ");
    return face_descriptors;
}


/*!
 \brief try and reorganise all of the face in the identities
 */
std::vector<Identity::Ptr> FaceRecognitionDlib::organise(std::vector<Identity::Ptr> ids){
    
    SmartSpecs::Timer::begin("[ FaceRecognitionDlib ] organise");

    
    //put all faces into one vector:
    std::vector<Face::Ptr> faces;
    for(auto const &id:ids){
        auto _fs = id->buff()->getSnapshot();
        faces.insert(faces.end(),_fs.begin(),_fs.end());
    }
    
    
    std::vector<dlib::matrix<dlib::rgb_pixel>> dlib_faces;
    for(auto const &face:faces){
        dlib::cv_image<dlib::bgr_pixel> cv_image(face->faceImage());
        dlib::matrix<dlib::rgb_pixel> dlib_image;
        dlib::assign_image(dlib_image, cv_image);
        
        auto shape = m_shape_predictor(dlib_image, face->dlib_roi());
        dlib::matrix<dlib::rgb_pixel> face_chip;
        dlib::extract_image_chip(dlib_image, dlib::get_face_chip_details(shape,150,0.25), face_chip);
        dlib_faces.push_back(std::move(face_chip));
    }
    
     std::vector<FaceDescriptor> face_descriptors = m_net(dlib_faces);
    
    std::vector<dlib::sample_pair> edges;
    for (size_t i = 0; i < face_descriptors.size(); ++i)
    {
        for (size_t j = i; j < face_descriptors.size(); ++j)
        {
            // Faces are connected in the graph if they are close enough.  Here we check if
            // the distance between two face descriptors is less than 0.6, which is the
            // decision threshold the network was trained to use.  Although you can
            // certainly use any other threshold you find useful.
            if (length(face_descriptors[i]-face_descriptors[j]) < 0.6)
                edges.push_back(dlib::sample_pair(i,j));
        }
    }
    std::vector<unsigned long> labels;
    const auto num_clusters = dlib::chinese_whispers(edges, labels);
    
    std::cout << "[ FaceRecognitionDlib ] found: "<< num_clusters <<"seperate identites" <<std::endl;
    
    SmartSpecs::Timer::end("[ FaceRecognitionDlib ] organise");

    return std::vector<Identity::Ptr>();
}


/*!
 \brief create desctiptors for an identity
 */
std::vector<FaceDescriptor> FaceRecognitionDlib::createDescriptor(Identity::Ptr &id){
    SmartSpecs::Timer::begin("[ FaceRecognitionDlib ] createDescriptor");

    std::vector<dlib::matrix<dlib::rgb_pixel>> dlib_faces;
    for(auto const &face:id->buff()->getSnapshot()){
        dlib::cv_image<dlib::bgr_pixel> cv_image(face->faceImage());
        dlib::matrix<dlib::rgb_pixel> dlib_image;
        dlib::assign_image(dlib_image, cv_image);
        
        auto shape = m_shape_predictor(dlib_image, face->dlib_roi());
        dlib::matrix<dlib::rgb_pixel> face_chip;
        dlib::extract_image_chip(dlib_image, dlib::get_face_chip_details(shape,150,0.25), face_chip);
        dlib_faces.push_back(std::move(face_chip));
    }
    
    std::vector<FaceDescriptor> face_descriptors = m_net(dlib_faces);
    
    
    //check how many identities we think (shoul dbe one but this is the check)
    
    std::vector<dlib::sample_pair> edges;
    for (size_t i = 0; i < face_descriptors.size(); ++i)
    {
        for (size_t j = i; j < face_descriptors.size(); ++j)
        {
            // Faces are connected in the graph if they are close enough.  Here we check if
            // the distance between two face descriptors is less than 0.6, which is the
            // decision threshold the network was trained to use.  Although you can
            // certainly use any other threshold you find useful.
            if (length(face_descriptors[i]-face_descriptors[j]) < 0.6)
                edges.push_back(dlib::sample_pair(i,j));
        }
    }
    std::vector<unsigned long> labels;
    const auto num_clusters = dlib::chinese_whispers(edges, labels);
    
    std::cout << "[ FaceRecognitionDlib ] found: "<< num_clusters <<"seperate identites" <<std::endl;
    
    SmartSpecs::Timer::end("[ FaceRecognitionDlib ] createDescriptor");
    
    return face_descriptors;
}


