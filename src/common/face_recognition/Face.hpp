//
//  Face.hpp
//  cpp_server
//
//  Created by Iain Wilson on 22/11/2018.
//

#ifndef Face_hpp
#define Face_hpp

#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <memory>
#include "common/Image.hpp"
#include <dlib/image_processing.h>

//class Identity;

/* HISTOGRAM BIN SIZES */
const int H_BINS{64};
const int S_BINS{64};

class Face{
    /*
     Contains all the details about a face, roi etc.
     */
public:
    typedef std::vector<cv::Point> Features;
    typedef std::shared_ptr<Face> Ptr;
    
    static Face::Ptr create(){
        return std::make_shared<Face>();
    }
    
    static Face::Ptr create(const cv::Rect &roi, const Image::Ptr &image){
        return std::make_shared<Face>(roi,image);
    }
    
    
    Face(){}
    Face(const cv::Rect &roi);
    Face(const cv::Rect &roi, const Image::Ptr &image);
    
    cv::Mat getPyr(const int level){
        if(level>=m_pyr_levels.size())
            return m_pyr_levels[m_pyr_levels.size()-1];
        return m_pyr_levels[level];
    }
    
    cv::Rect roi(const int level){
        // return roi for a specific pyramid level.
        if(level == 0)
            return m_roi;
        int scale = level *2;
        
        int x = m_roi.x / scale;
        int y = m_roi.y / scale;
        int w = m_roi.width / scale;
        int h = m_roi.height / scale;
        
        return cv::Rect(x,y,w,h);
    }
    
    cv::Rect roi()const{return m_roi;}
    void setRoi(const cv::Rect roi){m_roi = roi;}
    
    bool isValid();
    
    cv::Rect paddRoi(const float padd=0.2f){
        float w = m_roi.width*(1.0f+padd);
        float h = m_roi.height*(1.0f+padd);
        int x = m_roi.x - (w - m_roi.width)/2;
        int y = m_roi.y - (h - m_roi.height)/2;
        
        if(x<0) x=0;
        if(y<0) y=0;
        
        if(x+w>=m_image->width())
            w=m_image->width()-x-1;
        if(y+h>=m_image->height())
            h=m_image->height()-y-1;
        
        return cv::Rect(x,y,w,h);
    }
    
    void setFeatures(const Features &features){
        m_feature_points = features;
        m_has_features=true;
    }
    Features features()const {return m_feature_points;}
    
//    void draw(cv::Mat &image, const cv::Scalar &colour=cv::Scalar(255,0,0));
//    void drawHistogram(cv::Mat &image);
//
//    void createHistogram(const cv::Mat& image);
//    void create2DHistogram();
     cv::Mat create2DHistogram(const cv::Mat &image,const cv::Mat &mask,const int hbins=H_BINS,const int sbins=S_BINS);
     void create2DHistogram(const cv::Mat &image,const cv::Mat &mask,cv::Mat &histogram,const int hbins=H_BINS,const int sbins=S_BINS);
    
    
    void createFaceMask(const cv::Mat &image);
    bool hasFaceMask()const{return m_has_mask;}
    cv::Mat faceMask(const int pyrLevel=0){
        if(!m_has_mask)
            createFaceMask(m_image->get());
        
        if(pyrLevel == 0)
            return m_face_mask;
        cv::Mat mask;
        
        cv::pyrDown(m_face_mask,mask);
        for(int i=1;i<pyrLevel;++i)
            cv::pyrDown(m_face_mask,mask);
        return mask;
    }
    void setFaceMask(const cv::Mat mask){
        m_face_mask=mask;
        m_has_mask=true;
    }
    
    
    
    bool hasHistogram()const{return m_has_histogram;}
    cv::Mat histogram()const{return m_histogram;}
    void setHistogram(const cv::Mat &hist){m_histogram=hist;m_has_histogram=true;}
    
    double compareHistogram(const cv::Mat &hist);
    double compareHistogram();
    
    
  
    
    cv::Mat forehead(const cv::Mat &image){
        //using features and boundin box.
        //    int points[]={18,28,27}; (-1 on all)
        Face::Features points;
        points.push_back(m_feature_points[17]);
        points.push_back(cv::Point(m_feature_points[27].x,m_roi.y));
        points.push_back(m_feature_points[26]);
        
        const cv::Point* p[1] = {&points[0]};
        int size = points.size();
        cv::Mat mask= cv::Mat::zeros(image.size(),CV_8UC1);
        cv::fillPoly(mask,p,&size,1,cv::Scalar(255),8);
        return mask;
    }
    
    cv::Mat faceImage(){return m_image->get()(m_roi);}
    
    
    Image::Ptr image() {return m_image;}
    
    
    int age(){return m_image->age();}
    
    
    dlib::rectangle dlib_roi(){
        return dlib::rectangle((long)roi().tl().x, (long)roi().tl().y, (long)roi().br().x - 1, (long)roi().br().y - 1);
    }
    
    void setId(const int &id){m_id=id;}
    int id()const{return m_id;}
    
  
private:
    
    inline int getRand(const uint8_t* fm, const size_t &size){
        int index = rand() % size;
        if(fm[index]!=0)
            return getRand(fm,size);
        return index;
    }
    
    cv::Rect m_roi; // bounding box of face
    
    Image::Ptr m_image;
    
    bool m_has_features;
    Features m_feature_points;
    
    bool m_has_mask;
    cv::Mat m_face_mask;
    
    bool m_has_histogram;
    cv::Mat m_histogram;
    
    double m_hist_diff;

    std::vector<cv::Mat> m_pyr_levels;
    
    
    int m_id;
  
    
};


#endif /* Face_hpp */
