//
//  FaceRecognition.hpp
//  cpp_server
//
//  Created by Iain Wilson on 01/02/2019.
//


/*
 
 Start with the dlib implementaion, then add FaceNet
 
 
 */
#ifndef FaceRecognition_hpp
#define FaceRecognition_hpp

#include <stdio.h>
#include <memory>
#include <string>

#include <dlib/image_processing.h>
#include "Face.hpp"
#include "Identity.hpp"

typedef dlib::matrix<float,0,1> FaceDescriptor;

class FaceRecognitionBase{

public:
    FaceRecognitionBase(const std::string name):m_name(name){};
    virtual ~FaceRecognitionBase()=default;
    virtual std::vector<FaceDescriptor> createDescriptor(Face::Ptr &face)=0;
    virtual std::vector<FaceDescriptor> createDescriptor(Identity::Ptr &id)=0;
    virtual std::vector<Identity::Ptr> organise(std::vector<Identity::Ptr> ids)=0;
    
    
private:
    std::string m_name;
    
};


struct FaceRecognition{
    typedef std::shared_ptr<FaceRecognitionBase> Ptr;
    
    static FaceRecognition::Ptr create(const std::string &type);
    
};


#endif /* FaceRecognition_hpp */
