//
//  Face.cpp
//  cpp_server
//
//  Created by Iain Wilson on 22/11/2018.
//

#include "Face.hpp"
#include "common/timer.hpp"

Face::Face(const cv::Rect &roi,const Image::Ptr &image):
m_roi(roi),
m_image(image),
m_has_features(false),
m_has_mask(false),
m_has_histogram(false),
m_hist_diff(-1.0),
m_id(-1){
    
}



bool Face::isValid(){
    // check a few thigs...
    
    //dodgy roi setting
    if(m_roi.x>m_image->width())
        return false;
    
    if(m_roi.y>m_image->height())
        return false;
    
    if(m_roi.x < 0 || m_roi.y < 0)
        return false;
    
    if( (m_roi.x + m_roi.width) > m_image->width())
        return false;
    
    if( (m_roi.y + m_roi.height) > m_image->height())
        return false;
    
    return true;
}



void Face::createFaceMask(const cv::Mat &image){
    /*
     Create a mask of the face using the detected  feature points.
     */
    
    //a list of the external, feature points.
    int _face[]= {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,26,25,24,23,22,21,20,19,18,17,0};
    Face::Features ext_points;
    for(auto i:_face)
        ext_points.push_back(m_feature_points[i]);
    
    const cv::Point* p[1] = {&ext_points[0]};
    int size = ext_points.size();
    cv::Mat facemask = cv::Mat::zeros(image.size(),CV_8UC1);
    cv::fillPoly(facemask,p,&size,1,cv::Scalar(255),8);
    
    cv::bitwise_or(facemask,forehead(image),m_face_mask);
    m_has_mask = true;
}


cv::Mat Face::create2DHistogram(const cv::Mat &image,const cv::Mat &mask,const int hbins,const int sbins){
    cv::Mat hist;
    create2DHistogram(image,mask,hist,hbins,sbins);
    return hist;
}
void Face::create2DHistogram(const cv::Mat &image,const cv::Mat &mask,cv::Mat &histogram,const int hbins,const int sbins){
    
    //accumalative approach.
    // create a hisrogram from the ROI.
    cv::Mat hsv,hue;
    cv::cvtColor(image, hsv, cv::COLOR_BGR2HSV);
    
    
    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    //    int hbins = 30, sbins = 32;
    int histSize[] = {hbins, sbins,sbins};
    // hue varies from 0 to 179, see cvtColor
    float hranges[] = { 0, 180 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float sranges[] = { 0, 256 };
    float vranges[] = { 0, 256 };
    const float* ranges[] = { hranges, sranges,vranges};
    cv::Mat hist;
    // we compute the histogram from the 0-th and 1-st channels
    int channels[] = {0, 1, 2};
    calcHist( &hsv, 1, channels, mask, // do not use mask
             histogram, 2, histSize, ranges,
             true, // the histogram is uniform
             true );
    
    //    std::cout << histogram.size() <<std::endl;
    
    cv::normalize(histogram, histogram, 0, 1, cv::NORM_MINMAX,CV_32F,cv::Mat());
    //    return hist;
    
}

double Face::compareHistogram(const cv::Mat &hist){
    if(!m_has_histogram)
        return -1.0;
    
    //    std::cout <<"m_ist shape: "<<m_histogram.size() << " "<< m_histogram.channels() << " hist shape: "<< hist.size()<<" " <<hist.channels() <<std::endl;
    m_hist_diff = cv::compareHist(m_histogram,hist,cv::HISTCMP_CORREL);
    return m_hist_diff;
}

