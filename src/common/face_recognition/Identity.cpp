//
//  Identity.cpp
//  cpp_server
//
//  Created by Iain Wilson on 19/12/2018.
//

#include "Identity.hpp"

Identity::Identity(const Face::Ptr &face):m_has_new_data(false){
    
    m_faces = RingBuffer<Face::Ptr>::create(IDENTITY_FACE_BUFFER_SIZE);
    
    //create an input buffer...small.
    m_input_buffer = RingBuffer<Face::Ptr>::create();

    //set this Identity ID to that of the face that initialised this.
    m_id = face->id();
    add(face);
    std::cout << "[ Identity ] created a new ID: " << m_id << std::endl;
    
}


/*!
 \brief add a new face (that we think is appart of this  identity
 push onto the input buffer. and update the most recent.
 */
void Identity::add(const Face::Ptr &face){
    {
        std::lock_guard<std::mutex> lock(m_latest_face_mutex);
        m_latest_face = face;
        m_time=std::chrono::high_resolution_clock::now();
    }
    m_input_buffer->push(face);
}


double Identity::compareHistogram(const cv::Mat &hist){
    //TODO: loop through face list
    // for the mo, just get the most recent
    return m_faces->head()->compareHistogram(hist);
}
