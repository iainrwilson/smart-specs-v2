//
//  Logging.h
//  cpp_server
//
//  Created by Iain Wilson on 06/02/2019.
//

#ifndef Logging_h
#define Logging_h

#include <termcolor/termcolor.hpp>
#include <iostream>

#include <cstdarg>
inline  void __logI(const char * format,...){
    char buffer[256];
    va_list args;
    va_start (args,format);
    vsprintf (buffer, format,args);
    std::cout << termcolor::green << "[Info] " << buffer << std::endl;
    std::cout << termcolor::reset;
    va_end (args);
}
inline  void __logW(const char * format,...){
    char buffer[256];
    va_list args;
    va_start (args,format);
    vsprintf (buffer, format,args);
    std::cout << termcolor::yellow  << "[Warning] "<< buffer << std::endl;
    std::cout << termcolor::reset;
    va_end (args);
}
inline  void __logD(const char * format,...){
    char buffer[256];
    va_list args;
    va_start (args,format);
    vsprintf (buffer, format,args);
    std::cout << termcolor::magenta << "[Debug] " << buffer << std::endl;
    std::cout << termcolor::reset;
    va_end (args);
}
inline  void __logE(const char * format,...){
    char buffer[256];
    va_list args;
    va_start (args,format);
    vsprintf (buffer, format,args);
    std::cout << termcolor::red << "[Error] "<< buffer << std::endl;
    std::cout << termcolor::reset;
    va_end (args);
}

inline  void __log(const char * format,...){
    char buffer[256];
    va_list args;
    va_start (args,format);
    vsprintf (buffer, format,args);
    std::cout << buffer << std::endl;
    va_end (args);
}

#define LOG_TAG "SmartSpecs"

#ifdef TERMCOLOR

#define LOGI(...)  __logI(__VA_ARGS__)
#define LOGW(...)  __logW(__VA_ARGS__)
#define LOGD(...)  __logD(__VA_ARGS__)
#define LOGE(...)  __logE(__VA_ARGS__)

#else

#define LOGI(...)  __log(__VA_ARGS__)
#define LOGW(...)  __log(__VA_ARGS__)
#define LOGD(...)  __log(__VA_ARGS__)
#define LOGE(...)  __log(__VA_ARGS__)

#endif

#endif /* Logging_h */
