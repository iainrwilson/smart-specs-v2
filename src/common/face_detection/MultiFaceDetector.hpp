//
//  MultiFaceDetector.hpp
//  dlib
//
//  Created by Iain Wilson on 13/03/2019.
//

#ifndef MultiFaceDetector_hpp
#define MultiFaceDetector_hpp

#include <stdio.h>
#include <string>
#include "FaceDetector.hpp"
/*!
 \brief a class that encapsulates all of the face detectors,
 can run muliple detectors concurrently
 
 provided with a list of detectors
 
 opition - parallel process
 options - GPU
 options - filtering results
 
 */

const int PYR_LEVEL{0};

class MultiFaceDetector{

public:
    MultiFaceDetector();
    
    void add(const std::string &detector);
    
    std::vector<Face::Ptr> run(const Image::Ptr &image){
        if(m_parallel)
            return _runParallel(image);
        return _runSerial(image);
    };
    
private:
    
    std::vector<Face::Ptr> _runParallel(const Image::Ptr &image);
    std::vector<Face::Ptr> _runSerial(const Image::Ptr &image);

    std::vector<FaceDetectorBase::Ptr> m_detectors;
    bool m_parallel;
    
    
};
#endif /* MultiFaceDetector_hpp */
