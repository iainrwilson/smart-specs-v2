//
//  FaceDetector.hpp
//  cpp_server
//
//  Created by Iain Wilson on 21/03/2018.
//

#ifndef FaceDetector_hpp
#define FaceDetector_hpp

#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <string>
#include <dlib/image_processing.h>
#include <dlib/opencv.h>
#include "Face.hpp"
#include "common/timer.hpp"

#include "variables.hpp"

class FaceDetectorBase {

public:
    typedef std::shared_ptr<FaceDetectorBase> Ptr;
    FaceDetectorBase(const std::string &name);
    virtual ~FaceDetectorBase() = default;
    virtual std::vector<cv::Rect> detect(const cv::Mat &image)=0;
    
    std::string name()const {return m_name;}
    
    std::vector<cv::Point> detectLandmarks(const cv::Mat &img, const cv::Rect &roi);
    Face::Features detectLandmarks(const cv::Mat &image,const Face::Ptr face);

    std::vector<cv::Rect> detectEyes(const cv::Mat &image);
    void draw(cv::Mat &image,std::vector<cv::Rect> bboxs,const float scale=1.0);
    void drawPoints(cv::Mat &image, std::vector<cv::Point> points);
    
protected:
    std::string m_name;
    dlib::shape_predictor m_shape_predictor;
    cv::CascadeClassifier m_eye_cascade;

};



struct FaceDetector{
    typedef std::shared_ptr<FaceDetectorBase> Ptr;

    static FaceDetectorBase::Ptr create(const std::string &type);
};
#endif /* FaceDetector_hpp */
