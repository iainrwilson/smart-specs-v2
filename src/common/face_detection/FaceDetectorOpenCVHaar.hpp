//
//  FaceDetectorOpenCVHaar.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#ifndef FaceDetectorOpenCVHaar_hpp
#define FaceDetectorOpenCVHaar_hpp

#include <stdio.h>
#include "FaceDetector.hpp"
#include <opencv2/opencv.hpp>

class FaceDetectorOpenCVHaar : public FaceDetectorBase {

public:
    FaceDetectorOpenCVHaar();
    std::vector<cv::Rect> detect(const cv::Mat &image);
    std::vector<cv::Rect> detect(const cv::Mat &image,const cv::Size &minSize);

    
private:
    cv::CascadeClassifier m_face_cascade;
    
};
#endif /* FaceDetectorOpenCVHaar_hpp */
