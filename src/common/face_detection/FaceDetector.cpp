//
//  FaceDetector.cpp
//  cpp_server
//
//  Created by Iain Wilson on 21/03/2018.
//
#include "FaceDetector.hpp"

#include "FaceDetectorOpenCVHaar.hpp"
#include "FaceDetectorOpencvDnn.hpp"
#include "FaceDetectorDlibMmod.hpp"


FaceDetectorBase::Ptr FaceDetector::create(const std::string &type){
    
    if(type==FD_OPENCV_HAAR){
        return FaceDetectorBase::Ptr(new FaceDetectorOpenCVHaar);
    }else if(type == FD_OPENCV_DNN){
        return FaceDetectorBase::Ptr(new FaceDetectorOpenCVDnn);
    }else if(type == FD_DLIB_MMOD){
        return FaceDetectorBase::Ptr();
    }
    
    return FaceDetectorBase::Ptr();
}


FaceDetectorBase::FaceDetectorBase(const std::string &name):m_name(name){
    
    std::string home = getenv("SMARTSPECS_HOME");
    //std::string dataDir = "/Users/iain/experiments/face_detection/data/";
    std::string dataDir = home+"data/";
    std::string eye_cascade_name = "haarcascades/haarcascade_.xml";
    
    if(!m_eye_cascade.load(dataDir+eye_cascade_name))
        std::cout << "[FaceDetector] Error Loading Eye haarcascade" <<std::endl;
    
    dlib::deserialize(dataDir+"shape_predictor_68_face_landmarks.dat") >> m_shape_predictor;
    
}


std::vector<cv::Rect> FaceDetectorBase::detectEyes(const cv::Mat &image){
    SmartSpecs::Timer::begin("Eye Detect");

    std::vector<cv::Rect> results;
    double scaleFactor = 1.1;
    int minNeightbours = 2;
    int flags = 0;
    cv::Size minSize = cv::Size();
    cv::Size maxSize = cv::Size();
    m_eye_cascade.detectMultiScale(image,
                                    results,
                                    scaleFactor,
                                    minNeightbours,
                                    flags,
                                    minSize,
                                    maxSize);
    
    SmartSpecs::Timer::end("Eye Detect");
    return results;
}


Face::Features FaceDetectorBase::detectLandmarks(const cv::Mat &image,const Face::Ptr face){
    return detectLandmarks(image,face->roi());
}

std::vector<cv::Point> FaceDetectorBase::detectLandmarks(const cv::Mat &img, const cv::Rect &roi){
    //testing the dlib stuff.
    SmartSpecs::Timer::begin("Landmark Detect");
    cv::Mat image = img;
    if(img.channels()==3)
        cv::cvtColor(img, image, cv::COLOR_BGR2GRAY);
    
    //convert cv::Mat to dlib compatable image.
    dlib::cv_image<uchar> cimg(image);
    dlib::rectangle rect((long)roi.tl().x, (long)roi.tl().y, (long)roi.br().x - 1, (long)roi.br().y - 1);
    
    dlib::full_object_detection shape = m_shape_predictor(cimg,rect);
    std::vector<cv::Point> points;
    for (int i=0;i<shape.num_parts();i++){
        cv::Point point(shape.part(i).x(),shape.part(i).y());
        points.push_back(point);
    }
    SmartSpecs::Timer::end("Landmark Detect");
    return points;
}

void FaceDetectorBase::draw(cv::Mat &image, std::vector<cv::Rect> bboxs,const float scale){
    for (auto const& box: bboxs ){
        cv::Rect _box(box.height*scale,box.width*scale,box.x*scale,box.y*scale);
        cv::rectangle(image,_box,cv::Scalar(255,0,0));
    }
}

void FaceDetectorBase::drawPoints(cv::Mat &image, std::vector<cv::Point> points){
    for(auto const &point:points){
        cv::circle(image, point, 1, cv::Scalar(0,255,0));
    }
}

