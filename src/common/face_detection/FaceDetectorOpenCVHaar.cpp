//
//  FaceDetectorOpenCVHaar.cpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#include "FaceDetectorOpenCVHaar.hpp"

FaceDetectorOpenCVHaar::FaceDetectorOpenCVHaar() : FaceDetectorBase(FD_OPENCV_HAAR){
    //datadir
    std::string home = getenv("HOME");
    //std::string dataDir = "/Users/iain/experiments/face_detection/data/";
    std::string dataDir = home+"/experiments/face_detection/data/";
    
    std::string face_cascade_name = "haarcascades/haarcascade_frontalface_alt2.xml";
    std::string eye_cascade_name = "haarcascades/haarcascade_eye.xml";
    
    // Load Face Detector
    if(!m_face_cascade.load(dataDir+face_cascade_name))
        std::cout << "Error Loading haarcascade" <<std::endl;
    
}

std::vector<cv::Rect> FaceDetectorOpenCVHaar::detect(const cv::Mat &img){
    return detect(img,cv::Size(10,10));
}

std::vector<cv::Rect> FaceDetectorOpenCVHaar::detect(const cv::Mat &img, const cv::Size &minSize){
    SmartSpecs::Timer::begin("[FaceDetectorOpenCVHaar] Detect");
    
    cv::Mat input;
    if(img.channels()==3){
        cv::cvtColor(img,input,cv::COLOR_BGR2GRAY);
    }
    else
        input = img;
    
    std::vector<cv::Rect> results;
    double scaleFactor = 1.1;
    int minNeightbours = 3;
    int flags = 0;
    
    cv::Size maxSize = cv::Size();
    m_face_cascade.detectMultiScale(input,
                                    results,
                                    scaleFactor,
                                    minNeightbours,
                                    flags,
                                    minSize,
                                    maxSize);
    
    SmartSpecs::Timer::end("[FaceDetectorOpenCVHaar] Detect");
    return results;
}
