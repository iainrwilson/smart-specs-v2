//
//  MultiFaceDetector.cpp
//  dlib
//
//  Created by Iain Wilson on 13/03/2019.
//

#include "MultiFaceDetector.hpp"
#include <ThreadPool.h>

MultiFaceDetector::MultiFaceDetector():m_parallel(false){
    add(FD_OPENCV_DNN);
//    add(FD_OPENCV_HAAR);
}

void MultiFaceDetector::add(const std::string &detector){
    
    auto fd = FaceDetector::create(detector);
    if(fd.get() == nullptr)
        return;
    
    m_detectors.push_back(fd);
    
    //if only one, just do serial - if more. go parallel
    if(m_detectors.size() >1)
        m_parallel = true;
}

std::vector<Face::Ptr> MultiFaceDetector::_runParallel(const Image::Ptr &image){
    
    //assumes parallel
    
    ThreadPool pool(m_detectors.size());
    std::vector<std::future<std::pair<std::string,std::vector<cv::Rect>>>> results;
    
    for(const auto &det:m_detectors){
        results.emplace_back(pool.enqueue([&det,&image]{
            auto faces =  det->detect(image->getPyr(PYR_LEVEL));
            return std::pair<std::string,std::vector<cv::Rect>>(det->name(),faces);
        }));
    }
   
    
    //as we are using a pyramid image, check the level.
    int s = 1<<PYR_LEVEL;
    
    std::vector<Face::Ptr> out_faces;
    for(auto && result: results){
        auto _res = result.get();
        std::string _type = _res.first;
        auto faces = _res.second;
        for(auto &r:faces){
            
            cv::Rect _f(r.x*s,r.y*s,r.width*s,r.height*s);
            Face::Ptr face = Face::create(_f,image);\
            face->setType(_type);
            out_faces.push_back(face);
        }
    }
    return out_faces;
}

std::vector<Face::Ptr> MultiFaceDetector::_runSerial(const Image::Ptr &image){

    std::vector<Face::Ptr> out_faces;
    const int s = 1<<PYR_LEVEL;
    for(const auto &det:m_detectors){
        auto faces =  det->detect(image->getPyr(PYR_LEVEL));
        for(auto &r:faces){
            cv::Rect _f(r.x*s,r.y*s,r.width*s,r.height*s);
            Face::Ptr face = Face::create(_f,image);\
            face->setType(det->name());
            out_faces.push_back(face);
        }
    }
    return out_faces;
}
