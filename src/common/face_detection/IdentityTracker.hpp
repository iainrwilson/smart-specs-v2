//
//  IdentityTracker.hpp
//  cpp_server
//
//  Created by Iain Wilson on 23/01/2019.

/*

This class manages all the faces
 
it tracks individual faces, handles a faces lifecycle, deletes or updates.
 
 Holds a short history (10 frames), used to check the lifecycle of a face.

 Input is the output of a face detextor, <can be generalised to an Object Tracker as I may use this for text>
 Input is a frame <Image>
 
 output is a face object.
 
*/

#ifndef IdentityTracker_hpp
#define IdentityTracker_hpp

#include <stdio.h>

#include "common/Thread.hpp"
#include "Face.hpp"
#include "common/Image.hpp"
#include "tracker/Tracker.hpp"
#include "common/RingBuffer.hpp"
#include "skin_segmentation/SkinSegmenter.hpp"
#include "Identities.hpp"
#include "common/Thread.hpp"
//#include "face_detection/FaceRecognition.hpp"



class IdentityTracker : public Thread{
    /*!
     
     Identity detection:
        Uses a simple or complex approach
    Simple:
        Use existing positional and Histogram matching
    Complex:
        Dlib of FaceNet embeddings approach (needs GPU)
     
     
     most of the work is perfmored in Identities class, this is more of a wrapper / interface.
     
*/
public:
    
    typedef std::shared_ptr<IdentityTracker> Ptr;
    IdentityTracker();
    static Ptr create(){ return std::make_shared<IdentityTracker>();}


    /*!
     \brief Add a face, found by a face detector. Is processes by the Identty class - either matced to an exsiting ID or a new one is created.
     */
    void add(Face::Ptr &face);
    
    
    /*!
     \brief returns a list of the most recent valid faces.
     */
    std::vector<Face::Ptr> currentFaces(){return m_identities->currentFaces();}
    
    /*!
     \brief uses OpenCV to draw a list of current Identities, good for debugging.
     */
    cv::Mat showIds(){ return m_identities->showIds();}
    
    /*!
     \brief accessor to the identies.
     */
    Identities::Ptr identities()const {return m_identities;}
    
    void thread_run();
    
private:

    
    Identities::Ptr m_identities;
    std::mutex m_identity_mutex;
    
//    FaceRecognition::Ptr m_face_recognition;

    
};
#endif /* IdentityTracker_hpp */
