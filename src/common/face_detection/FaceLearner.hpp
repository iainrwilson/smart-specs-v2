//
//  FaceLearner.hpp
//  cpp_server
//
//  Created by Iain Wilson on 14/12/2018.
//

#ifndef FaceLearner_hpp
#define FaceLearner_hpp

#include <stdio.h>
#include "Face.hpp"
#include "skin_segmentation/SkinSegmenter.hpp"
#include "common/RingBuffer.hpp"
#include "common/Thread.hpp"




class FaceLearner : public Thread {
    /*
        class that manages online learning of a face.
     
     Skin segmentation:
        Data Generation
            uses incomming faces to create training data for skin segmentation
        Trains models.
            Uses new data to continioisly update model.
            Currentlu not using any streaming approaches, but could do.
     
        Stores all data for offline processing / validation and checking.
        Stores models for later use.
     
     Face Detection QA:
     Uses above tools (mostly skin seg) to aid tracker
     Not exaclty sure how to implement this.
     */
    
public:
    typedef std::shared_ptr<FaceLearner> Ptr;
    FaceLearner();
    
    static Ptr create(){ return std::make_shared<FaceLearner>(); }
    
    /*!
     \brief load an old, saved, model.
     
     TODO
     */
    void loadModel();
    
    /*!
     \brief save the current model to file.
     */
    void saveModel();
    
    
    /*!
     \brief do most of the work:
     performs skin seg training
     */
    void thread_run();
    
    /*!
     \brief add a face, place it on the input buffer.
     */
    void add(Face::Ptr &face);
    
    /*!
     \brief save the segmented face to file, for later use and debugginf
     */
    void save(Face::Ptr &face, const std::string &filename=std::string());

    
    /*!
     \brief train the skin segmenter
     */
    void train();
    
    /*!
     \brief perform skin detection - run the classifier.
     */
    cv::Mat detectSkin(const cv::Mat &image){
        if(m_use_lookup)
            return _detectSkin(image);
        
        std::lock_guard<std::mutex> lck(m_seg_mutex);
        SmartSpecs::Timer::begin("[ FaceLearner ] detectSkin()");
        cv::Mat skin = m_skin_seg_c->run(image);
        SmartSpecs::Timer::end("[ FaceLearner ] detectSkin()");
        return skin;
    }
    
    
    /*!
     \brief use the lookup table as a skin detector
     */
    cv::Mat _detectSkin(const cv::Mat &image){
//        SmartSpecs::Timer::begin("[ FaceLearner ] detectSkin() -- lookup");
//
        cv::Mat out = cv::Mat::zeros(image.size(),CV_8UC1);
        
        int nrows = image.rows;
        int ncols = image.cols;
        if(image.isContinuous()){
            ncols*=nrows;
            nrows=1;
        }
        
        const cv::Vec3b* p;
        for(int y=0; y<nrows; ++y){
            p = image.ptr<cv::Vec3b>(y);
            uint8_t* o = out.ptr<uint8_t>(y);
            uint32_t i = 0;
            uint8_t *dst = reinterpret_cast<uint8_t*>(&i);
          //  #pragma omp parallel for
            for(int x=0; x<ncols; ++x){
//                uint8_t* src = reinterpret_cast<uint8_t*>(p[x]);
                memcpy(dst,&p[x],sizeof(uint8_t)*3);
                o[x] = m_lookup[i];
            }
        }
        
//        SmartSpecs::Timer::end("[ FaceLearner ] detectSkin() -- lookup");
        return out;
    }



    /*!
     \brief is the skin segmenter ready (has it been trained?)
     */
    bool isReady()const {return m_skin_seg_ready;}
    //SkinSegmenter::Ptr getSkinClassifier(){return m_skin_seg_c;}

    
    /*!
     \brief show a grid of the faces used for training.
     */
    cv::Mat showFaces();
    
    cv::Mat candidates(){return m_candidates;}
    
private:

    cv::Mat m_candidates;
    
    /*!
     \brief select candidate faces from input buffer.
     
     criteria:
        1) Frontal faces - use the most frontal face (use dlib)
        2) Histogram comparison - use the most different.
     */
    Face::Ptr selectCandidate(cv::Mat &out);
    

    /*!
     \brief inint a lookup table....this should be just a load...being lazy.
     */
    void _initTable(){
        if(m_table_ready)
            return;
        const int size = 16777216;
        m_table = cv::Mat::zeros(1,size,CV_8UC3);
        auto ptr = m_table.ptr<cv::Vec3b>();
        for(uint32_t i=0;i<size;++i){
            uint8_t* p = reinterpret_cast<uint8_t*>(&i);
            ptr[i][0] = p[0];
            ptr[i][1] = p[1];
            ptr[i][2] = p[2];
        }
        m_table_ready = true;
    }
    
    /*!
     \brief generate lookup table from classifier.
     */
    void _createLookup(){
        SmartSpecs::Timer::begin("[ FaceLearner ] create lookup()");

        cv::Mat _table;
        _initTable();
        {
            std::lock_guard<std::mutex> lck(m_seg_mutex);
            _table = m_skin_seg_c->run(m_table);
        }
        const int size = 16777216;
        memcpy(&m_lookup[0],_table.data,sizeof(uint8_t)*size);
        
        SmartSpecs::Timer::end("[ FaceLearner ] create lookup()");
    };
    
    //uint8_t *m_lookup;
    std::vector<uint8_t> m_lookup;
    bool m_use_lookup;
    
    cv::Mat m_table;
    bool m_table_ready;
    
    bool m_skin_seg_ready;
    
    RingBuffer<Face::Ptr>::Ptr m_input_buffer;
    RingBuffer<Face::Ptr>::Ptr m_faces;
    
    SkinSegmenter::Ptr m_skin_seg_c; //current
    SkinSegmenter::Ptr m_skin_seg_t; //being trained
    SkinSegmenter::Ptr m_skin_seg;

    std::mutex m_seg_mutex;
    
    std::string m_data_dir;
    std::string m_output_folder;
    
};
#endif /* FaceLearner_hpp */
