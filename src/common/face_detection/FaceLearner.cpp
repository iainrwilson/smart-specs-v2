//
//  FaceLearner.cpp
//  cpp_server
//
//  Created by Iain Wilson on 14/12/2018.
//

#include "FaceLearner.hpp"
#include "variables.hpp"
#include <boost/filesystem.hpp>
#include <mlpack/methods/kmeans/kmeans.hpp>

FaceLearner::FaceLearner():Thread("FaceLearner"),m_lookup(16777216,0),m_use_lookup(true),m_table_ready(false),m_skin_seg_ready(false){
    
    //we need two skin segmenter, one to be trained, and another to run.
    m_skin_seg_c = SkinSegmenter::create(FACELEARNER_SEGMENTER_TYPE);
    m_skin_seg_c->setColorConversion(FACELEARNER_COLOURSPACE);
    
    m_skin_seg_t = SkinSegmenter::create(FACELEARNER_SEGMENTER_TYPE);
    m_skin_seg_t->setColorConversion(FACELEARNER_COLOURSPACE);
    
    
    //try a massive input buffer
    m_input_buffer = RingBuffer<Face::Ptr>::create(2);
    
    //segmenter used in training of other segmenters.
    m_skin_seg = SkinSegmenter::create(SEG_DECISION);
    m_skin_seg->setColorConversion(FACELEARNER_COLOURSPACE);
    m_skin_seg->setDataset(DATA_PRA);
    if(!m_skin_seg->load())
        SkinSegmenter::train(m_skin_seg, DATA_PRA);
    
    
    //make this buffer quite large - experiment with different sizes.
    m_faces = RingBuffer<Face::Ptr>::create(FACELEARNER_MAX_NO_FACES);
    
    
    //create an output folder name
    std::stringstream ss;
    ss << getenv("SMARTSPECS_HOME") << "/data/recorded/";
    m_data_dir  = ss.str();
    ss << std::chrono::system_clock::now().time_since_epoch().count();
    ss << "/";
    m_output_folder = ss.str();
    
    //try loading an olde model (before we create this outpuut dir
    
    //  loadModel();
    
    //make the actual output dir.
    boost::filesystem::path dir(m_output_folder);
    if(boost::filesystem::create_directory(dir))
        std::cout << "[ FaceLearner ] Created " << m_output_folder <<std::endl;
    else
        std::cout << "[ FaceLearner ] Error Creating " << m_output_folder <<std::endl;
    
    
}

void FaceLearner::add(Face::Ptr &face){
    // push a face on to the input buffer, ready to be processed.
    m_input_buffer->push(face);
}


void FaceLearner::save(Face::Ptr &face,const std::string &filename){

    auto roi = face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
    auto _t = face->timestamp();
   

    std::stringstream ss;
    ss << m_output_folder << "face_" ;
    if(filename == std::string())
        ss << _t.time_since_epoch().count();
    else
        ss << filename;
    
    cv::FileStorage fs(ss.str()+"_rect.yml", cv::FileStorage::WRITE);
    if( fs.isOpened() ){
        fs << "roi" << roi;
        fs.release();
    }
    
    cv::imwrite(ss.str()+"_image.tiff",face->trainigImage());
    cv::imwrite(ss.str()+"_label.tiff",face->fgdMask()(roi));
    cv::imwrite(ss.str()+"_mask.tiff",face->gcMask()(roi));

}


void FaceLearner::thread_run(){
//    if(m_input_buffer->size()<50)
//        return;
    
   
//    auto face = selectCandidate(m_candidates);
    auto face = m_input_buffer->pop();
   
    if(!face->isValid())
        return;
    
    bool okay = face->createTrainingData(m_skin_seg);
    
    if(okay){
        //add face to the faces buffer - this is used for the training etc.
        m_faces->push(face);
        
        //train the clasiffier.
        train();
        
        //save the data for offline assesments.
        save(face);
    }
}

Face::Ptr FaceLearner::selectCandidate(cv::Mat &out){
    SmartSpecs::Timer::begin("[ FaceLearner ] selectCandidate()");

    //get snapshot
    auto faces = m_input_buffer->getSnapshot();
    
    //use blur detection.
    // sort on blur
//    std::sort(faces.begin(),faces.end(),[](Face::Ptr a, Face::Ptr b){ return (a->calcBlur()<b->calcBlur()); });
    
    
    
    //kmeans on the histograms....
    //create dataset //H_BINS,S_BINS = 64, CV_32F
    int _max_h=0;
    int _max_w=0;
    
    arma::mat data(H_BINS,faces.size(),arma::fill::zeros);
    size_t clusters = 5;
    for(int i=0;i<faces.size();++i){
        cv::Mat _hist = faces[i]->imageHistogram();
        arma::colvec hist(reinterpret_cast<double*>(_hist.data),H_BINS);
        data.col(i) = hist;
        
        if(faces[i]->roi().width > _max_w)
            _max_w = faces[i]->roi().width;
        
        if(faces[i]->roi().height > _max_h)
            _max_h = faces[i]->roi().height;
    }
    
    arma::Row<size_t> assignments;
    mlpack::kmeans::KMeans<> k;
    k.Cluster(data, clusters, assignments);
    
    //order the faces into their clusters
    std::vector<std::vector<Face::Ptr>> face_clusters(clusters,std::vector<Face::Ptr>());
    for(int i=0;i<assignments.size();++i){
        face_clusters[assignments[i]].push_back(faces[i]);
    }
    
    
    //draw the clusters
    std::vector<cv::Mat> image_clusters(clusters,cv::Mat());
    //creat an mat per cluster, go horizontally
    int _max_c_w=0;
    for(int i=0;i<clusters;++i){
        cv::Mat _c(_max_h,_max_w*face_clusters[i].size(),CV_8UC3);
//        for(auto const &face:face_clusters[i]){
        for(int j=0;j<face_clusters[i].size();++j){
            auto _froi = face_clusters[i][j]->roi();
            cv::Rect _roi(_max_w*j,0,_froi.width,_froi.height);
            face_clusters[i][j]->faceImage().copyTo(_c(_roi));
        }
        image_clusters[i] = _c;
        if(_c.cols > _max_c_w)
            _max_c_w=_c.cols;
    }
    
    //merge the mats into one.
    cv::Mat cluster_image = cv::Mat::zeros(_max_h*clusters,_max_c_w,CV_8UC3);
    int y=0;
    for(auto const &c:image_clusters){
        c.copyTo(cluster_image(cv::Rect(0,y,c.cols,c.rows)));
        y+=c.rows;
    }
    
    
    //cv::imshow("clusters",cluster_image);
    
    out = cluster_image;
    
    SmartSpecs::Timer::end("[ FaceLearner ] selectCandidate()");
    return faces[0];
}



/*!
 \brief
 using all the valid identities, train the skin classifier on the stored images.
*/
void FaceLearner::train(){
    // is the buffer full
    if(m_faces->size() <= FACELEARNER_MIN_NO_FACES)
        return;

    SmartSpecs::Timer::begin("[ FaceLearner ] train()");
    
    //loop through collect data
    arma::mat all_data;
    arma::mat all_labels;
    for(auto &face:*m_faces){
        all_data = arma::join_rows(all_data,face->data());
        all_labels = arma::join_rows(all_labels,face->labels());
    }
    
    //do we actualy have any valid IDs and faces?
    if(all_data.size()==0)
        return;
    
    arma::Row<size_t> labels_row = arma::conv_to<arma::Row<size_t>>::from(all_labels.row(0));
    m_skin_seg_t->train(all_data,labels_row);
    
    //save the model - every time?
    saveModel();
    
    
    //swap the segs --  think about syncronisation and thread safety.
    {
        std::lock_guard<std::mutex> lck(m_seg_mutex);
        m_skin_seg_t.swap(m_skin_seg_c);
    }
    
    _createLookup();
    m_skin_seg_ready = true;
    
    SmartSpecs::Timer::end("[ FaceLearner ] train()");

}


void FaceLearner::loadModel(){
    //look for old models ... load the most recent.
    boost::filesystem::path p(m_data_dir);
    boost::filesystem::directory_iterator it(p);
    
    std::vector<std::string> dirs;
    
    for(;it!=boost::filesystem::directory_iterator();++it){
        if(!boost::filesystem::is_directory(it->path()))
            continue;
        dirs.push_back(it->path().string());
    }
    std::sort(std::begin(dirs),std::end(dirs));
    //try and load from the last folder.
    
    //be very carefull here. If the scene is very differe (lighting etc) it will go badly wrong.
    
    
    m_skin_seg_ready = m_skin_seg_c->load(dirs.back());
    if(m_skin_seg_ready){
        m_skin_seg->setDataset(DATA_NONE); //set b
        m_skin_seg->load(dirs.back());
    }
}

void FaceLearner::saveModel(){
    //use the current data folder for saving
    if(!this->isReady())
        return;
    
    //only save a model that has mode than 4 faces
    //the skin seg becomes active after a lower number (2), but may be worse quality.
    if(m_faces->size() < 4)
        return;
    
    m_skin_seg_t->save(m_output_folder);   
}

cv::Mat FaceLearner::showFaces(){
    
    //loop through and draw to an nxm image
    // 10xm
    auto faces = m_faces->getSnapshot();
    
    size_t _size = faces.size();
    int rows =_size/10;
    if(_size%10 != 0)
        ++rows;
    
   
    
    int max_w=0,max_h=0;
    for(auto &face:faces){
        if(max_w<face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).width)
            max_w=face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).width;
        if(max_h<face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).height)
            max_h=face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD).height;
    }

    
    int cols = 10;
    if(rows == 1)
        cols = _size;
    
    
    cv::Mat out = cv::Mat::zeros(max_h*rows,max_w*cols,CV_8UC3);
    int i=0;
    for(int y=0;y<rows;++y){
        for(int x=0;x<cols;++x){
            
            if(i>=_size)
                break;
            
            auto face = faces[i];
            auto _roi = face->roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
            
            cv::Rect roi(x*max_w,
                         y*max_h,
                         _roi.width,
                         _roi.height);
            face->maskedFace().copyTo(out(roi));
            ++i;
        }
    }
    return out;
}
