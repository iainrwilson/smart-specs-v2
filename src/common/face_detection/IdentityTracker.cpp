//
//  IdentityTracker.cpp
//  cpp_server
//
//  Created by Iain Wilson on 23/01/2019.
//

#include "IdentityTracker.hpp"
#include "skin_segmentation/HistogramSkinSegmenter.hpp"
#include <memory>

IdentityTracker::IdentityTracker():Thread("Face Tracker"){
    
    m_identities = Identities::create();
  
}

void IdentityTracker::add(Face::Ptr &face){
    m_identities->add(face);
}

/*!
 \brief  - thread that runs in the backgroud,
 
 perorms clean up and reorganisaiton of the identites.
 Runs skin seg learners.
 
 */
void IdentityTracker::thread_run(){

    //run the Identity clean up - reomve old ones. run every 2 seconds.
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    m_identities->clean();

}

