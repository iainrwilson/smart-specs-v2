//
//  FaceDetectorOpencvDnn.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#ifndef FaceDetectorOpencvDnn_hpp
#define FaceDetectorOpencvDnn_hpp

#include <stdio.h>
#include "FaceDetector.hpp"
#include <opencv2/core.hpp>
#include <opencv2/dnn.hpp>


class FaceDetectorOpenCVDnn : public FaceDetectorBase{
  
public:
    
    FaceDetectorOpenCVDnn();
    std::vector<cv::Rect> detect(const cv::Mat &image);

private:
    
    cv::dnn::Net m_net;
    
    
    
    
};


#endif /* FaceDetectorOpencvDnn_hpp */
