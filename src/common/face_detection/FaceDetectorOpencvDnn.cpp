//
//  FaceDetectorOpencvDnn.cpp
//  cpp_server
//
//  Created by Iain Wilson on 07/12/2018.
//

#include "FaceDetectorOpencvDnn.hpp"

FaceDetectorOpenCVDnn::FaceDetectorOpenCVDnn() : FaceDetectorBase(FD_OPENCV_DNN){
    
    std::string home = getenv("SMARTSPECS_HOME");
    std::string dataDir = home+"/data/models/";
    const std::string caffeConfigFile = dataDir+"deploy.prototxt";
    const std::string caffeWeightFile = dataDir+"res10_300x300_ssd_iter_140000_fp16.caffemodel";
    
    const std::string tensorflowConfigFile = dataDir+"opencv_face_detector.pbtxt";
    const std::string tensorflowWeightFile = dataDir+"opencv_face_detector_uint8.pb";
    
#ifdef USE_CAFFE
    m_net = cv::dnn::readNetFromCaffe(caffeConfigFile, caffeWeightFile);
#else
    m_net = cv::dnn::readNetFromTensorflow(tensorflowWeightFile, tensorflowConfigFile);
#endif
    
    
}





std::vector<cv::Rect> FaceDetectorOpenCVDnn::detect(const cv::Mat &image){
    SmartSpecs::Timer::begin("[FaceDetectorOpenCVDnn] Detect");
    
    
    const size_t inWidth = 300;
    const size_t inHeight = 300;
    const double inScaleFactor = 1.0;
    const float confidenceThreshold = 0.4;
    const cv::Scalar meanVal(104.0, 177.0, 123.0);
        
    int frameHeight = image.rows;
    int frameWidth = image.cols;
#ifdef USE_CAFFE
    cv::Mat inputBlob = cv::dnn::blobFromImage(image, inScaleFactor, cv::Size(inWidth, inHeight), meanVal, false, false);
#else
    cv::Mat inputBlob = cv::dnn::blobFromImage(image, inScaleFactor, cv::Size(inWidth, inHeight), meanVal, true, false);
#endif
    
    m_net.setInput(inputBlob, "data");
    cv::Mat detection = m_net.forward("detection_out");
    cv::Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());
    std::vector<cv::Rect> rois;
    
    for(int i = 0; i < detectionMat.rows; i++){
        float confidence = detectionMat.at<float>(i, 2);
        if(confidence > confidenceThreshold){
            int x1 = static_cast<int>(detectionMat.at<float>(i, 3) * frameWidth);
            int y1 = static_cast<int>(detectionMat.at<float>(i, 4) * frameHeight);
            int x2 = static_cast<int>(detectionMat.at<float>(i, 5) * frameWidth);
            int y2 = static_cast<int>(detectionMat.at<float>(i, 6) * frameHeight);
            rois.push_back(cv::Rect(x1,y1,x2-x1,y2-y1));
        }
    }

    
    SmartSpecs::Timer::end("[FaceDetectorOpenCVDnn] Detect");
    return rois;
}
