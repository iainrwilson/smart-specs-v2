#include "TCPServerAsync.hpp"
#include "common/Logging.hpp"

namespace SmartSpecs {
  namespace Networking {
  

    /******************** CONSTRUCTOR *************************/
    TCPServerAsync::TCPServerAsync(const std::string& port, QueryProcessor::Ptr queryProcessor):Thread("TCPServerAsync"),
      m_io_service(),
      m_signals(m_io_service),
      m_acceptor(m_io_service),
      m_manager(),
      m_socket(m_io_service),
      m_queryProcessor(queryProcessor)
    {
      // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
      try{
            asio::error_code ec;
            asio::ip::tcp::resolver resolver(m_io_service);
            asio::ip::tcp::endpoint endpoint(asio::ip::tcp::v4(),std::stoi(port)); //= *resolver.resolve({address, port});
            m_acceptor.open(endpoint.protocol(),ec);
            if(ec)
                LOGE("[TCPServerAsync::start] acceptor open error: %s",ec.message().c_str());
            m_acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true),ec);
            if(ec)
                LOGE("[TCPServerAsync::start] set_option error: %s",ec.message().c_str());
            m_acceptor.bind(endpoint);
            if(ec)
                LOGE("[TCPServerAsync::start] bind error: %s",ec.message().c_str());
          
          LOGI("[TCPServerAsync] Starting server on: %s",port.c_str());

            m_acceptor.listen();
            do_accept();
      }catch(std::exception &e){
          LOGE("[TCPServerAsync::TCPServerAsync()] %s",e.what());
      }
    }

    /************************ PUBLIC MEMBERS  **********************/
    
    void TCPServerAsync::thread_run(){
      try{  
          m_io_service.run();
        }catch(std::exception &e){
          LOGE("[TCPServerAsync::thread_run()] %s",e.what());
          this->stop();
      }
    }

    void TCPServerAsync::stop(){
       try{ 
           m_acceptor.close();
           m_manager.stop_all();
      }catch(std::exception &e){
          LOGE("[TCPServerAsync::stop()] %s",e.what());
      }
      Thread::stop();
    }

   
    /************************* PRIVATE MEMBERS ***********************/

      
      void TCPServerAsync::do_accept(){
          m_acceptor.async_accept(m_socket,
                                  [this](std::error_code ec){
                                      // Check whether the server was stopped by a signal before this
                                      // completion handler had a chance to run.
                                      if(!m_acceptor.is_open()) {
                                          return;
                                      }
                                      
                                      if(!ec){
                                          m_manager.start(std::make_shared<Connection>(
                                                                                       std::move(m_socket),
                                                                                       m_manager,
                                                                                       RequestHandler::create(m_queryProcessor)
                                                                                       )
                                                          );
                                      }
                                      do_accept();
                                  });
          
      }

   
   
  }
}
