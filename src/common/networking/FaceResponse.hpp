//
//  FaceResponse.hpp
//  smart_specs_server
//
//  Created by Iain Wilson on 18/02/2019.
//

#ifndef FaceResponse_hpp
#define FaceResponse_hpp

#include <stdio.h>

#include "Response.hpp"
namespace SmartSpecs{
    /*!
     \brief  This class returns identity information for a given face.
     */
    class FaceResponse: public ResponseBase{
        typedef std::shared_ptr<FaceResponse> Ptr;
        FaceResponse():ResponseBase(Request::TEXT){}
        
        FaceResponse(ResponseBase const& response):ResponseBase(response){
        }
        
        void encode();
        void decode();
        
    private:
        uint64_t m_id;
    };
}

#endif /* FaceResponse_hpp */
