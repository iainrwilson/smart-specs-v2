//
//  TextResponse.hpp
//  smart_specs_server
//
//  Created by Iain Wilson on 18/02/2019.
//

#ifndef TextResponse_hpp
#define TextResponse_hpp

#include <stdio.h>
#include "Response.hpp"

namespace SmartSpecs{
    class TextResponse: public ResponseBase {
    public:
        typedef std::shared_ptr<TextResponse> Ptr;
        TextResponse():ResponseBase(Request::TEXT){}
        
        TextResponse(ResponseBase const& response):ResponseBase(response){
        }
        
        
        
        void encode();
        void decode();
    };

    
}
#endif /* TextResponse_hpp */
