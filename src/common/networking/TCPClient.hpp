#ifndef __SMARTSPECS__NETWORKING__TCPCLIENT_H
#define __SMARTSPECS__NETWORKING__TCPCLIENT_H

#include <ctime>
#include <iostream>
#include <string>
#include <asio.hpp>
#include <memory>
#include <opencv2/opencv.hpp>
#include <boost/array.hpp>
#include "Messages.hpp"
#include "Response.hpp"

using asio::ip::tcp;

namespace SmartSpecs {

	namespace Networking{
		class TCPClient{

		public:
            typedef std::shared_ptr<TCPClient> Ptr;

			TCPClient();
            TCPClient(const std::string& host);
			bool ping();
            
			void setRemote(const std::string host, const std::string port);
            std::shared_ptr<tcp::socket> connect();
            size_t read(std::shared_ptr<tcp::socket> socket,std::vector<char>& readbuf);

            
			const std::string sendQuery(const std::string& query);
            const bool sendFrame( const cv::Mat& frame);
            const bool sendFrame( const Image::Ptr& frame);

            const std::string sendMessage(Message::Ptr &message);
            const std::string sendResponse(Response::Ptr &response);
            
            static std::shared_ptr<TCPClient> create() {
                return std::make_shared<TCPClient>();
            }
            
        private:
			asio::io_service m_io_service;
			std::string m_host;
			std::string m_port;

		};
	}
}

#endif //__SMARTSPECS__NETWORKING__TCPCLIENT_H
