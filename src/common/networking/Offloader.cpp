//
//  Offloader.cpp
//  asio_client
//
//  Created by Iain Wilson on 19/07/2017.
//
//

#include "Offloader.hpp"

#include "common/Logging.hpp"

namespace SmartSpecs{
    
    
    Offloader::Offloader(RingBuffer<Image::Ptr>::Ptr in_buff, RingBuffer<Response::Ptr>::Ptr res, const std::string& ip_address):Thread("offloader"){
        m_buffer = in_buff;
        m_results_buffer = res;
        m_tcp = Networking::TCPClient::create();
        m_tcp->setRemote(ip_address,"8081");
    }
    
    
    
    void Offloader::send_request(const std::string &type){
        
        /// send a request to the remote server.
        // e.g. Face / person detect, Text detect, superresolution
        
        if (type == Request::PING){
        
            //connect to remote;
            std::shared_ptr<tcp::socket> socket = m_tcp->connect();
             m_tcp->sendQuery(Request::PING);
            
        }else if(type == Request::SAVE){
            /*
             Send single frame to be saved.
             Testing communications.
             */
            
            //set the remote server mode
            m_tcp->sendQuery(Request::SAVE);
        }
        else if(type == Request::FACE){
            /*
             Send single frame expect bounding boxes in response
             */
            
        }else if (type == Request::TEXT){
            /*
             Send single frame expect bounding boxes in response
             */
            
            //get the most recent frame from the buffer
            Image::Ptr img = m_buffer->head();
            if (img.get() == 0)
                return;
            
            //connect to remote;
            std::shared_ptr<tcp::socket> socket = m_tcp->connect();
            
            //send request type
            //m_control_tcp->sendQuery(Request::TEXT);
            
            //send frame
            //const bool success = m_tcp->sendFrame(socket, img);
//            if (!success){
//                LOGE("[Offloader::send_request] Error in sending Frame");
//                return;
//            }
            
            Response::Ptr response = Response::create(Request::TEXT);
            std::vector<char> buffer(1024);
            size_t count = m_tcp->read(socket,buffer);
            if (count == 4){
                //then read again. as results are the incomming data size;
                // first lot should be a number
                unsigned int fnumber = *(unsigned int*)&buffer[0];
                std::cout << "[Offloader::send_request] 1st recieved: " << count << " bytes; " << fnumber << std::endl;

                count = m_tcp->read(socket,buffer);
            }
            
            if(count==0){
                LOGE("[Offloader::send_request] Error in reading");
                return;
            }
//            response->process(buffer,count);
            m_results_buffer->push(response);
            return;
            
        }else if (type == Request::SUPERRES){
            /*
             Send multiple frames
            
             Depending on buffer size, send N frames starting from time of request.
             */

            //init server
            m_tcp->sendQuery(Request::SUPERRES_LOAD);
            
            //copy data from frame_buffer, buffer is a FILOd
            std::vector<Image::Ptr> frames = m_buffer->getSnapshot();
            std::vector<Image::Ptr>::iterator it = frames.begin();
            
            //connect to remote;
            
            LOGD("[Offloader::send_request] sending %d frames",frames.size());
            
            for(;it!=frames.end();++it){
                //std::shared_ptr<tcp::socket> socket = m_tcp->connect();
                bool err = m_tcp->sendFrame((*it)->get());
                if(!err)
                    break;
            }
            
            //TODO flesh this out... each read is unique to the request type
            /*std::vector<char> buffer;
            size_t count = m_tcp->read(socket,buffer);
            m_tcp->read(socket,buffer);
            if(count==0){d
                LOGE("[Offloader::send_request] Error in reading");
                return;
            }*/
            
            
           // m_control_tcp->sendQuery(Request::SUPERRES_RUN);
            
            
            return;
            
        }else if (type == Request::LOWLIGHT){
            /*
             Send multiple frames
             */
        }
        else{
            LOGE("[Offloader::send_request] Unknown request type %s",type.c_str());
        }
        
    }
    
    
    void Offloader::thread_run(){
        
        this->send_request(Request::TEXT);
       
    }


}
