#ifndef __SMARTSPECS__NETWORKING__REQUESTHANDLER_H
#define __SMARTSPECS__NETWORKING__REQUESTHANDLER_H

#include <memory>
#include <string>
#include <functional>
#include "QueryProcessor.hpp"
#include "Messages.hpp"

namespace SmartSpecs{
  namespace Networking {
 
    // handles all incoming commands, processed and generates response.
    class RequestHandler{
      
    public:
        typedef std::shared_ptr<RequestHandler> Ptr;
        explicit RequestHandler(QueryProcessor::Ptr queryProcessor);

        static Ptr create(QueryProcessor::Ptr queryProcessor);
        
      RequestHandler(const RequestHandler&) = delete;
      RequestHandler& operator=(const RequestHandler&) = delete;
      
      bool check(const std::string& command);
      void process();
      bool process_header();
     
      std::string reply(); 
    
        uint8_t* readData(){
            m_read_message = Message::create();
            return m_read_message->data();
            
        }
        uint8_t* readBody(){return m_read_message->body();}
        const int readBodySize() const {return m_read_message->bodyLength();}
    private:
        Message::Ptr m_read_message;
        Message::Ptr m_write_message;
        std::string m_reply;
        std::string m_command;
        QueryProcessor::Ptr m_queryProcessor;
    
    };

 }
}

#endif // __SMARTSPECS__NETWORKING__REQUESTHANDLER_H
