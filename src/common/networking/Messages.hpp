//
//  Messages.hpp
//  cpp_server
//
//  Created by Iain Wilson on 07/02/2019.
//

#ifndef Messages_h
#define Messages_h

#include <memory>
#include <chrono>
#include <cstdint>
#include <iterator>
#include <string>
#include "Logging.hpp"
#include "Image.hpp"
#include "Response.hpp"
//#include "Face.hpp"
#include <opencv2/opencv.hpp>

namespace  SmartSpecs {
    
    const int HEADER_LENGTH{7};
    /*
     Header: char  = mode/type 'C' command, 'I' image;
     uint32_t data_packet_size;
     uint8_t packet index; (0 of 2)
     uint8_t packet count; (number of packets)
     */
    
//    const int MAX_BODY_LENGTH{327680}; //64*1024*5;
    const int MAX_BODY_LENGTH{3110408};
    class Message{
        
    public:
        
        typedef std::shared_ptr<Message> Ptr;
        
        // header length, contains one char for mode, and 4 for data packet size
        enum { header_length = HEADER_LENGTH };
        
        //arbitraty size chosen.
        enum { max_body_length = MAX_BODY_LENGTH };
        
        /*!
         /brief constructor - default mode is command (C)
         */
        Message(char mode='C',int packet_count=1,int packet_index=0,int body_length=0):
        m_mode(mode),
        m_packet_count(packet_count),
        m_packet_index(packet_index),
        m_body_length(body_length){
        }
        
        static Ptr create(char mode='C',int packet_count=1,int packet_index=0,int body_length=0){
            return std::make_shared<Message>(mode,packet_count,packet_index,body_length);
        }
        
        /*!
         \brief return the current message mode.
         C = command
         I = Image
         */
        const char mode() const {return m_mode;}
        
        /*!
         \brief return total length of the message
         */
        size_t length() const { return HEADER_LENGTH + m_body_length; }
        
        void setBodyLength(const int &length){ m_body_length = length;}
        int bodyLength() const {return m_body_length;}
        
        int packetCount() const{return m_packet_count;}
        void setPacketCount(const int &count){m_packet_count = count;}
        
        int packetIndex()const {return m_packet_index;}
        void setPacketIndex(const int &index){m_packet_index = index;}
        
        /*!
         \brief return pointer to the begining of the message
         */
        const uint8_t* data() const {return m_data; }
        uint8_t* data() { return m_data; }
        
        /*!
         \brief return pointer to begining of the message body
         */
        const uint8_t* body() const{ return m_data + header_length; }
        uint8_t* body() { return m_data + header_length; }
        
        const uint8_t* end() const {return body()+m_body_length;}
        uint8_t* end() {return body()+m_body_length;}
      
        void encodeHeader(){
            m_data[0] = m_mode;
            uint32_t *dest = (uint32_t*)&m_data[1];
            std::memcpy(dest, &m_body_length, sizeof(uint32_t));
            m_data[5] = m_packet_index;
            m_data[6] = m_packet_count;
            
//            LOGD("[Message::encodeHeader] mode: %c size: %d, index: %d, count: %d",m_mode,m_body_length,m_packet_index,m_packet_count);
        }
        
        /*!
         \brief decode the header, fomr the byte array into the member variables.
         THe first byte of the  Header contains the type of message
         e.g. CMD = 'C' or IMG 'I'
         the next 4 are the size of the data package
         5th = packet index
         6th = packet count
         */
        bool decodeHeader(){
            m_mode = m_data[0];
            memcpy(&m_body_length,(uint32_t*)&m_data[1],sizeof(uint32_t));
            m_packet_index = m_data[5];
            m_packet_count = m_data[6];
//            LOGD("[Message::decodeHeader] mode: %c size: %d, index: %d, count: %d",m_mode,m_body_length,m_packet_index,m_packet_count);


            // make sure the message is not tooo big
            if (m_body_length > MAX_BODY_LENGTH){
                m_body_length = 0;
                return false;
            }
            return true;
        }
        
    private:
        char m_mode;
        uint8_t m_data[HEADER_LENGTH+MAX_BODY_LENGTH];
        uint8_t m_packet_count;
        uint8_t m_packet_index;
        uint32_t m_body_length;
    
    };
    
    
    
    
    struct Messages{
        
        inline uint64_t CurrentTime_milliseconds(){
            return std::chrono::duration_cast<std::chrono::milliseconds>
            (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
        }
        
        /*!
         \brief decode message - returns a string
         */
        static std::string decode(const Message::Ptr &message){
            return std::string(reinterpret_cast<char*>(message->body()),message->bodyLength());
        }
        
        /*!
         \brief from a vector of messages create an image.
         */
        static Image::Ptr decode(const std::vector<Message::Ptr> &messages);
        
        /*!
         \brief from a vector of messages create an image.
         */
        static void decode(const std::vector<Message::Ptr> &messages, cv::Mat &img, std::chrono::high_resolution_clock::time_point &time);
        
        
        /*!
         generate a single command message from a string
         */
        static Message::Ptr encode(const std::string &text);
        
        /*!
            wrapper for an Image,
         */
        static std::vector<Message::Ptr> encode(const Image::Ptr image);
        
//        /*!
//         wrapper for a face (just send the cropped image)
//         */
//        static std::vector<Message::Ptr> encode(const Face::Ptr &face);
//
        
        /*!
         \brief generate a vector of messages that encapsulate the image, the last 8 bytes are a timestamp.
         */
        static std::vector<Message::Ptr> encode(const cv::Mat& image,const uint64_t &timestamp,const char &type);
        
        /*!
         \brief generate a message from a response object
         */
        static Message::Ptr encode(const Response::Ptr& response);
         
        
    };
}



#endif /* Messages_h */
