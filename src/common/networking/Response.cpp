//
//  Response.cpp
//  asio_client
//
//  Created by Iain Wilson on 20/07/2017.
//
//

#include "Response.hpp"
#include "TextResponse.hpp"
#include "FaceResponse.hpp"
#include "Logging.hpp"

namespace SmartSpecs{
    
    Response::Ptr Response::create(const std::string& type){
        if(type==Request::TEXT)
            return std::make_shared<TextResponse>();
        
        return std::make_shared<ResponseBase>(type);
    }
    
    Response::Ptr Response::create(const uint8_t* data, const int size){
        return std::make_shared<ResponseBase>(data,size);
    }
    
    Response::Ptr Response::create(Response::Ptr &resp){
        if(resp->type()==Request::TEXT)
            return std::make_shared<TextResponse>(*resp);
        return Response::Ptr();
    }

    
    
    
    void ResponseBase::encodeHeader(const size_t &size){
        //clear current buffer
        m_data.clear();
        
        //resize the data buffer - header is 4 for type, 8 for timestamp.
        m_data.resize(size+(header_length));
        
        //first 4 bytes are the type of response
        std::copy(m_type.begin(),m_type.end(),m_data.begin());
        
        //next 8 bytes are the timestamp (usualy that of the processed frame)
        uint64_t timestamp  = m_time.time_since_epoch().count();

        //copy timestamp into data vec.
        uint64_t *dest = reinterpret_cast<uint64_t*>(&m_data[4]);
        memcpy(dest,&timestamp,sizeof(uint64_t));
        
    }
    
    void ResponseBase::decodeHeader(){
       
        //first 4 bytes are the type of response
        m_type.resize(4);
        std::copy(m_data.begin(),m_data.begin()+4,m_type.begin());
        
        //byte 6-12 are a timestamp;
        uint64_t _time;
        std::memcpy(&_time,(uint64_t*)&m_data[4],sizeof(uint64_t));

        //convert the uint64_t to chrono timepoint.
        std::chrono::high_resolution_clock::duration d(_time);
        m_time = std::chrono::high_resolution_clock::time_point(d);

    }
    
   
    
}


/// TEXT REPONSE

//namespace SmartSpecs{
//
//    const bool TextResponse::process(std::vector<char>& readbuf,size_t count){
//        //process the reponse
//
//        std::string data(std::begin(readbuf),std::begin(readbuf)+count);
//       // std::cout << "2nd recieved: " << count << " " << data << std::endl;
//
//
//        //return true;
//        std::string buf; // Have a buffer string
//        std::stringstream ss(data); // Insert the string into a stream
//        std::vector<std::string> tokens; // Create vector to hold our words
//        while (ss >> buf)
//            tokens.push_back(buf);
//
//        int nBoxes = tokens.size();
//        std::vector<cv::Rect2d> boxes;
//        m_frame_number = std::stoi(tokens[0]);
//
//        if (nBoxes >= 5) {
//            for(int i=1;i<nBoxes;i+=4){
//                int x1 = std::stoi(tokens[i]);
//                int y1 = std::stoi(tokens[i+1]);
//                int x2 = std::stoi(tokens[i+2]);
//                int y2 = std::stoi(tokens[i+3]);
//                cv::Rect2d b(x1,y1,x2-x1,y2-y1);
//                boxes.push_back(b);
//                // std::cout << b <<std::endl;
//            }
//        }
//
//        m_data = boxes;
//
//        return true;
//    }
//
//
//}
