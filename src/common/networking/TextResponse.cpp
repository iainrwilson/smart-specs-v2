//
//  TextResponse.cpp
//  smart_specs_server
//
//  Created by Iain Wilson on 18/02/2019.
//

#include "TextResponse.hpp"

namespace SmartSpecs{
    
    /*!
     \brief convert the local data (rois) to a byte array.
     */
    void TextResponse::encode(){
        
        //set the size of the data packet . each roi takes up 8 bytes.
        //8 bytes per roi, 1 for roi count,
        encodeHeader(m_rois.size()*8+1);
        
        //fisrt byte is the number of rois.
        *(body()) = static_cast<uint8_t>(m_rois.size());
        
        //rois are Rect2i = 16bit signed
        // x,y,height,width
        int i=0;
        int16_t tmp[4];
        const uint8_t* src = reinterpret_cast<uint8_t*>(tmp);
        for(auto const &roi:m_rois){
            tmp[0] = roi.x;
            tmp[1] = roi.y;
            tmp[2] = roi.height;
            tmp[3] = roi.width;
            uint8_t* dest = body()+1+(i*8);
            std::memcpy(dest,src,sizeof(uint8_t)*8);
            i++;
        }
    }
    
    /*!
     \brief convert incomming byte array to vector of rois
     */
    void TextResponse::decode(){
        m_rois.clear();
        
        //first byte is the number of rois
        m_size = *body();
        
        int16_t tmp[4];
        uint8_t* dest = reinterpret_cast<uint8_t*>(tmp);
        for(int i=0;i<m_size;i++){
            const uint8_t* src = body()+1+(i*8);
            std::memcpy(dest, src, sizeof(uint8_t)*8);
            m_rois.push_back(cv::Rect2i(tmp[0],tmp[1],tmp[2],tmp[3]));
        }
    }
    
    
}
