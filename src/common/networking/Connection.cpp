#include "Connection.hpp"
#include "ConnectionManager.hpp"
#include "common/Logging.hpp"
#include "Messages.hpp"

namespace SmartSpecs{
  namespace Networking {

    /* ****** CONSTRUCTOR ***********/
      Connection::Connection(asio::ip::tcp::socket socket, ConnectionManager& manager, RequestHandler::Ptr handler):
      m_socket(std::move(socket)),
      m_manager(manager),
      m_handler(handler)
    {
        m_header_size=4;
        
    }
    
    /**************** PUBLIC MEMBERS ********************/

    void Connection::start(){
      do_read_header();
    }

    void Connection::stop(){
      m_socket.close();
    }

    /*****************  PRIVATE MEMBERS ******************/

      void Connection::do_read_header(){
          // read the header which is a predefined size.
          //m_handler.newMessage();
          //data is read straight into the Message class
          auto self(shared_from_this());
          asio::async_read(m_socket,asio::buffer(m_handler->readData(),HEADER_LENGTH),
                           [this,self](std::error_code ec,std::size_t bytes_transferred){
                               if(!ec && m_handler->process_header()){
                                       do_read_body();
                               }else{
//                                   LOGE("[Connection::do_read_header] Error: %s ",ec.message().c_str());
                                   m_socket.close();
                               }
                           });
          
      }
      
      
      void Connection::do_read_body(){
          // read the header which is a predefined size.
          
          auto self(shared_from_this());
          asio::async_read(m_socket,asio::buffer(m_handler->readBody(), m_handler->readBodySize()),
                           [this,self](std::error_code ec,std::size_t bytes_transferred){
                               if(!ec || asio::error::eof){
                                   m_handler->process();
                                   do_write();
                               }else{
                                   LOGE("[Connection::do_read_body] Error: %s",ec.message().c_str());
                               }
                           });
      }
      
    
      
 
    void Connection::do_write(){
      //response to incomming request processed by the Handler. (m_handler)
      
      // write data is held in m_handler .
     auto self(shared_from_this());
      asio::async_write(m_socket,
			asio::buffer(m_handler->reply()),
			[this,self](std::error_code ec, std::size_t bt){
                
			  if(!ec){
			    //initiate connetion closure
			    asio::error_code ec;
			    m_socket.shutdown(asio::ip::tcp::socket::shutdown_both,ec);
              }else{
                  LOGE("[Connection::do_write] write error %s",ec.message().c_str());
              }
			  
			  if(ec!=asio::error::operation_aborted){
			    m_manager.stop(self);
			  }
			  
			});
    }
    
  }
}
