#ifndef __SMARTSPECS__NETWORKING__CONNECTIONMANAGER_H
#define __SMARTSPECS__NETWORKING__CONNECTIONMANAGER_H

#include <set>
#include "Connection.hpp"


namespace SmartSpecs {
  namespace Networking{
    class ConnectionManager{
  
    public:
      ConnectionManager();
      ~ConnectionManager();

      //disable copy constructor and assignment operator
      ConnectionManager(const ConnectionManager&) = delete;
      ConnectionManager& operator=(const ConnectionManager&) = delete;
     
      void start(Connection::Ptr c);
      void stop(Connection::Ptr c);
    
      void stop_all();

    private:
      std::set<Connection::Ptr> m_connections;

    };
  }
}
#endif // __SMARTSPECS__NETWORKING__CONNECTIONMANAGER_H
