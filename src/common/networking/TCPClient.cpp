//
//  TCPClient.cpp
//  smart-specs-linux
//
//  Created by Iain Wilson on 08/02/2015.
//
//

#include "TCPClient.hpp"
#include "common/Logging.hpp"

namespace SmartSpecs {
	namespace Networking {
    
		TCPClient::TCPClient():m_io_service(),m_host("localhost"),m_port("8001"){
		}
		TCPClient::TCPClient(const std::string& host):m_io_service(),m_host(host),m_port("8001"){
		}
		void TCPClient::setRemote(const std::string host, const std::string port){
			m_port = port;
			m_host = host;
		}

		bool TCPClient::ping(){
			std::string response(sendQuery("PING"));
			if(response == "PING")
				return true;
			return false;
		}
        
        
        std::shared_ptr<tcp::socket> TCPClient::connect(){
            std::shared_ptr<tcp::socket> socket;
            try{
                tcp::resolver resolver(m_io_service);
                tcp::resolver::query query(m_host.c_str(), m_port.c_str());
                tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
                
                socket = std::make_shared<tcp::socket>(m_io_service);
                
                asio::connect(*socket, endpoint_iterator);

                return socket;
                
            }catch(std::exception& e){
//                LOGE("[TCPClient::connect()] Connect Failed %s",e.what());
                return socket;
            }
            
        }
        
        
        size_t TCPClient::read(std::shared_ptr<tcp::socket> socket, std::vector<char>& readbuf){
            try{
                asio::error_code error;
                //std::vector<char> readbuf(1024);
                size_t count = socket->read_some(asio::buffer(readbuf), error);
                if(error){
                    LOGE("[TCPClient::send()] Read Error read_some: %s]",error.message().c_str());
                }
                
//                std::cout << "[TCPClient::read] read some: " << count << std::endl;
                return count;
                
            }catch(std::exception& e){
                LOGE("[TCPClient::read()] read Failed %s",e.what());
            
            }
            return 0;
        }
        
    
        const bool TCPClient::sendFrame(const Image::Ptr& frame){
            
//            std::cout << "[TCPClient::sendFrame] " << m_host  <<" " <<m_port<< std::endl;
            
            //convert frame into messages.
            auto messages = Messages::encode(frame);
            
            try{
                auto socket = connect();
                for(auto &msg:messages){
                    std::string ret = sendMessage(msg);
                }
                
//                std::cout << "[TCPClient::sendFrame] complete" <<std::endl;
                return true;
                
            }catch (std::exception& e)
            {
                LOGE("[TCPClient::sendFrameOnly()] %s",e.what());
                return false;
            }
        }
        
        
        const bool TCPClient::sendFrame(const cv::Mat& frame){
            
//            std::cout << "[TCPClient::sendFrame] " << m_host  <<" " <<m_port<< std::endl;
            
            //convert frame into messages.
            auto messages = Messages::encode(frame, 100,'I');
            
            try{
                auto socket = connect();
                for(auto &msg:messages){
                    std::string ret = sendMessage(msg);
                }
                return true;
                
            }catch (std::exception& e)
            {
                LOGE("[TCPClient::sendFrameOnly()] %s",e.what());
                return false;
            }
        }
        
        const std::string TCPClient::sendMessage(Message::Ptr &message){
            
//            LOGD("[TCPClient::sendMessage] Sending: %d byte to %s::%s", message->length(),m_host.c_str(), m_port.c_str());
            
            auto socket = connect();
            
            //send message - we send all of the data in one go.
            // the reader handles splitting up the data.
            asio::error_code error;
            size_t written = socket->write_some(asio::buffer(message->data(),message->length()),error);
            if(error){
//                LOGE("[TCPClient::sendMessage] Error sending message : %s",error.message().c_str());
                return "ERROR";
            }

            std::vector<char> buf(1024);
            //read reply
            size_t count = socket->read_some(asio::buffer(buf,4), error);
            if(error){
//                LOGE("[TCPClient::sendMessage] Error reading reply: %s",error.message().c_str());
                return "ERROR";
            }
            buf.shrink_to_fit();
            std::string result(std::begin(buf),std::begin(buf)+count);
            return result;
        }
        
        const std::string TCPClient::sendQuery(const std::string &query){
            auto msg = Messages::encode(query);
            return sendMessage(msg);
        }
        
        const std::string TCPClient::sendResponse(Response::Ptr &response){
            auto msg = Messages::encode(response);
            return sendMessage(msg);
        }

	}
}
