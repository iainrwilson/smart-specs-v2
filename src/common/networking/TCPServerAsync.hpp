#ifndef __SMARTSPECS__NETWORKING__TCPSERVERASYNC_H
#define __SMARTSPECS__NETWORKING__TCPSERVERASYNC_H

#include <asio.hpp>
#include <memory>
#include "ConnectionManager.hpp"
#include "common/Thread.hpp"

#ifdef __ANDROID__
#include <jni.h>
#endif

namespace SmartSpecs {
    
    /*!
     \namespace SmartSpecs::Networking
     \brief Contains all the networking stuff.
     */
  namespace Networking {

    class TCPServerAsync : public Thread{
    public:
    	typedef std::shared_ptr<TCPServerAsync> Ptr;
    	TCPServerAsync(const TCPServerAsync&) = delete;
      TCPServerAsync& operator=(const TCPServerAsync&) = delete;
      
      //construct to listen on a specific address and port.
      explicit TCPServerAsync(const std::string& port, QueryProcessor::Ptr queryProcessor);
    
        static TCPServerAsync::Ptr create(const std::string& port, QueryProcessor::Ptr queryProcessor){
            return std::make_shared<TCPServerAsync>(port,queryProcessor);
        }
      //run the io_service loop
      void thread_run();

      void stop();
   //   void start();

    private:
      
      void do_accept();
      
      void do_await_stop();

      //! The io_service used to perform asynchronous operations.
      asio::io_service m_io_service;
      
      //! The signal_set is used to register for process termination notifications.
      asio::signal_set m_signals;
      
      //! Acceptor used to listen for incoming connections.
      asio::ip::tcp::acceptor m_acceptor;

      //! The connection manager which owns all live connections.
      ConnectionManager m_manager;

      //! The next socket to be accepted.
      asio::ip::tcp::socket m_socket;

      //! The handler for all incoming requests.
      QueryProcessor::Ptr m_queryProcessor;
    };

  
 
  }
}

#endif //__SMARTSPECS__NETWORKING__TCPSERVERASYNC_H
