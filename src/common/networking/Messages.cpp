//
//  Messages.cpp
//  dlib
//
//  Created by Iain Wilson on 12/02/2019.
//

#include <stdio.h>
#include "Messages.hpp"

namespace SmartSpecs {

    /*!
     \brief generate a message from a response object
     */
    Message::Ptr Messages::encode(const Response::Ptr& response){
        response->encode();
        
        
        //check the data is not larget than max message size
        if(response->dataLength() > Message::max_body_length){
            //TODO ---
        }
        
        auto msg = Message::create('R',1,0,response->dataLength());
        
        //copy response data into message
        std::copy(response->data(),response->data()+response->dataLength(),msg->body());
        msg->encodeHeader();
        return msg;
    }
    
    
    /*!
     generate a single command message from a string
     */
    Message::Ptr Messages::encode(const std::string &text){
        auto msg = Message::create('C',1,0,text.size());
        msg->encodeHeader();
        std::copy(text.c_str(),text.c_str()+text.size(),msg->body());
        return msg;
    }
    
    
    /*!
     wrapper for an Image,
     */
    std::vector<Message::Ptr> Messages::encode(const Image::Ptr image){
        
        uint64_t time = image->timestamp().time_since_epoch().count();
        return encode(image->get(),time,'I');
    }
    
//    /*!
//     \brief Create a message from a face object
//     */
//    std::vector<Message::Ptr> Messages::encode(const Face::Ptr &face){
//        //just send the cropped roi image.
//        uint64_t time = face->image()->timestamp().time_since_epoch().count();
//        return encode(face->faceImage(),time,'F');
//    }
    
    
    /*!
     \brief generate a vector of messages that encapsulate the image, the last 8 bytes are a timestamp.
     */
    std::vector<Message::Ptr> Messages::encode(const cv::Mat& image,const uint64_t &timestamp,const char &type){
        
        std::vector<uint8_t> img_buf;
        //encode image
        cv::imencode(".jpg",image, img_buf);
//        LOGI("[Messages::encode] image size: %d",img_buf.size());
        
        // the last 8 bytes are a uint64_t version of the std::chrono time point.
        std::vector<uint8_t> time(8);
        uint64_t *dest = (uint64_t*)&time[0];
        memcpy(dest,&timestamp,sizeof(uint64_t));
        //append the time vector to the end.
        img_buf.insert(img_buf.end(),time.begin(),time.end());
        
        //calculate the number of packets required.
        uint32_t frame_size = img_buf.size();
        
//        LOGI("[Messages::encode] image size: %d, time:%d",frame_size,timestamp);
        
        uint8_t packet_count=0;
        int pc_major = frame_size / MAX_BODY_LENGTH;
        int pc_minor = frame_size % MAX_BODY_LENGTH;
        if(pc_major == 0){
            packet_count = 1;
        }else{
            packet_count = pc_major;
            if(pc_minor!=0)
                packet_count++;
        }
        uint8_t packet_counter=0;
        
        int remaining = frame_size;
        //image current position pointer
        const uchar* p_image = &img_buf[0];
        unsigned int send_length = std::min(MAX_BODY_LENGTH,remaining);
        std::vector<Message::Ptr> out;
        while(remaining>0){
            
            auto msg = Message::create(type,packet_count,packet_counter,send_length);
            msg->encodeHeader();
            std::copy(p_image,p_image+send_length,msg->body());
            out.push_back(msg);
            
            p_image += send_length;
            remaining -= send_length;
            send_length = std::min(MAX_BODY_LENGTH,remaining);
            packet_counter++;
            
        }
        return out;
    }
    
    
    /*!
     \brief from a vector of messages create an image.
     */
    Image::Ptr Messages::decode(const std::vector<Message::Ptr> &messages){
        cv::Mat _img;
        std::chrono::high_resolution_clock::time_point time;
        decode(messages, _img, time);
        Image::Ptr img = Image::create(_img);
        img->setTimestamp(time);
        return img;
    }

    

    /*!
     \brief from a vector of messages create an image.
     */
    void Messages::decode(const std::vector<Message::Ptr> &messages, cv::Mat &img, std::chrono::high_resolution_clock::time_point &time){
        
        std::vector<uint8_t> data;
        for(auto const &msg:messages){
            //header should have already been decoded.
            data.insert(data.end(),msg->body(),msg->end());
            LOGI("[Messages::decode] data size: %d",data.size());
        }
        
        //remember the last 8 bytes are a time stamp;
        uint64_t _time;
        auto ts_it =std::prev(data.end(),8);
        std::memcpy(&_time,(uint64_t*)&(*ts_it),sizeof(uint64_t));
        //erase the last 8 bytes from the image data vector
        auto it = std::prev(data.end(),8);
        data.erase(it,data.end());
        
       
        //convert the uint64_t to chrono timepoint.
        std::chrono::high_resolution_clock::duration d(_time);
        time = std::chrono::high_resolution_clock::time_point(d);
        
        LOGI("[Messages::decode] image size: %d, time:%d",data.size(),_time);
        
        int height = 1080;
        int width = 1920;
        
        cv::Mat mNV(height + height/2, width, CV_8UC1, &data[0]);

        //hack to handle yuv from android
        img = cv::Mat(height, width, CV_8UC3);
//        std::memcpy(_img.data, &data[0], data.size());
        cv::cvtColor(mNV,img, cv::COLOR_YUV2BGR_I420);
        
        //decode image.
//        img = cv::imdecode(cv::Mat(data), 1);
        
    }
}
