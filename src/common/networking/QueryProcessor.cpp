//
//  QueryProcessor.cpp
//  smart-specs-client
//
//  Created by Iain Wilson on 11/02/2019.
//

#include "QueryProcessor.hpp"
namespace SmartSpecs {

    QueryProcessor::QueryProcessor(std::function<std::string (const std::string&)> command,
                                   std::function<std::string (const Image::Ptr&)> image):m_command_processor(command),m_image_processor(image){
        m_response_processor = [](const SmartSpecs::Response::Ptr& r){return "NONE";};
    }
    QueryProcessor::QueryProcessor(std::function<std::string (const std::string&)> command,
                                   std::function<std::string (const Image::Ptr&)> image,
                                   std::function<std::string (const Response::Ptr&)> response):m_command_processor(command),m_image_processor(image),m_response_processor(response){
    
    }
    
    /*!
     \brief process incoming message, dispatch to relevant processor.
     Is the message a Command (C) or an Image (I)
     */
    std::string QueryProcessor::process(SmartSpecs::Message::Ptr &message){
        if(message->mode() == 'C'){ //67
            return processQuery(message);
        }else if(message->mode() == 'I'){ //73
            return processFrame(message);
        }else if(message->mode() == 'R'){
            return processResponse(message);
        }else if(message->mode() == 'F'){
            //face procesing mode.
            return processFrame(message);
        }
        return "ERROR";
    }
        
    /*!
     \brief process incomming response data
     */
    std::string QueryProcessor::processResponse(Message::Ptr &message){

        //copy  message data into response data.
        auto response = Response::create(message->body(),message->bodyLength());
        response->decodeHeader();
    
        //upcast to the correct type
        if(response->type() == Request::TEXT){
            auto text_response = Response::create(response);
            text_response->decode();
            
            //send to the response processor;
            return m_response_processor(text_response);
        }
        return "ERROR";
    }
        
    /*!
     \brief process an incoming query, used to change the state
     */
    std::string QueryProcessor::processQuery(Message::Ptr &message){
        
        std::string cmd = Messages::decode(message);
        return m_command_processor(cmd);
    }
    /*!
     \brief process an incoming Image -
     Decodes the data, re-packs as it can come in multiple messages.
     when image is complet, send to the image processor. (run function)
     */
    std::string QueryProcessor::processFrame(Message::Ptr& message){
        
        //this assumes that the incomming messages will be in the correct order
        int index = message->packetIndex();
        int count = message->packetCount();
        
        LOGD("[QueryProcessor::processFrame] recieved : %d of %d (%d)",index+1,count,message->length());
        
        m_message_buffer.push_back(message);
        
        if(index >= count-1){
            LOGD("[QueryProcessor::processFrame] got an image in %d packets",m_message_buffer.size());
        
            Image::Ptr img = Messages::decode(m_message_buffer);
            
            //clear the message buffer.
            m_message_buffer.clear();
            
            // run the image processing ..
            return m_image_processor(img);
        }
        
        return "OKAY";
        
    }
}
