#ifndef __SMARTSPECS__NETWORKING__TCPSERVER_H
#define __SMARTSPECS__NETWORKING__TCPSERVER_H
#include <ctime>
#include <iostream>
#include <string>
#include <asio.hpp>
#include "common/Thread.hpp"

//#include "Engine.h"
class Engine;
namespace SmartSpecs {
	namespace Networking {
		class TCPServer : public Thread {

		public:
			typedef std::shared_ptr<TCPServer> Ptr;
			TCPServer(std::function<std::string (const std::string&)> queryProcessor);
			TCPServer(std::function<std::string (const std::string&)> queryProcessor,const int port);
			void thread_run();
			void start();
			void stop();

		private:
			Engine* m_parent;

			size_t read(std::vector<char>& input);
			void write(const std::string& data);

			std::function<std::string (const std::string&)> m_queryProcessor;



			std::unique_ptr<asio::ip::tcp::socket> m_socket;
			std::unique_ptr<asio::ip::tcp::acceptor> m_tcp_acceptor;
			asio::io_service m_io_service;

			int m_tcp_port;


		};
	}
}
#endif //__SMARTSPECS__NETWORKING__TCPSERVER_H
