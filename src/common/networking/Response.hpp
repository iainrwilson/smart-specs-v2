//
//  Response.hpp
//  asio_client
//
//  Created by Iain Wilson on 20/07/2017.
//
//

#ifndef Response_hpp
#define Response_hpp

#include <stdio.h>
#include <memory>
#include <chrono>
#include <opencv2/opencv.hpp>
//#include "Messages.hpp"

namespace SmartSpecs {

    namespace Request {
        const std::string PING{"PING"};
        const std::string FACE{"FACE"};
        const std::string TEXT{"TEXT"};
        const std::string SUPERRES{"SUPERRES"};
        const std::string SUPERRES_RUN{"SUPERRES_RUN"};
        const std::string SUPERRES_LOAD{"SUPERRES_LOAD"};
        const std::string LOWLIGHT{"LOWLIGHT"};
        const std::string LOWLIGHT_LOAD{"LOWLIGHT_LOAD"};
        const std::string LOWLIGHT_RUN{"LOWLIGHT_RUN"};
        const std::string SAVE{"SAVE"};
    };
    
    
    typedef std::vector<cv::Rect2d> TextBoxes;

    
    class ResponseBase{
    public:
        
        // header length, contains one char for mode, and 4 for data packet size
        enum { header_length = 13 };
        
        ResponseBase(const std::string& type):m_type(type){};
        
        ResponseBase(const uint8_t* _data,const size_t size){
            //init m_data
            m_data.resize(size+header_length);
            memcpy(data(),_data,sizeof(uint8_t)*size);
        }
        
        ResponseBase(ResponseBase const& obj){
            m_data = obj.m_data;
            m_rois = obj.m_rois;
            m_type = obj.m_type;
            m_time = obj.m_time;
            m_size = obj.m_size;
        }
        
        virtual ~ResponseBase() = default;
        std::string type() const {return m_type;}
        void setType(const std::string& type){m_type = type;}
        
        void add(const cv::Rect2i &roi){m_rois.push_back(roi);}
        
        void setTimestamp(const std::chrono::high_resolution_clock::time_point &time){m_time=time;}
        std::chrono::high_resolution_clock::time_point timestamp()const {return m_time;}
        
        /*!
         \brief returns a pointer to the start of the data array.
         */
        const uint8_t* data() const {return &m_data[0];}
        uint8_t* data() {return &m_data[0];}

        const uint8_t* body() const {return data() + header_length;}
        uint8_t* body() {return data()+header_length;}
        
        void setData(std::vector<uint8_t> data){m_data=data;}
        size_t dataLength() const {return m_data.size();}
        
        virtual void encode(){};
        virtual void decode(){};
        
        void encodeHeader(const size_t &size);
        void decodeHeader();
        
        std::vector<cv::Rect2i> rois() const {return m_rois;}
        
        int age(){
            auto now = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - timestamp());
            return duration.count();
        }
    protected:
        std::string m_type;
        std::vector<uint8_t> m_data;
        std::vector<cv::Rect2i> m_rois;
        std::chrono::high_resolution_clock::time_point m_time;
        uint8_t m_size;
        
    };
    
    
    struct Response{
        typedef std::shared_ptr<ResponseBase> Ptr;
        static Ptr create(const std::string& type="NONE");
        static Ptr create(const uint8_t* data, const int size);
        static Ptr create(Response::Ptr &resp);
    };
    
    
    
//    class Response{
//    public:
//        typedef std::shared_ptr<Response> Ptr;
//        Response(){};
//        virtual ~Response(){};
//        Response(const std::string name):m_name(name){};
//        Response(const std::string name, const int fnumber):m_name(name),m_frame_number(fnumber){};
//        std::string type() const {return m_name;}
//        int frameNumber() const {return m_frame_number;}
//
//        template<typename T>
//        static Response::Ptr create(const int fnumber,typename T::Data data){
//            return std::make_shared<T>(fnumber,data);
//        }
//        template<typename T>
//        static typename T::Ptr create(){
//            return std::make_shared<T>();
//        }
//
//        virtual const bool process(std::vector<char>& readbuf,size_t count) = 0;
//
//    protected:
//        std::string m_name;
//        int m_frame_number;
//    };
    
    
//    class FaceResponse: public Response {
//    public:
//        typedef std::vector<cv::Rect2d> Data;
//        FaceResponse():Response(Request::FACE){}
//        FaceResponse(int fnumber,Data data):Response(Request::FACE,fnumber),m_data(data){}
//        void setData(Data data){m_data=data;}
//        Data data(){return m_data;}
//
//        const bool process(std::vector<char>& readbuf,size_t count);
//    private:
//        Data m_data;
//
//    };
//
//    class TextResponse: public Response {
//
//    public:
//        typedef std::vector<cv::Rect2d> Data;
//        TextResponse(): Response(Request::TEXT){}
//        TextResponse(int fnumber, Data data): Response(Request::TEXT,fnumber),m_data(data){}
//        Data data(){return m_data;}
//
//        const bool process(std::vector<char>& readbuf,size_t count);
//    private:
//        Data m_data;
//    };
//
    
}

#endif /* Response_hpp */
