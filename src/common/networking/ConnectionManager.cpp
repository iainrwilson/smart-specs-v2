#include "ConnectionManager.hpp"

namespace SmartSpecs{
  namespace Networking{

    ConnectionManager::ConnectionManager(){
    }

    ConnectionManager::~ConnectionManager(){
      stop_all();
    }

    void ConnectionManager::start(Connection::Ptr c){
      m_connections.insert(c);
      c->start();		       
    }
    void ConnectionManager::stop(Connection::Ptr c){
      m_connections.erase(c);
      c->stop();
    }

    void ConnectionManager::stop_all(){
      for (auto c: m_connections)
        c->stop();
      m_connections.clear();
    }
  }
}
