#include "RequestHandler.hpp"
#include "common/Logging.hpp"

namespace SmartSpecs {
  namespace Networking{
  
    /******************* CONSTRUCTOR ********************/
      RequestHandler::RequestHandler(QueryProcessor::Ptr queryProcessor):m_reply("DEFAULT"),m_queryProcessor(std::move(queryProcessor)){
          m_read_message = Message::create();
          m_write_message = Message::create();
    }
      
      RequestHandler::Ptr RequestHandler::create(QueryProcessor::Ptr queryProcessor){
          return std::make_shared<RequestHandler>(queryProcessor);
      }
    
    /********************* PUBLIC MEMBERS ********************/
    bool RequestHandler::check(const std::string& command){
      m_command = command;
      return true;
    }
    
    bool RequestHandler::process_header(){
      return m_read_message->decodeHeader();
    }
    
    void RequestHandler::process(){
      // run the command...  and handle output
//        LOGI("[RequestHandler::process] processing message: %c ",m_read_message->mode());
        m_reply = m_queryProcessor->process(m_read_message);
    }

    std::string RequestHandler::reply(){
      ///return the reply
//        LOGI("[RequestHandler::reply] %s",m_reply.c_str());
        return m_reply;
    }
    
      
  }
}
