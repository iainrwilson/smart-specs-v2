#ifndef __SMARTSPECS__NETWORKING__CONNECTION_H
#define __SMARTSPECS__NETWORKING__CONNECTION_H


#include <array>
#include <memory>
#include <asio.hpp>
#include "RequestHandler.hpp"


namespace SmartSpecs{
  namespace Networking{
    class ConnectionManager;

    class Connection : public std::enable_shared_from_this<Connection>{
      
    public:
      typedef std::shared_ptr<Connection> Ptr;
      Connection(const Connection&) = delete;
      Connection& operator=(const Connection&) = delete;


      //construct a connection from the given socket
        explicit Connection(asio::ip::tcp::socket socket, ConnectionManager& manager, RequestHandler::Ptr handler);
      
      void start();
      void stop();

    private:
        void do_read();
        void do_read_header();
        void do_read_body();
        void do_write();
      
        
      asio::ip::tcp::socket m_socket;
      ConnectionManager& m_manager;
      RequestHandler::Ptr m_handler;
      
      std::string m_request;
      std::string m_reply;
      
      std::array<char,64*1024 *5 + 4> m_buffer; //max size for 1280*1024*3

        
        int m_header_size;
        int m_body_size;
    };
  }
}
#endif //__SMARTSPECS__NETWORKING__CONNECTION_H

