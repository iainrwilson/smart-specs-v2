//
//  QueryProcessor.hpp
//  smart-specs-client
//
//  Created by Iain Wilson on 11/02/2019.
//

#ifndef QueryProcessor_hpp
#define QueryProcessor_hpp

#include <stdio.h>
#include <stdio.h>
#include "Messages.hpp"
#include <memory>
#include "Image.hpp"
#include "Response.hpp"
namespace SmartSpecs {
    
    class QueryProcessor{
        
    public:
        typedef std::shared_ptr<QueryProcessor> Ptr;
        QueryProcessor(std::function<std::string (const std::string&)> command,
                       std::function<std::string (const Image::Ptr&)> image);
        static QueryProcessor::Ptr create(std::function<std::string (const std::string&)> command,
                                          std::function<std::string (const Image::Ptr&)> image){ return std::make_shared<QueryProcessor>(command,image);}
        
        QueryProcessor(std::function<std::string (const std::string&)> command,
                                       std::function<std::string (const Image::Ptr&)> image,
                                       std::function<std::string (const Response::Ptr&)> response);
        
        static QueryProcessor::Ptr create(std::function<std::string (const std::string&)> command,
                                       std::function<std::string (const Image::Ptr&)> image,
                                     std::function<std::string (const Response::Ptr&)> response){
            return std::make_shared<QueryProcessor>(command,image,response);
        }
        
        
        std::string process(Message::Ptr &message);
        
    protected:
        
        std::string processFrame(Message::Ptr &message);
        std::string processQuery(Message::Ptr &message);
        std::string processResponse(Message::Ptr &message);
        std::vector<Message::Ptr> m_message_buffer;
        
        std::function<std::string (const std::string&)> m_command_processor;
        std::function<std::string (const Image::Ptr&)> m_image_processor;
        std::function<std::string (const Response::Ptr&)> m_response_processor;
    };
}

#endif /* QueryProcessor_hpp */
