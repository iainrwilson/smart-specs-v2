//
//  FaceResponse.cpp
//  smart_specs_server
//
//  Created by Iain Wilson on 18/02/2019.
//

#include "FaceResponse.hpp"
namespace SmartSpecs {
    
    /*!
     \brief convert the local data (rois) to a byte array.
     */
    void FaceResponse::encode(){
        
        //only 8 bytes needed for body
        encodeHeader(8);
        
        //face id is stored as a uint64_t
        // actual name is handled by the client.
        std::memcpy(body(),reinterpret_cast<uint8_t*>(&m_id),sizeof(uint64_t));
        
    }
    
    /*!
     \brief convert incomming byte array to vector of rois
     */
    void FaceResponse::decode(){
        //face id is stored as a uint64_t
        std::memcpy(reinterpret_cast<uint8_t*>(&m_id),body(),sizeof(uint64_t));
    }
    
}
