//
//  Offloader.hpp
//  asio_client
//
//  Created by Iain Wilson on 19/07/2017.
//
//

#ifndef Offloader_hpp
#define Offloader_hpp

#include <iostream>
#include "common/Thread.hpp"
//#include "includes.h"
#include "common/RingBuffer.hpp"
#include "TCPClient.hpp"
#include "TCPServerAsync.hpp"
#include "common/Image.hpp"
#include "common/networking/Response.hpp"

namespace SmartSpecs {

    class Offloader : public Thread{
    
    public:
        typedef std::shared_ptr<Offloader> Ptr;
        Offloader(RingBuffer<Image::Ptr>::Ptr buff,RingBuffer<Response::Ptr>::Ptr res,const std::string& ip_adddress);
        
        
        void thread_run();
        
        void send_request(const std::string& type);
        
        bool ping(){return m_tcp->ping();}
        
        bool sendFrame(const cv::Mat& frame){return m_tcp->sendFrame(frame);}
        bool sendFrame(const Image::Ptr& frame){return m_tcp->sendFrame(frame);}
        std::string sendQuery(const std::string& query){
            return m_tcp->sendQuery(query);
        }
        
        void setRemote(const std::string& host,const std::string port){
            m_tcp->setRemote(host, port);
        }
        
        static Offloader::Ptr create(RingBuffer<Image::Ptr>::Ptr buff,RingBuffer<Response::Ptr>::Ptr res,const std::string& ip_address){
            return std::make_shared<Offloader>(buff,res,ip_address);
        }
        
        Networking::TCPClient::Ptr tcp() const { return m_tcp;}
        
        
    private:
        RingBuffer<Image::Ptr>::Ptr m_buffer;
        RingBuffer<Response::Ptr>::Ptr m_results_buffer;

        Networking::TCPClient::Ptr m_tcp;
        
//        Networking::TCPServerAsync::Ptr m_tcp_asnc;
        
    };
} //SmartSpecs



#endif /* Offloader_hpp */
