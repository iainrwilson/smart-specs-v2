//
//  TCPServer.cpp
//  smart-specs
//
//  Created by Iain Wilson on 08/02/2015.
//
//

#include "TCPServer.hpp"

#include "common/Logging.hpp"

using asio::ip::tcp;

namespace SmartSpecs{
    namespace Networking{
		TCPServer::TCPServer(std::function<std::string (const std::string&)> queryProcessor):Thread("TCPServer"),
			m_queryProcessor(std::move(queryProcessor)),
            m_io_service(),
            m_tcp_port(8001){
		}
		TCPServer::TCPServer(std::function<std::string (const std::string&)> queryProcessor, const int port):Thread("TCPServer"),
			m_queryProcessor(std::move(queryProcessor)),
            m_io_service(),
            m_tcp_port(port){
		}
		void TCPServer::start(){

			try{
				m_tcp_acceptor.reset(new tcp::acceptor(m_io_service,tcp::endpoint(tcp::v4(),m_tcp_port)));
				m_socket.reset(new tcp::socket(m_io_service));
			}catch(std::exception& e){
				LOGE("[TCPServer::start()] %s",e.what());
				return;
			}
			LOGI("[TCPServer::start()] Success");
			Thread::start();
		}

		void TCPServer::stop(){
		if(!isRunning())
		  return;
		if(m_socket->is_open()){
		  m_socket->shutdown(tcp::socket::shutdown_both);
		  m_socket->close();
		}

		m_tcp_acceptor->close();

		//m_tcp_acceptor->cancel();
		m_io_service.stop();
		Thread::stop();

		}

		void TCPServer::thread_run(){
			try{
				std::cout << "[TCPServer::Waiting]" << std::endl;
				// asio::ip::tcp::socket m_socket(*m_io_service);
				m_tcp_acceptor->accept(*m_socket);


			   std::vector<char> input(1024);
			   size_t len = read(input);

				std::string data(input.begin(),input.end());

			   LOGI("[TCPServer::thread_run()] Received: %d :: %s",len, data.c_str());
			   //LOGE("[TCPServer::thread_run()] Received");
				//process query... if SET

			   if(m_queryProcessor){
				  const std::string resp =   m_queryProcessor(data);
				//respond to command
				write(resp);
			  }
			  else
				LOGE("[TCPServer::thread_run()] NULLPTR CALLED");

	
			   m_socket->close();
			}catch(std::exception& e){
				LOGE("[TCPServer::thread_run()] %s",e.what());
				this->stop();
			}
		}
        
		size_t TCPServer::read(std::vector<char>& data){
			try{
				asio::error_code ec;
	
				size_t len = m_socket->read_some(asio::buffer(data),ec);
				if(ec == asio::error::eof)
					return len;
				else if(ec)
					return -1;
				return len;
			}catch(std::exception& e){
				LOGE("[TCPServer::read()] %s",e.what());
				this->stop();
			}
            return -1;
		}

		void TCPServer::write(const std::string& data){
			try{
				asio::error_code ec;
				m_socket->write_some(asio::buffer(data),ec);
			}catch(std::exception& e){
						LOGE("[TCPServer::write()] %s",e.what());
						this->stop();
			}
		}
    }
}
