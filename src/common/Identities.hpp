//
//  Identities.hpp
//  cpp_server
//
//  Created by Iain Wilson on 04/02/2019.
//

#ifndef Identities_hpp
#define Identities_hpp

#include <stdio.h>
#include "Identity.hpp"
/*!
 \brief Wrapper class around std::vector - handles cloning properly
 
 TODO finish this ....
 
 */
class Identities{
    
public:
    typedef std::shared_ptr<Identities> Ptr;
    
    Identities();
    static Ptr create(){
        return std::make_shared<Identities>();
    }

    /*!
     \brief checks the face identity, if found add to existing, otherwise create new.
     */
    void add(const Face::Ptr &face){

        int _id = checkIdentity(face);
        if(_id<0){
            //then create new id.
            _id = rand() % 10000;
        }
        
        face->setId(_id);
        {
            std::unique_lock<std::mutex> lock(m_identity_mutex);
            //check if identity exists.
            
            auto id = m_ids.find(_id);
            if (id == m_ids.end()){
                //not found..create new
                m_ids[face->id()] = Identity::create(face);
            }else{
                id->second->add(face);
            }
        }
    }

    
    int checkIdentity(const Face::Ptr &face);
    std::vector<Face::Ptr> currentFaces();
    void clean();
    cv::Mat showIds();
   
    
    size_t size() const {return m_ids.size();}
    Identity::Ptr& operator[](int i){return m_ids[i];}
    
    using iterator = std::map<int,Identity::Ptr>::iterator;
    using const_iterator = std::map<int,Identity::Ptr>::const_iterator;
    iterator begin(){return m_ids.begin();}
    const_iterator begin() const {return m_ids.begin();}
    iterator end(){return m_ids.end();}
    const_iterator end() const {return m_ids.end();}
    iterator erase(iterator id){return m_ids.erase(id);}
    
    
    void lock(){m_identity_mutex.lock();}
    
    void unlock(){m_identity_mutex.unlock();}
    
private:

    std::map<int,Identity::Ptr> m_ids;
    std::map<int,Face::Ptr> m_faces;
    std::mutex m_identity_mutex;
    std::mutex m_faces_mutex;
    std::mutex m_mutex;
};
#endif /* Identities_hpp */
