//
//  Face.cpp
//  cpp_server
//
//  Created by Iain Wilson on 22/11/2018.
//

#include "Face.hpp"
#include "common/timer.hpp"
#include "skin_segmentation/HistogramSkinSegmenter.hpp"
#include "skin_segmentation/DecisionTreeSkinSegmenter.hpp"

const int FACELEARNER_GC_ITER{2};


Face::Face(const cv::Rect &roi,const Image::Ptr &image):
m_roi(roi),
m_image(image),
m_has_features(false),
m_has_mask(false),
m_has_histogram(false),
m_hist_diff(-1.0),
m_has_skin(false),
m_has_training_data(false),
m_id(-1),
m_is_bad(false),
m_is_tracked(false),
m_has_fgd_mask(false),
m_has_bgd_mask(false),
m_type("NONE"),
m_has_blur(false),
m_has_image_histogram(false){
}

void Face::draw(cv::Mat &image,const cv::Scalar &colour){
    
    /* just draw the rect */
    cv::Scalar _colour  = colour;
    //    cv::Rect box(m_roi.height*scale,m_roi.width*scale,m_roi.x*scale,m_roi.y*scale);
    if(m_is_bad)
        _colour = cv::Scalar(0,0,255);
    
    //FD roi
    cv::rectangle(image,m_roi,colour,2);
 
    //padded roi
    //cv::rectangle(image,paddRoi(),cv::Scalar(0,255,0));
    std::stringstream ss;
    ss << "ID " << m_id;
    int baseline=0;
    int thickness =1;
    double fontScale = 1.0;
    int fontFace =cv::FONT_HERSHEY_PLAIN;
    cv::Size textSize = cv::getTextSize(ss.str(),fontFace,fontScale,thickness, &baseline);
    baseline+=thickness;
    
    cv::Point textOrg = m_roi.tl();
    int padd = 12;
    cv::rectangle(image, textOrg, textOrg+cv::Point(textSize.width+padd,(textSize.height+padd)), _colour,-1);
    cv::putText(image, ss.str(), textOrg+cv::Point(padd/2,textSize.height+padd/2), fontFace, fontScale, cv::Scalar(255,255,255),thickness,cv::LINE_AA);
    
    //draw features
    if(m_has_features){
        for(auto const &point:m_feature_points){
            cv::circle(image, point, 3, _colour);
        }
    }
    
    
//    //draw the mask
//    if(m_has_mask){
//        cv::Mat mask;
//        cv::cvtColor(m_face_mask,mask, cv::COLOR_GRAY2BGR);
//        cv::addWeighted(image, 1.0, mask, 0.3, 1.0, image);
//    }
// 
    
    
    
    //drawHistogram(image);
    
}


void Face::save(const std::string &filename){
    
    //save the roi and training data.
    
    
    
    
}



bool Face::isValid(){
    // check a few thigs...
    
    //dodgy roi setting
    if(m_roi.x>m_image->width())
        return false;
    
    if(m_roi.y>m_image->height())
        return false;
    
    if(m_roi.x < 0 || m_roi.y < 0)
        return false;
    
    if( (m_roi.x + m_roi.width) > m_image->width())
        return false;

    if( (m_roi.y + m_roi.height) > m_image->height())
        return false;
    
    float _width = static_cast<float>(m_roi.width)/m_image->width();
    if(_width < FACEDETECT_MIN_SIZE){
        // report for debugging.
        std::cout << "FACE TOO SMALL:: " <<  _width << std::endl;
        return false;
    }
    
    return true;
}


void Face::segmentSkin(const cv::Mat &image,SkinSegmenterBase::Ptr seg){
    m_skin = seg->run(image(m_roi));
    m_has_skin=true;
}


void Face::drawHistogram(cv::Mat &image){
    // used for debugging
    
    if(!m_has_histogram)
        return;
    cv::Mat img;
    cv::Mat hist;
    m_histogram.convertTo(hist, CV_8U,255);
    cv::pyrDown(hist,hist);
    cv::cvtColor(hist,img,cv::COLOR_GRAY2BGR);
    cv::Rect roi;
    if(m_roi.x+m_roi.width + hist.cols > image.cols)
        roi=cv::Rect(m_roi.x-hist.cols,m_roi.y,hist.cols,hist.rows);
    else
        roi=cv::Rect(m_roi.x+m_roi.width,m_roi.y,hist.cols,hist.rows);
    img.copyTo(image(roi));
}


void Face::createFaceMask(const cv::Mat &image){
    /*
     Create a mask of the face using the detected  feature points.
     */
    
    //a list of the external, feature points.
    int _face[]= {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,26,25,24,23,22,21,20,19,18,17,0};
    Face::Features ext_points;
    for(auto i:_face)
        ext_points.push_back(m_feature_points[i]);
    
    const cv::Point* p[1] = {&ext_points[0]};
    int size = ext_points.size();
    cv::Mat facemask = cv::Mat::zeros(image.size(),CV_8UC1);
    cv::fillPoly(facemask,p,&size,1,cv::Scalar(255),8);
    
    cv::bitwise_or(facemask,forehead(image),m_face_mask);
    m_has_mask = true;
}

cv::Mat Face::createHistogram(const cv::Mat &image){
    
    // create a hisrogram from the ROI.
    cv::Mat hsv,mask,hue;
    cv::cvtColor(image, hsv, cv::COLOR_BGR2HLS);
    //threshold
    cv::inRange(hsv,cv::Scalar(0.0,60.,32.),cv::Scalar(180.,255.,255.),mask);
    int ch[] = {0, 0};
    hue.create(hsv.size(), hsv.depth());
    cv::mixChannels(&hsv, 1, &hue, 1, ch, 1);
    
    int hsize=64;
    float hranges[] = {0,180};
    const float* phranges = hranges;
    
//    cv::Mat full_mask = mask;
//    if(m_has_mask)
//        full_mask = mask & m_face_mask;
    cv::Mat _histogram;
    cv::calcHist(&hue, 1, 0, cv::Mat(), _histogram, 1, &hsize, &phranges);
    cv::normalize(_histogram, _histogram, 0, 1, cv::NORM_MINMAX,CV_64F,cv::Mat());
    return _histogram;
    
}
void Face::create2DHistogram(){
    cv::Mat mask = cv::Mat();
    if(m_has_mask)
        mask = m_face_mask;
    m_histogram = HistogramSkinSegmenter::create2DHistogram(m_image->get(),mask);
    m_has_histogram = true;
}

void Face::create2DHistogram(const cv::Mat &image){
    cv::Mat mask = cv::Mat();
    if(m_has_mask)
        mask = m_face_mask;
    m_histogram = HistogramSkinSegmenter::create2DHistogram(image,mask);
    m_has_histogram = true;
}
void Face::create2DHistogram(const cv::Mat &image,cv::Mat &histogram){
    cv::Mat mask = cv::Mat();
    if(m_has_mask)
        mask = m_face_mask;
    HistogramSkinSegmenter::create2DHistogram(image,mask,histogram);
    m_histogram = histogram;
    m_has_histogram = true;
}

cv::Mat Face::imageHistogram(){
    if(m_has_image_histogram)
        return m_image_histogram;
    m_image_histogram = createHistogram(faceImage());
    m_has_image_histogram = true;
    return m_image_histogram;
}

double Face::compareHistogram(const cv::Mat &hist){
    if(!m_has_histogram)
        return -1.0;
    
//    std::cout <<"m_ist shape: "<<m_histogram.size() << " "<< m_histogram.channels() << " hist shape: "<< hist.size()<<" " <<hist.channels() <<std::endl;
    m_hist_diff = cv::compareHist(m_histogram,hist,cv::HISTCMP_CORREL);
    return m_hist_diff;
}

double Face::compareHistogram(){
    return 0.;
}

bool Face::createTrainingData(const SkinSegmenter::Ptr &skin_seg){
    
    m_skin_seg = skin_seg;
    
    if(m_has_training_data)
        return checkTrainingData();

    SmartSpecs::Timer::begin("[ Face ] createTrainingData()");
    
    //TODO choose if to use a pyramid level.
    
    // use the landmarks to get cheeks / forehead and outside of face
    cv::Mat mask = cv::Mat::zeros(m_image->size(FACELEARNER_TRAIN_PYR_LEVEL),CV_8UC1);
    cv::Mat face_mask = cv::Mat::zeros(m_image->size(FACELEARNER_TRAIN_PYR_LEVEL),CV_8UC1);
    
    cv::Mat hsv;// = image;
//
//    if(FACELEARNER_COLOURSPACE == CC_BGR2HSV)
//        cv::cvtColor(m_image->get(FACELEARNER_TRAIN_PYR_LEVEL), hsv, cv::COLOR_BGR2HSV);
//    else if(FACELEARNER_COLOURSPACE == CC_BGR2LAB)
//        cv::cvtColor(m_image->get(FACELEARNER_TRAIN_PYR_LEVEL), hsv, cv::COLOR_BGR2Lab);
//    else if(FACELEARNER_COLOURSPACE == CC_NONE)
//        hsv = m_image->get(FACELEARNER_TRAIN_PYR_LEVEL);
//
    SkinSegmenterBase::convertColor(m_image->get(FACELEARNER_TRAIN_PYR_LEVEL),hsv,FACELEARNER_COLOURSPACE);
    
    
    const cv::Scalar other(0);
    const cv::Scalar skin(255);
    const cv::Scalar face_area(1);
    
    auto _roi_padd = roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
    
    //get the face mask, base on feature points.
    mask = faceMask(FACELEARNER_TRAIN_PYR_LEVEL).clone();
    
    
    //try the grab cut algorithn.
    cv::Mat _image = m_image->get(FACELEARNER_TRAIN_PYR_LEVEL);
    cv::Mat _mask = cv::Mat(mask.size(),CV_8UC1,cv::GC_PR_BGD);
    
    //set mask foreground values
    _mask.setTo(cv::GC_PR_FGD,mask);
    //errode mask
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5,5));
    cv::erode(_mask,_mask,kernel);
    cv::erode(mask,mask,kernel);

    const int _bg_size = _roi_padd.width * 0.1; //set BG markers to be 10% of face width.
    cv::Point _tl(_roi_padd.tl());
    const int _roi_width = _roi_padd.width;
    //add background values  - use top corners,
    cv::rectangle(_mask, _tl, _tl+cv::Point(_bg_size,_bg_size), cv::GC_BGD,-1);
    cv::rectangle(_mask, _tl+cv::Point(_roi_width-_bg_size,0),_tl+cv::Point(_roi_width,_bg_size), cv::GC_BGD,-1);

    cv::rectangle(mask, _tl, _tl+cv::Point(_bg_size,_bg_size),255,-1);
    cv::rectangle(mask, _tl+cv::Point(_roi_width-_bg_size,0),_tl+cv::Point(_roi_width,_bg_size), 255,-1);

//    std::stringstream ss;
//    ss << age();
//
//    cv::imwrite("mask_" + ss.str()+".png",mask);
//    cv::imwrite("image_" + ss.str()+".png",_image);
    

    cv::Mat bgModel,fgModel;
    cv::grabCut(_image,_mask,_roi_padd, bgModel, fgModel,FACELEARNER_GC_ITER,cv::GC_INIT_WITH_MASK | cv::GC_INIT_WITH_RECT);
    
    cv::Mat _bg_mask;
    cv::Mat _fg_mask;
    
    cv::compare(_mask,cv::GC_PR_FGD,_fg_mask,cv::CMP_EQ);
    cv::compare(_mask,cv::GC_PR_BGD,_bg_mask,cv::CMP_EQ);

//    cv::Mat foreground(_image.size(),CV_8UC3,cv::Scalar(0,0,0));
//    cv::Mat background(_image.size(),CV_8UC3,cv::Scalar(0,0,0));
//    _image.copyTo(foreground,_fg_mask);
//    _image.copyTo(background,_bg_mask);
//    cv::imwrite("foreground_" + ss.str()+".png",foreground);
//    cv::imwrite("background_" + ss.str()+".png",background);

    //set the masks.
    this->setBgdMask(_bg_mask);
    this->setFgdMask(_fg_mask);
    //threhsold mask
    this->setGcMask(mask);
    
    //crop the mask
    cv::Mat __mask = _mask(_roi_padd).clone();
    //crop hsv
    cv::Mat _hsv = hsv(_roi_padd).clone();
    
    
//    closing = cv.morphologyEx(img, cv.MORPH_CLOSE, kernel)
    
    /*******  CREEATE SKIN DATA  ************/
    
    //count all the pixles in the mask (this is the number of skin pixels)
    //int count = cv::countNonZero(_fg_mask);
    
    //create amram mat and row
    // 3 = rows, count = cols;
   
    
    //loop through cropped mask
    int total = __mask.total();
    m_data = arma::mat(3,total,arma::fill::zeros);
    m_labels = arma::mat(1,total,arma::fill::zeros);
    
    auto m = __mask.ptr<uint8_t>();
    auto i = _hsv.ptr<cv::Vec3b>();
    int c=0;
    for(int x=0;x<total;++x){
        if(m[x]==cv::GC_PR_BGD){ // not skin
            m_labels(c) = 0;
        }else if(m[x]==cv::GC_PR_FGD){ //skin;
            m_labels(c) = 1;
        }
        
        
        //use offline skin detector, to help with false labels.
        //        if(skin_seg.get() != nullptr){
        //            cv::Mat _point = skin_seg->run(point);
        //            if(_point.ptr<uint8_t>()[0]==255){
        //                continue;
        //            }
        //        }
        m_data(0,c) = static_cast<double>(i[x][0]);
        m_data(1,c) = static_cast<double>(i[x][1]);
        m_data(2,c) = static_cast<double>(i[x][2]);
        c++;
    }
    
    m_has_training_data = true;
    SmartSpecs::Timer::end("[ Face ] createTrainingData()");
    
    return checkTrainingData();
}

bool Face::checkTrainingData(){
    if(!m_has_training_data)
        return false;
    
    //first check is it very imbalances
    
    int _skin=0;
    int total = m_labels.size();
    for(int i=0;i<total;++i){
        if(m_labels[i] == 1) //not skin
            _skin++;
    }
    
    float skin = static_cast<float>(_skin)/total;
    //if skin is  are >
//    std::cout << "Skin Ratio:: "<<skin <<std::endl;
//    if(skin > 0.8 || skin <0.2)
//        std::cout << "Face rejected "<<skin <<std::endl;
//        return false;
//    
//    /* use the offline trained segmenter to check */
//    cv::Mat img = trainigImage();
//    cv::Mat __skin = m_skin_seg->run(img);
//    cv::Mat _skin_fg;
//    auto _roi = roi(FACELEARNER_TRAIN_PYR_LEVEL,FACELEARNER_MASK_PADD);
//    cv::Mat _mask = m_fgd_mask(_roi);
//    
//    cv::bitwise_xor(__skin, _mask, _skin_fg);
//    auto _s_count = cv::countNonZero(_skin_fg);
//    int _total =  _mask.total();
//    float _diff = static_cast<float>(_s_count)/static_cast<float>(_total);
//
//    std::cout << " Skin test count s: " << " DIFF: " << _diff << std::endl;
//    
    //feels like 10% is a good cuttof
//    if(_diff > 0.1)
//        return false;
    
    return true;
}
