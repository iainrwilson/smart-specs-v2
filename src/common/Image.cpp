//
//  Image.cpp
//  cpp_server
//
//  Created by Iain Wilson on 22/11/2018.
//

#include <stdio.h>
#include "Image.hpp"
Image::Image():m_has_image(false),m_level(0),m_has_skin(false),m_has_gray(false){
    m_time=std::chrono::high_resolution_clock::now();
}

Image::Image(const cv::Mat &image):m_image(image),m_has_image(true),m_level(0),m_has_skin(false),m_has_gray(false){
    m_time=std::chrono::high_resolution_clock::now();
}

//void Image::segmentFaces(){
//
//    
//    //check image has skin;
//    if(!m_has_skin)
//        return;
//
//    // need to know what pyramid levels have been used
//    // so we can scale things.
//    // default at the moment is 2 for the skin.
//    
//    // loop through all the faces.
//    for(auto &face : m_faces){
//        
//        //get a scaled roi (for the pyramid level used to make the skin)
//        auto roi = face->roi(2);
//        
//        cv::Mat skin = m_skin(roi);
//        
//        cv::Mat kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3));
//        int open=2;
//        for(int i=0;i<open;++i){
//            cv::dilate(skin,skin,kernel);
//        }
//        for(int i=0;i<open;++i){
//            cv::erode(skin, skin,kernel);
//        }
//    }
//    
//    
////
////    std::stringstream ss;
////    ss << face_counter <<"_";
////
////    cv::Mat face_rect = cv::Mat::zeros(skin.size(),CV_8UC1);
////    cv::rectangle(face_rect, face->paddRoi(), cv::Scalar(255),-1);
////    //            cv::bitwise_and(face_rect, test, test);
////
////    Cartoon cartoon;
////    cv::Mat cart = cartoon.run(_img,false,skin);
//    
//}
