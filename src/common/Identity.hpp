//
//  Identity.hpp
//  cpp_server
//
//  Created by Iain Wilson on 19/12/2018.
//

#ifndef Identity_hpp
#define Identity_hpp

#include <stdio.h>
#include "Face.hpp"
#include "common/RingBuffer.hpp"
#include "variables.hpp"
#include <memory>
#include "common/timer.hpp"
#include "common/Thread.hpp"

typedef RingBuffer<Face::Ptr>::Ptr FaceBuffer;

class Identity : public Thread{
    
public:
    typedef std::shared_ptr<Identity> Ptr;
    
    static Ptr create(const Face::Ptr &face){
        return std::make_shared<Identity>(face);
    }
    
    Identity(const Face::Ptr &face);
    ~Identity() = default;
    
    void add(const Face::Ptr &face);
    
    void thread_run();
    
    
    double compareHistogram(const cv::Mat &hist);
    cv::Mat drawIdentity();

    int id() const {return m_id;}
    FaceBuffer buff(){return m_faces;}
    bool isFull(){ return m_faces->full();}
    
    bool valid(){
        // a valid ID must meet certain critera
        
        //must have more than the minimum number of faces
        if(m_faces->size() < IDENTITY_MIN_NO_FACES)
            return false;
        
        //most recent face must be within limits.
        return age() < IDENTITY_AGE_LIMIT;
        
    }
    
    int age(){
        return age(std::chrono::high_resolution_clock::now());
    }
    int age(const std::chrono::high_resolution_clock::time_point &time) {
//        std::lock_guard<std::mutex> lock(m_latest_face_mutex);
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(time - m_time);
        return duration.count();
    }
    
    
    
    Face::Ptr head(){return m_faces->head();}
    
   
    Face::Ptr latest() {
        std::lock_guard<std::mutex> lock(m_latest_face_mutex);
        return m_latest_face;
    }
    
    
private:
    int m_id;
    std::chrono::high_resolution_clock::time_point m_time;
    bool m_has_new_data;
    FaceBuffer m_faces;
    FaceBuffer m_input_buffer;
    
    Face::Ptr m_latest_face;
    std::mutex m_latest_face_mutex;
    
    

    
};

#endif /* Identity_hpp */
